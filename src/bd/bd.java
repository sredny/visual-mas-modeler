package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class bd {
	private static Connection con;
	private static Statement stat;
	private PreparedStatement prep;
	private StringProperty url;
	private ObservableList<String> servicio,actos_habla,caracterizacion,a_tecnicas,a_tipo,a_mecanismo,r_tenica,r_lenguaje,r_estrategia,tipo_datos,LenguajeComunicacion;

	public bd(){
		connect();
		url=null;
		fill_all();
	}

	public void fill_all(){
		fill_Acto_habla();
		fill_AprendizajeMecanismo();
		fill_AprendizajeTecnica();
		fill_AprendizajeTipo();
		fill_Caracterizacion();
		fill_RazonamientoEstrategias();
		fill_RazonamientoLenguaje();
		fill_RazonamientoTecnica();
		fill_Servicio();
		fill_Tipos();
		fill_LenguajeConocimiento();
		fill_URL();
	}

	public void connect(){
		try {
			Class.forName("org.sqlite.JDBC").newInstance();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			con = DriverManager.getConnection("jdbc:sqlite:mydb.db");
			stat = con.createStatement();
			stat.executeUpdate("create table if not exists servicios(tipos varchar(50));");
			stat.executeUpdate("create table if not exists actos_habla(tipos varchar(50));");
			stat.executeUpdate("create table if not exists caracterizacion(tipos varchar(50));");
			stat.executeUpdate("create table if not exists aprendizaje_ti(tipos varchar(50));");
			stat.executeUpdate("create table if not exists aprendizaje_tec(tecnicas varchar(50));");
			stat.executeUpdate("create table if not exists aprendizaje_mec(mecanismos varchar(50));");
			stat.executeUpdate("create table if not exists Tipos(descripcion varchar(50));");
			stat.executeUpdate("create table if not exists LenguajeComunicacion(descripcion varchar(50));");
			//stat.executeUpdate("drop table if exists razonamiento_tecnica");
			stat.executeUpdate("create table if not exists razonamiento_tecnica(tecnicas varchar(50));");
			stat.executeUpdate("create table if not exists razonamiento_lenguaje(lenguajes varchar(50));");
			stat.executeUpdate("create table if not exists razonamiento_estrategias(estrategias varchar(50));");
			stat.executeUpdate("create table if not exists Utilidades(clave varchar(50),valor varchar(200));");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//fin de connect


	//====================Create===================
		public void saveTipos(String new_tipo) {
	        try {
	            prep = con.prepareStatement("insert into Tipos values(?);");
	            prep.setString(1, new_tipo);
	            prep.execute();
	            tipo_datos.add(new_tipo);
	        } catch (SQLException ex) {

	        }
	    }//fin de save tipo de datos

		public void saveLastURL(String new_url) {
	        try {
	            prep = con.prepareStatement("insert or replace into Utilidades (clave,valor) values('url',?);");
	            prep.setString(1, new_url);
	            prep.execute();
	            setUrl(new SimpleStringProperty(new_url));
	        } catch (SQLException ex) {

	        }
	    }//fin de save tipo de datos

		public void saveLenguajeComunicacion(String new_tipo) {
	        try {
	            prep = con.prepareStatement("insert into LenguajeComunicacion values(?);");
	            prep.setString(1, new_tipo);
	            prep.execute();
	            LenguajeComunicacion.add(new_tipo);
	        } catch (SQLException ex) {

	        }
	    }//fin de save tipo de datos
		public void UpdateLenguajeComunicacion(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update LenguajeComunicacion set descripcion=(?) where descripcion=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            LenguajeComunicacion.remove(antiguo);
	           LenguajeComunicacion.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}
		public void saveRazonamiento_estrategia(String new_estrategia) {
	        try {
	            prep = con.prepareStatement("insert into razonamiento_estrategias values(?);");
	            prep.setString(1, new_estrategia);
	            prep.execute();
	            r_estrategia.add(new_estrategia);
	        } catch (SQLException ex) {

	        }
	    }//fin de save lenguaje de razonamiento

		public void UpdateRazonamiento_estrategia(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update razonamiento_estrategias set estrategias=(?) where estrategias=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            r_estrategia.remove(antiguo);
	            r_estrategia.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}


		public void saveRazonamiento_lenguaje(String new_lenguaje) {
	        try {
	            prep = con.prepareStatement("insert into razonamiento_lenguaje values(?);");
	            prep.setString(1, new_lenguaje);
	            prep.execute();
	           r_lenguaje.add(new_lenguaje);
	        } catch (SQLException ex) {

	        }
	    }//fin de save lenguaje de razonamiento

		public void UpdateRazonamiento_lenguaje(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update razonamiento_lenguaje set lenguajes=(?) where lenguajes=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            r_lenguaje.remove(antiguo);
	            r_lenguaje.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void saveRazonamiento_tecnica(String new_tec) {
	        try {
	            prep = con.prepareStatement("insert into razonamiento_tecnica values(?);");
	            prep.setString(1, new_tec);
	            prep.execute();
	            r_tenica.add(new_tec);
	        } catch (SQLException ex) {

	        }
	    }//fin de save tecanica de razonamiento

		public void UpdateRazonamiento_tecnica(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update razonamiento_tecnica set tenicas=(?) where tenicas=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            r_tenica.remove(antiguo);
	            r_tenica.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void saveAprendizaje_mec(String new_mec) {
	        try {
	            prep = con.prepareStatement("insert into aprendizaje_mec values(?);");
	            prep.setString(1, new_mec);
	            prep.execute();
	            a_mecanismo.add(new_mec);
	        } catch (SQLException ex) {

	        }
	    }//fin de save mecanismo de aprendizaje

		public void UpdateAprendizajeMec(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update aprendizaje_mec set mecanismos=(?) where mecanismos=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            a_mecanismo.remove(antiguo);
	            a_mecanismo.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void saveAprendizaje_tec(String new_tec) {
	        try {
	            prep = con.prepareStatement("insert into aprendizaje_tec values(?);");
	            prep.setString(1, new_tec);
	            prep.execute();
	            a_tecnicas.add(new_tec);
	        } catch (SQLException ex) {

	        }
	    }//fin de save tipo de aprendizaje

		public void UpdateAprendizajeTec(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update aprendizaje_tec set tecnicas=(?) where tecnicas=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            a_tecnicas.remove(antiguo);
	            a_tecnicas.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void UpdateTipo(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update Tipos set descripcion=(?) where descripcion=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            tipo_datos.remove(antiguo);
	            tipo_datos.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void saveAprendizaje_tipo(String new_tipo) {
	        try {
	            prep = con.prepareStatement("insert into aprendizaje_ti values(?);");
	            prep.setString(1, new_tipo);
	            prep.execute();
	            a_tipo.add(new_tipo);
	        } catch (SQLException ex) {

	        }
	    }//fin de save tipo de aprendizaje

		public void UpdateAprendizajeTipo(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update aprendizaje_ti set tipos=(?) where tipos=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            a_tipo.remove(antiguo);
	            a_tipo.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void saveCaracterizacion(String new_caract) {
	        try {
	            prep = con.prepareStatement("insert into caracterizacion values(?);");
	            prep.setString(1, new_caract);
	            prep.execute();
	          caracterizacion.add(new_caract);
	        } catch (SQLException ex) {

	        }
	    }//fin de save caracterizacion

		public void Reinit_TiposDatos(){
			this.saveTipos("byte");
			this.saveTipos("short");
			this.saveTipos("int");
			this.saveTipos("long");
			this.saveTipos("float");
			this.saveTipos("double");
			this.saveTipos("boolean");
			this.saveTipos("char");
			this.saveTipos("String");
			this.saveTipos("Object");
		}

		public void UpdateCaracterizacion(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update caracterizacion set tipos=(?) where tipos=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            caracterizacion.remove(antiguo);
	            caracterizacion.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

		public void UpdateServicio(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update servicios set tipos=(?) where tipos=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();

	            servicio.remove(antiguo);
	            servicio.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

	   public void saveServicio(String new_se) {
	        try {
	            prep = con.prepareStatement("insert into servicios values(?);");
	            prep.setString(1, new_se);
	            prep.execute();
	           servicio.add(new_se);
	        } catch (SQLException ex) {

	        }
	    }//fin de save servicio

	   public void saveActoHabla(String new_acto) {
	        try {
	            prep = con.prepareStatement("insert into actos_habla values(?);");
	            prep.setString(1, new_acto);
	            prep.execute();
	           actos_habla.add(new_acto);
	        } catch (SQLException ex) {

	        }
	    }//fin de save servicio

	   public void UpdateActosHabla(String nuevo,String antiguo){
			try {
	            prep = con.prepareStatement("update actos_habla set tipos=(?) where tipos=(?);");
	            prep.setString(2,antiguo);
	            prep.setString(1,nuevo);
	            prep.execute();
	            actos_habla.remove(antiguo);
	            actos_habla.add(nuevo);
	        } catch (SQLException ex) {

	        }
		}

	public ObservableList<String> getServicio() {
		return servicio;
	}

	public void setServicio(ObservableList<String> servicio) {
		this.servicio = servicio;
	}

	public ObservableList<String> getActos_habla() {
		return actos_habla;
	}

	public void setActos_habla(ObservableList<String> actos_habla) {
		this.actos_habla = actos_habla;
	}

	public ObservableList<String> getCaracterizacion() {
		return caracterizacion;
	}

	public void setCaracterizacion(ObservableList<String> caracterizacion) {
		this.caracterizacion = caracterizacion;
	}

	public ObservableList<String> getTipo_datos() {
		return tipo_datos;
	}

	public void setTipo_datos(ObservableList<String> tipo_datos) {
		this.tipo_datos = tipo_datos;
	}

	public ObservableList<String> getA_tecnicas() {
		return a_tecnicas;
	}

	public void setA_tecnicas(ObservableList<String> a_tecnicas) {
		this.a_tecnicas = a_tecnicas;
	}

	public ObservableList<String> getA_tipo() {
		return a_tipo;
	}

	public void setA_tipo(ObservableList<String> a_tipo) {
		this.a_tipo = a_tipo;
	}

	public ObservableList<String> getA_mecanismo() {
		return a_mecanismo;
	}

	public void setA_mecanismo(ObservableList<String> a_mecanismo) {
		this.a_mecanismo = a_mecanismo;
	}

	public ObservableList<String> getR_tenica() {
		return r_tenica;
	}

	public void setR_tenica(ObservableList<String> r_tenica) {
		this.r_tenica = r_tenica;
	}

	public ObservableList<String> getR_lenguaje() {
		return r_lenguaje;
	}

	public void setR_lenguaje(ObservableList<String> r_lenguaje) {
		this.r_lenguaje = r_lenguaje;
	}

	public ObservableList<String> getR_estrategia() {
		return r_estrategia;
	}

	public void setR_estrategia(ObservableList<String> r_estrategia) {
		this.r_estrategia = r_estrategia;
	}

	//====================READ==================================
	   public void fill_RazonamientoEstrategias(){
		   r_estrategia = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from razonamiento_estrategias");
				while (rs.next()) {
		           r_estrategia.add(rs.getString("estrategias"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_LenguajeConocimiento(){
		   LenguajeComunicacion = FXCollections.observableArrayList();
		   LenguajeComunicacion.add("No aplica");
		   ResultSet lc;
			try {
				lc = con.createStatement().executeQuery("select * from LenguajeComunicacion");
				while (lc.next()) {
		           LenguajeComunicacion.add(lc.getString("descripcion"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_URL(){
		   ResultSet url_bd;
			try {
				url_bd = con.createStatement().executeQuery("select valor from Utilidades where clave='url'");
				while (url_bd.next()) {
		           setUrl(new SimpleStringProperty(url_bd.getString("valor")));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_Tipos(){
		   tipo_datos = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from Tipos");
				while (rs.next()) {
		           tipo_datos.add(rs.getString("descripcion"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_RazonamientoLenguaje(){
		   r_lenguaje = FXCollections.observableArrayList();
		   r_lenguaje.add("No aplica");
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from razonamiento_lenguaje");
				while (rs.next()) {
		           r_lenguaje.add(rs.getString("lenguajes"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_RazonamientoTecnica(){
		   r_tenica = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from razonamiento_tecnica");
				while (rs.next()) {
		           r_tenica.add(rs.getString("tecnicas"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_AprendizajeMecanismo(){
		   a_mecanismo = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from aprendizaje_mec");
				while (rs.next()) {
		           a_mecanismo.add(rs.getString("mecanismos"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_AprendizajeTecnica(){
		   a_tecnicas = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from aprendizaje_tec");
				while (rs.next()) {
		           a_tecnicas.add(rs.getString("tecnicas"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_AprendizajeTipo(){
		   a_tipo = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from aprendizaje_ti");
				while (rs.next()) {
		           a_tipo.add(rs.getString("tipos"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }

	   public void fill_Servicio(){
		   servicio = FXCollections.observableArrayList();
           ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("select * from servicios");
			while (rs.next()) {
               servicio.add(rs.getString("tipos"));
           }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void fill_Acto_habla(){
		   actos_habla = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from actos_habla");
				while (rs.next()) {
		           actos_habla.add(rs.getString("tipos"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   }

	   public void fill_Caracterizacion(){
		   caracterizacion = FXCollections.observableArrayList();
		   ResultSet rs;
			try {
				rs = con.createStatement().executeQuery("select * from caracterizacion");
				while (rs.next()) {
		           caracterizacion.add(rs.getString("tipos"));
		       }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	   //====================Update===============================
	//====================Deelete=============================
	   public void eliminar_servicio(String item){

           try {
        	   prep = con.prepareStatement("delete from servicios where tipos=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   servicio.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   }

	   public void eliminar_url(){

           try {
        	   prep = con.prepareStatement("delete from Utilidades where clave='url';");
        	   prep.execute();
        	   url=null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   }

	   public void eliminar_tipo(String item){

           try {
        	   prep = con.prepareStatement("delete from Tipos where descripcion=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   tipo_datos.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   }

	   public void vaciar_tipo(){

           try {
        	   prep = con.prepareStatement("delete from Tipos where 1;");
        	   prep.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   }

	   public void eliminar_LenguajeComunicacion(String item){

           try {
        	   prep = con.prepareStatement("delete from LenguajeComunicacion where descripcion=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   LenguajeComunicacion.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	   }

	   public void eliminar_Acto(String item){

           try {
        	   prep = con.prepareStatement("delete from actos_habla where tipos=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   actos_habla.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_Caracterizacion(String item){

           try {
        	   prep = con.prepareStatement("delete from caracterizacion where tipos=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   caracterizacion.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_AprendizajeTecnica(String item){

           try {
        	   prep = con.prepareStatement("delete from aprendizaje_tec where tecnicas=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   a_tecnicas.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_AprendizajeMecanismo(String item){

           try {
        	   prep = con.prepareStatement("delete from aprendizaje_mec where mecanismos=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   a_mecanismo.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_AprendizajeTipos(String item){

           try {
        	   prep = con.prepareStatement("delete from aprendizaje_ti where tipos=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   a_tipo.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_RazonamientoTecnicas(String item){

           try {
        	   prep = con.prepareStatement("delete from razonamiento_tecnica where tecnicas=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   r_tenica.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_RazonamientoLenguajes(String item){

           try {
        	   prep = con.prepareStatement("delete from razonamiento_lenguaje where lenguajes=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   r_lenguaje.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   public void eliminar_RazonamientoEstrategias(String item){

           try {
        	   prep = con.prepareStatement("delete from razonamiento_estrategias where estrategias=(?);");
        	   prep.setString(1, item);
        	   prep.execute();
        	   r_estrategia.remove(item);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	public ObservableList<String> getLenguajeComunicacion() {
		return LenguajeComunicacion;
	}

	public void setLenguajeComunicacion(ObservableList<String> lenguajeComunicacion) {
		LenguajeComunicacion = lenguajeComunicacion;
	}

	public StringProperty getUrl() {
		return url;
	}

	public void setUrl(StringProperty url) {
		this.url = url;
	}
};

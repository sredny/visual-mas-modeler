package application.CodeGenerator;

import java.io.File;

import application.Main;
import application.model.agent;

public class Generador {
	public static Main mainap;
	private static String class_name;
	private static String dir;
	private static clase clase_principal=new clase();

 /**
  * should exist 2 parts: to process, and to generate
  */

 	public Generador(){

 	}

 	public static void Generate(Main m,agent a){
 		mainap=m;
 	}

 	public static void reinit(){
 		clase_principal.reinit();
 	}

	public Main getMainapp() {
		return mainap;
	}

	public static void setMainapp(Main mainapp) {
		mainap = mainapp;
	}

	public static String getCode() {
		return clase_principal.get_Code();
	}

	public static void print_code(){
		System.out.println(clase_principal.get_Code());
		writte();
	}

	public String getClass_name() {
		return class_name;
	}

	public static void setClass_name(String class_name_n) {
		class_name = class_name_n;
	}

	public static void writte(){
		clase_principal.setNombre(clase_principal.getNombre().replace(" ", Generador.mainap.getCaracter_sustitucion()));
		Writter.escribir(dir + File.separator + clase_principal.getNombre() + ".java" , clase_principal.get_Code());
	}

	public static String getDir() {
		return dir;
	}

	public static void setDir(String dir) {
		Generador.dir = dir;
	}

	public clase getClase_principal() {
		return clase_principal;
	}

	public static void setClase_principal(clase clase_principal) {
		Generador.clase_principal = clase_principal;
	}

}

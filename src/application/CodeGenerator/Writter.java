package application.CodeGenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Writter {

	public static void escribir(String nombreArchivo,String content){
		File f;
		f = new File(nombreArchivo);

		//Escritura
		try{
		FileWriter w = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(w);
		PrintWriter wr = new PrintWriter(bw);
		content.replaceAll("/*null*/","");
		content.replaceAll("null", "Sin información");
		wr.write(content);//escribimos en el archivo
		wr.close();
		bw.close();
		}catch(IOException e){

			System.out.println(e.getStackTrace());
		};

	}


}

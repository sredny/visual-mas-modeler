package application.CodeGenerator;

import java.util.ArrayList;
import java.util.List;

public class clase {
	private String nombre,extend;
	private String preambulo;
	private List<String> lines_inside_start_method=new ArrayList<String>();
	private List<String> methods=new ArrayList<String>();
	private List<String> attributes=new ArrayList<String>();
	private List<String> imports=new ArrayList<String>();
	private List<clase> clases=new ArrayList<clase>();
	private String principal_method;
	private String constructor;
	private String comments;
	private clase clasePadre;

	public clase(){
		preambulo="";
	}

	public clase(String className){
		this.extend="";
		nombre=className;
		preambulo="";
	}

	public void push_line_inside(String lines){
		lines_inside_start_method.add(lines);
	}

	public void add_library(String library){
		if(!imports.contains(library))
			imports.add(library);
	}

	public void add_attribute(String attribute){
		if(!this.attributes.contains(attribute))
			attributes.add(attribute);
	}

	public void add_method(String method){
		methods.add(method);
	}

	public void add_class(clase clss){
		clases.add(clss);
	}

	public clase find_class(String ClassName){
		for (clase c : clases) {
			if(c.getNombre().equals(ClassName))
				return c;
		}
		return null;
	}

	public String get_Code(){
		String code="";
		boolean band=false;

		for (String libreria : imports) {
			code+=libreria+"\n";
			band=true;
		}

		if(band)
			code+="\n";

		//-----comments
		if(comments!=null)
			code+="\n /*"+comments+"*/\n\n";

		//----class name and extends
		if(preambulo!=null)
			code+="/*"+preambulo+"*/\n\n";
		code+="public class "+nombre+extend+"{";

		//-----atributos
		for (String atributo : attributes) {
			code+="\n" + atributo + ";";
		}

		if(constructor!=null){
			code+="\n\n\t"+constructor;
		}

		if(principal_method!=null){
		//-------principal method
		code+="\n\n\t"+principal_method+"{\n";

			//-------lines inside principal method---
			for (String line : lines_inside_start_method) {
				code+="\n\t\t"+line;
			}
		code+="\n\n\t }\n //*Fin de metodo*//";
		}

		//----methods------------
		for (String metodo : methods) {
			if(metodo!=null)
				code+="\n\t"+metodo+"\n";
		}

		//----classes----

		for (clase class_ : clases) {
			code+="\n\n\t"+class_.get_Code()+"\n";
		}

		//-----end of class
		code+="\n}//end of class";

		return code;
	}

	public void reinit(){
		lines_inside_start_method=new ArrayList<String>();
		methods=new ArrayList<String>();
		attributes=new ArrayList<String>();
	    imports=new ArrayList<String>();
		clases=new ArrayList<clase>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getExtend() {
		return extend;
	}

	public void setExtend(String extend) {
		this.extend = " extends "+extend;
	}

	public List<String> getLines_inside_start_method() {
		return lines_inside_start_method;
	}

	public void setLines_inside_start_method(List<String> lines_inside_start_method) {
		this.lines_inside_start_method = lines_inside_start_method;
	}

	public List<String> getMethods() {
		return methods;
	}

	public void setMethods(List<String> methods) {
		this.methods = methods;
	}

	public List<String> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<String> attributes) {
		this.attributes = attributes;
	}

	public List<String> getImports() {
		return imports;
	}

	public void setImports(List<String> imports) {
		this.imports = imports;
	}

	public List<clase> getClases() {
		return clases;
	}

	public void setClases(List<clase> clases) {
		this.clases = clases;
	}

	public String getPrincipal_method() {
		return principal_method;
	}

	public void setPrincipal_method(String principal_method) {
		this.principal_method = principal_method;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPreambulo() {
		return preambulo;
	}

	public void setPreambulo(String preambulo) {
		this.preambulo = preambulo;
	}

	public void addPreambulo(String line){
		if(line==null || line.equals(""))
			return;
		this.preambulo+="\n"+line+"\n";
	}

	public String getConstructor() {
		return constructor;
	}

	public void setConstructor(String constructor) {
		this.constructor = constructor;
	}

	public clase getClasePadre() {
		return clasePadre;
	}

	public void setClasePadre(clase clasePadre) {
		this.clasePadre = clasePadre;
	}


}

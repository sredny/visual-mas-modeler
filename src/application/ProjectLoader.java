package application;
import java.io.File;
import java.io.IOException;

import application.UML.Actividades;
import application.UML.estado;
import application.model.ActoHabla;
import application.model.Actor;
import application.model.Caracteristica;
import application.model.Caso_uso;
import application.model.MarcoOntologicoColectivo;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalC;
import application.model.OntologiaSituacionalI;
import application.model.Parametros;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.asociacion;
import application.model.conversacion;
import application.model.objetivo;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.TreeItem;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class ProjectLoader {
	private File file;
	private Main main;
	private XPath xPath =  XPathFactory.newInstance().newXPath();

	public ProjectLoader(){}
	public ProjectLoader(File file){
		this.file=file;
	}

	public void refactorizar(){

		 TreeItem<Object> raiz=new TreeItem<Object>("Agentes");
		 TreeItem<Object> nodo_moc=new TreeItem<Object>("Modelo de inteligencia Colectivo");
	     TreeItem<Object> root= new TreeItem<Object>("root");
	     root.getChildren().add(raiz);
	     root.getChildren().add(nodo_moc);
	     main.getRootcontroller().getArbol().setRoot(root);
	     main.setRaiz(raiz);
	     main.setNodo_moc(nodo_moc);

		for (agent  agente: this.main.getAgentData()) {
			agente.refactor(this.main);
			main.getRaiz().getChildren().add(agente.getNodo_arbol());
		}

		for (MarcoOntologicoColectivo MOC : main.getColectivo()) {
			MOC.refactor(main);
			nodo_moc.getChildren().add(MOC.getNodo_arbol());
		}

	}

	public void read(Main main) throws SAXException, IOException, XPathExpressionException{
		this.main=main;
		main.getAgentData().clear();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(this.file);
			doc.getDocumentElement().normalize();

			//obtengo todos los agentes
			NodeList nList = doc.getElementsByTagName("agent");//obtengo todos los agentes
			for(int i=0;i<nList.getLength();i++){
				agente(nList.item(i));
			}//fin for agentes

			NodeList IC= doc.getElementsByTagName("InteligenciaColectiva");

			if(IC!=null)
				for(int i=0;i<IC.getLength();i++)
					InteligenciaColectiva(IC.item(i));

			refactorizar();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void agente(Node agenteN) throws XPathExpressionException{
		agent a=new agent();
		Element eElement = (Element) agenteN;
		a.id=eElement.getAttribute("id");
		a.setName(new SimpleStringProperty(eElement.getAttribute("name")));
		NodeList hijos=agenteN.getChildNodes();

		for(int i=0;i<hijos.getLength();i++){
			if (hijos.item(i).getNodeType() == Node.ELEMENT_NODE) {
				if(hijos.item(i).getNodeName().equals("AgentModel")){
					this.Modelo_Agentes(hijos.item(i), a);
				}//modelo de agentes

				if(hijos.item(i).getNodeName().equals("TaskModel")){
					this.Modelo_Tareas(hijos.item(i), a);
				}//modelo de agentes

				if(hijos.item(i).getNodeName().equals("CommunicationModel")){
					this.Modelo_Comunicaciones(hijos.item(i), a);
				}//modelo de comunicaciones

				if(hijos.item(i).getNodeName().equals("CoordinationModel")){
					this.Modelo_Coordinacion(hijos.item(i), a);
				}

				if(hijos.item(i).getNodeName().equals("MarcoOntologicoIndividual")){
					this.Modelo_InteligenciaI(hijos.item(i), a);
				}

				if(hijos.item(i).getNodeName().equals("Asociaciones")){
					this.asociaciones(hijos.item(i), a);
				}

				if(hijos.item(i).getNodeName().equals("Actores")){
					this.actores(hijos.item(i), a);
				}

				if(hijos.item(i).getNodeName().equals("CasosUso")){
					this.casos_uso(hijos.item(i), a);
				}
			}//es nodo
		}//fin for
		main.getAgentData().add(a);
		//main.getRaiz().getChildren().add(a.getNodo_arbol());
	}

	private void Modelo_Tareas(Node item, agent a) {
		TareaModel modelo=a.getModelo_tareas();
		modelo.getNodo_arbol().getChildren().addAll(a.getModelo_agentes().getNodo().getChildren());
		try {
			NodeList tareas = (NodeList) xPath.compile("Task").evaluate(item, XPathConstants.NODESET);

			for(int i=0;i<tareas.getLength();i++){
				Tarea tarea=new Tarea();
				Node descripcion=(Node) xPath.compile("Descripcion").evaluate(tareas.item(i), XPathConstants.NODE);
				Node objetivo=(Node) xPath.compile("Objetivo").evaluate(tareas.item(i), XPathConstants.NODE);
				Node precondicion=(Node) xPath.compile("Precondicion").evaluate(tareas.item(i), XPathConstants.NODE);
				Node ServicioAsociado=(Node) xPath.compile("ServicioAsociado").evaluate(tareas.item(i), XPathConstants.NODE);
				Node Nombre=(Node) xPath.compile("Nombre").evaluate(tareas.item(i), XPathConstants.NODE);
				Node servicioId=(Node) xPath.compile("ServicioID").evaluate(tareas.item(i), XPathConstants.NODE);
				Node comportamiento=(Node) xPath.compile("Comportamiento").evaluate(tareas.item(i), XPathConstants.NODE);

				if(descripcion!=null)
					tarea.setDescripcion(descripcion.getTextContent());
				if(objetivo!=null)
					tarea.setObjetivo(objetivo.getTextContent());
				if(precondicion!=null)
					tarea.setPrecondicion(precondicion.getTextContent());
				if(ServicioAsociado!=null)
					tarea.setServicio_asociado(new SimpleStringProperty(ServicioAsociado.getTextContent()));
				if(Nombre!=null)
					tarea.setNombre(new SimpleStringProperty(Nombre.getTextContent()));
				if(servicioId!=null)
					tarea.setServicio_asociado(new SimpleStringProperty(servicioId.getTextContent()));
				if(comportamiento!=null)
					tarea.setComportamiento(new SimpleStringProperty(comportamiento.getTextContent()));


				NodeList subtareas = (NodeList) xPath.compile("SubTasks/SubTask").evaluate(tareas.item(i), XPathConstants.NODESET);
				NodeList parametros = (NodeList) xPath.compile("Parametros/Parametro").evaluate(tareas.item(i), XPathConstants.NODESET);

				for(int x=0;x<subtareas.getLength();x++){
					tarea.getSubtareas().add(subtareas.item(x).getTextContent());
				}

				for(int x=0;x<parametros.getLength();x++){

					Parametros p=new Parametros();
					Node name=(Node) xPath.compile("Nombre").evaluate(parametros.item(x), XPathConstants.NODE);
					Node desc=(Node) xPath.compile("Descripcion").evaluate(parametros.item(x), XPathConstants.NODE);
					Node tipo=(Node) xPath.compile("Tipo").evaluate(parametros.item(x), XPathConstants.NODE);

					if(name!=null)
						p.setNombre(new SimpleStringProperty(name.getTextContent()));
					if(desc!=null)
						p.setDescripcion(new SimpleStringProperty(desc.getTextContent()));
					if(tipo!=null)
						p.setTipo(new SimpleStringProperty(tipo.getTextContent()));
					tarea.getParametros().add(p);
				}
				modelo.getTareas().add(tarea);
			}//fin del for

		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		a.setModelo_tareas(modelo);
	}

	private void Modelo_Agentes(Node ma,agent a) throws XPathExpressionException{

		Node posicion = (Node) xPath.compile("Posicion").evaluate(ma, XPathConstants.NODE);
		Node marcoReferencia = (Node) xPath.compile("MarcoReferencia").evaluate(ma, XPathConstants.NODE);
		Node descripcion = (Node) xPath.compile("AgenteDescripcion").evaluate(ma, XPathConstants.NODE);
		Node componentes = (Node) xPath.compile("Componentes").evaluate(ma, XPathConstants.NODE);
		Node objetivos = (Node) xPath.compile("Objetivos").evaluate(ma, XPathConstants.NODE);
		Node servicios = (Node) xPath.compile("Servicios").evaluate(ma, XPathConstants.NODE);
		Node propiedades = (Node) xPath.compile("Propiedades").evaluate(ma, XPathConstants.NODE);
		Node capacidad = (Node) xPath.compile("Capacidad").evaluate(ma, XPathConstants.NODE);
		Node restriccion = (Node) xPath.compile("Restriccion").evaluate(ma, XPathConstants.NODE);
		Node objetivs = (Node) xPath.compile("Objetivos").evaluate(ma, XPathConstants.NODE);


		AgentModel modelo=new AgentModel();
		TareaModel tm=new TareaModel();
		a.setModelo_tareas(tm);

		if(posicion!=null)
			modelo.setAgenteposicion(posicion.getTextContent());
		if(marcoReferencia!=null)
			modelo.setAgenteMarco_referencia(marcoReferencia.getTextContent());
		if(descripcion!=null)
			modelo.setAgenteDescripcion(descripcion.getTextContent());

		if(componentes!=null){
			NodeList compo = (NodeList) xPath.compile("componente").evaluate(componentes, XPathConstants.NODESET);
			for(int j=0;j<compo.getLength();j++){
				modelo.getAgenteComponentes().add(compo.item(j).getTextContent());
			}//fin for
		}
		if(objetivos!=null){
			NodeList objetivoss = (NodeList) xPath.compile("Objetivo").evaluate(objetivos, XPathConstants.NODESET);

			for(int k=0;k<objetivoss.getLength();k++){
				objetivo ob=new objetivo();
				Node nombre = (Node) xPath.compile("Nombre").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node desc = (Node) xPath.compile("Descripcion").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node activacion = (Node) xPath.compile("Activacion").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node finalizacion = (Node) xPath.compile("Finalizacion").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node exito = (Node) xPath.compile("Exito").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node fracaso = (Node) xPath.compile("Fracaso").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node ontologia = (Node) xPath.compile("Ontologia").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node entrada = (Node) xPath.compile("Entradas").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node salida = (Node) xPath.compile("Salidas").evaluate(objetivoss.item(k), XPathConstants.NODE);
				Node id = (Node) xPath.compile("Id").evaluate(objetivoss.item(k), XPathConstants.NODE);

				if(nombre!=null){
					ob.setObjetivoNombre(nombre.getTextContent());
				}
				if(id!=null){
					ob.setId(id.getTextContent());
				}
				if(desc!=null){
					ob.setObjetivoDescripcion(desc.getTextContent());
				}
				if(activacion!=null){
					ob.setObjetivoActivacion(activacion.getTextContent());
				}
				if(finalizacion!=null){
					ob.setObjetivoFinalizacion(finalizacion.getTextContent());
				}
				if(exito!=null){
					ob.setObjetivoExito(exito.getTextContent());
				}
				if(fracaso!=null){
					ob.setObjetivoFracaso(fracaso.getTextContent());
				}
				if(ontologia!=null){
					ob.setObjetivoOntologia(ontologia.getTextContent());
				}
				if(entrada!=null){
					NodeList in = (NodeList) xPath.compile("Entrada").evaluate(entrada, XPathConstants.NODESET);
					for(int j=0;j<in.getLength();j++){
						ob.getObjetivoEntrada().add(in.item(j).getTextContent());
					}//fin for
				}
				if(salida!=null){
					NodeList out = (NodeList) xPath.compile("Salida").evaluate(salida, XPathConstants.NODESET);
					for(int j=0;j<out.getLength();j++){
						ob.getObjetivoSalida().add(out.item(j).getTextContent());
					}//fin for
				}
				modelo.getObjetivo().add(ob);
			}//fin for
		}
		if(servicios!=null){
			NodeList services = (NodeList) xPath.compile("Servicio").evaluate(servicios, XPathConstants.NODESET);
			for(int j=0;j<services.getLength();j++){
				Servicio S=new Servicio();
				Element elemento= (Element)services.item(j);

				S.setNombre(elemento.getAttribute("nombre"));
				S.setId(elemento.getAttribute("id"));
				Node descr = (Node) xPath.compile("descripcion").evaluate(services.item(j), XPathConstants.NODE);
				Node tipo = (Node) xPath.compile("tipo").evaluate(services.item(j), XPathConstants.NODE);
				Node comportamiento = (Node) xPath.compile("Comportamiento").evaluate(services.item(j), XPathConstants.NODE);
				NodeList ParametrosE = (NodeList) xPath.compile("Parametros/Entrada/Parametro").evaluate(services.item(j), XPathConstants.NODESET);
				NodeList ParametrosS = (NodeList) xPath.compile("Parametros/Salida/Parametro").evaluate(services.item(j), XPathConstants.NODESET);
				NodeList Propiedades = (NodeList) xPath.compile("Propiedades/Caracteristica").evaluate(services.item(j), XPathConstants.NODESET);
				Node objetivo = (Node) xPath.compile("Objetivo").evaluate(services.item(j), XPathConstants.NODE);


				if(descr!=null)
					S.setDescripcion(descr.getTextContent());
				if(tipo!=null)
					S.setTipo(tipo.getTextContent());
				if(comportamiento!=null)
					S.setComportamiento(comportamiento.getTextContent());
				if(objetivo!=null)
					S.setObjetivoId(objetivo.getTextContent());

				if(Propiedades!=null)
					for(int x=0;x<Propiedades.getLength();x++){
						S.getPropiedades().add(caracterizacion(Propiedades.item(x)));
					}//fin for

				for(int x=0;x<ParametrosE.getLength();x++){
					Parametros p=new Parametros();
					Node name=(Node) xPath.compile("Nombre").evaluate(ParametrosE.item(x), XPathConstants.NODE);
					Node desc=(Node) xPath.compile("Descripcion").evaluate(ParametrosE.item(x), XPathConstants.NODE);
					Node tipoP=(Node) xPath.compile("Tipo").evaluate(ParametrosE.item(x), XPathConstants.NODE);

					if(name!=null)
						p.setNombre(new SimpleStringProperty(name.getTextContent()));
					if(desc!=null)
						p.setDescripcion(new SimpleStringProperty(desc.getTextContent()));
					if(tipoP!=null)
						p.setTipo(new SimpleStringProperty(tipoP.getTextContent()));
					S.getServicioPentrada().add(p);
				}
				for(int x=0;x<ParametrosS.getLength();x++){
					Parametros p=new Parametros();
					Node name=(Node) xPath.compile("Nombre").evaluate(ParametrosS.item(x), XPathConstants.NODE);
					Node desc=(Node) xPath.compile("Descripcion").evaluate(ParametrosS.item(x), XPathConstants.NODE);
					Node tipoPS=(Node) xPath.compile("Tipo").evaluate(ParametrosS.item(x), XPathConstants.NODE);

					if(name!=null)
						p.setNombre(new SimpleStringProperty(name.getTextContent()));
					if(desc!=null)
						p.setDescripcion(new SimpleStringProperty(desc.getTextContent()));
					if(tipoPS!=null)
						p.setTipo(new SimpleStringProperty(tipoPS.getTextContent()));
					S.getServicioPsalida().add(p);
				}

				modelo.getServicios().add(S);
				tm.getNodo_servicios().getChildren().add(S.getNodo());
				//modelo.getNodo().getChildren().add(S.getNodo());//<--Descomentar para mostrar el nodo en el modelo de tareas
			}//fin for
		}
		if(propiedades!=null){
			Node name = (Node) xPath.compile("Nombre").evaluate(propiedades, XPathConstants.NODE);
			Node calidad_p = (Node) xPath.compile("Calidad").evaluate(propiedades, XPathConstants.NODE);
			Node auditable_p = (Node) xPath.compile("Auditable").evaluate(propiedades, XPathConstants.NODE);
			Node garantia_p = (Node) xPath.compile("Garantia").evaluate(propiedades, XPathConstants.NODE);
			Node capacidad_p = (Node) xPath.compile("Capacidad").evaluate(propiedades, XPathConstants.NODE);
			Node confiabilidad_p = (Node) xPath.compile("Confiabilidad").evaluate(propiedades, XPathConstants.NODE);

			if(name!=null)
				modelo.setPropiedadesNombre(name.getTextContent());
			if(calidad_p!=null)
				modelo.setPropiedadesCalidad(calidad_p.getTextContent());
			if(auditable_p!=null)
				modelo.setPropiedadesAuditable(auditable_p.getTextContent());
			if(garantia_p!=null)
				modelo.setPropiedadesGarantia(garantia_p.getTextContent());
			if(capacidad_p!=null)
				modelo.setPropiedadesCapacidad(capacidad_p.getTextContent());
			if(confiabilidad_p!=null)
				modelo.setPropiedadesConfiabilidad(confiabilidad_p.getTextContent());
		}
		if(capacidad!=null){
			Node habilidades = (Node) xPath.compile("Habilidades").evaluate(capacidad, XPathConstants.NODE);
			Node representacion = (Node) xPath.compile("Representacion").evaluate(capacidad, XPathConstants.NODE);
			Node lenguaje = (Node) xPath.compile("Lenguaje").evaluate(capacidad, XPathConstants.NODE);
			if(habilidades!=null)
				modelo.setCapacidadHabilidades(habilidades.getTextContent());
			if(representacion!=null)
				modelo.setCapacidadRepresentacion(representacion.getTextContent());
			if(lenguaje!=null)
				modelo.setCapacidadLenguaje(lenguaje.getTextContent());
		}
		if(restriccion!=null){
			Node normas = (Node) xPath.compile("Normas").evaluate(restriccion, XPathConstants.NODE);
			Node preferencias = (Node) xPath.compile("Preferencias").evaluate(restriccion, XPathConstants.NODE);
			Node permisos = (Node) xPath.compile("Permisos").evaluate(restriccion, XPathConstants.NODE);

			if(normas!=null)
				modelo.setRestriccionNormas(normas.getTextContent());
			if(preferencias!=null)
				modelo.setRestriccionPreferencias(preferencias.getTextContent());
			if(permisos!=null)
				modelo.setRestriccionPermisos(permisos.getTextContent());
		}
		a.setModelo_agentes(modelo);
	}

	private void Modelo_Comunicaciones(Node mcom,agent a) throws XPathExpressionException{
		CommunicationModel modelo=new CommunicationModel();
		NodeList actos = (NodeList) xPath.compile("ActoHabla").evaluate(mcom, XPathConstants.NODESET);

		for(int i=0;i<actos.getLength();i++){
			Element eElement = (Element) actos.item(i);
			Node objetivo=(Node) xPath.compile("objetivo").evaluate(actos.item(i), XPathConstants.NODE);
			Node Datos=(Node) xPath.compile("DatosIntercambiado").evaluate(actos.item(i), XPathConstants.NODE);
			Node precondicion=(Node) xPath.compile("Precondicion").evaluate(actos.item(i), XPathConstants.NODE);
			Node terminacion=(Node) xPath.compile("CondicionTerminacion").evaluate(actos.item(i), XPathConstants.NODE);
			Node desc=(Node) xPath.compile("Descripcion").evaluate(actos.item(i), XPathConstants.NODE);
			NodeList agentes=(NodeList) xPath.compile("AgentesParticipantes/Agente").evaluate(actos.item(i), XPathConstants.NODESET);

			ActoHabla ah=new ActoHabla();
			ah.setNombre(new SimpleStringProperty(eElement.getAttribute("nombre")));
			ah.setTipo(new SimpleStringProperty(eElement.getAttribute("tipo")));
			ah.id=eElement.getAttribute("id");

			if(objetivo!=null)
				ah.setObjetivo(new SimpleStringProperty(objetivo.getTextContent()));
			if(Datos!=null)
				ah.setDatosintercambiados(new SimpleStringProperty(Datos.getTextContent()));
			if(precondicion!=null)
				ah.setPrecondicion(new SimpleStringProperty(precondicion.getTextContent()));
			if(terminacion!=null)
				ah.setCTerminacion(new SimpleStringProperty(terminacion.getTextContent()));
			if(desc!=null)
				ah.setDescripcion(new SimpleStringProperty(desc.getTextContent()));

			for(int x=0;x<agentes.getLength();x++){
				ah.getAgentesids().add(agentes.item(x).getTextContent());
			}//fin del for

			modelo.getActo().add(ah);

		}//fin del for

		a.setModelo_comunicaciones(modelo);
	}
	private void Modelo_Coordinacion(Node mcoor,agent a) throws XPathExpressionException{
		CoordinationModel cm=new CoordinationModel();
		NodeList Conversaciones = (NodeList) xPath.compile("Conversation").evaluate(mcoor, XPathConstants.NODESET);

		for(int i=0;i<Conversaciones.getLength();i++){
			conversacion c=new conversacion();
			Element e = (Element) Conversaciones.item(i);
			c.setNombre(new SimpleStringProperty(e.getAttribute("nombre")));
			c.setPrecondicion(new SimpleStringProperty(e.getAttribute("precondicion")));
			c.setTerminacion(new SimpleStringProperty(e.getAttribute("terminacion")));
			c.setDescripcion(new SimpleStringProperty(e.getAttribute("descripcion")));
			c.setObjetivo(new SimpleStringProperty(e.getAttribute("objetivo")));

			NodeList Actos = (NodeList) xPath.compile("ActosHabla/ActoHabla").evaluate(Conversaciones.item(i), XPathConstants.NODESET);
			NodeList Agents = (NodeList) xPath.compile("agentes/agente").evaluate(Conversaciones.item(i), XPathConstants.NODESET);

			for(int x=0;x<Actos.getLength();x++){
				c.getActos_habla().add(Actos.item(x).getTextContent());
			}
			for(int x=0;x<Agents.getLength();x++){
				c.getAgentes().add(Agents.item(x).getTextContent());
			}
			cm.getConversaciones().add(c);
			cm.getNodo_arbol().getChildren().add(c.getNodo_arbol());
		}//fin for

		a.setModelo_coordinacion(cm);
	}

	private void Modelo_InteligenciaI(Node MII,agent a) throws XPathExpressionException{
		NodeList marcos = (NodeList) xPath.compile("MarcoOntologicoIndividual").evaluate(MII, XPathConstants.NODESET);

		for(int i=0;i<marcos.getLength();i++){
			Element e = (Element) marcos.item(i);
			MarcoOntologicoIndividual moi=new MarcoOntologicoIndividual();

			moi.setNombre(new SimpleStringProperty(e.getAttribute("nombre")));
			moi.setDescripcion(new SimpleStringProperty(e.getAttribute("descripcion")));

			NodeList historica = (NodeList) xPath.compile("OntologiasHistoricas/OntologiaHistorica").evaluate(e, XPathConstants.NODESET);
			NodeList dominio = (NodeList) xPath.compile("OntologiasDominio/OntologiaDominio").evaluate(e, XPathConstants.NODESET);
			NodeList situacional = (NodeList) xPath.compile("OntologiasSituacional/OntologiaSituacionalIndividual").evaluate(e, XPathConstants.NODESET);

			for (int x=0;x<historica.getLength();x++) {
				moi.getHistorica().add(historica(historica.item(x)));
			}

			for(int x=0;x<dominio.getLength();x++){
				moi.getDominio().add(dominio(dominio.item(x)));
			}

			for(int x=0;x<situacional.getLength();x++){
				moi.getSituacional().add(situacional(situacional.item(x)));
			}

			a.getMarco_ontologico_individual().add(moi);

		}//fin for i

	}

	private MecanismoAprendizaje MAprendizaje(Node ma) throws XPathExpressionException{
		MecanismoAprendizaje aprendizaje=new MecanismoAprendizaje();

		Node nombre = (Node) xPath.compile("Nombre").evaluate(ma, XPathConstants.NODE);
		Node tipo = (Node) xPath.compile("Tipo").evaluate(ma, XPathConstants.NODE);
		Node tecnica = (Node) xPath.compile("TecnicaRepresentacion").evaluate(ma, XPathConstants.NODE);
		Node fuente = (Node) xPath.compile("FuenteAprendizaje").evaluate(ma, XPathConstants.NODE);
		Node mecanismo = (Node) xPath.compile("MecanismoActualizacion").evaluate(ma, XPathConstants.NODE);

		if(nombre!=null)
			aprendizaje.setNombre(new SimpleStringProperty(nombre.getTextContent()));


		if(tipo!=null)
			aprendizaje.setTipo(new SimpleStringProperty(tipo.getTextContent()));

		if(tecnica!=null)
			aprendizaje.setTecnica_representacion(new SimpleStringProperty(tecnica.getTextContent()));

		if(fuente!=null)
			aprendizaje.setFuente_aprendizaje(new SimpleStringProperty(fuente.getTextContent()));

		if(mecanismo!=null)
			aprendizaje.setMecanismo(new SimpleStringProperty(mecanismo.getTextContent()));

		return aprendizaje;
	}

	private MecanismoRazonamiento MRazonamiento(Node mr) throws XPathExpressionException{
		MecanismoRazonamiento razonamiento= new MecanismoRazonamiento();

		Node finformacion = (Node) xPath.compile("FuenteInformacion").evaluate(mr, XPathConstants.NODE);
		Node falimentacion = (Node) xPath.compile("FuenteAlimentacion").evaluate(mr, XPathConstants.NODE);
		Node tecnica = (Node) xPath.compile("TecnicaInferencia").evaluate(mr, XPathConstants.NODE);
		Node Lenguaje = (Node) xPath.compile("LenguajeRepresentacion").evaluate(mr, XPathConstants.NODE);
		Node relacion = (Node) xPath.compile("Relacion").evaluate(mr, XPathConstants.NODE);
		Node estrategia = (Node) xPath.compile("EstrategiaRazonamiento").evaluate(mr, XPathConstants.NODE);

		if(finformacion!=null)
			razonamiento.setFuente_informacion(new SimpleStringProperty(finformacion.getTextContent()));

		if(falimentacion!=null)
			razonamiento.setFuente_alimentacion(new SimpleStringProperty(falimentacion.getTextContent()));
		if(tecnica!=null)
			razonamiento.setTecnica_inferencia(new SimpleStringProperty(tecnica.getTextContent()));
		if(Lenguaje!=null)
			razonamiento.setLenguaje_representacion(new SimpleStringProperty(Lenguaje.getTextContent()));
		if(relacion!=null)
			razonamiento.setRelacion(new SimpleStringProperty(relacion.getTextContent()));
		if(estrategia!=null)
			razonamiento.setEstrategia_razonamiento(new SimpleStringProperty(estrategia.getTextContent()));
		return razonamiento;
	}

	private Caracteristica caracterizacion(Node c) throws XPathExpressionException{
		Caracteristica caract=new Caracteristica();

		Node tipo = (Node) xPath.compile("tipo").evaluate(c, XPathConstants.NODE);
		Node nombre = (Node) xPath.compile("nombre").evaluate(c, XPathConstants.NODE);
		Node valor = (Node) xPath.compile("ValorOmision").evaluate(c, XPathConstants.NODE);
		Node descripcion = (Node) xPath.compile("Descripcion").evaluate(c, XPathConstants.NODE);

		if(tipo!=null)
			caract.setTipo(new SimpleStringProperty(tipo.getTextContent()));
		if(nombre!=null)
			caract.setNombre(new SimpleStringProperty(nombre.getTextContent()));
		if(valor!=null)
			caract.setValor_omision(new SimpleStringProperty(valor.getTextContent()));
		if(descripcion!=null)
			caract.setDescripcion(new SimpleStringProperty(descripcion.getTextContent()));

		return caract;
	}

	private OntologiaHistorica historica(Node onto) throws XPathExpressionException{
		OntologiaHistorica historia=new OntologiaHistorica();

		Node Descripcion = (Node) xPath.compile("Descripcion").evaluate(onto, XPathConstants.NODE);
		Node fuente = (Node) xPath.compile("Fuente").evaluate(onto, XPathConstants.NODE);
		Node vmax = (Node) xPath.compile("VMaximo").evaluate(onto, XPathConstants.NODE);
		Node vmin = (Node) xPath.compile("VMinimo").evaluate(onto, XPathConstants.NODE);
		Node ma = (Node) xPath.compile("MecanismoAprendizaje").evaluate(onto, XPathConstants.NODE);
		Node mr = (Node) xPath.compile("MecanismoRazonamiento").evaluate(onto, XPathConstants.NODE);
		NodeList caracteristicas = (NodeList) xPath.compile("Caracterizacion").evaluate(onto, XPathConstants.NODESET);

		if(Descripcion!=null)
			historia.setDescripcion(new SimpleStringProperty(Descripcion.getTextContent()));
		if(fuente!=null)
			historia.setFuente(new SimpleStringProperty(fuente.getTextContent()));
		if(vmax!=null)
			historia.setVmaximo(new SimpleStringProperty(vmax.getTextContent()));
		if(vmin!=null)
			historia.setVminimo(new SimpleStringProperty(vmin.getTextContent()));
		if(ma!=null){
			historia.setAprendizaje(MAprendizaje(ma));
		}
		if(mr!=null)
			historia.setRazonamiento(MRazonamiento(mr));
		for(int i=0;i<caracteristicas.getLength();i++)
			historia.getCaracterizacion().add(caracterizacion(caracteristicas.item(i)));

		return historia;
	}

	private OntologiaDominio dominio(Node domi) throws XPathExpressionException{
		OntologiaDominio dominio=new OntologiaDominio();

		Node fuente = (Node) xPath.compile("Fuente").evaluate(domi, XPathConstants.NODE);
		Node vmax = (Node) xPath.compile("VMaximo").evaluate(domi, XPathConstants.NODE);
		Node vmin = (Node) xPath.compile("VMinimo").evaluate(domi, XPathConstants.NODE);
		Node ma = (Node) xPath.compile("MecanismoAprendizaje").evaluate(domi, XPathConstants.NODE);
		Node mr = (Node) xPath.compile("MecanismoRazonamiento").evaluate(domi, XPathConstants.NODE);
		NodeList caracteristicas = (NodeList) xPath.compile("Caracterizacion").evaluate(domi, XPathConstants.NODESET);
		Node descripcion = (Node) xPath.compile("Descripcion").evaluate(domi, XPathConstants.NODE);

		if(descripcion!=null)
			dominio.setDescripcion(new SimpleStringProperty(descripcion.getTextContent()));
		if(fuente!=null)
			dominio.setFuente(new SimpleStringProperty(fuente.getTextContent()));
		if(vmax!=null)
			dominio.setVmaximo(new SimpleStringProperty(vmax.getTextContent()));
		if(vmin!=null)
			dominio.setVminimo(new SimpleStringProperty(vmin.getTextContent()));
		if(ma!=null)
			dominio.setAprendizaje(MAprendizaje(ma));
		if(mr!=null)
			dominio.setRazonamiento(MRazonamiento(mr));
		for(int i=0;i<caracteristicas.getLength();i++)
			dominio.getCaracterizacion().add(caracterizacion(caracteristicas.item(i)));

		return dominio;
	}

	private OntologiaSituacionalI situacional(Node situ) throws XPathExpressionException{
		OntologiaSituacionalI situacional=new OntologiaSituacionalI();

		Node valor = (Node) xPath.compile("ConocimientoEstrategico/ValorConocimiento").evaluate(situ, XPathConstants.NODE);

		Node proceso = (Node) xPath.compile("ConocimientoEstrategico/ProcesoGenerador").evaluate(situ, XPathConstants.NODE);
		Node confiabilidad = (Node) xPath.compile("ConocimientoEstrategico/Confiabilidad").evaluate(situ, XPathConstants.NODE);
		Node vmax = (Node) xPath.compile("ConocimientoEstrategico/VMaximo").evaluate(situ, XPathConstants.NODE);
		Node vmin = (Node) xPath.compile("ConocimientoEstrategico/VMinimo").evaluate(situ, XPathConstants.NODE);
		Node agente = (Node) xPath.compile("ConocimientoEstrategico/AgenteGenerador").evaluate(situ, XPathConstants.NODE);
		NodeList caracte = (NodeList) xPath.compile("ConocimientoEstrategico/Caracterizacion/Caracteristica").evaluate(situ, XPathConstants.NODESET);

		Node nombre = (Node) xPath.compile("ConocimientoTareas/Nombre").evaluate(situ, XPathConstants.NODE);
		Node agenteCT = (Node) xPath.compile("ConocimientoTareas/Agente").evaluate(situ, XPathConstants.NODE);

		Node ma = (Node) xPath.compile("MecanismoAprendizaje").evaluate(situ, XPathConstants.NODE);
		Node mr = (Node) xPath.compile("MecanismoRazonamiento").evaluate(situ, XPathConstants.NODE);


		if(valor!=null)
			situacional.getEstrategico().setValor_conocimiento(new SimpleStringProperty(valor.getTextContent()));
		if(proceso!=null)
			situacional.getEstrategico().setProceso_generador(new SimpleStringProperty(proceso.getTextContent()));
		if(confiabilidad!=null)
			situacional.getEstrategico().setConfiabilidad(new SimpleStringProperty(confiabilidad.getTextContent()));
		if(vmax!=null)
			situacional.getEstrategico().setVmaximo(new SimpleStringProperty(vmax.getTextContent()));
		if(vmin!=null)
			situacional.getEstrategico().setVminimo(new SimpleStringProperty(vmin.getTextContent()));
		if(agente!=null){
			situacional.getEstrategico().setAgenteID(agente.getTextContent());
		}
		if(caracte!=null)
			for(int i=0;i<caracte.getLength();i++)
				situacional.getEstrategico().getCaracterizacion().add(caracterizacion(caracte.item(i)));

		if(nombre!=null)
			situacional.getTareas().setNombre(new SimpleStringProperty(nombre.getTextContent()));
		if(agenteCT!=null)
			situacional.getTareas().setAgenteID(agenteCT.getTextContent());
		if(ma!=null)
			situacional.setAprendizaje(MAprendizaje(ma));
		if(mr!=null)
			situacional.setRazonamiento(MRazonamiento(mr));

		return situacional;
	}//

	private void asociaciones(Node asoN,agent a) throws XPathExpressionException{
		NodeList asociaciones = (NodeList) xPath.compile("asociacion").evaluate(asoN, XPathConstants.NODESET);

		for(int i=0;i<asociaciones.getLength();i++){
			asociacion aso=new asociacion();
			Element elemento= (Element)asociaciones.item(i);

			aso.setActor_id(elemento.getAttribute("actor"));
			aso.setCu_id(elemento.getAttribute("casoUso"));
			aso.setTipo(elemento.getAttribute("tipo"));
			aso.setBase(elemento.getAttribute("base"));
			aso.setTarget(elemento.getAttribute("target"));
			a.getAsociaciones().add(aso);
		}

	}

	private asociacion asociacionesCU(Node asoN) throws XPathExpressionException{

			asociacion aso=new asociacion();
			Element elemento= (Element)asoN;

			aso.setActor_id(elemento.getAttribute("actor"));
			aso.setCu_id(elemento.getAttribute("casoUso"));
			aso.setTipo(elemento.getAttribute("tipo"));
			aso.setBase(elemento.getAttribute("base"));
			aso.setTarget(elemento.getAttribute("target"));
		return aso;

	}

	private estado estadoCU(Node asoN) throws XPathExpressionException{

		estado est=new estado();
		NodeList relations = (NodeList) xPath.compile("Relation").evaluate(asoN, XPathConstants.NODESET);
		Element elemento= (Element)asoN;

		est.setId(elemento.getAttribute("id"));
		est.setName(elemento.getAttribute("nombre"));
		est.setTipo(elemento.getAttribute("tipo"));

		for(int i=0;i<relations.getLength();i++){
			Element r= (Element)relations.item(i);
			if(r.getAttribute("tipo").equals("entrada"))
				est.getIncoming().add(relations.item(i).getTextContent());
			if(r.getAttribute("tipo").equals("salida"))
				est.getOutgoing().add(relations.item(i).getTextContent());
		}//fin del for

	return est;

}
	private void actores(Node actor,agent a) throws XPathExpressionException{

		NodeList ac = (NodeList) xPath.compile("Actor").evaluate(actor, XPathConstants.NODESET);

		for(int i=0;i<ac.getLength();i++){
			Actor actor_n=new Actor();
			Element elemento= (Element)ac.item(i);

			actor_n.setName(elemento.getAttribute("nombre"));
			actor_n.setTipo(elemento.getAttribute("tipo"));
			actor_n.setCodigo(elemento.getAttribute("codigo"));

			a.getLista_actores().add(actor_n);
		}
	}

	private Actividades diagrama_actividades(Node d) throws XPathExpressionException{
		Actividades diagrama=new Actividades();
		Element elemento= (Element)d;

		diagrama.setId(elemento.getAttribute("id"));
		diagrama.setNombre(elemento.getAttribute("nombre"));

		NodeList aso = (NodeList) xPath.compile("Asociaciones/asociacion").evaluate(d, XPathConstants.NODESET);

		for(int i=0;i<aso.getLength();i++){
			diagrama.getAsociaciones().add(asociacionesCU(aso.item(i)));
		}//fin del for

		NodeList estados = (NodeList) xPath.compile("Estados/estado").evaluate(d, XPathConstants.NODESET);
		for(int i=0;i<estados.getLength();i++){
			estado estado = estadoCU(estados.item(i));
			diagrama.getEstados().add(estado);
			diagrama.getNodo_arbol().getChildren().add(estado.getNodo_arbol());
		}//fin del for

		return diagrama;
	}

	private void casos_uso(Node CU,agent a) throws XPathExpressionException{
		NodeList ac = (NodeList) xPath.compile("CasoUso").evaluate(CU, XPathConstants.NODESET);

		for(int i=0;i<ac.getLength();i++){
			Caso_uso caso=new Caso_uso();
			Node nombre = (Node) xPath.compile("Nombre").evaluate(ac.item(i), XPathConstants.NODE);
			Node codigo = (Node) xPath.compile("Codigo").evaluate(ac.item(i), XPathConstants.NODE);
			Node descripcion= (Node) xPath.compile("Descripcion").evaluate(ac.item(i), XPathConstants.NODE);
			Node precondicion = (Node) xPath.compile("Precondicion").evaluate(ac.item(i), XPathConstants.NODE);
			Node Cfracaso = (Node) xPath.compile("CondicionFracaso").evaluate(ac.item(i), XPathConstants.NODE);
			Node Cexito = (Node) xPath.compile("CondicionExito").evaluate(ac.item(i), XPathConstants.NODE);
			Node diagrama = (Node) xPath.compile("DiagramaActividades").evaluate(ac.item(i), XPathConstants.NODE);

			if(nombre!=null)
				caso.setNombre(nombre.getTextContent());
			if(codigo!=null)
				caso.setCodigo(codigo.getTextContent());
			if(descripcion!=null)
				caso.setDescripcion(descripcion.getTextContent());
			if(precondicion!=null)
				caso.setPrecondicion(precondicion.getTextContent());
			if(Cfracaso!=null)
				caso.setCondicionFracaso(Cfracaso.getTextContent());
			if(Cexito!=null)
				caso.setCondicionExito(Cexito.getTextContent());
			if(diagrama!=null){
				caso.setDiagrama_actividades(diagrama_actividades(diagrama));
			}

			a.getCasos_uso().add(caso);
			a.getCasos_Uso_nodo().getChildren().add(caso.getNodo_arbol());
		}//fin for
	}

	private void InteligenciaColectiva(Node IC) throws XPathExpressionException{

		NodeList marco = (NodeList) xPath.compile("MarcoOntologicoColectivo").evaluate(IC, XPathConstants.NODESET);

		main.setColectivo(FXCollections.observableArrayList());
		for(int i=0;i<marco.getLength();i++){
			Element e=(Element)marco.item(i);
			MarcoOntologicoColectivo moc=new MarcoOntologicoColectivo();

			moc.setNombre(new SimpleStringProperty(e.getAttribute("name")));
			moc.setDescripcion(new SimpleStringProperty(e.getAttribute("descripcion")));
			NodeList objetivos = (NodeList) xPath.compile("Objetivos/Objetivo").evaluate(marco.item(i), XPathConstants.NODESET);

			if(objetivos!=null)
				for(int j=0;j<objetivos.getLength();j++){
					moc.getObjetivos().add(objetivos.item(j).getTextContent());
				}//fin for

			NodeList agentes = (NodeList) xPath.compile("Agentes/Agente").evaluate(marco.item(i), XPathConstants.NODESET);

			if(agentes!=null)
				for(int j=0;j<agentes.getLength();j++){
					moc.getAgentes_s().add(agentes.item(j).getTextContent());
				}//fin for

			NodeList ontologias = (NodeList) xPath.compile("OntologiasSituacionalesColectivas/OntologiaSituacionalColectiva").evaluate(marco.item(i), XPathConstants.NODESET);
			if(ontologias!=null)
				for(int j=0;j<ontologias.getLength();j++){
					moc.getOntologias().add(situacionalColectiva(ontologias.item(j)));
				}//fin for

			main.getColectivo().add(moc);
		}//fin del for

	}

	private OntologiaSituacionalC situacionalColectiva(Node s) throws XPathExpressionException{
		OntologiaSituacionalC c=new OntologiaSituacionalC();
		Node desc= (Node) xPath.compile("Descripcion").evaluate(s, XPathConstants.NODE);
		Node fuente = (Node) xPath.compile("Fuente").evaluate(s, XPathConstants.NODE);
		Node confia = (Node) xPath.compile("Confiabilidad").evaluate(s, XPathConstants.NODE);
		Node vmin = (Node) xPath.compile("ValorMinimo").evaluate(s, XPathConstants.NODE);
		Node vmax = (Node) xPath.compile("ValorMaximo").evaluate(s, XPathConstants.NODE);
		Node ma = (Node) xPath.compile("MecanismoAprendizaje").evaluate(s, XPathConstants.NODE);
		Node mr = (Node) xPath.compile("MecanismoRazonamiento").evaluate(s, XPathConstants.NODE);
		NodeList caracteristicas = (NodeList) xPath.compile("Caracterizacion/Caracteristica").evaluate(s, XPathConstants.NODESET);

		if(desc!=null)
			c.setDescripcion(new SimpleStringProperty(desc.getTextContent()));
		if(fuente!=null)
			c.setFuente(new SimpleStringProperty(fuente.getTextContent()));
		if(confia!=null)
			c.setConfiabilidad(new SimpleStringProperty(confia.getTextContent()));
		if(vmin!=null)
			c.setValor_minimo(new SimpleStringProperty(vmin.getTextContent()));
		if(vmax!=null)
			c.setValor_maximo(new SimpleStringProperty(vmax.getTextContent()));
		if(ma!=null)
			c.setAprendizaje(MAprendizaje(ma));
		if(mr!=null)
			c.setRazonamiento(MRazonamiento(mr));
		if(caracteristicas!=null)
			for(int x=0;x<caracteristicas.getLength();x++){
				c.getCaracterizacion().add(caracterizacion(caracteristicas.item(x)));
			}//fin for

		return c;
	}

}

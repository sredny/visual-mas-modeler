package application;

import java.util.LinkedList;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ObservableListAdapter<T> extends XmlAdapter<LinkedList<T>, ObservableList<T>> {

		@Override
		public ObservableList<T> unmarshal(LinkedList<T> v) throws Exception {
		    return FXCollections.observableList(v);
		}

		@Override
		public LinkedList<T> marshal(ObservableList<T> v) throws Exception {
		    LinkedList<T> list = new LinkedList<T>();
		    list.addAll(v);
		    return list;
		}
}
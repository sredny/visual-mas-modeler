package application.view;


import java.io.File;
import java.io.IOException;

import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import application.Main;
import application.ProjectLoader;
import application.model.agent;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class principalController {

    @FXML
    private Label UseCases;
     @FXML
    private Label NameLabel;
    private Stage dialogStage;

	private Main mainApp;

    public principalController(){}

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {}
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

    }

    public void showAgentDetails(agent a){

    	if (a != null) {
            // Fill the labels with info from the agent object.
           NameLabel.setText(a.getNameString());
           for(int i=0;i<a.getCasos_uso().size();i++){
        		UseCases.setText(UseCases.getText()+"-"+a.getCasos_uso().get(i).getNombre());

        	}

        } else {

            // agent is null, remove all the text.
           NameLabel.setText("");
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDeleteAgent() {

    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new agent.
     * @throws IOException
     */
    @FXML
    private void handleNewAgent() throws IOException {
    	 mainApp.showAddAgentDialog();
    }

    @FXML
	private void handleConfiguracion(){
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Configuration.fxml"));
		AnchorPane page;
		try {
			page = (AnchorPane) loader.load();
			ConfigurationController controller = loader.getController();
			controller.setMainapp(mainApp);
			Stage dialog = new Stage();
			dialog.setTitle("Configuración de Parametros");
			dialog.initModality(Modality.WINDOW_MODAL);
			dialog.initOwner(mainApp.getPrimaryStage());
			Scene scene = new Scene(page);
			dialog.setScene(scene);
			dialog.showAndWait();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    @FXML
	private void abrir_proyecto() throws SAXException, IOException, XPathExpressionException{
    	this.mainApp.getRootcontroller().abrir_proyecto();
	}

    @FXML
   	private void about() {
       	this.mainApp.getRootcontroller().handleAbout();
   	}



}

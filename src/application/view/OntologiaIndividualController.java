package application.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.Main;
import application.model.Caracteristica;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalI;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class OntologiaIndividualController {
	private Main mainapp;
	private agent agente;
	private MarcoOntologicoIndividual current=new  MarcoOntologicoIndividual();
	@FXML
	private ListView<MarcoOntologicoIndividual> marcos;
	@FXML
	private TextField nombre;
	@FXML
	private TextArea descripcion;
	private boolean replace=false;
	@FXML
	private Label titulo;
	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private Button guardar;
	@FXML
	private Label modo_edicion;
	@FXML
	private BorderPane ontologias;
	@FXML
	private AnchorPane principal;
	@FXML
	private AnchorPane panelOntologias;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	@FXML
	private ListView<OntologiaHistorica> list_OHistoricas;
	@FXML
	private ListView<OntologiaSituacionalI> list_OSituacionales;
	@FXML
	private ListView<OntologiaDominio> list_ODominio;
	@FXML
	private ListView<Caracteristica> caracteristicas;
	@FXML
	private ScrollPane panel_principal;


	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	private boolean selected=false;
	public TreeItem<Object> nodoArbol=null;
	private OntologiasIndividualController controllerontologias;

	@FXML
    private void initialize() {
		current=new  MarcoOntologicoIndividual();

		marcos.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue)->ShowMarcoDetails(newValue));
		iniciar_elementos_ontologias();
		marcos.setCellFactory(new Callback<ListView<MarcoOntologicoIndividual>, ListCell<MarcoOntologicoIndividual>>() {
			@Override
			public ListCell<MarcoOntologicoIndividual> call(ListView<MarcoOntologicoIndividual> arg0) {
				ListCell<MarcoOntologicoIndividual> cell= new ListCell<MarcoOntologicoIndividual>(){
					protected void updateItem(MarcoOntologicoIndividual obj, boolean f){
						super.updateItem(obj,f);
						if (obj != null) {
                            setText(obj.getNombre().get());
                        }
						if(f || obj == null) {
	                         setGraphic(null);
	                         setText(null);
	                    }
					}
				};

				ContextMenu contextMenu = new ContextMenu();
        		MenuItem deleteItem = new MenuItem();
                deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                deleteItem.setOnAction(event -> eliminarItem(cell.getItem()));

                MenuItem CopyItem=new MenuItem();
                CopyItem.textProperty().bind(Bindings.format("Copiar en portapeles"));
                CopyItem.setOnAction(event-> Copy(cell.getItem()));
                contextMenu.getItems().addAll(deleteItem,CopyItem);

                cell.setContextMenu(contextMenu);
				return cell;
			}

			private void Copy(MarcoOntologicoIndividual item) {
				iniciar_tabla();
				filas=FXCollections.observableArrayList();

				filas.add(new Fila("Marco ontol�gico del Individuo",""));
				filas.add(new Fila("Nombre",item.getNombre().get()));
				filas.add(new Fila("Ontolog�as que la integran:",""));

				for (OntologiaDominio  d: item.getDominio()) {
					filas.add(new Fila("O. De dominio",d.getDescripcion().get()));
				}
				for (OntologiaHistorica  h: item.getHistorica()) {
					filas.add(new Fila("O. Hist�rica",h.getDescripcion().get()));
				}
				for (OntologiaSituacionalI  s: item.getSituacional()) {
					filas.add(new Fila("O. Situacional","Conocimiento de tareas:"+s.getTareas().getNombre().get()));
				}

				tabla.setItems(filas);
				TableUtils.copySelectionToClipboard(tabla);
				mainapp.getMensaje().set("Ontolog�a individual copiada en el portapapeles");
			}

			private void eliminarItem(MarcoOntologicoIndividual item) {
				agente.getMarco_ontologico_individual().remove(item);
				agente.getModeloInteligencia().getChildren().remove(item.getNodoarbol());
				marcos.setItems(agente.getMarco_ontologico_individual());
				mainapp.getMensaje().set("Ontolog�a individual eliminada: "+item.getNombre().get() );
			}
		});

		ContextMenu contextMenuPrincipal = new ContextMenu();
		MenuItem copiar_marco = new MenuItem();
		copiar_marco.textProperty().bind(Bindings.format("Copiar marco ontol�gico"));
		copiar_marco.setOnAction(event -> mainapp.getRootcontroller().handleCopyMarcoOntologicoI(current));
		contextMenuPrincipal.getItems().add(copiar_marco);
		panel_principal.setContextMenu(contextMenuPrincipal);
	}

	public MarcoOntologicoIndividual getCurrent() {
		return current;
	}


	public void iniciar_elementos_ontologias(){
		caracteristicas.setCellFactory(new Callback<ListView<Caracteristica>, ListCell<Caracteristica>>() {

			@Override
			public ListCell<Caracteristica> call(ListView<Caracteristica> arg0) {
				ListCell<Caracteristica> cell=new ListCell<Caracteristica>(){
					 @Override
	                    protected void updateItem(Caracteristica c, boolean bln) {
	                        super.updateItem(c, bln);
	                        if (c != null) {
	                            setText(c.getNombre().get());
	                        }
	                        if(bln || c == null) {
	                            setGraphic(null);
	                            setText(null);
	                          }

	                        ContextMenu contextMenu = new ContextMenu();

	                        MenuItem deleteItem = new MenuItem();
	                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
	                        deleteItem.setOnAction(event -> caracteristicas.getItems().remove(this.getItem()));
	                        contextMenu.getItems().addAll(deleteItem);
	                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
	                            if (isNowEmpty) {
	                                this.setContextMenu(null);
	                            } else {
	                                this.setContextMenu(contextMenu);
	                            }
	                        });

	                    }
				};
				return cell;
			}
		});

		list_ODominio.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{ controllerontologias.showDominio(newValue);});

		list_ODominio.setCellFactory(new Callback<ListView<OntologiaDominio>, ListCell<OntologiaDominio>>() {
			@Override
			public ListCell<OntologiaDominio> call(ListView<OntologiaDominio> arg0) {
				ListCell<OntologiaDominio> cell=new ListCell<OntologiaDominio>(){
					 @Override
	                    protected void updateItem(OntologiaDominio c, boolean bln) {
	                        super.updateItem(c, bln);
	                        if (c != null) {
	                            setText(c.getDescripcion().get());
	                        }

	                        if(bln || c == null) {
	                            setGraphic(null);
	                            setText(null);
	                          }

	                        ContextMenu contextMenu = new ContextMenu();
	                        MenuItem deleteItem = new MenuItem();
	                        MenuItem copyItem = new MenuItem();
	                        MenuItem copyMA = new MenuItem();
	                        MenuItem copyMR = new MenuItem();

	                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
	                        copyItem.textProperty().bind(Bindings.format("Copiar"));
	                        copyMA.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
	                        copyMR.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));

	                        deleteItem.setOnAction(event -> current.getDominio().remove(this.getItem()));
	                        copyItem.setOnAction(event->Copy_dominio(this.getItem()));
	                        copyMA.setOnAction(event->Copy_MA(this.getItem().getAprendizaje()));
	                        copyMR.setOnAction(event->Copy_MR(this.getItem().getRazonamiento()));
	                        contextMenu.getItems().addAll(deleteItem,copyItem,copyMA,copyMR);
	                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
	                            if (isNowEmpty) {
	                                this.setContextMenu(null);
	                            } else {
	                                this.setContextMenu(contextMenu);
	                            }
	                        });

	                    }



				};
				return cell;
			}
			private void Copy_MR(MecanismoRazonamiento item) {
				mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(item);
			}
			private void Copy_dominio(OntologiaDominio item) {
				mainapp.getRootcontroller().handleCopyOntologiaDominio(item);
			}
			private void Copy_MA(MecanismoAprendizaje item) {
				mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(item);
			}
		});


		list_OHistoricas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> controllerontologias.showHistorica(newValue));
		list_OHistoricas.setCellFactory(new Callback<ListView<OntologiaHistorica>, ListCell<OntologiaHistorica>>() {
			@Override
			public ListCell<OntologiaHistorica> call(ListView<OntologiaHistorica> arg0) {
				ListCell<OntologiaHistorica> cell=new ListCell<OntologiaHistorica>(){
					 @Override
	                    protected void updateItem(OntologiaHistorica c, boolean bln) {
	                        super.updateItem(c, bln);
	                        if (c != null) {
	                            setText(c.getDescripcion().get());
	                        }

	                        if(bln || c == null) {
	                            setGraphic(null);
	                            setText(null);
	                          }

	                        ContextMenu contextMenu = new ContextMenu();
	                        MenuItem deleteItem = new MenuItem();
	                        MenuItem copyItem = new MenuItem();
	                        MenuItem copyMA = new MenuItem();
	                        MenuItem copyMR = new MenuItem();

	                        copyItem.textProperty().bind(Bindings.format("Copiar"));
	                        copyMA.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
	                        copyMR.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));

	                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
	                        deleteItem.setOnAction(event -> current.getHistorica().remove(this.getItem()));
	                        copyItem.setOnAction(event ->Copy_historica(this.getItem()));
	                        copyMA.setOnAction(event ->Copy_MA(this.getItem().getAprendizaje()));
	                        copyMR.setOnAction(event ->Copy_MR(this.getItem().getRazonamiento()));
	                        contextMenu.getItems().addAll(deleteItem,copyItem,copyMA,copyMR);

	                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
	                            if (isNowEmpty) {
	                                this.setContextMenu(null);
	                            } else {
	                                this.setContextMenu(contextMenu);
	                            }
	                        });

	                    }
				};
				return cell;
			}
			private void Copy_MR(MecanismoRazonamiento item) {
				mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(item);
			}
			private void Copy_historica(OntologiaHistorica item) {
				mainapp.getRootcontroller().handleCopyOntologiaHistorica(item);
			}
			private void Copy_MA(MecanismoAprendizaje item) {
				mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(item);
			}
		});


		list_OSituacionales.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> controllerontologias.showSituacional(newValue));
		list_OSituacionales.setCellFactory(new Callback<ListView<OntologiaSituacionalI>, ListCell<OntologiaSituacionalI>>() {
			@Override
			public ListCell<OntologiaSituacionalI> call(ListView<OntologiaSituacionalI> arg0) {
				ListCell<OntologiaSituacionalI> cell=new ListCell<OntologiaSituacionalI>(){
					 @Override
	                    protected void updateItem(OntologiaSituacionalI c, boolean bln) {
	                        super.updateItem(c, bln);
	                        if (c != null) {
	                            setText("Ontolog�a situacional");
	                        }
	                        if(bln || c == null) {
	                            setGraphic(null);
	                            setText(null);
	                          }

	                        ContextMenu contextMenu = new ContextMenu();
	                        MenuItem deleteItem = new MenuItem();
	                        MenuItem copyE = new MenuItem();
	                        MenuItem copyT = new MenuItem();
	                        MenuItem copyMA = new MenuItem();
	                        MenuItem copyMR = new MenuItem();

	                        copyE.textProperty().bind(Bindings.format("Copiar Conocimiento Estrat�gico"));
	                        copyT.textProperty().bind(Bindings.format("Copiar Conocimiento de Tareas"));
	                        copyMA.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
	                        copyMR.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));

	                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
	                        deleteItem.setOnAction(event -> current.getHistorica().remove(this.getItem()));
	                        copyE.setOnAction(event->Copy_Estrategico(list_OSituacionales.getSelectionModel().getSelectedItem()));
	                        copyT.setOnAction(event->Copy_Tareas(list_OSituacionales.getSelectionModel().getSelectedItem()));
	                        copyMA.setOnAction(event->Copy_MA(list_OSituacionales.getSelectionModel().getSelectedItem().getAprendizaje()));
	                        copyMR.setOnAction(event->Copy_MR(list_OSituacionales.getSelectionModel().getSelectedItem().getRazonamiento()));

	                        contextMenu.getItems().addAll(deleteItem,copyE,copyT,copyMA,copyMR);
	                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
	                            if (isNowEmpty) {
	                                this.setContextMenu(null);
	                            } else {
	                                this.setContextMenu(contextMenu);
	                            }
	                        });

	                    }
				};
				return cell;
			}
			private void Copy_MR(MecanismoRazonamiento item) {
				mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(item);
			}
			private void Copy_Tareas(OntologiaSituacionalI item) {
				mainapp.getRootcontroller().handleCopyConocimientoTareas(item);
			}
			private void Copy_Estrategico(OntologiaSituacionalI item) {
				mainapp.getRootcontroller().handleCopyConocimientoEstrategico(item);
			}
			private void Copy_MA(MecanismoAprendizaje item) {
				mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(item);
			}
		});
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	public void setCurrent(MarcoOntologicoIndividual current) {
		this.current = current;
		marcos.setItems(agente.getMarco_ontologico_individual());
		nombre.setText(this.current.getNombre().get());
		descripcion.setText(this.current.getDescripcion().get());
		controllerontologias.setMarco(current);
		if(!current.getNombre().get().equals("")){
			marcos.getSelectionModel().select(current);
			replace=true;
		}
	}

	private void ShowMarcoDetails(MarcoOntologicoIndividual newValue) {
		if(newValue==null)
			return;
		current=newValue;
		selected=true;
		modo_edicion.setVisible(true);
		nombre.setText(current.getNombre().get());
		descripcion.setText(current.getDescripcion().get());
		caracteristicas.setItems(FXCollections.observableArrayList());
		list_ODominio.setItems(current.getDominio());
		list_OHistoricas.setItems(current.getHistorica());
		list_OSituacionales.setItems(current.getSituacional());
		controllerontologias.setMarco(current);
		guardar.setText("Guardar cambios");
		panel_principal.requestFocus();
	}

	public Main getMainapp() {
		return mainapp;
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/OntologiasIndividual.fxml"));
        AnchorPane principalOverview;
		try {
			principalOverview = (AnchorPane) loader.load();
			ontologias.setCenter(principalOverview);
	        controllerontologias = loader.getController();
	        controllerontologias.setControlador_marco(this);
	        controllerontologias.setMarco(current);
	        controllerontologias.setMainapp(mainapp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public agent getAgente() {
		return agente;
	}

	public void setAgente(agent agente) {
		this.agente = agente;
		titulo.setText(agente.getNameString()+" - Modelo de Inteligencia Individual");
		marcos.setItems(agente.getMarco_ontologico_individual());
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		if(selected==true){
			guardar.setText("Guardar cambios");
			modo_edicion.setVisible(true);
		}
	}

	@FXML
	public void handleBack(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/IntelligenceModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			intelligenceModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainapp);
            //mainapp.getRootLayout().setCenter(page);
            mainapp.getRootcontroller().getDerecho().setCenter(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void handleAddOntology(){
		/*
		 * Para abrir en nueva ventana-> descomentar
		 * FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/OntologiasIndividual.fxml"));
		AnchorPane page;
		try {
			page = (AnchorPane) loader.load();
			OntologiasIndividualController controller = loader.getController();
			controller.setMainapp(mainapp);
			controller.setMarco(current);

			Stage dialog = new Stage();

			dialog.setTitle("Ontolog�as del Marco");
			dialog.initModality(Modality.WINDOW_MODAL);
			dialog.setMaxHeight(640);
			dialog.initOwner(mainapp.getPrimaryStage());
			Scene scene = new Scene(page);
			dialog.setScene(scene);
			dialog.showAndWait();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		principal.setVisible(false);
		panelOntologias.setVisible(true);

	}

	public AnchorPane getPrincipal() {
		return principal;
	}

	public void setPrincipal(AnchorPane principal) {
		this.principal = principal;
	}

	public AnchorPane getPanelOntologias() {
		return panelOntologias;
	}

	public void setPanelOntologias(AnchorPane panelOntologias) {
		this.panelOntologias = panelOntologias;
	}

	public void guardar(){
		if(replace==true||selected){
			replace=false;
			agente.getMarco_ontologico_individual().remove(current);
		}
		agente.getMarco_ontologico_individual().add(current);
		agente.getModeloInteligencia().getChildren().add(current.getNodoarbol());
		current=new MarcoOntologicoIndividual();
		controllerontologias.setMarco(current);
		marcos.getSelectionModel().clearSelection();
		list_ODominio.setItems(current.getDominio());
		list_OHistoricas.setItems(current.getHistorica());
		list_OSituacionales.setItems(current.getSituacional());
		caracteristicas.setItems(FXCollections.observableArrayList());
		marcos.getSelectionModel().clearSelection();
	}

	public boolean reescribirMarco(){

		if(selected){
			if(nombre.getText()==null||nombre.getText().isEmpty()){
				mainapp.getMensaje().set("Por favor ingrese nombre");
				mainapp.MarcarError(nombre);
				return true;
			}
			else if(descripcion.getText()==null||descripcion.getText().isEmpty()){
				mainapp.getMensaje().set("Por favor ingrese una descripci�n v�lida");
				mainapp.MarcarError(descripcion);
				mainapp.MarcarCorrecto(nombre);
				return true;
			}
			else{
				if(repetido(current, nombre.getText())){
					mainapp.getMensaje().set("Ya existe una Ontolog�a con el nombre indicado");
					return true;
				}

				mainapp.MarcarCorrecto(nombre);
				mainapp.MarcarCorrecto(descripcion);

				current.setNombre(new SimpleStringProperty(nombre.getText()));
				current.setDescripcion(new SimpleStringProperty(descripcion.getText()));
				guardar.setText("Registrar");
				vaciar();
				modo_edicion.setVisible(false);
				current=new MarcoOntologicoIndividual();
				mainapp.getMensaje().set("Ontolog�a individual actualizada exitosamente");
				list_ODominio.setItems(current.getDominio());

				list_OHistoricas.setItems(current.getHistorica());
				list_OSituacionales.setItems(current.getSituacional());
				controllerontologias.setMarco(current);
			}
			selected=false;
			marcos.getSelectionModel().clearSelection();
			//marcos.refresh();
                        marcos.setVisible(false);
                        marcos.setVisible(true);
			return true;
		}//fin if
		return false;
	}

	public boolean repetido(MarcoOntologicoIndividual marco, String nombre){
		for (MarcoOntologicoIndividual marco_ : agente.getMarco_ontologico_individual()) {
			if(nombre.equals(marco_.getNombre().get())&&marco_.hashCode()!=marco.hashCode())
				return true;
		}

		return false;
	}

	public boolean fieldsNonEmpty() {
		if(!nombre.getText().isEmpty())
			return true;
		if(!descripcion.getText().isEmpty())
			return true;
		if(current.getDominio().size()>0)
			return true;
		if(current.getHistorica().size()>0)
			return true;
		if(current.getSituacional().size()>0)
			return true;
		return false;
	}
	@FXML
	public void handleRegistrar(){
		//si ya existe el mismo nombre->reescribir,de lo contrario gurdara
				if(reescribirMarco())
					return;

		//current.setNodoarbol(new TreeItem<Object>(this));
		//TreeItem<Object> marc=new TreeItem<Object>(current);
		//agente.getModeloInteligencia().getChildren().add(marc);
		if(nombre.getText()==null||nombre.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese nombre");
			mainapp.MarcarError(nombre);
		}
		else if(descripcion.getText()==null||descripcion.getText().isEmpty()){
			mainapp.MarcarCorrecto(nombre);
			mainapp.getMensaje().set("Por favor ingrese una descripci�n v�lida");
			mainapp.MarcarError(descripcion);
		}
		else{
			if(repetido(current, nombre.getText())){
				mainapp.getMensaje().set("Ya existe una Ontolog�a con el nombre indicado");
				return;
			}

			mainapp.MarcarCorrecto(nombre);
			mainapp.MarcarCorrecto(descripcion);

			current.setNombre(new SimpleStringProperty(nombre.getText()));
			current.setDescripcion(new SimpleStringProperty(descripcion.getText()));

			guardar();
			vaciar();
			mainapp.getMensaje().set("Ontolog�a individual Registrada exitosamente");
		}

	}

	@FXML
	private void handleKeyPressed(KeyEvent ke){
		if(ke.getCode()==KeyCode.ESCAPE){
			vaciar();
			replace=false;
			selected=false;
		    modo_edicion.setVisible(false);
			marcos.getSelectionModel().clearSelection();
			guardar.setText("Registrar");
			current=new MarcoOntologicoIndividual();
			list_ODominio.setItems(current.getDominio());
			list_OHistoricas.setItems(current.getHistorica());
			list_OSituacionales.setItems(current.getSituacional());
			controllerontologias.setMarco(current);
			caracteristicas.setItems(FXCollections.observableArrayList());
			controllerontologias.vaciar();
			principal.setVisible(true);
			panelOntologias.setVisible(false);
		}
	}

	public void vaciar(){
		nombre.setText("");
		descripcion.setText("");
	}

	@FXML
	public void HandleComunicaciones(){

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CommunicationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CommunicationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainapp);
             Tab current= mainapp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText("M. de Comunicaciones");
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void HandleCoordinacion(){

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainapp);
            Tab current= mainapp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText("M. de Coordinacion");
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void handleAgente(){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
			controller.setAgente(agente,false);
            controller.setMainApp(mainapp);
            Tab current= mainapp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText("M. de Agentes");
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void handleTarea(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
			controller.setMainApp(mainapp);
			Tab current= mainapp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText("M. de Tareas");
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ListView<OntologiaHistorica> getList_OHistoricas() {
		return list_OHistoricas;
	}

	public void setList_OHistoricas(ListView<OntologiaHistorica> list_OHistoricas) {
		this.list_OHistoricas = list_OHistoricas;
	}

	public ListView<OntologiaSituacionalI> getList_OSituacionales() {
		return list_OSituacionales;
	}

	public void setList_OSituacionales(ListView<OntologiaSituacionalI> list_OSituacionales) {
		this.list_OSituacionales = list_OSituacionales;
	}

	public ListView<OntologiaDominio> getList_ODominio() {
		return list_ODominio;
	}

	public void setList_ODominio(ListView<OntologiaDominio> list_ODominio) {
		this.list_ODominio = list_ODominio;
	}

	public ListView<Caracteristica> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(ListView<Caracteristica> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}


}

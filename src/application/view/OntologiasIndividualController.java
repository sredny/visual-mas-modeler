package application.view;
import application.Main;
import application.model.Caracteristica;
import application.model.ConocimientoEstrategico;
import application.model.ConocimientoTareas;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalC;
import application.model.OntologiaSituacionalI;
import application.model.agent;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class OntologiasIndividualController {
	private Main mainapp;
	private MarcoOntologicoIndividual marco;
	private OntologiaHistorica currentH;
	private OntologiaDominio currentD;
	private ObservableList<Caracteristica> caracteristicas_l=FXCollections.observableArrayList();
	private OntologiaSituacionalI currentS;
	@FXML
	private ComboBox<String> tipo_ontologia;
	@FXML
	private AnchorPane ontologia_historica;
	@FXML
	private AnchorPane ontologia_dominio;
	@FXML
	private AnchorPane ontologia_situacional;
	private String actual;

	@FXML
	private TextArea ohDescripcion;
	@FXML
	private TextArea ohMomento;
	@FXML
	private TextField ohFuente;
	@FXML
	private TextField ohMaximo;
	@FXML
	private TextField ohMinimo;
	@FXML
	private TextField ohCaracteristica_nombre;
	@FXML
	private TextField ohCaracteristica_predeterminado;
	@FXML
	private ComboBox<String> ohCaracteristica_tipo;

	@FXML
	private TextArea odDescripcion;
	@FXML
	private TextField odFuente;
	@FXML
	private TextField odMaximo;
	@FXML
	private TextField odMinimo;
	@FXML
	private TextField odCaracteristica_nombre;
	@FXML
	private TextField odCaracteristica_predeterminado;
	@FXML
	private ComboBox<String> odCaracteristica_tipo;

	@FXML
	private TextArea osEstrategico_valor;
	@FXML
	private ComboBox<agent> osEstrategico_agente;
	@FXML
	private TextField osEstrategico_procesoG;
	@FXML
	private TextField osEstrategico_grado;
	@FXML
	private TextField osEstrategico_maximo;
	@FXML
	private TextField osEstrategico_minimo;
	@FXML
	private TextField osEstrategico_Caracteristica_nombre;
	@FXML
	private TextField osEstrategico_Caracteristica_predeterminado;
	@FXML
	private ComboBox<String> osEstrategico_Caracteristica_tipo;

	@FXML
	private TextField osTarea_nombre;
	@FXML
	private ComboBox<agent> osTarea_agente;

	@FXML
	private TextField MA_nombre;
	@FXML
	private ComboBox<String> MA_tipo;
	@FXML
	private ComboBox<String> MA_tecnica;
	@FXML
	private ComboBox<String> MA_mecanismo;
	@FXML
	private TextField fuente;

	@FXML
	private TextArea MR_informacion;
	@FXML
	private TextArea MR_alimentacion;
	@FXML
	private TextArea MR_relacion;
	@FXML
	private ComboBox<String> MR_tecnica;
	@FXML
	private ComboBox<String> MR_lenguaje;
	@FXML
	private ComboBox<String> MR_estrategia;
	@FXML
	private Button registrar;
	@FXML
	private ScrollPane panel_aprendizaje;
	@FXML
	private ScrollPane panel_razonamiento;
	@FXML
	private ScrollPane panel_dominio;
	@FXML
	private ScrollPane panel_historica;
	@FXML
	private ScrollPane panel_tareas;
	@FXML
	private ScrollPane panel_estrategico;
	@FXML
	private Label modo_edicion;
	private OntologiaIndividualController controlador_marco;
	@FXML
	private TabPane tabPanel_principal;
	@FXML
	private Tab panel_general;
	@FXML
	private Tab panel_Maprendizaje;
	@FXML
	private Tab panel_Mrazonamiento;
	@FXML
	private TabPane panel_oSituacional;
	@FXML
	private Tab tab_estrategico;
	@FXML
	private Tab tab_tareas;

	@FXML
	private void initialize() {
		ontologia_historica.setVisible(true);
		ontologia_dominio.setVisible(false);
		ontologia_situacional.setVisible(false);
		actual="historica";
		currentD=new OntologiaDominio();
		currentH=new OntologiaHistorica();
		currentS=new OntologiaSituacionalI();

		tipo_ontologia.setOnAction((event)->{
			vaciar();
			switch_screen(tipo_ontologia.getSelectionModel().getSelectedItem());
		});

		osEstrategico_agente.setConverter(new StringConverter<agent>() {
			@Override
			public String toString(agent object) {
				return object.getNameString();
			}
			@Override
			public agent fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});
		osTarea_agente.setConverter(new StringConverter<agent>() {
			@Override
			public String toString(agent object) {
				return object.getNameString();
			}
			@Override
			public agent fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});


		//Menus de contexto para copiar

		//contexto copiar ontologia de dominio
		 ContextMenu contextMenuOD = new ContextMenu();

         MenuItem CopyItemOD=new MenuItem();
         CopyItemOD.textProperty().bind(Bindings.format("Copiar en portapeles"));
         CopyItemOD.setOnAction(event-> mainapp.getRootcontroller().handleCopyOntologiaDominio(currentD));

         MenuItem CopyMAItem=new MenuItem();
         CopyMAItem.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
         CopyMAItem.setOnAction(event-> mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(currentD.getAprendizaje()));

         MenuItem CopyMRItem=new MenuItem();
         CopyMRItem.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));
         CopyMRItem.setOnAction(event-> mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(currentD.getRazonamiento()));

         contextMenuOD.getItems().addAll(CopyItemOD,CopyMAItem,CopyMRItem);
         panel_dominio.setContextMenu(contextMenuOD);

       //contexto copiar ontologia de historica
		 ContextMenu contextMenuOH = new ContextMenu();

         MenuItem CopyItemOH=new MenuItem();
         CopyItemOH.textProperty().bind(Bindings.format("Copiar en portapeles"));
         CopyItemOH.setOnAction(event-> mainapp.getRootcontroller().handleCopyOntologiaHistorica(currentH));

         MenuItem CopyMAItemH=new MenuItem();
         CopyMAItemH.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
         CopyMAItemH.setOnAction(event-> mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(currentH.getAprendizaje()));

         MenuItem CopyMRItemH=new MenuItem();
         CopyMRItemH.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));
         CopyMRItemH.setOnAction(event-> mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(currentH.getRazonamiento()));

         contextMenuOH.getItems().addAll(CopyItemOH,CopyMAItemH,CopyMRItemH);
         panel_historica.setContextMenu(contextMenuOH);


       //contexto copiar ontologia de situacional
		 ContextMenu contextMenuOS = new ContextMenu();

         MenuItem CopyItemOCE=new MenuItem();
         CopyItemOCE.textProperty().bind(Bindings.format("Copiar Conocimiento estratégico"));
         CopyItemOCE.setOnAction(event->mainapp.getRootcontroller().handleCopyConocimientoEstrategico(currentS));

         MenuItem CopyItemOCT=new MenuItem();
         CopyItemOCT.textProperty().bind(Bindings.format("Copiar Conocimiento de tareas"));
         CopyItemOCT.setOnAction(event->mainapp.getRootcontroller().handleCopyConocimientoTareas(currentS));

         MenuItem CopyMAItemS=new MenuItem();
         CopyMAItemS.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
         CopyMAItemS.setOnAction(event-> mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(currentS.getAprendizaje()));

         MenuItem CopyMRItemS=new MenuItem();
         CopyMRItemS.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));
         CopyMRItemS.setOnAction(event-> mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(currentS.getRazonamiento()));

         contextMenuOS.getItems().addAll(CopyItemOCE,CopyItemOCT,CopyMAItemS,CopyMRItemS);
         panel_estrategico.setContextMenu(contextMenuOS);
         panel_tareas.setContextMenu(contextMenuOS);
	}


	private EventHandler<ActionEvent> copiarTareas(OntologiaSituacionalI tareas) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
					mainapp.getRootcontroller().handleCopyConocimientoTareas(tareas);
			}
		};
	}

	private EventHandler<ActionEvent> copiarEstrategico(OntologiaSituacionalI estrategico) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
					mainapp.getRootcontroller().handleCopyConocimientoEstrategico(estrategico);
			}
		};
	}

	private EventHandler<ActionEvent> copiarHistorica(OntologiaHistorica c) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
					mainapp.getRootcontroller().handleCopyOntologiaHistorica(c);
			}
		};
	}

	private EventHandler<ActionEvent> copiarDominio(OntologiaDominio c) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
					mainapp.getRootcontroller().handleCopyOntologiaDominio(c);
			}
		};
	}


	private EventHandler<ActionEvent> copiarMecanismoRazonamiento(MecanismoRazonamiento razonamiento) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

					mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(razonamiento);
			}
		};// TODO Auto-generated method stub

	}

	private EventHandler<ActionEvent> copiarMecanismoAprendizaje(MecanismoAprendizaje aprendizaje) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
					mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(aprendizaje);
			}
		};// TODO Auto-generated method stub

	}

	public void showDominio(OntologiaDominio ontologia){
		if(ontologia==null)
			return;
		switch_screen("Ontología de Dominio");
		registrar.setText("Guardar cambios");
		show_mecanismoA(ontologia.getAprendizaje());
		show_mecanismoR(ontologia.getRazonamiento());

		caracteristicas_l=FXCollections.observableArrayList();
		caracteristicas_l=ontologia.getCaracterizacion();
		controlador_marco.getCaracteristicas().setItems(FXCollections.observableArrayList(caracteristicas_l));

		odDescripcion.setText(ontologia.getDescripcion().get());
		odFuente.setText(ontologia.getFuente().get());
		odMaximo.setText(ontologia.getVmaximo().get());
		odMinimo.setText(ontologia.getVminimo().get());
		controlador_marco.getPrincipal().setVisible(false);
		controlador_marco.getPanelOntologias().setVisible(true);
		tipo_ontologia.getSelectionModel().select("Ontología de Dominio");


		currentD=ontologia;
	}

	public void showHistorica(OntologiaHistorica ontologia){
		if(ontologia==null)
			return;

		switch_screen("Ontología Histórica");
		registrar.setText("Guardar cambios");
		show_mecanismoA(ontologia.getAprendizaje());
		show_mecanismoR(ontologia.getRazonamiento());

		caracteristicas_l=FXCollections.observableArrayList();
		caracteristicas_l=ontologia.getCaracterizacion();
		controlador_marco.getCaracteristicas().setItems(FXCollections.observableArrayList(caracteristicas_l));

		ohDescripcion.setText(ontologia.getDescripcion().get());
		ohFuente.setText(ontologia.getFuente().get());
		ohMaximo.setText(ontologia.getVmaximo().get());
		ohMinimo.setText(ontologia.getVminimo().get());
		ohMomento.setText(ontologia.getMomento().get());
		controlador_marco.getPrincipal().setVisible(false);
		controlador_marco.getPanelOntologias().setVisible(true);
		tipo_ontologia.getSelectionModel().select("Ontología Histórica");
		currentH=ontologia;
	}

	public void showSituacional(OntologiaSituacionalI ontologia){
		if(ontologia==null)
			return;
		switch_screen("Ontología Situacional");
		registrar.setText("Guardar cambios");
		show_mecanismoA(ontologia.getAprendizaje());
		show_mecanismoR(ontologia.getRazonamiento());

		caracteristicas_l=FXCollections.observableArrayList();
		caracteristicas_l=ontologia.getEstrategico().getCaracterizacion();
		controlador_marco.getCaracteristicas().setItems(FXCollections.observableArrayList(caracteristicas_l));

		osEstrategico_valor.setText(ontologia.getEstrategico().getValor_conocimiento().get());
		osEstrategico_agente.getSelectionModel().select(ontologia.getEstrategico().getAgente_generador());
		osEstrategico_procesoG.setText(ontologia.getEstrategico().getProceso_generador().get());
		osEstrategico_grado.setText(ontologia.getEstrategico().getConfiabilidad().get());
		osEstrategico_maximo.setText(ontologia.getEstrategico().getVmaximo().get());
		osEstrategico_minimo.setText(ontologia.getEstrategico().getVminimo().get());

		osTarea_agente.getSelectionModel().select(ontologia.getTareas().getAgente());
		osTarea_nombre.setText(ontologia.getTareas().getNombre().get());
		currentS=ontologia;
		controlador_marco.getPrincipal().setVisible(false);
		controlador_marco.getPanelOntologias().setVisible(true);
		tipo_ontologia.getSelectionModel().select("Ontología Situacional");
		//cargar datos
	}

	public void show_mecanismoA(MecanismoAprendizaje ma){

		MA_nombre.setText(ma.getNombre().get());
		MA_tipo.getSelectionModel().select(ma.getTipo().get());
		MA_tecnica.getSelectionModel().select(ma.getTecnica_representacion().get());
		fuente.setText(ma.getFuente_aprendizaje().get());
		MA_mecanismo.getSelectionModel().select(ma.getMecanismo().get());
	}

	public void show_mecanismoR(MecanismoRazonamiento mr){
		MR_informacion.setText(mr.getFuente_informacion().get());
		MR_alimentacion.setText(mr.getFuente_alimentacion().get());
		MR_relacion.setText(mr.getRelacion().get());
		MR_tecnica.getSelectionModel().select(mr.getTecnica_inferencia().get());
		MR_lenguaje.getSelectionModel().select(mr.getLenguaje_representacion().get());
		MR_estrategia.getSelectionModel().select(mr.getEstrategia_razonamiento().get());
	}

	public void switch_screen(String screen){
		tipo_ontologia.getSelectionModel().select(screen);
		if(screen.equals("Ontología de Dominio")){
			ontologia_historica.setVisible(false);
			ontologia_dominio.setVisible(true);
			ontologia_situacional.setVisible(false);
			actual="dominio";
		}

		if(screen.equals("Ontología Histórica")){
			ontologia_historica.setVisible(true);
			ontologia_dominio.setVisible(false);
			ontologia_situacional.setVisible(false);
			actual="historica";
		}
		if(screen.equals("Ontología Situacional")){
			ontologia_historica.setVisible(false);
			ontologia_dominio.setVisible(false);
			ontologia_situacional.setVisible(true);
			actual="situacional";
		}
	}

	public MarcoOntologicoIndividual getMarco() {
		return marco;
	}

	@FXML
	public void handleAddCaracteristicaHistorica(){
		if(ohCaracteristica_nombre.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un nombre para la característica");
			mainapp.MarcarError(ohCaracteristica_nombre);
			return;
		}
		mainapp.MarcarCorrecto(ohCaracteristica_nombre);

		if(ohCaracteristica_predeterminado.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un valor inicial para la característica");
			mainapp.MarcarError(ohCaracteristica_predeterminado);
			return;
		}
		mainapp.MarcarCorrecto(ohCaracteristica_predeterminado);

		if(ohCaracteristica_tipo.getSelectionModel().getSelectedIndex()==-1){
			mainapp.getMensaje().set("Por favor seleccione un tipo de característica");
			mainapp.MarcarError(ohCaracteristica_tipo);
			return;
		}
		mainapp.MarcarCorrecto(ohCaracteristica_tipo);

		Caracteristica temp=new Caracteristica(ohCaracteristica_tipo.getSelectionModel().getSelectedItem(),ohCaracteristica_nombre.getText(),ohCaracteristica_predeterminado.getText());
		//currentH.getCaracterizacion().add(temp);
		controlador_marco.getCaracteristicas().getItems().add(temp);
		//caracteristicas_l.add(temp);
		ohCaracteristica_tipo.getSelectionModel().clearSelection();
		ohCaracteristica_predeterminado.setText("");
		ohCaracteristica_nombre.setText("");
	}

	@FXML
	public void handleAddCaracteristicaDominio(){
		if(odCaracteristica_nombre.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un nombre para la característica");
			mainapp.MarcarError(odCaracteristica_nombre);
			return;
		}
		mainapp.MarcarCorrecto(odCaracteristica_nombre);

		if(odCaracteristica_predeterminado.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un valor inicial para la característica");
			mainapp.MarcarError(odCaracteristica_nombre);
			return;
		}
		mainapp.MarcarCorrecto(odCaracteristica_nombre);

		if(odCaracteristica_tipo.getSelectionModel().getSelectedIndex()==-1){
			mainapp.getMensaje().set("Por favor seleccione un tipo de característica");
			mainapp.MarcarError(odCaracteristica_tipo);
			return;
		}
		mainapp.MarcarCorrecto(odCaracteristica_tipo);

		Caracteristica temp=new Caracteristica(odCaracteristica_tipo.getSelectionModel().getSelectedItem(), odCaracteristica_nombre.getText(), odCaracteristica_predeterminado.getText());
		//currentD.getCaracterizacion().add(temp);
		//caracteristicas_l.add(temp);
		controlador_marco.getCaracteristicas().getItems().add(temp);
		odCaracteristica_nombre.setText("");
		odCaracteristica_predeterminado.setText("");
		odCaracteristica_tipo.getSelectionModel().clearSelection();
	}

	@FXML
	public void handleAddCaracteristicaSituacional(){
		if(osEstrategico_Caracteristica_nombre.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un nombre para la característica");
			mainapp.MarcarError(osEstrategico_Caracteristica_nombre);
			return;
		}
		mainapp.MarcarCorrecto(osEstrategico_Caracteristica_nombre);

		if(osEstrategico_Caracteristica_predeterminado.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un valor inicial para la característica");
			mainapp.MarcarError(osEstrategico_Caracteristica_predeterminado);
			return;
		}
		mainapp.MarcarCorrecto(osEstrategico_Caracteristica_predeterminado);

		if(osEstrategico_Caracteristica_tipo.getSelectionModel().getSelectedIndex()==-1){
			mainapp.getMensaje().set("Por favor seleccione un tipo de característica");
			mainapp.MarcarError(osEstrategico_Caracteristica_tipo);
			return;
		}
		mainapp.MarcarCorrecto(osEstrategico_Caracteristica_tipo);

		Caracteristica temp=new Caracteristica(osEstrategico_Caracteristica_tipo.getSelectionModel().getSelectedItem(), osEstrategico_Caracteristica_nombre.getText(), osEstrategico_Caracteristica_predeterminado.getText());
		//currentS.getEstrategico().getCaracterizacion().add(temp);
		//caracteristicas_l.add(temp);
		controlador_marco.getCaracteristicas().getItems().add(temp);
		osEstrategico_Caracteristica_tipo.getSelectionModel().clearSelection();
		osEstrategico_Caracteristica_nombre.setText("");
		osEstrategico_Caracteristica_predeterminado.setText("");
	}

	@FXML
	public void handleRegistrar(){

		boolean error_mecanismo_aprendizaje=false;
		boolean error_mecanismo_razonamiento=false;
		//Validando Mecanismo de aprendizaje
		if(MA_nombre.getText().isEmpty()){
			error_mecanismo_aprendizaje=true;
			mainapp.MarcarError(MA_nombre);
		}
		else
			mainapp.MarcarCorrecto(MA_nombre);
		if(MA_tipo.getSelectionModel().getSelectedItem()==null){
			error_mecanismo_aprendizaje=true;
			mainapp.MarcarError(MA_tipo);
		}
		else
			mainapp.MarcarCorrecto(MA_tipo);

		if(MA_tecnica.getSelectionModel().getSelectedItem()==null){
			error_mecanismo_aprendizaje=true;
			mainapp.MarcarError(MA_tecnica);
		}
		else
			mainapp.MarcarCorrecto(MA_tecnica);

		if(fuente.getText().isEmpty()){
			error_mecanismo_aprendizaje=true;
			mainapp.MarcarError(fuente);
		}
		else
			mainapp.MarcarCorrecto(fuente);

		if(MA_mecanismo.getSelectionModel().getSelectedItem()==null){
			error_mecanismo_aprendizaje=true;
			mainapp.MarcarError(MA_mecanismo);
		}
		else
			mainapp.MarcarCorrecto(MA_mecanismo);

		if(error_mecanismo_aprendizaje){
			mainapp.getMensaje().set("Hay errores en la información del mecanismo de aprendizaje");
			tabPanel_principal.getSelectionModel().select(panel_Maprendizaje);
			return;
		}

		//Validar mecanismo de razonamiento
		if(MR_informacion.getText().isEmpty()){
			error_mecanismo_razonamiento=true;
			mainapp.MarcarError(MR_informacion);
		}
		else
			mainapp.MarcarCorrecto(MR_informacion);

		if(MR_alimentacion.getText().isEmpty()){
			error_mecanismo_razonamiento=true;
			mainapp.MarcarError(MR_alimentacion);
		}
		else
			mainapp.MarcarCorrecto(MR_alimentacion);

		if(MR_tecnica.getSelectionModel().getSelectedItem()==null){
			error_mecanismo_razonamiento=true;
			mainapp.MarcarError(MR_tecnica);
		}
		else
			mainapp.MarcarCorrecto(MR_tecnica);

		if(MR_lenguaje.getSelectionModel().getSelectedItem()==null){
			error_mecanismo_razonamiento=true;
			mainapp.MarcarError(MR_lenguaje);
		}
		else
			mainapp.MarcarCorrecto(MR_lenguaje);

		if(MR_relacion.getText().isEmpty()){
			error_mecanismo_razonamiento=true;
			mainapp.MarcarError(MR_relacion);
		}
		else
			mainapp.MarcarCorrecto(MR_relacion);

		if(MR_estrategia.getSelectionModel().getSelectedItem()==null){
			error_mecanismo_razonamiento=true;
			mainapp.MarcarError(MR_estrategia);
		}
		else
			mainapp.MarcarCorrecto(MR_estrategia);

		if(error_mecanismo_razonamiento){
			mainapp.getMensaje().set("Hay errores en la información del mecanismo de Razonamiento");
			tabPanel_principal.getSelectionModel().select(panel_Mrazonamiento);
			return;
		}

		MecanismoAprendizaje ma=new MecanismoAprendizaje(MA_nombre.getText(),MA_tipo.getSelectionModel().getSelectedItem(), MA_tecnica.getSelectionModel().getSelectedItem(), fuente.getText(), MA_mecanismo.getSelectionModel().getSelectedItem());
		MecanismoRazonamiento mr=new MecanismoRazonamiento(MR_informacion.getText(), MR_alimentacion.getText(), MR_tecnica.getSelectionModel().getSelectedItem(), MR_lenguaje.getSelectionModel().getSelectedItem(), MR_relacion.getText(), MR_estrategia.getSelectionModel().getSelectedItem());

		if(actual.equals("situacional")){
			SaveSituacional(ma, mr);
		}
		if(actual.equals("historica")){
			SaveHistorica(ma, mr);
		}
		if(actual.equals("dominio")){
			saveDominio(ma, mr);
		}
	}

	public void SaveSituacional(MecanismoAprendizaje ma,MecanismoRazonamiento mr){
		boolean error=false;
		if(osEstrategico_agente.getSelectionModel().getSelectedItem()==null){
			mainapp.MarcarError(osEstrategico_agente);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osEstrategico_agente);

		if(osEstrategico_valor.getText().isEmpty()){
			mainapp.MarcarError(osEstrategico_valor);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osEstrategico_valor);

		if(osEstrategico_procesoG.getText().isEmpty()){
			mainapp.MarcarError(osEstrategico_procesoG);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osEstrategico_procesoG);

		if(osEstrategico_grado.getText().isEmpty()){
			mainapp.MarcarError(osEstrategico_grado);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osEstrategico_grado);

		if(osEstrategico_maximo.getText().isEmpty()){
			mainapp.MarcarError(osEstrategico_maximo);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osEstrategico_maximo);

		if(osEstrategico_minimo.getText().isEmpty()){
			mainapp.MarcarError(osEstrategico_minimo);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osEstrategico_minimo);
		if(error){
			mainapp.getMensaje().set("Hay errores en la información general del conocimiento estratégico");
			tabPanel_principal.getSelectionModel().select(panel_general);
			panel_oSituacional.getSelectionModel().select(tab_estrategico);
			return;
		}


		if(osTarea_nombre.getText().isEmpty()){
			mainapp.MarcarError(osTarea_nombre);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osTarea_nombre);

		if(osTarea_agente.getSelectionModel().getSelectedItem()==null){
			mainapp.MarcarError(osTarea_agente);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(osTarea_agente);

		if(error){
			mainapp.getMensaje().set("Hay errores en la información general del conocimiento de tareas");
			tabPanel_principal.getSelectionModel().select(panel_general);
			panel_oSituacional.getSelectionModel().select(tab_tareas);
			return;
		}
		currentS.setAprendizaje(ma);
		currentS.setRazonamiento(mr);

		currentS.getEstrategico().setValor_conocimiento(new SimpleStringProperty(osEstrategico_valor.getText()));
		currentS.getEstrategico().setAgente_generador(osEstrategico_agente.getSelectionModel().getSelectedItem());
		currentS.getEstrategico().setConfiabilidad(new SimpleStringProperty(osEstrategico_grado.getText()));
		currentS.getEstrategico().setProceso_generador(new SimpleStringProperty(osEstrategico_procesoG.getText()));
		currentS.getEstrategico().setVmaximo(new SimpleStringProperty(osEstrategico_maximo.getText()));
		currentS.getEstrategico().setVminimo(new SimpleStringProperty(osEstrategico_minimo.getText()));

		currentS.getTareas().setAgente(osTarea_agente.getSelectionModel().getSelectedItem());
		currentS.getTareas().setNombre(new SimpleStringProperty(osTarea_nombre.getText()));
		currentS.getEstrategico().setCaracterizacion(controlador_marco.getCaracteristicas().getItems());

		if(registrar.getText().equals("Registrar")){
			mainapp.getMensaje().set("Ontología situacional individual registrada");
			marco.getSituacional().add(currentS);
			marco.getNodoarbol().getChildren().add(currentS.nodoarbol);

		}else{
			registrar.setText("Registrar");
		}
		currentS=new OntologiaSituacionalI();
		vaciar();
	}

	public void SaveHistorica(MecanismoAprendizaje ma,MecanismoRazonamiento mr){

		Boolean error=false;
		if(ohDescripcion.getText().isEmpty()){
			mainapp.MarcarError(ohDescripcion);
			error=true;
		}else
			mainapp.MarcarCorrecto(ohDescripcion);

		if(ohFuente.getText().isEmpty()){
			mainapp.MarcarError(ohFuente);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(ohFuente);

		if(ohMomento.getText().isEmpty()){
			mainapp.MarcarError(ohMomento);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(ohMomento);

		if(ohMaximo.getText().isEmpty()){
			mainapp.MarcarError(ohMaximo);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(ohMaximo);

		if(ohMinimo.getText().isEmpty()){
			mainapp.MarcarError(ohMinimo);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(ohMinimo);

		if(error){
			mainapp.getMensaje().set("Hay errores en la información de la ontología histórica");
			tabPanel_principal.getSelectionModel().select(panel_general);
			return;

		}

		currentH.setAprendizaje(ma);
		currentH.setRazonamiento(mr);
		currentH.setDescripcion(new SimpleStringProperty(ohDescripcion.getText()));
		currentH.setFuente(new SimpleStringProperty(ohFuente.getText()));
		currentH.setMomento(new SimpleStringProperty(ohMomento.getText()));
		currentH.setVmaximo(new SimpleStringProperty(ohMaximo.getText()));
		currentH.setVminimo(new SimpleStringProperty(ohMinimo.getText()));
		currentH.setCaracterizacion(controlador_marco.getCaracteristicas().getItems());

		if(registrar.getText().equals("Registrar")){
			marco.getHistorica().add(currentH);
			marco.getNodoarbol().getChildren().add(currentH.nodoarbol);
			mainapp.getMensaje().set("Ontología histórica registrada");
		}else{
			registrar.setText("Registrar");
		}
			currentH=new OntologiaHistorica();
		vaciar();
	}

	public void saveDominio(MecanismoAprendizaje ma, MecanismoRazonamiento mr){

		boolean error=false;
		if(odDescripcion.getText().isEmpty()){
			mainapp.MarcarError(odDescripcion);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(odDescripcion);

		if(odFuente.getText().isEmpty()){
			mainapp.MarcarError(odFuente);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(odFuente);

		if(odMaximo.getText().isEmpty()){
			error=true;
			mainapp.MarcarError(odMaximo);
		}
		else
			mainapp.MarcarCorrecto(odMaximo);

		if(odMinimo.getText().isEmpty()){
			mainapp.MarcarError(odMinimo);
			error=true;
		}else
			mainapp.MarcarCorrecto(odMinimo);

		if(error){
			mainapp.getMensaje().set("Hay errores en la información de la ontología de dominio");
			tabPanel_principal.getSelectionModel().select(panel_general);
		}

		currentD.setAprendizaje(ma);
		currentD.setRazonamiento(mr);

		currentD.setDescripcion(new SimpleStringProperty(odDescripcion.getText()));
		currentD.setFuente(new SimpleStringProperty(odFuente.getText()));
		currentD.setVmaximo(new SimpleStringProperty(odMaximo.getText()));
		currentD.setVminimo(new SimpleStringProperty(odMinimo.getText()));
		currentD.setCaracterizacion(controlador_marco.getCaracteristicas().getItems());

		if(registrar.getText().equals("Registrar")){
			marco.getDominio().add(currentD);
			marco.getNodoarbol().getChildren().add(currentD.nodoarbol);
			mainapp.getMensaje().set("Ontología de dominio registrada");
		}else{
			registrar.setText("Registrar");
		}

		currentD=new OntologiaDominio();
		vaciar();
	}


	public void vaciar(){
		caracteristicas_l=FXCollections.observableArrayList();
		controlador_marco.getCaracteristicas().setItems(caracteristicas_l);
		registrar.setText("Registrar");

		MA_mecanismo.getSelectionModel().clearSelection();
		MA_nombre.setText("");
		MA_tecnica.getSelectionModel().clearSelection();
		MA_tipo.getSelectionModel().clearSelection();

		MR_alimentacion.setText("");
		MR_estrategia.getSelectionModel().clearSelection();
		MR_informacion.setText("");
		MR_lenguaje.getSelectionModel().clearSelection();
		MR_relacion.setText("");
		MR_tecnica.getSelectionModel().clearSelection();

		osEstrategico_agente.getSelectionModel().clearSelection();
		osEstrategico_Caracteristica_nombre.setText("");
		osEstrategico_Caracteristica_predeterminado.setText("");
		osEstrategico_grado.setText("");
		osEstrategico_maximo.setText("");
		osEstrategico_minimo.setText("");
		osEstrategico_procesoG.setText("");
		osEstrategico_valor.setText("");
		osTarea_agente.getSelectionModel().clearSelection();
		osTarea_nombre.setText("");

		odCaracteristica_nombre.setText("");
		odCaracteristica_predeterminado.setText("");
		odCaracteristica_tipo.getSelectionModel().clearSelection();
		odDescripcion.setText("");
		odFuente.setText("");
		odMaximo.setText("");
		odMinimo.setText("");

		ohCaracteristica_nombre.setText("");
		ohCaracteristica_predeterminado.setText("");
		ohCaracteristica_tipo.getSelectionModel().clearSelection();
		ohDescripcion.setText("");
		ohFuente.setText("");
		ohMaximo.setText("");
		ohMinimo.setText("");
		ohMomento.setText("");
		fuente.setText("");

		if(controlador_marco.getList_ODominio().getSelectionModel().getSelectedIndex()>-1)
			controlador_marco.getList_ODominio().getSelectionModel().clearSelection();
		if(controlador_marco.getList_OHistoricas().getSelectionModel().getSelectedIndex()>-1)
			controlador_marco.getList_OHistoricas().getSelectionModel().clearSelection();
		if(controlador_marco.getList_OHistoricas().getSelectionModel().getSelectedIndex()>-1)
			controlador_marco.getList_OSituacionales().getSelectionModel().clearSelection();
	}

	@FXML
	public void handleCerrar(){
		Stage s=(Stage) MR_informacion.getScene().getWindow();
		s.close();
	}

	public Main getMainapp() {
		return mainapp;
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;
		ohCaracteristica_tipo.setItems(mainapp.getDatabase().getCaracterizacion());
		MA_tipo.setItems(mainapp.getDatabase().getA_tipo());
        MA_tecnica.setItems(mainapp.getDatabase().getA_tecnicas());
		MA_mecanismo.setItems(mainapp.getDatabase().getA_mecanismo());
		MR_tecnica.setItems(mainapp.getDatabase().getR_tenica());
		MR_lenguaje.setItems(mainapp.getDatabase().getR_lenguaje());
		MR_estrategia.setItems(mainapp.getDatabase().getR_estrategia());
		osTarea_agente.setItems(mainapp.getAgentData());
		osEstrategico_agente.setItems(mainapp.getAgentData());
	}

	public OntologiaIndividualController getControlador_marco() {
		return controlador_marco;
	}


	public void setControlador_marco(OntologiaIndividualController controlador_marco) {
		this.controlador_marco = controlador_marco;
		controlador_marco.getCaracteristicas().setItems(FXCollections.observableArrayList());
	}

	public void setMarco(MarcoOntologicoIndividual marco) {
		this.marco = marco;
		controlador_marco.getList_ODominio().setItems(this.marco.getDominio());
		controlador_marco.getList_OHistoricas().setItems(this.marco.getHistorica());
		controlador_marco.getList_OSituacionales().setItems(this.marco.getSituacional());
	}

	@FXML
	private void handleVolver(){
		controlador_marco.getPrincipal().setVisible(true);
		controlador_marco.getPanelOntologias().setVisible(false);
	}
}

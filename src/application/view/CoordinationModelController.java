package application.view;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import application.Main;
import application.model.ActoHabla;
import application.model.MarcoOntologicoIndividual;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class CoordinationModelController {
	private Main mainApp;
	private agent agente;
	private conversacion current;
	private Stage dialogStage;
	@FXML
	private Label Agente_iniciador;
	@FXML
	private TextField nombre;
	@FXML
	private ComboBox<agent> participantes;
	@FXML
	private ComboBox<ActoHabla> hablas;
	@FXML
	private TextArea precondicion;
	@FXML
	private Label titulo;
	@FXML
	private TextArea objetivo;
	@FXML
	private TextArea terminacion;
	@FXML
	private TextArea descripcion;
	@FXML
	private ListView<conversacion> conversaciones;
	@FXML
	private ListView<ActoHabla> actos;
	@FXML
	private ListView<agent> listParticipantes;
	@FXML
	private GridPane panelx;
	private ObservableList<agent> agente_list_items=FXCollections.observableArrayList();
	@FXML
	private Button guardar;
	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	private boolean selected=false;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	@FXML
	private Label modo_edicion;
	private ObservableList<Fila> filas=FXCollections.observableArrayList();

	@FXML
    private void initialize() {
		iniciar_tabla();

		  // filter right mouse button
		conversaciones.addEventFilter(MouseEvent.MOUSE_PRESSED, e ->
        {
            if( e.isSecondaryButtonDown()) {
                e.consume();
            }

        });

//        conversaciones.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);

		conversaciones.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> ShowconversacionValues(newValue));
		conversaciones.setCellFactory(new Callback<ListView<conversacion>, ListCell<conversacion>>() {
			@Override
			public ListCell<conversacion> call(ListView<conversacion> param) {
				ListCell<conversacion> cell = new ListCell<conversacion>(){

                    @Override
                    protected void updateItem(conversacion c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNombre().get());
                        }
                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }


                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem deleteItem = new MenuItem();
                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                        deleteItem.setOnAction(event -> {conversaciones.getItems().remove(this.getItem());
                        								mainApp.setSomething_has_changed(false);
                        								});
                        contextMenu.getItems().addAll(deleteItem);
                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                            if (isNowEmpty) {
                                this.setContextMenu(null);
                            } else {
                                this.setContextMenu(contextMenu);
                            }
                        });
                    }
                };
                return cell;
			}
		});

		MenuItem copiar=new MenuItem("Copiar Modelo");
		copiar.setOnAction(copy_Coordination());
		final ContextMenu contextMenu = new ContextMenu(copiar);
		    panelx.setOnMouseClicked(new EventHandler<MouseEvent>() {
		        @Override
		        public void handle(MouseEvent mouseEvent) {
		        	if(mouseEvent.getButton()==MouseButton.SECONDARY)
		        		contextMenu.show(
		        				panelx,
		        				mouseEvent.getScreenX(),
		        				mouseEvent.getScreenY()
		        				);
		        }
		    });

		listParticipantes.setCellFactory(new Callback<ListView<agent>, ListCell<agent>>() {

			@Override
			public ListCell<agent> call(ListView<agent> param) {
				ListCell<conversacion> cell2 = new ListCell<>();
	            ContextMenu contextMenu = new ContextMenu();

	            MenuItem deleteItem = new MenuItem();
	            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
	            deleteItem.setOnAction(event -> {eliminar_conversacion(cell2.getItem());
	            								mainApp.setSomething_has_changed(false);});
	            contextMenu.getItems().addAll(deleteItem);
	            cell2.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
	                if (isNowEmpty) {
	                    cell2.setContextMenu(null);
	                } else {
	                    cell2.setContextMenu(contextMenu);
	                }
	            });
				ListCell<agent> cell = new ListCell<agent>(){

                    @Override
                    protected void updateItem(agent c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNameString());
                        }

                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }

                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem deleteItem = new MenuItem();
                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));

                        deleteItem.setOnAction(event ->{
								                        	Alert alert = new Alert(AlertType.CONFIRMATION);
								                        	alert.setTitle("Di�logo de confirmaci�n");
								                        	alert.setHeaderText("Est�s a punto de eliminar un agente participante");
								                        	alert.setContentText("Si lo eliminas, los actos del habla relacionados a la conversaci�n tambi�n se desligar�n de la conversaci�n. Quiere continuar con la operaci�n?");

								                        	Optional<ButtonType> result = alert.showAndWait();
								                        	if (result.get() == ButtonType.OK){
								                        		mainApp.setSomething_has_changed(false);
								                        		hablas.getItems().removeAll(listParticipantes.getSelectionModel().getSelectedItem().getModelo_comunicaciones().getActo());
		                        								participantes.getItems().add(this.getItem());
		                        								actos.getItems().removeAll(this.getItem().getModelo_comunicaciones().getActo());
		                        								listParticipantes.getItems().remove(this.getItem());
								                        	} else {
								                        	    // ... user chose CANCEL or closed the dialog
								                        	}


                        });

                        contextMenu.getItems().addAll(deleteItem);
                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                            if (isNowEmpty) {
                                this.setContextMenu(null);
                            } else {
                                this.setContextMenu(contextMenu);
                            }
                        });
                    }
                };
                return cell;
			}
		});

		actos.setCellFactory(new Callback<ListView<ActoHabla>, ListCell<ActoHabla>>() {

			@Override
			public ListCell<ActoHabla> call(ListView<ActoHabla> param) {
				ListCell<ActoHabla> cell = new ListCell<ActoHabla>(){
                    @Override
                    protected void updateItem(ActoHabla c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNombre().get());
                        }

                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }

                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem deleteItem = new MenuItem();
                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                        deleteItem.setOnAction(event -> {
                        	mainApp.setSomething_has_changed(false);
                        	hablas.getItems().add(this.getItem());
                        	actos.getItems().remove(this.getItem());
                        });
                        contextMenu.getItems().addAll(deleteItem);
                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                            if (isNowEmpty) {
                                this.setContextMenu(null);
                            } else {
                                this.setContextMenu(contextMenu);
                            }
                        });
                    }
                };
                return cell;
			}
		});

		current=new conversacion();
		participantes.setConverter(new StringConverter<agent>() {

			@Override
			public String toString(agent object) {
				return object.getNameString();
			}
			@Override
			public agent fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});

    }

	private EventHandler<ActionEvent> copy_Coordination(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			copy();
		}};
	}

	private void eliminar_conversacion(conversacion con){
		mainApp.setSomething_has_changed(false);
		agente.getModelo_coordinacion().getNodo_arbol().getChildren().remove(con);
		agente.getModelo_coordinacion().getConversaciones().remove(con);
		conversaciones.getItems().remove(con);
	}

	private boolean IsDuplicated(String name){
		for (conversacion c : agente.getModelo_coordinacion().getConversaciones()) {
			if(c.getNombre().get().equals(name)&&current.hashCode()!=c.hashCode())
				return true;
		}
		return false;
	}

	public boolean fieldsNonEmpty() {
		if(!nombre.getText().isEmpty())
			return true;
		if(!objetivo.getText().isEmpty())
			return true;
		if(!precondicion.getText().isEmpty())
			return true;
		if(!terminacion.getText().isEmpty())
			return true;
		if(!descripcion.getText().isEmpty())
			return true;
		if(listParticipantes.getItems().size()>0)
			return true;
		if(actos.getItems().size()>0)
			return true;
		return false;
	}
	@FXML
	public void handleRegistrar(){
		boolean error=false;
		if(nombre.getText()==null||nombre.getText().isEmpty()||!mainApp.isValidId(nombre.getText())){
			mainApp.MarcarError(nombre);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(nombre);

		if(objetivo.getText()==null||objetivo.getText().isEmpty()){
			mainApp.MarcarError(objetivo);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(objetivo);

		if(precondicion.getText()==null||precondicion.getText().isEmpty()){
			mainApp.MarcarError(precondicion);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(precondicion);

		if(terminacion.getText()==null||terminacion.getText().isEmpty()){
			mainApp.MarcarError(terminacion);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(terminacion);

		if(descripcion.getText()==null||descripcion.getText().isEmpty()){
			error=true;
			mainApp.MarcarError(descripcion);
		}
		else
			mainApp.MarcarCorrecto(descripcion);

		if(error){
			mainApp.getMensaje().set("Por favor corrija los campos marcados en rojo");
			return;
		}

		if(!selected){
			if(IsDuplicated(nombre.getText())){
				mainApp.getMensaje().set("Por favor ingrese un nombre v�lido.Ya existe una conversaci�n con �ste nombre");
				return;
			}else{
				agente.getModelo_coordinacion().getConversaciones().add(current);
				agente.getModelo_coordinacion().getNodo_arbol().getChildren().add(current.getNodo_arbol());
				mainApp.getMensaje().set("Conversacion registrada exitosamente");
			}
		}
		else{
			mainApp.getMensaje().set("Conversaci�n actualizada exitosamente");
			guardar.setText("Registrar");
		}

		mainApp.setSomething_has_changed(true);
		current.setNombre(new SimpleStringProperty(nombre.getText()));
		current.setPrecondicion(new SimpleStringProperty(precondicion.getText()));
		current.setTerminacion(new SimpleStringProperty(terminacion.getText()));
		current.setDescripcion(new SimpleStringProperty(descripcion.getText()));
		current.setObjetivo(new SimpleStringProperty(objetivo.getText()));
		current.setParticipantes(listParticipantes.getItems());
		current.setHablas(actos.getItems());
		conversaciones.getSelectionModel().clearSelection();
		modo_edicion.setVisible(false);

		selected=false;
		current=new conversacion();
		nombre.setText("");
		precondicion.setText("");
		terminacion.setText("");
		descripcion.setText("");
		objetivo.setText("");
		agente_list_items=FXCollections.observableArrayList();
		listParticipantes.setItems(FXCollections.observableArrayList());
		hablas.setItems(FXCollections.observableArrayList());
		actos.setItems(FXCollections.observableArrayList());
		agente_list_items.addAll(mainApp.getAgentData());
		participantes.setItems(agente_list_items);
		hablas.setItems(HablasAll());
		//conversaciones.refresh();
		//listParticipantes.refresh();
                conversaciones.setVisible(false);
                conversaciones.setVisible(true);
                listParticipantes.setVisible(false);
                listParticipantes.setVisible(true);
		conversaciones.getSelectionModel().clearSelection();
		//mainApp.FillTree(mainApp.getRootcontroller().getArbol());
	}

	@FXML
	private void handleKeyPressed(KeyEvent ke){
		if(ke.getCode()==KeyCode.ESCAPE){
			modo_edicion.setVisible(false);
			current=new conversacion();
			conversaciones.getSelectionModel().clearSelection();
			nombre.setText("");
			precondicion.setText("");
			terminacion.setText("");
			descripcion.setText("");
			objetivo.setText("");
			agente_list_items=FXCollections.observableArrayList();
			current.setParticipantes(FXCollections.observableArrayList());
			listParticipantes.setItems(FXCollections.observableArrayList());
			actos.setItems(FXCollections.observableArrayList());
			agente_list_items.addAll(mainApp.getAgentData());
			participantes.setItems(agente_list_items);
			hablas.setItems(FXCollections.observableArrayList());
			guardar.setText("Registrar");
		}
	}

	@FXML
	public void handleAdjuntarSecuencia(){
		 FileChooser fileChooser = new FileChooser();
	     // Set extension filter
	     FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
	           "XML files (*.xml,*.xmi)", "*.xml","*.xmi");
	     fileChooser.getExtensionFilters().add(extFilter);
        // Show save file dialog
        File file = fileChooser.showOpenDialog(dialogStage);
        current.setDiagrama_secuencia(file);
        mainApp.setSomething_has_changed(false);
        mainApp.getMensaje().set("Archivo adjuntado exitosamente");
	}


	@FXML
	private void copy(){
		if(current.getNombre().get()==null){
			mainApp.getMensaje().set("Por favor, seleccione una conversaci�n existente");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Conversaci�n",""));
		filas.add(new Fila("Conversaci�n",current.getNombre().get()));
		filas.add(new Fila("Objetivo",current.getObjetivo().get()));

		int i=0;
		String x="";
		for (agent a : current.getParticipantes()) {
			x+=a.getNameString();
			i++;
			if(i<current.getParticipantes().size())
				x+=",";
		}

		filas.add(new Fila("Agentes Participantes",x));
		filas.add(new Fila("iniciador",agente.getNameString()));

		x="";
		i=0;
		for (ActoHabla a : current.getHablas()) {
			x+=a.getNombre().get();
			i++;
			if(i<current.getHablas().size())
				x+=",";
		}
		filas.add(new Fila("Actos del Habla",x));
		filas.add(new Fila("Precondici�n",current.getPrecondicion().get()));
		filas.add(new Fila("Condici�n de Terminaci�n",current.getTerminacion().get()));
		filas.add(new Fila("Descripci�n",current.getDescripcion().get()));
		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Modelo de Coordinaci�n copiado en el portapapeles");
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	private void ShowconversacionValues(conversacion newValue) {
		if(newValue==null)
			return;
		current=newValue;
		hablas.setItems(FXCollections.observableArrayList());
		selected=true;
		agente_list_items=FXCollections.observableArrayList();
		agente_list_items.addAll(mainApp.getAgentData());
		participantes.setItems(agente_list_items);
		participantes.getItems().removeAll(current.getParticipantes());

		for (agent agente : current.getParticipantes()) {
			hablas.getItems().addAll(agente.getModelo_comunicaciones().getActo());
		}
		hablas.getItems().removeAll(current.getHablas());

		listParticipantes.setItems(FXCollections.observableArrayList(newValue.getParticipantes()));
		actos.setItems(FXCollections.observableArrayList(newValue.getHablas()));
		objetivo.setText(newValue.getObjetivo().get());

		nombre.setText(newValue.getNombre().get());
		precondicion.setText(newValue.getPrecondicion().get());
		terminacion.setText(newValue.getTerminacion().get());
		descripcion.setText(newValue.getDescripcion().get());
		guardar.setText("Guardar Cambios");
		modo_edicion.setVisible(true);
	}

	public Main getMainApp() {
		return mainApp;
	}
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;


		agente_list_items.addAll(mainApp.getAgentData());
		participantes.setItems(agente_list_items);

		hablas.setConverter(new StringConverter<ActoHabla>() {
			public String toString(ActoHabla object) {
				return object.getNombre().get();
			}
			@Override
			public ActoHabla fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});


		hablas.setItems(FXCollections.observableArrayList());

	}

	public ObservableList<ActoHabla> HablasAll(){
		ObservableList<ActoHabla> lista=FXCollections.observableArrayList();

		for (agent agentemp : mainApp.getAgentData()) {
			for (ActoHabla habla : agentemp.getModelo_comunicaciones().getActor()) {
				lista.add(habla);
			}
		}
		return lista;
	}
	@FXML
	public void HandleNext(){
		mainApp.showIntelligenceModel(getAgente(), new MarcoOntologicoIndividual());
	}

	@FXML
	public void HandleAgente(){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
			controller.setAgente(agente,false);
            controller.setMainApp(mainApp);
           // mainApp.getRootLayout().setCenter(page);
           // mainApp.getRootcontroller().getDerecho().setCenter(page);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Agentes";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void HandleTareas(){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
			controller.setMainApp(mainApp);
			//mainApp.getRootLayout().setCenter(page);
			//mainApp.getRootcontroller().getDerecho().setCenter(page);
			Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
			String Tabname=agente.getNameString()+"-M. de Tareas";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@FXML
	public void HandleBack(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/CommunicationModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			CommunicationModelController controller = loader.getController();
            controller.setAgente(agente);
			controller.setMainApp(mainApp);
			//mainApp.getRootLayout().setCenter(page);
			//mainApp.getRootcontroller().getDerecho().setCenter(page);
			Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
			String Tabname=agente.getNameString()+"-M. de Comunicaciones";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public agent getAgente() {
		return agente;
	}

	public void setAgente(agent agente) {
		this.agente = agente;
		titulo.setText(agente.getNameString()+" - Modelo de Coordinaci�n");
		conversaciones.setItems(agente.getModelo_coordinacion().getConversaciones());
		Agente_iniciador.setText(agente.getNameString());
		listParticipantes.setItems(FXCollections.observableArrayList(current.getParticipantes()));
        //sumar todas las hablas de los agentes y asignarlo  a hablas comobobox
	}

	@FXML
	public void handleAgregarParticipante(){
		if(participantes.getSelectionModel().getSelectedItem()==null)
			return;

		mainApp.setSomething_has_changed(false);
		listParticipantes.getItems().add(participantes.getSelectionModel().getSelectedItem());
		hablas.getItems().addAll(participantes.getSelectionModel().getSelectedItem().getModelo_comunicaciones().getActo());
		mainApp.getMensaje().set("Participante agregado: "+participantes.getSelectionModel().getSelectedItem().getNameString());
		participantes.getItems().remove(participantes.getSelectionModel().getSelectedItem());
		participantes.getSelectionModel().clearSelection();
	}

	@FXML
	public void handleAgregarHabla(){
		mainApp.setSomething_has_changed(true);
		mainApp.getMensaje().set("Acto de Habla agregado: "+hablas.getSelectionModel().getSelectedItem());
		actos.getItems().add(hablas.getSelectionModel().getSelectedItem());
		hablas.getItems().remove(hablas.getSelectionModel().getSelectedItem());
		hablas.getSelectionModel().clearSelection();
		mainApp.FillTree(mainApp.getRootcontroller().getArbol());
	}

	public conversacion getCurrent() {
		return current;
	}

	public void setCurrent(conversacion current) {
		selected=true;
		ShowconversacionValues(current);
		this.current = current;
	}



}

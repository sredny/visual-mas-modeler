package application.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.LimitExceededException;

import application.Main;
import application.model.Caracteristica;
import application.model.MarcoOntologicoColectivo;
import application.model.OntologiaSituacionalC;
import application.model.agent;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class OntologiaColectivaController {
	private Main mainapp;
	private agent agente;
	private MarcoOntologicoColectivo moc=new MarcoOntologicoColectivo();
	private ObservableList<String> objetivos=FXCollections.observableArrayList();
	@FXML
	private TextField nombre;
	@FXML
	private TextField objetivo;
	@FXML
	private TextArea descripcion;
	@FXML
	private ComboBox<agent> agentes_combo;
	@FXML
	private ListView<String> objetivos_l;
	@FXML
	private ListView<MarcoOntologicoColectivo> marco_l;
	@FXML
	private ListView<agent> agentes_l;
	private boolean selected=false;
	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	@FXML
	private Button guardar;
	@FXML
	private BorderPane ontologias;
	@FXML
	private AnchorPane principal;
	@FXML
	private ListView<OntologiaSituacionalC> situaciones;
	@FXML
	private ListView<Caracteristica> caracteristicas;
	private OSituacionalColectivaController controller;
	@FXML
	private ScrollPane Scroll_principal;
	@FXML
	private Label visible_mode;

	@FXML
    private void initialize() {

		ContextMenu contextMenu = new ContextMenu();

        MenuItem CopyItem=new MenuItem();
        CopyItem.textProperty().bind(Bindings.format("Copiar en portapeles"));
        CopyItem.setOnAction(event-> mainapp.getRootcontroller().handleCopyMarcoOntologicoC(moc));

        contextMenu.getItems().addAll(CopyItem);
        Scroll_principal.setContextMenu(contextMenu);

		agentes_l.setCellFactory(new Callback<ListView<agent>, ListCell<agent>>() {
			@Override
			public ListCell<agent> call(ListView<agent> arg0) {
				ListCell<agent> cell = new ListCell<agent>(){
                    @Override
                    protected void updateItem(agent c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNameString());
                        }
                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }

                    }
                };
                ContextMenu contextMenu = new ContextMenu();
    			MenuItem deleteItem=new MenuItem();
                deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                deleteItem.setOnAction(event->{mainapp.setSomething_has_changed(false);
                								agentes_combo.getItems().add(cell.getItem());
                								agentes_l.getItems().remove(cell.getItem());});
                contextMenu.getItems().addAll(deleteItem);

                cell.setContextMenu(contextMenu);
				return cell;
			}
		});

		objetivos_l.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> arg0) {
				ListCell<String> cell = new ListCell<String>(){
                    @Override
                    protected void updateItem(String c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c);
                        }
                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }

                    }
                };
                ContextMenu contextMenu = new ContextMenu();
    			MenuItem deleteItem=new MenuItem();
                deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                deleteItem.setOnAction(event-> {
                	mainapp.setSomething_has_changed(false);
                	objetivos_l.getItems().remove(cell.getItem());
                });

                contextMenu.getItems().addAll(deleteItem);

                cell.setContextMenu(contextMenu);
				return cell;
			}
		});

		agentes_combo.setConverter(new StringConverter<agent>() {
			@Override
			public String toString(agent obj) {
				if(obj!=null)
					return obj.getName().get();
				return "";
			}
			@Override
			public agent fromString(String name) {
				return new agent(name);
			}
		});

		marco_l.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> ShowMarcoValues(newValue));

		marco_l.setCellFactory(new Callback<ListView<MarcoOntologicoColectivo>, ListCell<MarcoOntologicoColectivo>>() {
			@Override
			public ListCell<MarcoOntologicoColectivo> call(ListView<MarcoOntologicoColectivo> param) {
				ListCell<MarcoOntologicoColectivo> cell = new ListCell<MarcoOntologicoColectivo>(){
                    @Override
                    protected void updateItem(MarcoOntologicoColectivo c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNombre().get());
                        }
                    }
                };

        		ContextMenu contextMenu = new ContextMenu();
        		MenuItem deleteItem = new MenuItem();
                deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                deleteItem.setOnAction(event -> {eliminarItem(cell.getItem());mainapp.setSomething_has_changed(true);});

                MenuItem CopyItem=new MenuItem();
                CopyItem.textProperty().bind(Bindings.format("Copiar en portapeles"));
                CopyItem.setOnAction(event-> Copy(cell.getItem()));
                contextMenu.getItems().addAll(deleteItem,CopyItem);

                cell.setContextMenu(contextMenu);
                return cell;
			}

			private void Copy(MarcoOntologicoColectivo item) {
				iniciar_tabla();
				filas=FXCollections.observableArrayList();
				filas.add(new Fila("Marco Ontol�gico Colectivo",""));
				filas.add(new Fila("Nombre",item.getNombre().get()));

				int i=0;
				for (String s : item.getObjetivos()) {
					if(i==0)
						filas.add(new Fila("Objetivos Colectivos",s));
					else
						filas.add(new Fila("",s));
					i++;
				}

				filas.add(new Fila("Descripci�n",item.getDescripcion().get()));

				i=0;
				for (OntologiaSituacionalC o : item.getOntologias()) {
					if(i==0)
						filas.add(new Fila("Ontolog�as que la integran",o.getDescripcion().get()));
					else
						filas.add(new Fila("",o.getDescripcion().get()));
					i++;
				}

				if(i==0)
					filas.add(new Fila("Ontolog�as que la integran","No hay ontolog�as registradas"));

				i=0;
				for (agent a : item.getAgentes()) {
					if(i==0)
						filas.add(new Fila("Agentes que la conocen",a.getNameString()));
					else
						filas.add(new Fila("",a.getNameString()));
					i++;
				}//fin del for

				if(i==0)
					filas.add(new Fila("Agentes que la conocen","No hay agentes asignados"));

				tabla.setItems(filas);
				TableUtils.copySelectionToClipboard(tabla);
				mainapp.getMensaje().set("Marco ontol�gico copiado en el portapapeles");
			}
		});


		agentes_combo.setConverter(new StringConverter<agent>() {
			@Override
			public String toString(agent obj) {
				if(obj!=null)
					return obj.getNameString();
				return "";
			}
			@Override
			public agent fromString(String name) {
				return new agent(name);
			}
		});
		objetivos_l.setItems(objetivos);
		agentes_l.setItems(moc.getAgentes());

	}

	public void iniciar_elementos_ontologias(){
		situaciones.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{ controller.ShowSituacionValues(newValue);
																												principal.setVisible(false);
																												ontologias.setVisible(true);
																											});
		situaciones.setCellFactory(new Callback<ListView<OntologiaSituacionalC>, ListCell<OntologiaSituacionalC>>() {

			@Override
			public ListCell<OntologiaSituacionalC> call(ListView<OntologiaSituacionalC> arg0) {
				ListCell<OntologiaSituacionalC> cell = new ListCell<OntologiaSituacionalC>(){
                    @Override
                    protected void updateItem(OntologiaSituacionalC c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getDescripcion().get());
                        }

                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }
                    }
                };

                ContextMenu contextMenu = new ContextMenu();
                MenuItem CopyItem=new MenuItem();
                CopyItem.textProperty().bind(Bindings.format("Copiar en portapeles"));
                CopyItem.setOnAction(event-> Copy(cell.getItem()));
                MenuItem CopyMAItem=new MenuItem();
                CopyMAItem.textProperty().bind(Bindings.format("Copiar Mecanismo de Aprendizaje"));
                CopyMAItem.setOnAction(event-> Copy_MA(cell.getItem()));
                MenuItem CopyMRItem=new MenuItem();
                CopyMRItem.textProperty().bind(Bindings.format("Copiar Mecanismo de Razonamiento"));
                CopyMRItem.setOnAction(event-> Copy_MR(cell.getItem()));

                MenuItem deleteItem = new MenuItem();
                deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                deleteItem.setOnAction(event -> {situaciones.getItems().remove(cell.getItem());
                								mainapp.setSomething_has_changed(true);
                								});

                cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                    if (isNowEmpty) {
                        cell.setContextMenu(null);
                    } else {
                        cell.setContextMenu(contextMenu);
                    }
                });
                contextMenu.getItems().addAll(CopyItem,CopyMAItem,CopyMRItem,deleteItem);

                cell.setContextMenu(contextMenu);
                return cell;
			}

			private void Copy_MR(OntologiaSituacionalC item) {
				filas=FXCollections.observableArrayList();
				iniciar_tabla();
				filas.add(new Fila("Mecanismo de Razonamiento",""));
				filas.add(new Fila("Fuente de Informaci�n",item.getRazonamiento().getFuente_informacion().get()));
				filas.add(new Fila("Fuente de Alimentaci�n",item.getRazonamiento().getFuente_alimentacion().get()));
				filas.add(new Fila("Tecnica de Inferencia",item.getRazonamiento().getTecnica_inferencia().get()));
				filas.add(new Fila("Lenguaje de Representaci�n del conocimiento",item.getRazonamiento().getLenguaje_representacion().get()));
				filas.add(new Fila("Relaci�n Tarea-Inferencia",item.getRazonamiento().getRelacion().get()));
				filas.add(new Fila("Estrategias de razonamiento",item.getRazonamiento().getEstrategia_razonamiento().get()));

				tabla.setItems(filas);
				TableUtils.copySelectionToClipboard(tabla);
			}

			private void Copy_MA(OntologiaSituacionalC item) {
				filas=FXCollections.observableArrayList();
				iniciar_tabla();
				filas.add(new Fila("Mecanismo de Aprendizaje Colectivo",""));
				filas.add(new Fila("Nombre",item.getAprendizaje().getNombre().get()));
				filas.add(new Fila("Tipo",item.getAprendizaje().getTipo().get()));
				filas.add(new Fila("Tecnica de representaci�n",item.getAprendizaje().getTecnica_representacion().get()));
				filas.add(new Fila("Fuente de Aprendizaje",item.getAprendizaje().getFuente_aprendizaje().get()));
				filas.add(new Fila("Mecanismo de actualizaci�n",item.getAprendizaje().getMecanismo().get()));
				tabla.setItems(filas);
				TableUtils.copySelectionToClipboard(tabla);
			}

			public void Copy(OntologiaSituacionalC item) {
				filas=FXCollections.observableArrayList();
				iniciar_tabla();
				filas.add(new Fila("Ontolog�a Situacional colectiva",""));
				filas.add(new Fila("Descripci�n","Conocimiento colectivo"));
				filas.add(new Fila("Fuente",item.getFuente().get()));
				filas.add(new Fila("Grado de confiabilidad",item.getConfiabilidad().get()));
				filas.add(new Fila("Caracterizaci�n",""));


				for (Caracteristica c : item.getCaracterizacion()) {
					filas.add(new Fila("Nombre:"+c.getNombre().get(),"Valor por Omisi�n:"+c.getValor_omision().get()));
				}//fin del for
				filas.add(new Fila("Valor m�ximo",item.getValor_maximo().get()));
				filas.add(new Fila("Valor minimo",item.getValor_minimo().get()));

				tabla.setItems(filas);
				TableUtils.copySelectionToClipboard(tabla);
			}
		});


		caracteristicas.getSelectionModel().selectedItemProperty().addListener((observable,oldvalue,newValue)->controller.ShowCaracteristicaValues(newValue));
		caracteristicas.setCellFactory(new Callback<ListView<Caracteristica>, ListCell<Caracteristica>>() {

			@Override
			public ListCell<Caracteristica> call(ListView<Caracteristica> param) {
				ListCell<Caracteristica> cell = new ListCell<Caracteristica>(){

                    @Override
                    protected void updateItem(Caracteristica c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNombre().get());
                        }

                        if(bln || c == null) {
                            setGraphic(null);
                            setText(null);
                          }

                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem deleteItem = new MenuItem();
                        deleteItem.textProperty().bind(Bindings.format("Eliminar"));
                        deleteItem.setOnAction(event ->{mainapp.setSomething_has_changed(true);
                        								caracteristicas.getItems().remove(this.getItem());
                        });
                        contextMenu.getItems().addAll(deleteItem);
                        this.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                            if (isNowEmpty) {
                                this.setContextMenu(null);
                            } else {
                                this.setContextMenu(contextMenu);
                            }
                        });
                    }
                };
                return cell;
			}
		});
	}

	public void Copy(OntologiaSituacionalC item) {
		filas=FXCollections.observableArrayList();
		iniciar_tabla();
		filas.add(new Fila("Ontolog�a Situacional colectiva",""));
		filas.add(new Fila("Descripci�n","Conocimiento colectivo"));
		filas.add(new Fila("Fuente",item.getFuente().get()));
		filas.add(new Fila("Grado de confiabilidad",item.getConfiabilidad().get()));
		filas.add(new Fila("Caracterizaci�n",""));


		for (Caracteristica c : item.getCaracterizacion()) {
			filas.add(new Fila("Nombre:"+c.getNombre().get(),"Valor por Omisi�n:"+c.getValor_omision().get()));
		}//fin del for
		filas.add(new Fila("Valor m�ximo",item.getValor_maximo().get()));
		filas.add(new Fila("Valor minimo",item.getValor_minimo().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
	}

	public void Copy_MR(OntologiaSituacionalC item) {
		filas=FXCollections.observableArrayList();
		iniciar_tabla();
		filas.add(new Fila("Mecanismo de Razonamiento",""));
		filas.add(new Fila("Fuente de Informaci�n",item.getRazonamiento().getFuente_informacion().get()));
		filas.add(new Fila("Fuente de Alimentaci�n",item.getRazonamiento().getFuente_alimentacion().get()));
		filas.add(new Fila("Tecnica de Inferencia",item.getRazonamiento().getTecnica_inferencia().get()));
		filas.add(new Fila("Lenguaje de Representaci�n del conocimiento",item.getRazonamiento().getLenguaje_representacion().get()));
		filas.add(new Fila("Relaci�n Tarea-Inferencia",item.getRazonamiento().getRelacion().get()));
		filas.add(new Fila("Estrategias de razonamiento",item.getRazonamiento().getEstrategia_razonamiento().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
	}

	public void Copy_MA(OntologiaSituacionalC item) {
		filas=FXCollections.observableArrayList();
		iniciar_tabla();
		filas.add(new Fila("Mecanismo de Aprendizaje Colectivo",""));
		filas.add(new Fila("Nombre",item.getAprendizaje().getNombre().get()));
		filas.add(new Fila("Tipo",item.getAprendizaje().getTipo().get()));
		filas.add(new Fila("Tecnica de representaci�n",item.getAprendizaje().getTecnica_representacion().get()));
		filas.add(new Fila("Fuente de Aprendizaje",item.getAprendizaje().getFuente_aprendizaje().get()));
		filas.add(new Fila("Mecanismo de actualizaci�n",item.getAprendizaje().getMecanismo().get()));
		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	private void eliminarItem(MarcoOntologicoColectivo mo){

		//marco_l.getItems().remove(mo);

		mainapp.setSomething_has_changed(true);
		mainapp.getColectivo().remove(mo);
		marco_l.setItems(mainapp.getColectivo());
		mainapp.getMensaje().set("Marco ontol�gico eliminado: "+mo.getNombre().get());
	}

	@FXML
	public void handleBack(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/IntelligenceModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			intelligenceModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainapp);
            //mainapp.getRootLayout().setCenter(page);
            mainapp.getRootcontroller().getDerecho().setCenter(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void ShowMarcoValues(MarcoOntologicoColectivo newValue) {


		if(newValue==null){
			moc=new MarcoOntologicoColectivo();
			return;
		}

		moc=newValue;
		selected=true;
		visible_mode.setVisible(true);
		nombre.setText(newValue.getNombre().get());
		objetivos_l.setItems(FXCollections.observableArrayList(newValue.getObjetivos()));
		//moc.getAgentes().addAll(newValue.getAgentes());
		agentes_l.setItems(FXCollections.observableArrayList(moc.getAgentes()));
		agentes_combo.setItems(FXCollections.observableArrayList(mainapp.getAgentData()));
		agentes_combo.getItems().removeAll(moc.getAgentes());
		descripcion.setText(newValue.getDescripcion().get());
		caracteristicas.setItems(FXCollections.observableArrayList());
		situaciones.setItems(FXCollections.observableArrayList(moc.getOntologias()));
		controller.setMoc(moc);
		guardar.setText("Guardar cambios");
		principal.setVisible(true);
		ontologias.setVisible(false);
	}

	public MarcoOntologicoColectivo getMoc() {
		return moc;
	}

	public void setMoc(MarcoOntologicoColectivo moc) {
		this.moc = moc;
		marco_l.getSelectionModel().select(moc);
		ShowMarcoValues(getMoc());
	}

	public Main getMainapp() {
		return mainapp;
	}

	public void fill(){
		ObservableList<OntologiaSituacionalC> nueva=FXCollections.observableArrayList();
		ObservableList<agent> agentes_l=FXCollections.observableArrayList();
		agentes_l=mainapp.getAgentData();
		agentes_combo.setItems(FXCollections.observableArrayList(mainapp.getAgentData()));
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;
		marco_l.setItems(mainapp.getColectivo());
		fill();

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/OSituacionalColectiva.fxml"));
        AnchorPane principalOverview;
		try {
			principalOverview = (AnchorPane) loader.load();
			// Set person overview into the center of root layout.
	        //rootLayout.setCenter(principalOverview);
	        ontologias.setCenter(principalOverview);
	        // Give the controller access to the main app.
	        controller = loader.getController();
	        controller.setControlador_marco(this);
	        controller.setMoc(moc);
	        controller.setMainapp(mainapp);
	        iniciar_elementos_ontologias();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public agent getAgente() {
		return agente;
	}
	public void setAgente(agent agente) {
		this.agente = agente;
		moc.getAgentes().add(agente);
	}

	@FXML
	public void handleAgregarObjetivo(){
		if(objetivo.getText().isEmpty())
			return;
		objetivos_l.getItems().add(objetivo.getText());

		mainapp.setSomething_has_changed(true);
		objetivo.setText("");
		mainapp.getMensaje().set("Objetivo agregado correctamente");
	}

	@FXML
	public void handleAgregarAgente(){

		agent agente_seleccionado;
		String nombre=agentes_combo.getEditor().getText();
		agent ac=null;

		mainapp.setSomething_has_changed(true);
		/*
		if(nombre.isEmpty()||nombre==null){
			agente_seleccionado=agentes_combo.getSelectionModel().getSelectedItem();
			System.out.println("id del agente:"+agente_seleccionado.id+" id del primero:"+agentes_combo.getSelectionModel().getSelectedItem());
		}
		else{
			agente_seleccionado=new agent(nombre);
			agentes_combo.getEditor().setText("");
		}*/
		agentes_l.getItems().add(agentes_combo.getSelectionModel().getSelectedItem());
		agentes_combo.getItems().remove(agentes_combo.getSelectionModel().getSelectedItem());
		agentes_combo.getSelectionModel().clearSelection();
		mainapp.getMensaje().set("Agente agregado correctamente ");
	}

	public boolean exist(String nombre, MarcoOntologicoColectivo marco){
		for (MarcoOntologicoColectivo mo : mainapp.getColectivo()) {
			if(mo.getNombre().get().equals(nombre)&&marco.hashCode()!=mo.hashCode())
				return true;
		}
		return false;
	}

	public boolean fieldsNonEmpty() {
		if(!nombre.getText().isEmpty())
			return true;
		if(!descripcion.getText().isEmpty())
			return true;
		if(agentes_l.getItems().size()>0)
			return true;
		if(situaciones.getItems().size()>0)
			return true;
		if(objetivos_l.getItems().size()>0)
			return true;
		return false;
	}

	@FXML
	public void handleRegistrar(){
		String nombre_=nombre.getText();

		if(nombre_==null||nombre_.isEmpty()||exist(nombre_,moc)){
			mainapp.getMensaje().set("Indique un nombre de marco ontol�gico v�lido");
			if(exist(nombre_,moc))
				mainapp.getMensaje().set("Ya hay un marco ontologico con este nombre");
			mainapp.MarcarError(nombre);
			return;
		}
		mainapp.MarcarCorrecto(nombre);

		if(descripcion.getText()==null||descripcion.getText().isEmpty()){
			mainapp.getMensaje().set("Indique una descripcion de marco ontol�gico v�lido");
			mainapp.MarcarError(descripcion);
			return;
		}
		mainapp.MarcarCorrecto(descripcion);

		moc.setAgentes(agentes_l.getItems());
		moc.setNombre(new SimpleStringProperty(nombre.getText()));
		moc.setObjetivos(objetivos);
		moc.setDescripcion(new SimpleStringProperty(descripcion.getText()));
		moc.setAgentes(agentes_l.getItems());
		moc.setOntologias(situaciones.getItems());
		moc.setObjetivos(objetivos_l.getItems());
		if(selected){
			mainapp.getMensaje().set("Marco Ontol�gico actualizado exitosamente");
		}
		else{
			mainapp.getColectivo().add(moc);
			mainapp.getMensaje().set("Marco Ontol�gico registrado exitosamente");
			mainapp.getNodo_moc().getChildren().add(moc.getNodo_arbol());
		}

		vaciar();
	}

	@FXML
	public void handleAgregarOntologiaSituacional(){
		/*FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/OSituacionalColectiva.fxml"));
		AnchorPane page;
		try {
			page = (AnchorPane) loader.load();

		OSituacionalColectivaController controller = loader.getController();
        controller.setMoc(moc);
        controller.setMainapp(mainapp);
		Stage dialog = new Stage();

        dialog.setTitle("Informaci�n de Ontolog�a Situacional");
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(mainapp.getPrimaryStage());
        Scene scene = new Scene(page);
        dialog.setScene(scene);
        dialog.showAndWait();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		principal.setVisible(false);
		ontologias.setVisible(true);
	}

	@FXML
	private void handleEliminar(){
		mainapp.getColectivo().remove(moc);

		mainapp.setSomething_has_changed(true);
		mainapp.getMensaje().set("Maro ontol�gico eliminado:"+moc.getNombre().get());
	}

	public BorderPane getOntologias() {
		return ontologias;
	}

	public void setOntologias(BorderPane ontologias) {
		this.ontologias = ontologias;
	}

	public AnchorPane getPrincipal() {
		return principal;
	}

	public void setPrincipal(AnchorPane principal) {
		this.principal = principal;
	}

	public ListView<OntologiaSituacionalC> getSituaciones() {
		return situaciones;
	}

	public void setSituaciones(ListView<OntologiaSituacionalC> situaciones) {
		this.situaciones = situaciones;
	}

	public ListView<Caracteristica> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(ListView<Caracteristica> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	@FXML
	private void handleSalirEdicion(KeyEvent ke){
		if(ke.getCode()==KeyCode.ESCAPE){
			visible_mode.setVisible(false);
			vaciar();
		}
	}

	private void vaciar(){
		//marco_l.refresh();
                marco_l.setVisible(false);
                marco_l.setVisible(true);
		marco_l.getSelectionModel().clearSelection();
		nombre.setText("");
		objetivo.setText("");
		descripcion.setText("");
		guardar.setText("Registrar marco");
		selected=false;
		moc=new MarcoOntologicoColectivo();
		controller.setControlador_marco(this);
		controller.setMoc(moc);
		moc.setOntologias(FXCollections.observableArrayList());
		moc.getAgentes().clear();
		caracteristicas.setItems(FXCollections.observableArrayList());
		situaciones.setItems(FXCollections.observableArrayList());
		agentes_combo.setItems(FXCollections.observableArrayList(mainapp.getAgentData()));
		mainapp.setSomething_has_changed(true);
		objetivos=FXCollections.observableArrayList();
		objetivos.setAll(moc.getObjetivos());
		objetivos_l.setItems(objetivos);
		agentes_l.setItems(moc.getAgentes());
		agentes_l.getSelectionModel().clearSelection();
		situaciones.getSelectionModel().clearSelection();
		objetivos_l.getSelectionModel().clearSelection();
		caracteristicas.getSelectionModel().clearSelection();
	}


}

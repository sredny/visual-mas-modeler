package application.view;

import java.io.IOException;
import application.Main;
import application.model.ActoHabla;
import application.model.MarcoOntologicoIndividual;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class CommunicationModelController {
	private Main mainApp;
	private agent agente;
	@FXML
	private TextField nombre;
	@FXML
	private ComboBox<String> tipo;
	@FXML
	private ComboBox<agent> agentes_participantes;
	@FXML
	private TextArea objetivo;
	@FXML
	private TextArea datos_intercambiados;
	@FXML
	private TextArea precondicion;
	@FXML
	private TextArea terminacion;
	@FXML
	private TextArea descripcion;
	@FXML
	private TableView<ActoHabla> tabla_actos;
	 @FXML
	 private TableColumn<ActoHabla, String> NameColumn;
	@FXML
	private TableView<agent> tabla_agentes;
	@FXML
	private TableColumn<agent, String> AgenteColumn;
	@FXML
	private Label titulo;
	@FXML
	private Label modo_edicion;
	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	@FXML
	private GridPane panelx;
	private ActoHabla current;
	public ActoHabla getCurrent() {
		return current;
	}

	public void setCurrent(ActoHabla current) {
		this.current = current;
	}

	@FXML
	private Button guardar;


	/*
	 * ACL Mensajes
	 *
	 * ACLMessage msg = new ACLMessage( ACLMessage.INFORM );
       msg.setContent("I sell seashells at $10/kg" );
	 *
	 * */

	@FXML
    private void initialize() {
		NameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		AgenteColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
		current=new ActoHabla();
		tabla_agentes.setItems(FXCollections.observableArrayList());
		iniciar_tabla();
		tabla_actos.getSelectionModel().selectedItemProperty().addListener(
	    		(observable, oldValue, newValue) -> ShowActoValues(newValue));

		agentes_participantes.setConverter(new StringConverter<agent>() {
			@Override
			public String toString(agent object) {
				return object.getNameString();
			}

			@Override
			public agent fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});

		ContextoTablas();

		MenuItem copiar=new MenuItem("Copiar Modelo");
		copiar.setOnAction(copy_Comunicacion());
		final ContextMenu contextMenu = new ContextMenu(copiar);
		    panelx.setOnMouseClicked(new EventHandler<MouseEvent>() {
		        @Override
		        public void handle(MouseEvent mouseEvent) {
		        	if(mouseEvent.getButton()==MouseButton.SECONDARY)
		        		contextMenu.show(
		        				panelx,
		        				mouseEvent.getScreenX(),
		        				mouseEvent.getScreenY()
		        				);
		        }
		    });
    }

	public TableView<ActoHabla> getTabla_actos() {
		return tabla_actos;
	}

	public void setTabla_actos(TableView<ActoHabla> tabla_actos) {
		this.tabla_actos = tabla_actos;
	}

	private void ContextoTablas(){
		tabla_actos.setRowFactory(new Callback<TableView<ActoHabla>, TableRow<ActoHabla>>() {
            public TableRow<ActoHabla> call(TableView<ActoHabla> tableView) {
                final TableRow<ActoHabla> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	mainApp.setSomething_has_changed(false);
                    	agente.getModelo_comunicaciones().getNodo_arbol().getChildren().remove(row.getItem().getNodo_arbol());
                    	tabla_actos.getItems().remove(row.getItem());
                    	vaciarform();
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                return row ;
            }
        });

		tabla_agentes.setRowFactory(new Callback<TableView<agent>, TableRow<agent>>() {
            public TableRow<agent> call(TableView<agent> tableView) {
                final TableRow<agent> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	mainApp.setSomething_has_changed(true);
                    	agentes_participantes.getItems().add(row.getItem());
                    	tabla_agentes.getItems().remove(row.getItem());
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                return row ;
            }
        });
	}

	private EventHandler<ActionEvent> copy_Comunicacion(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopy();
		}};
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	@FXML
	private void handleCopy(){
		if(current.getNombre().get()==null){
			mainApp.getMensaje().set("Por favor, seleccione un acto de habla existente");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Comunicaciones",""));
		filas.add(new Fila("Acto del Habla",current.getNombre().get()));
		filas.add(new Fila("Tipo",current.getTipo().get()));
		filas.add(new Fila("Objetivo",current.getObjetivo().get()));

		String x="";
		int i=0;
		for (agent a : current.getAgentesAH()) {
			x+=a.getNameString();
			if(i<current.getAgentesAH().size())
				x+=",";
		}
		filas.add(new Fila("Agentes Participantes",x));
		filas.add(new Fila("Iniciador",agente.getNameString()));
		filas.add(new Fila("Datos intercambiados",current.getDatosintercambiados().get()));
		filas.add(new Fila("Precondicion",current.getPrecondicion().get()));
		filas.add(new Fila("Condición de Terminación",current.getCTerminacion().get()));
		filas.add(new Fila("Descripción",current.getDescripcion().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Modelo de Comunicaciones copiado en el portapapeles");
	}


	public Main getMainApp() {
		return mainApp;
	}
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
		ObservableList<agent> a=FXCollections.observableArrayList();
		a.addAll(mainApp.getAgentData());
		agentes_participantes.setItems(FXCollections.observableArrayList(a));
        tipo.setItems(mainApp.getDatabase().getActos_habla());
	}

	@FXML
	public void HandleAgente(){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
			controller.setAgente(agente,false);
            controller.setMainApp(mainApp);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Agentes";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void HandleNext(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainApp);
           // mainApp.getRootLayout().setCenter(page);
          //  mainApp.getRootcontroller().getDerecho().setCenter(page);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Coordinacion";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void HandleInteligencia(){
		mainApp.showIntelligenceModel(getAgente(), new MarcoOntologicoIndividual());
	}

	@FXML
	private void handleKeyPressed(KeyEvent ke){
		if(ke.getCode()==KeyCode.ESCAPE){
			modo_edicion.setVisible(false);
			vaciarform();
			tabla_actos.getSelectionModel().clearSelection();
			boolean is=false;
			guardar.setText("Registrar");
			modo_edicion.setVisible(false);
			current=new ActoHabla();
			ObservableList<agent> ab=FXCollections.observableArrayList();
			ab.addAll(mainApp.getAgentData());
			agentes_participantes.setItems(ab);
			tabla_agentes.setItems(FXCollections.observableArrayList());
		}
	}


	public void vaciarform(){
		nombre.setText("");
		tipo.getSelectionModel().clearSelection();
		objetivo.setText("");
		datos_intercambiados.setText("");
		precondicion.setText("");
		terminacion.setText("");
		descripcion.setText("");

	}

	public boolean is_repeated(String name){
		for (ActoHabla acto : agente.getModelo_comunicaciones().getActo()) {
			if(acto.getNombre().get().equals(name)&&acto.hashCode()!=current.hashCode())
				return true;
		}
		return false;
	}


	public boolean fieldsNonEmpty() {
		if(!nombre.getText().isEmpty())
			return true;
		if(!objetivo.getText().isEmpty())
			return true;
		if(!datos_intercambiados.getText().isEmpty())
			return true;
		if(!precondicion.getText().isEmpty())
			return true;
		if(!terminacion.getText().isEmpty())
			return true;
		if(!descripcion.getText().isEmpty())
			return true;
		if(tabla_agentes.getItems().size()>0)
			return true;
		return false;
	}

	/**
	 * Funcion que guarda un acto de habla en la lista correspondiente
	 *
	 * **/
	@FXML
	public void handleRegistrar(){
		boolean error=false;
		if(nombre.getText().isEmpty()||nombre.getText()==null||!mainApp.isValidId(nombre.getText())){
			mainApp.MarcarError(nombre);
			error=true;
		}
		if(is_repeated(nombre.getText())){
			mainApp.getMensaje().set("Ya existe un acto de habla con el nombre indicado!");
			mainApp.MarcarError(nombre);
			return;
		}
		else
			mainApp.MarcarCorrecto(nombre);

		if(tipo.getSelectionModel().getSelectedItem()==null){
			mainApp.MarcarError(tipo);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(tipo);

		if(objetivo.getText().isEmpty()||objetivo.getText()==null){
			mainApp.MarcarError(objetivo);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(objetivo);

		if(datos_intercambiados.getText().isEmpty()||datos_intercambiados.getText()==null){
			mainApp.MarcarError(datos_intercambiados);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(datos_intercambiados);

		if(precondicion.getText().isEmpty()||precondicion.getText()==null){
			mainApp.MarcarError(precondicion);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(precondicion);

		if(terminacion.getText().isEmpty()||terminacion.getText()==null){
			error=true;
			mainApp.MarcarError(terminacion);
		}else
			mainApp.MarcarCorrecto(terminacion);

		if(descripcion.getText().isEmpty()||descripcion.getText()==null){
			error=true;
			mainApp.MarcarError(descripcion);
		}
		else
			mainApp.MarcarCorrecto(descripcion);

		if(error){
			mainApp.getMensaje().set("Por favor corrija los campos marcados en rojo");
			return;
		}

		mainApp.setSomething_has_changed(true);
		current.setAgentes(tabla_agentes.getItems());
		current.setNombre(new SimpleStringProperty(nombre.getText()));
		current.setTipo(new SimpleStringProperty(tipo.getSelectionModel().getSelectedItem()));
		current.setObjetivo(new SimpleStringProperty(objetivo.getText()));
		current.setDatosintercambiados(new SimpleStringProperty(datos_intercambiados.getText()));
		current.setPrecondicion(new SimpleStringProperty(precondicion.getText()));
		current.setCTerminacion(new SimpleStringProperty(terminacion.getText()));
		current.setDescripcion(new SimpleStringProperty(descripcion.getText()));
		ObservableList<agent> a=FXCollections.observableArrayList();
		a.addAll(mainApp.getAgentData());
		vaciarform();
		agentes_participantes.setItems(a);

		if(!guardar.getText().equals("Guardar Cambios")){
			agente.getModelo_comunicaciones().getActor().add(current);
			agente.getModelo_comunicaciones().getNodo_arbol().getChildren().add(current.getNodo_arbol());
		}

		guardar.setText("Registrar");
		modo_edicion.setVisible(false);

		current=new ActoHabla();
		ObservableList<agent> ab=FXCollections.observableArrayList();
		ab.addAll(mainApp.getAgentData());
		agentes_participantes.setItems(ab);

		tabla_agentes.setItems(FXCollections.observableArrayList());
		mainApp.getMensaje().set("Acto de habla registrado!");
		mainApp.FillTree(mainApp.getRootcontroller().getArbol());
		tabla_actos.getSelectionModel().clearSelection();
		//tabla_actos.refresh();
                tabla_actos.setVisible(false);
                tabla_actos.setVisible(true);

	}//

	@FXML
	public void handleAgregar(){
		mainApp.setSomething_has_changed(true);
		tabla_agentes.getItems().add(agentes_participantes.getSelectionModel().getSelectedItem());
		agentes_participantes.getItems().remove(agentes_participantes.getSelectionModel().getSelectedItem());
		agentes_participantes.getSelectionModel().clearSelection();
		mainApp.getMensaje().set("Agente agregado");
	}

	@FXML
	public void HandleBack(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainApp);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Tareas";
			current.setText(Tabname);
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void ShowActoValues(ActoHabla acto){
		//igualar acto con ah

		if(acto==null)
			return;

		current=acto;
		nombre.setText(acto.getNombre().get());
		current.setNombre(acto.getNombre());

		modo_edicion.setVisible(true);
		tipo.getSelectionModel().select(acto.getTipo().get());
		current.setTipo(acto.getTipo());

		objetivo.setText(acto.getObjetivo().get());
		current.setObjetivo(acto.getObjetivo());

		current.setAgentes(acto.getAgentesAH());
		tabla_agentes.getItems().clear();
		tabla_agentes.setItems(FXCollections.observableArrayList(current.getAgentesAH()));
		//temp.removeAll(acto.getAgentes());
		agentes_participantes.setItems(FXCollections.observableArrayList(mainApp.getAgentData()));
		agentes_participantes.getItems().removeAll(acto.getAgentesAH());

		current.setDatosintercambiados(acto.getDatosintercambiados());
		datos_intercambiados.setText(acto.getDatosintercambiados().get());

		current.setPrecondicion(acto.getPrecondicion());
		precondicion.setText(acto.getPrecondicion().get());

		terminacion.setText(acto.getCTerminacion().get());
		current.setCTerminacion(acto.getCTerminacion());

		descripcion.setText(acto.getDescripcion().get());
		current.setDescripcion(acto.getDescripcion());

		guardar.setText("Guardar Cambios");

	}

	public agent getAgente() {
		return agente;
	}

	public void setAgente(agent agente) {
		this.agente = agente;
		titulo.setText(agente.getNameString()+" - Modelo de Comunicaciones");
		tabla_actos.setItems(agente.getModelo_comunicaciones().getActor());
	}


}

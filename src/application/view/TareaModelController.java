package application.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import application.Main;
import application.model.MarcoOntologicoIndividual;
import application.model.Parametros;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class TareaModelController {
	private Main mainApp;

	 @FXML
	 private TableView<Tarea> TareaTable;
	 @FXML
	 private TableColumn<Tarea, String> NameColumn;
	 @FXML
	 private TableColumn<Tarea, String> ServiceColumn;

	 @FXML
	 private TableView<Parametros> ParametroTable;
	 @FXML
	 private TableColumn<Parametros, String> ParametroNameColumn;
	 @FXML
	 private TableColumn<Parametros, String> ParametroDescriptionColumn;
	 @FXML
	 private TableColumn<Parametros, String> ParametrotipoColumn;
	 @FXML
	 private Label titulo;
	 @FXML
	 private TextField Nombre_tarea;
	 @FXML
	 private TextArea objetivo_tarea;
	 @FXML
	 private TextArea descripcion_tarea;
	 @FXML
	 private TextField nombre_parametro;
	 @FXML
	 private TextArea descripcion_parametro;
	 @FXML
	 private TextArea precondicion;
	 @FXML
	 private ComboBox<Servicio> Servicio;
	 @FXML
	 private ComboBox<String> comportamiento;
	 @FXML
	 private ComboBox<String> tipo_dato;
	 @FXML
	 private TextArea subtareas;
	 @FXML
	 private Label modo_edicion;
	 private agent agente;
	private Tarea CurrentTarea;
	@FXML
	private Button guardar;
	@FXML
	private GridPane panelx;
	private boolean band_selected;

	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	private ObservableList<Fila> filas=FXCollections.observableArrayList();

	public void seleccionarTarea(Tarea t){
		CurrentTarea=t;
		ShowTareaValues(t);
	}

	@FXML
    private void initialize() {
		CurrentTarea=new Tarea();
		ParametroNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		ParametrotipoColumn.setCellValueFactory(cellData -> cellData.getValue().getTipo());
		ParametroDescriptionColumn.setCellValueFactory(cellData -> cellData.getValue().getDescripcion());
		ParametroNameColumn.prefWidthProperty().bind(ParametroTable.widthProperty().divide(3));
		ParametrotipoColumn.prefWidthProperty().bind(ParametroTable.widthProperty().divide(3));
		ParametroDescriptionColumn.prefWidthProperty().bind(ParametroTable.widthProperty().divide(3));

		NameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		ServiceColumn.setCellValueFactory(cellData -> cellData.getValue().getAsociado().getNombre());
		NameColumn.prefWidthProperty().bind(TareaTable.widthProperty().divide(2));
		ServiceColumn.prefWidthProperty().bind(TareaTable.widthProperty().divide(2));

		Servicio.setPromptText("Seleccione un servicio");
		iniciar_tabla();
		contextoTablas();

		MenuItem copiar=new MenuItem("Copiar Modelo");
		copiar.setOnAction(copy_tareas());
		final ContextMenu contextMenu = new ContextMenu(copiar);
		    panelx.setOnMouseClicked(new EventHandler<MouseEvent>() {
		        @Override
		        public void handle(MouseEvent mouseEvent) {
		        	if(mouseEvent.getButton()==MouseButton.SECONDARY)
		        		contextMenu.show(
		        				panelx,
		        				mouseEvent.getScreenX(),
		        				mouseEvent.getScreenY()
		        				);
		        }
		    });


		TareaTable.getSelectionModel().selectedItemProperty().addListener(
	    		(observable, oldValue, newValue) -> ShowTareaValues(newValue));
		Servicio.setConverter(new StringConverter<Servicio>() {

			@Override
			public String toString(Servicio object) {
				return object.getNombre().get();
			}

			@Override
			public Servicio fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});

		Tooltip tool=new Tooltip("Puedes arrastrar las tareas para cambiar el orden");
		TareaTable.setTooltip(tool);
		TareaTable.setRowFactory(tv -> {
            TableRow<Tarea> row = new TableRow<>();

            row.setOnDragDetected(event -> {
                if (! row.isEmpty()) {
                    Integer index = row.getIndex();
                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(Main.SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });

            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(Main.SERIALIZED_MIME_TYPE)) {
                    if (row.getIndex() != ((Integer)db.getContent(Main.SERIALIZED_MIME_TYPE)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });

            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(Main.SERIALIZED_MIME_TYPE)) {
                    int draggedIndex = (Integer) db.getContent(Main.SERIALIZED_MIME_TYPE);
                    Tarea draggedPerson = TareaTable.getItems().remove(draggedIndex);
                    int dropIndex ;
                    if (row.isEmpty()) {
                        dropIndex = TareaTable.getItems().size() ;
                    } else {
                        dropIndex = row.getIndex();
                    }
                    TareaTable.getItems().add(dropIndex, draggedPerson);
                    event.setDropCompleted(true);
                    TareaTable.getSelectionModel().select(dropIndex);
                    event.consume();
                }
            });

            final ContextMenu contextMenuT = new ContextMenu();
            final MenuItem removeMenuItemT = new MenuItem("Eliminar");
            removeMenuItemT.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
            		row.getItem().getAsociado().getNodo().getChildren().remove(row.getItem().getNodo_arbol());
                	TareaTable.getItems().remove(row.getItem());
                	vaciar();
                }
            });
        	MenuItem copiarT=new MenuItem("Copiar Modelo");
    		copiarT.setOnAction(copy_tareas());
            contextMenuT.getItems().addAll(removeMenuItemT, copiarT);

         // Set context menu on row, but use a binding to make it only show for non-empty rows:
            row.contextMenuProperty().bind(
                    Bindings.when(row.emptyProperty())
                    .then((ContextMenu)null)
                    .otherwise(contextMenuT)
            );
            return row ;
        });

		Servicio.setCellFactory(new Callback<ListView<Servicio>,ListCell<Servicio>>(){
            public ListCell<Servicio> call(ListView<Servicio> l){
                return new ListCell<Servicio>(){
                	@Override

                    protected void updateItem(Servicio item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getNombre().get());
                        }
                    }
                } ;
            }
        });
	}

	private void contextoTablas(){
		ParametroTable.setRowFactory(new Callback<TableView<Parametros>, TableRow<Parametros>>() {
            public TableRow<Parametros> call(TableView<Parametros> tableView) {
                final TableRow<Parametros> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	ParametroTable.getItems().remove(row.getItem());
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                return row ;
            }
        });
/*
		TareaTable.setRowFactory(new Callback<TableView<Tarea>, TableRow<Tarea>>() {
            public TableRow<Tarea> call(TableView<Tarea> tableView) {
                final TableRow<Tarea> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	TareaTable.getItems().remove(row.getItem());
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
                System.out.println("Lo agrega");
             // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                return row ;
            }
        });*/
	}

	private EventHandler<ActionEvent> copy_tareas(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopy();
		}};
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	public Main getMainApp() {
		return mainApp;
	}
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
		tipo_dato.setItems(mainApp.getDatabase().getTipo_datos());
		ParametroTable.setItems(FXCollections.observableArrayList());
	    TareaTable.setItems(agente.getModelo_tareas().getTareas());
	    comportamiento.setItems(mainApp.getOptionalBehaviours());
	}

	@FXML
	private void handleCopy(){
		if(CurrentTarea.getNombre().get()==null){
			mainApp.getMensaje().set("Por favor, seleccione una tarea existente");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Tareas",""));
		filas.add(new Fila("Nombre",CurrentTarea.getNombre().get()));
		filas.add(new Fila("Objetivo",CurrentTarea.getObjetivo()));
		filas.add(new Fila("Descripci�n",CurrentTarea.getDescripcion()));
		filas.add(new Fila("Servicios asociados",CurrentTarea.getAsociado().getNombre().get()));

		String x="";
		int i=0;
		for (String s : CurrentTarea.getSubtareas()) {
			if(i>0)
				x+=",";
			i++;
			x+=s;
		}
		filas.add(new Fila("Subtareas",x));
		filas.add(new Fila("Ingredientes",""));
		for (Parametros p : CurrentTarea.getParametros()) {
			filas.add(new Fila(p.getNombre().get(),p.getDescripcion().get()));
		}

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Modelo de Tareas copiado en el portapapeles");
	}

	@FXML
	public void HandleNext(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/CommunicationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CommunicationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainApp);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Comunicaciones";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@FXML
	public void HandleBack(){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
			controller.setAgente(agente,false);
            controller.setMainApp(mainApp);
            //mainApp.getRootLayout().setCenter(page);
            //mainApp.getRootcontroller().getDerecho().setCenter(page);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Agentes";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void handleKeyPressed(KeyEvent ke){
		if(ke.getCode()==KeyCode.ESCAPE){
			modo_edicion.setVisible(false);
			Nombre_tarea.setText("");
			descripcion_tarea.setText("");
			objetivo_tarea.setText("");
			precondicion.setText("");
			nombre_parametro.setText("");
			descripcion_parametro.setText("");
			Servicio.getSelectionModel().clearSelection();
			subtareas.setText("");
			band_selected=false;
			CurrentTarea=new Tarea();
			CurrentTarea.getParametros().removeAll();
			CurrentTarea.setParametros(FXCollections.observableArrayList());
			ParametroTable.setItems(FXCollections.observableArrayList());
			TareaTable.getSelectionModel().clearSelection();
			vaciar();
			guardar.setText("Registrar Tarea");
		}
	}

	@FXML
	public void HandleCoordinacion(){

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainApp);
            //mainApp.getRootLayout().setCenter(page);
            //mainApp.getRootcontroller().getDerecho().setCenter(page);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Coordinacion";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void HandleInteligencia(){
		mainApp.showIntelligenceModel(getAgente(), new MarcoOntologicoIndividual());
	}

	public agent getAgente() {
		return agente;
	}

	public void ShowTareaValues(Tarea item){
		if(item==null){
			CurrentTarea=new Tarea();
			return;
		}
		band_selected=true;
		CurrentTarea=item;
		ParametroTable.setItems(FXCollections.observableArrayList(CurrentTarea.getParametros()));
		Nombre_tarea.setText(CurrentTarea.getNombre().get());
		objetivo_tarea.setText(CurrentTarea.getObjetivo());
		descripcion_tarea.setText(CurrentTarea.getDescripcion());
		precondicion.setText(CurrentTarea.getPrecondicion());
		CurrentTarea.setServicio_asociado(item.getServicio_asociado());
		subtareas.setText(item.getSubtareasS());
		Servicio.getSelectionModel().select(item.getAsociado());

		if(item.getComportamiento()!=null)
			comportamiento.getSelectionModel().select(item.getComportamiento().get());
		else
			comportamiento.getSelectionModel().clearSelection();

		modo_edicion.setVisible(true);
		guardar.setText("Guardar Cambios");
	}

	public void setAgente(agent agente) {
		this.agente = agente;
		titulo.setText(agente.getNameString()+" -Modelo de Tareas");
		Servicio.setItems(agente.getModelo_agentes().getServicios());
	}

	@FXML
	public void handle_AgregarParametro(){

		if(nombre_parametro.getText()==null || nombre_parametro.getText().equals("")){
			mainApp.getMensaje().set("Por favor indique un nombre de ingrediente v�lido");
			mainApp.MarcarError(nombre_parametro);
			return;
		}

		if(CurrentTarea.exist(nombre_parametro.getText())){
			mainApp.getMensaje().set("Por favor indique otro nombre");
			mainApp.MarcarError(nombre_parametro);
			return;
		}
		mainApp.MarcarCorrecto(nombre_parametro);

		if(tipo_dato.getSelectionModel().getSelectedItem()==null){
			mainApp.getMensaje().set("Por favor indique un tipo de dato v�lido");
			mainApp.MarcarError(tipo_dato);
			return;
		}
		mainApp.MarcarCorrecto(tipo_dato);

		Parametros param=new Parametros(nombre_parametro.getText(), descripcion_parametro.getText(),tipo_dato.getSelectionModel().getSelectedItem());
		ParametroTable.getItems().add(param);
		nombre_parametro.setText("");
		descripcion_parametro.setText("");
		tipo_dato.getSelectionModel().clearSelection();
		mainApp.getMensaje().set("Par�metro agregado: "+param.getNombre().get());
	}//fin de agregar parametro

	public boolean reescribirTarea(){
		String name=Nombre_tarea.getText();

		if(band_selected){
			if(name!=null){
					StringProperty myoldName=CurrentTarea.getNombre();
					CurrentTarea.setNombre(new SimpleStringProperty(name));

					int count=0;
					for (Tarea t : this.agente.getModelo_tareas().getTareas()) {
						if(t.getNombre().get().equals(CurrentTarea.getNombre().get())&&t.getAsociado()==Servicio.getSelectionModel().getSelectedItem()){
							count++;
						}
					}

					if(count>1){
						CurrentTarea.setNombre(myoldName);
						mainApp.MarcarError(Nombre_tarea);
						mainApp.getMensaje().set("No pueden existir tareas con el mismo nombre");
						return true;
					}
					CurrentTarea.setComportamiento(new SimpleStringProperty(comportamiento.getSelectionModel().getSelectedItem()));
					mainApp.MarcarCorrecto(Nombre_tarea);
					Nombre_tarea.setText("");


					CurrentTarea.setDescripcion(descripcion_tarea.getText());
					descripcion_tarea.setText("");

					CurrentTarea.setObjetivo(objetivo_tarea.getText());
					objetivo_tarea.setText("");

					CurrentTarea.setPrecondicion(precondicion.getText());
					precondicion.setText("");

					nombre_parametro.setText("");
					descripcion_parametro.setText("");
					CurrentTarea.setParametros(ParametroTable.getItems());
					CurrentTarea.setAsociado(Servicio.getSelectionModel().getSelectedItem());
					Servicio.getSelectionModel().clearSelection();

					String []subs=subtareas.getText().split(",");
					List<String> subsL=new ArrayList<String>();
					for (String tar : subs) {
						subsL.add(tar);
					}
					CurrentTarea.setSubtareas(subsL);
					subtareas.setText("");

					CurrentTarea=new Tarea();
					CurrentTarea.getParametros().removeAll();
					CurrentTarea.setParametros(FXCollections.observableArrayList());
					ParametroTable.setItems(FXCollections.observableArrayList());
					mainApp.getMensaje().set("Tarea Actualizada exitosamente");
					modo_edicion.setVisible(false);
					band_selected=false;
					//TareaTable.refresh();
                                         TareaTable.getColumns().get(0).setVisible(false);
                                        TareaTable.getColumns().get(0).setVisible(true);
					guardar.setText("Registrar Tarea");
					TareaTable.getSelectionModel().clearSelection();
					comportamiento.getSelectionModel().clearSelection();
					return true;
			}
		}

		return false;
	}

	public void vaciar(){
		Nombre_tarea.setText("");
		descripcion_tarea.setText("");
		objetivo_tarea.setText("");
		precondicion.setText("");
		nombre_parametro.setText("");
		descripcion_parametro.setText("");
		Servicio.getSelectionModel().clearSelection();
		subtareas.setText("");
		CurrentTarea=new Tarea();
		CurrentTarea.getParametros().removeAll();
		CurrentTarea.setParametros(FXCollections.observableArrayList());
		ParametroTable.setItems(CurrentTarea.getParametros());
		modo_edicion.setVisible(false);
		band_selected=false;
                
		//TareaTable.refresh();
                TareaTable.getColumns().get(0).setVisible(false);
                TareaTable.getColumns().get(0).setVisible(true);
		guardar.setText("Registrar Tarea");
		TareaTable.getSelectionModel().clearSelection();
		comportamiento.getSelectionModel().clearSelection();
	}

	//check is some field have some data and it's not saved
	public boolean fieldsNonEmpty(){
		if(!Nombre_tarea.getText().isEmpty())
			return true;
		if(!descripcion_tarea.getText().isEmpty())
			return true;
		if(!objetivo_tarea.getText().isEmpty())
			return true;
		if(!subtareas.getText().isEmpty())
			return true;
		if(ParametroTable.getItems().size()>0)
			return true;
		return false;
	}


	@FXML
	public void handle_AgregarTarea(){
		//si ya existe el mismo nombre->reescribir,de lo contrario gurdara
		if(reescribirTarea())
			return;
		boolean error=false;
		if(Nombre_tarea.getText()==null||Nombre_tarea.getText().equals("")&&!mainApp.isValidId(Nombre_tarea.getText())){
			mainApp.getMensaje().set("Por favor indique un nombre de tarea v�lido");
			mainApp.MarcarError(Nombre_tarea);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(Nombre_tarea);

		if(descripcion_tarea.getText()==null || descripcion_tarea.getText().equals("")){
			mainApp.MarcarError(descripcion_tarea);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(descripcion_tarea);

		if(objetivo_tarea.getText()==null || objetivo_tarea.getText().equals("")){
			mainApp.MarcarError(objetivo_tarea);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(objetivo_tarea);

		if(Servicio.getSelectionModel().getSelectedItem()==null){
			mainApp.MarcarError(Servicio);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(Servicio);


		String []subs=subtareas.getText().split(",");
		List<String> subsL=new ArrayList<String>();
		for (String tar : subs) {
			subsL.add(tar);
		}
		CurrentTarea.setSubtareas(subsL);
		band_selected=false;
		CurrentTarea.setParametros(ParametroTable.getItems());
		int count=0;

		for (Tarea t : agente.getModelo_tareas().getTareas()) {
			if(Nombre_tarea.getText().equals(t.getNombre().get())&&t.getAsociado()==Servicio.getSelectionModel().getSelectedItem()){
				count++;
			}
		}
		if(count>0){
			mainApp.MarcarError(Nombre_tarea);
			mainApp.getMensaje().set("Ya existe una tarea con el nombre proporcionado");
			return;
		}

		CurrentTarea.setNombre(new SimpleStringProperty(Nombre_tarea.getText()));
		CurrentTarea.setComportamiento(new SimpleStringProperty(comportamiento.getSelectionModel().getSelectedItem()));
		CurrentTarea.setDescripcion(descripcion_tarea.getText());
		CurrentTarea.setObjetivo(objetivo_tarea.getText());
		CurrentTarea.setPrecondicion(precondicion.getText());
		CurrentTarea.setAsociado(Servicio.getSelectionModel().getSelectedItem());

		nombre_parametro.setText("");
		descripcion_parametro.setText("");
		Nombre_tarea.setText("");
		subtareas.setText("");
		Servicio.getSelectionModel().clearSelection();
		objetivo_tarea.setText("");
		precondicion.setText("");
		descripcion_tarea.setText("");
		comportamiento.getSelectionModel().clearSelection();

		agente.getModelo_tareas().getTareas().add(CurrentTarea);
		CurrentTarea.getAsociado().getNodo().getChildren().add(CurrentTarea.getNodo_arbol());
		TareaTable.getSelectionModel().clearSelection();
		CurrentTarea=new Tarea();
		CurrentTarea.getParametros().removeAll();
		CurrentTarea.setParametros(FXCollections.observableArrayList());
		ParametroTable.setItems(CurrentTarea.getParametros());
		mainApp.getMensaje().set("Tarea registrada exitosamente");
		mainApp.FillTree(mainApp.getRootcontroller().getArbol());
		guardar.setText("Registrar Tarea");
	}

	public TableView<Tarea> getTareaTable() {
		return TareaTable;
	}

	public void setTareaTable(TableView<Tarea> tareaTable) {
		TareaTable = tareaTable;
	}

	public TableColumn<Tarea, String> getNameColumn() {
		return NameColumn;
	}

	public void setNameColumn(TableColumn<Tarea, String> nameColumn) {
		NameColumn = nameColumn;
	}

	public TableColumn<Tarea, String> getServiceColumn() {
		return ServiceColumn;
	}

	public void setServiceColumn(TableColumn<Tarea, String> serviceColumn) {
		ServiceColumn = serviceColumn;
	}

	public TableView<Parametros> getParametroTable() {
		return ParametroTable;
	}

	public void setParametroTable(TableView<Parametros> parametroTable) {
		ParametroTable = parametroTable;
	}

	public TableColumn<Parametros, String> getParametroNameColumn() {
		return ParametroNameColumn;
	}

	public void setParametroNameColumn(TableColumn<Parametros, String> parametroNameColumn) {
		ParametroNameColumn = parametroNameColumn;
	}

	public TableColumn<Parametros, String> getParametroDescriptionColumn() {
		return ParametroDescriptionColumn;
	}

	public void setParametroDescriptionColumn(TableColumn<Parametros, String> parametroDescriptionColumn) {
		ParametroDescriptionColumn = parametroDescriptionColumn;
	}

	public TableColumn<Parametros, String> getParametrotipoColumn() {
		return ParametrotipoColumn;
	}

	public void setParametrotipoColumn(TableColumn<Parametros, String> parametrotipoColumn) {
		ParametrotipoColumn = parametrotipoColumn;
	}

	public TextField getNombre_tarea() {
		return Nombre_tarea;
	}

	public void setNombre_tarea(TextField nombre_tarea) {
		Nombre_tarea = nombre_tarea;
	}

	public TextArea getObjetivo_tarea() {
		return objetivo_tarea;
	}

	public void setObjetivo_tarea(TextArea objetivo_tarea) {
		this.objetivo_tarea = objetivo_tarea;
	}

	public TextArea getDescripcion_tarea() {
		return descripcion_tarea;
	}

	public void setDescripcion_tarea(TextArea descripcion_tarea) {
		this.descripcion_tarea = descripcion_tarea;
	}

	public TextField getNombre_parametro() {
		return nombre_parametro;
	}

	public void setNombre_parametro(TextField nombre_parametro) {
		this.nombre_parametro = nombre_parametro;
	}

	public TextArea getDescripcion_parametro() {
		return descripcion_parametro;
	}

	public void setDescripcion_parametro(TextArea descripcion_parametro) {
		this.descripcion_parametro = descripcion_parametro;
	}

	public TextArea getPrecondicion() {
		return precondicion;
	}

	public void setPrecondicion(TextArea precondicion) {
		this.precondicion = precondicion;
	}

	public ComboBox<Servicio> getServicio() {
		return Servicio;
	}

	public void setServicio(ComboBox<Servicio> servicio) {
		Servicio = servicio;
	}

	public ComboBox<String> getTipo_dato() {
		return tipo_dato;
	}

	public void setTipo_dato(ComboBox<String> tipo_dato) {
		this.tipo_dato = tipo_dato;
	}

	public TextArea getSubtareas() {
		return subtareas;
	}

	public void setSubtareas(TextArea subtareas) {
		this.subtareas = subtareas;
	}

	public Tarea getCurrentTarea() {
		return CurrentTarea;
	}

	public void setCurrentTarea(Tarea currentTarea) {
		CurrentTarea = currentTarea;
	}

	public GridPane getPanelx() {
		return panelx;
	}

	public void setPanelx(GridPane panelx) {
		this.panelx = panelx;
	}

	public TableColumn<Fila, String> getClave() {
		return clave;
	}

	public void setClave(TableColumn<Fila, String> clave) {
		this.clave = clave;
	}

	public TableColumn<Fila, String> getValor() {
		return valor;
	}

	public void setValor(TableColumn<Fila, String> valor) {
		this.valor = valor;
	}

	public TableView<Fila> getTabla() {
		return tabla;
	}

	public void setTabla(TableView<Fila> tabla) {
		this.tabla = tabla;
	}

	public ObservableList<Fila> getFilas() {
		return filas;
	}

	public void setFilas(ObservableList<Fila> filas) {
		this.filas = filas;
	}

}

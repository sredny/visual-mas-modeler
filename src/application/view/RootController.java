package application.view;

import java.awt.Insets;
import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import com.sun.javafx.logging.Logger;

import application.Main;
import application.ProjectLoader;
import application.XMIParser;
import application.CodeGenerator.Generador;
import application.model.ActoHabla;
import application.model.Actor;
import application.model.Caracteristica;
import application.model.Caso_uso;
import application.model.MarcoOntologicoColectivo;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalC;
import application.model.OntologiaSituacionalI;
import application.model.Parametros;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;
import copy.Fila;
import copy.TableUtils;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

public class RootController {

	@FXML
	private MenuBar menu;
	private Main mainapp;
	private Stage dialogStage;
	private File selectedDirectory;
	@FXML
	private Label mensaje;
	private StringProperty mens;
	@FXML
	private MenuItem parametros;
	@FXML
	private MenuItem NuevoProyecto;
	@FXML
	private MenuItem NuevoAgente;
	@FXML
	private MenuItem ExportarXML;
	@FXML
	private MenuItem CargarProyecto;
	@FXML
	private MenuItem Guardar;
	@FXML
	private MenuItem Cerrar;
	@FXML
	private MenuItem AcercaDe;
	@FXML
	private ImageView nuevo;
	@FXML
	private ImageView guardar;
	@FXML
	private ImageView colectivo;
	@FXML
	private ImageView codigo;
	@FXML
	private ImageView abrir;
	@FXML
	private ImageView guardarComo;
	@FXML
	private BorderPane derecho;
	@FXML
	private TreeView<Object> arbol;
	@FXML
	private TabPane tabs;
	@FXML
	private Tab defaultTab;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	private TreeItem<Object> moc;
	private DirectoryChooser directoryChooser;


	@FXML
	private void initialize() {
		iniciar_tabla();


		NuevoAgente.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/imagenes/NuevoAgente-16.png"))));
		ExportarXML.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/imagenes/SaveAs-16.png"))));
		CargarProyecto.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/imagenes/OpenProject-16.png"))));
		NuevoAgente.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/imagenes/NuevoAgente-16.png"))));
		Guardar.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/imagenes/Save-16.png"))));
		parametros.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/imagenes/Config-16.png"))));
		AcercaDe.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/logo/Snowflake-16.png"))));
		directoryChooser = new DirectoryChooser();
		 Tooltip t = new Tooltip("Nuevo Agente");
	     Tooltip.install(nuevo, t);

	     t=new Tooltip("Guardar");
	     Tooltip.install(guardar, t);

	     t=new Tooltip("Modelo de Inteligencia Colectiva");
	     Tooltip.install(colectivo, t);

	     t=new Tooltip("Generar C�digo");
	     Tooltip.install(codigo, t);

	     t=new Tooltip("Abrir proyecto");
	     Tooltip.install(abrir, t);

	     t=new Tooltip("Guardar Como");
	     Tooltip.install(guardarComo, t);

	    Hyperlink hlink = new Hyperlink();
  		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
  		hlink.setGraphic(new ImageView(image));
  		hlink.setFocusTraversable(false);
 		defaultTab.setGraphic(hlink);

 		hlink.setOnAction(new EventHandler<ActionEvent>() {
 	        @Override
 	        public void handle(ActionEvent e) {
 	        	 tabs.getTabs().remove(defaultTab);
 	         }
 		});
	}

	public TreeItem<Object> getMoc() {
		return moc;
	}

	public void setMoc(TreeItem<Object> moc) {
		this.moc = moc;
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	public TabPane getTabs() {
		return tabs;
	}

	public void setTabs(TabPane tabs) {
		this.tabs = tabs;
	}

	public Tab getDefaultTab() {
		return defaultTab;
	}

	public void setDefaultTab(Tab defaultTab) {
		this.defaultTab = defaultTab;
	}

	public BorderPane getDerecho() {
		return derecho;
	}

	public void setDerecho(BorderPane derecho) {
		this.derecho = derecho;
	}

	public TreeView<Object> getArbol() {
		return arbol;
	}

	public void setArbol(TreeView<Object> arbol) {
		this.arbol = arbol;
	}

	public Main getMainapp() {
		return mainapp;
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;
		mensaje.textProperty().bind(mainapp.getMensaje());

		this.arbol();
	}

	@FXML
	private void handleNewAgent() throws IOException{
		mainapp.showAddAgentDialog();
	}

	@FXML
	private void handleClose(){
		Stage s=(Stage) menu.getScene().getWindow();
		s.close();
	}

	@FXML
	private void handleNuevoColectivo(){
		mainapp.NuevoColectivo();
	}

	@FXML
	public void handleAbout(){
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/About.fxml"));
		AnchorPane page;
		try {
			page = (AnchorPane) loader.load();
			Stage dialog = new Stage();
			dialog.setTitle("Acerca de");
			dialog.setMaxHeight(469);
			dialog.setMaxWidth(430);
			dialog.setMinHeight(469);
			dialog.setMinWidth(430);
			dialog.initModality(Modality.WINDOW_MODAL);
			dialog.initOwner(mainapp.getPrimaryStage());
			Scene scene = new Scene(page);
			dialog.setScene(scene);
			dialog.showAndWait();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void handleGenerarCodigo(){
		Generador.mainap=this.mainapp;
		// Create the custom dialog.
		Dialog<Pair<String, Boolean>> dialog = new Dialog<>();
		dialog.setTitle("Generar c�digo");
		dialog.setHeaderText("Por favor ingrese la informaci�n para proceder a la generaci�n de c�digo del proyecto");

		// Set the icon (must be included in the project).
		dialog.setGraphic(new ImageView(this.getClass().getResource("/imagenes/java.png").toString()));

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("Generar", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

		Label selected_file=new Label();
		Button openFile = new Button("Seleccione directorio");
		openFile.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {

                selectedDirectory = directoryChooser.showDialog(mainapp.getPrimaryStage());
                if(selectedDirectory == null){
                	selected_file.setText("No seleccion� directorio");
                }else{
                	Generador.setDir(selectedDirectory.getAbsolutePath());
               	 	mainapp.getMensaje().set(selectedDirectory.getAbsolutePath());
               	 	selected_file.setText(selectedDirectory.getAbsolutePath());
               	 	directoryChooser.setInitialDirectory(selectedDirectory);
                }
            }
        });

		ObservableList<String> options =
			    FXCollections.observableArrayList(
			        "sutituir con _",
			        "Suprimir espacios");
		ComboBox<String> comboBox = new ComboBox<String>(options);
		comboBox.getSelectionModel().select(0);
		CheckBox comments = new CheckBox("Agregar comentarios");
		comments.setSelected(true);

		grid.add(new Label("Seleccione la carpeta destino:"), 0, 0);
		grid.add(new Label("Como desea que sean tratados los espacios en blanco?"), 0, 1);
		grid.add(comboBox, 1, 1);
		grid.add(comments, 0, 3);
		grid.add(openFile, 1, 0);
		grid.add(selected_file, 0, 4);

		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(false);

		dialog.getDialogPane().setContent(grid);
		dialog.getDialogPane().setStyle("-fx-background-color:white");

		 //Request focus on the open file button
		Platform.runLater(() -> openFile.requestFocus());

		// Convert the result to a username-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
		    if (dialogButton == loginButtonType) {
		      return new Pair<>(comboBox.getSelectionModel().getSelectedItem(), comments.isSelected());
		    }
		    return null;
		});

		Optional<Pair<String, Boolean>> result = dialog.showAndWait();

		result.ifPresent(resultado -> {
		    if(Generador.getDir()!=null&&!Generador.getDir().isEmpty()){

		    	String caracter="";
		    	if(resultado.getKey().equals("sutituir con _")){
		    		caracter="_";
		    	}
		    	mainapp.setCaracter_sustitucion(caracter);
		    	mainapp.setAgregar_comentarios(resultado.getValue());
		    	mainapp.aceptar();

		    	//Alerta para informar generacion correcta del codigo
		    	Alert alert = new Alert(AlertType.INFORMATION);
		    	alert.setTitle("Informaci�n");
		    	alert.setHeaderText("C�digo generado correctamente");
		    	alert.setContentText("C�digo generado sin errores en el directorio:"+Generador.getDir());

		    	alert.showAndWait();
		    }
		    else{
		    	this.handleGenerarCodigo();
		    }
		});

	}

	@FXML
	private void GuardarComo() throws UnsupportedEncodingException{
		String content=mainapp.project_toXML();

		FileChooser fileChooser = new FileChooser();

		 if(mainapp.getDirectorio()!=null)
	        	fileChooser.setInitialDirectory(mainapp.getDirectorio().getParentFile());

		 //Set extension filter
	    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MSA files (*.msa)", "*.msa");
	    fileChooser.getExtensionFilters().add(extFilter);
	    //Show save file dialog
	   // File file=fileChooser.showSaveDialog(dialogStage);

	    File file=fileChooser.showSaveDialog(mainapp.getPrimaryStage());

	    if(file!=null){
	    	this.mainapp.setDirectorio(file);
	    	this.mainapp.getDatabase().saveLastURL(file.getParentFile().getAbsolutePath());
	    	mainapp.getPrimaryStage().setTitle(file.getName()+" -"+mainapp.getName_application());
	    	SaveFile(content, file);
		}

	}

	@FXML
	public void Guardar() throws FileNotFoundException, UnsupportedEncodingException{

		for (Tab tab : tabs.getTabs()) {
			if(mainapp.getAgents_controller().containsKey(tab.getText())){
				AgentModelController c=mainapp.getAgents_controller().get(tab.getText());
				c.SaveData();
			}
			if(mainapp.getUCS_controller().containsKey(tab.getText())){
				DetailsController c=mainapp.getUCS_controller().get(tab.getText());
				c.Guardar();
			}
		}

		if(mainapp.getAgentData().size()==0&&mainapp.getColectivo().size()==0&&mainapp.getInteligencia_colectiva().size()==0)
			return;

	String content=mainapp.project_toXML();

    FileChooser fileChooser = new FileChooser();
    //Set extension filter
    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MSA files (*.msa)", "*.msa");
    fileChooser.getExtensionFilters().add(extFilter);
    //Show save file dialog
    File file = null;


    if(mainapp.isUrl_fromDB()){
    	fileChooser.setInitialDirectory(mainapp.getDirectorio());
    	file=fileChooser.showSaveDialog(mainapp.getPrimaryStage());
    }
    else if(mainapp.getDirectorio()==null){
    	file=fileChooser.showSaveDialog(mainapp.getPrimaryStage());
    }
    else if(mainapp.getDirectorio()!=null){
     	file=mainapp.getDirectorio();
    }

    if(file!=null){
    	this.mainapp.setDirectorio(file);
    	this.mainapp.getDatabase().saveLastURL(file.getParentFile().getAbsolutePath());
	}

    	if(mainapp.getDirectorio()!=null){
    		mainapp.getPrimaryStage().setTitle(file.getName()+" -"+mainapp.getName_application());
    		SaveFile(content, this.mainapp.getDirectorio());
    	}
	}

	private void SaveFile(String content, File file) throws UnsupportedEncodingException{
		byte[] encoded = content.getBytes(StandardCharsets.UTF_8);
		mainapp.setSomething_has_changed(false);

		try {
            PrintWriter printWriter = new PrintWriter(file,"UTF-8");
            printWriter.println(content);
            printWriter.close();

           /* Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Operaci�n exitosa");
            alert.setHeaderText(null);
            alert.setContentText("Se ha guardado el proyecto correctamente!");
            alert.showAndWait();*/
            mainapp.getPrimaryStage().setTitle(file.getName()+" -"+mainapp.getName_application());
            mainapp.getMensaje().set("Se ha guardado el proyecto correctamente");

        } catch (IOException ex) {
        	Alert alert = new Alert(AlertType.ERROR);
        	alert.setTitle("Operacion fallida");
        	alert.setHeaderText("No se ha podido guardar correctamente el proyecto");
        	alert.setContentText("Por favor int�ntelo de nuevo,compruebe la extensi�n del archivo!");
        	alert.showAndWait();
        }
    }

	@FXML
	public void abrir_proyecto() throws SAXException, IOException, XPathExpressionException{
		 FileChooser fileChooser = new FileChooser();
	     FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
	                "MSA files (*.msa)", "*.msa");
	        fileChooser.getExtensionFilters().add(extFilter);

	        // Show save file dialog
	        if(mainapp.getDirectorio()!=null){
	        	if(mainapp.isUrl_fromDB()&&mainapp.getDirectorio().isDirectory()){
	        		fileChooser.setInitialDirectory(mainapp.getDirectorio());
	        		}
	        	else if(mainapp.getDirectorio().isFile())
	        		fileChooser.setInitialDirectory(mainapp.getDirectorio().getParentFile());
	        	else
	        		fileChooser.setInitialDirectory(mainapp.getDirectorio());
	        }


	       File file = fileChooser.showOpenDialog(mainapp.getPrimaryStage());

	        if (file != null) {
	       		this.mainapp.setDirectorio(file);
	        	ProjectLoader loader=new ProjectLoader(file);
	        	mainapp.getPrimaryStage().setTitle(file.getName()+" -"+mainapp.getName_application());
	        	tabs.getTabs().clear();
	        	this.mainapp.getDatabase().saveLastURL(file.getParentFile().getAbsolutePath());
	        	loader.read(this.mainapp);
	        	mainapp.setSomething_has_changed(false);
	        }

	}

	@FXML
	private void handle_NuevoProyecto() throws IOException{
		//preguntar si desea guardar,en tal caso dispara el evento
		//vaciar agentdata y modelo de inteligencia
		if(mainapp.getAgentData().size()>0){
			DesplegarAlerta();
		}
	}

	private void DesplegarAlerta() throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Nuevo Proyecto");
		alert.setHeaderText("�Desea guardar el proyecto actual?");
		alert.setContentText("Considere que si no guarda los cambios, el proyecto no podr� ser devuelto a su estado actual");

		ButtonType buttonTypeGuardar = new ButtonType("Guardar");
		ButtonType buttonTypeDismiss = new ButtonType("No Guardar");
		ButtonType buttonTypeCancel = new ButtonType("Cancelar", ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(buttonTypeGuardar, buttonTypeDismiss, buttonTypeCancel);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeGuardar){
			this.Guardar();
			clean();
			mainapp.getPrimaryStage().setTitle(mainapp.getName_application());
			mainapp.reiniciar_arbol();
		} else if (result.get() == buttonTypeDismiss) {
			clean();
			mainapp.reiniciar_arbol();
			mainapp.getPrimaryStage().setTitle(mainapp.getName_application());
		} else {
			//seleccion� cancelar
		}

	}

	public boolean DesplegarAlertaFinal() throws JAXBException, IOException {
		/*if(!mainapp.something_has_changed){
			return true;
		}*/

		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Guardar Proyecto");
		alert.setHeaderText("�Desea guardar el proyecto actual?");
		alert.setContentText("Considere que si no guarda los cambios, el proyecto no podr� ser devuelto a su estado actual");

		ButtonType buttonTypeGuardar = new ButtonType("Guardar");
		ButtonType buttonTypeDismiss = new ButtonType("No Guardar");
		ButtonType buttonTypeCancel = new ButtonType("Cancelar", ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(buttonTypeGuardar, buttonTypeDismiss, buttonTypeCancel);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeGuardar){
			this.Guardar();
			return true;
		} else if (result.get() == buttonTypeDismiss) {
			return true;//presion� no guardar
		} else {
			return false;
			//seleccion� cancelar
		}
	}

	private void clean() throws IOException{
		mainapp.getAgentData().clear();
		mainapp.getBelongs().clear();
		mainapp.getColectivo().clear();
		mainapp.getInteligencia_colectiva().clear();
		this.mainapp.setDirectorio(this.mainapp.getDirectorio().getParentFile());
		this.mainapp.setUrl_fromDB(true);
		//this.mainapp.setDirectorio(null);
		tabs.getTabs().clear();


		 TreeItem<Object> raiz = new TreeItem<Object>("Agentes");
		 TreeItem<Object> nodo_moc=new TreeItem<Object>("Modelo de inteligencia Colectivo");
	     TreeItem<Object> root= new TreeItem<Object>("root");
	     root.getChildren().add(raiz);
	     root.getChildren().add(nodo_moc);
	     mainapp.setRaiz(raiz);
	     mainapp.setNodo_moc(nodo_moc);
	     mainapp.getRootcontroller().getArbol().setRoot(root);
	     mainapp.getRootcontroller().getArbol().setShowRoot(false);
		 mainapp.FillTree(this.arbol);
		 mainapp.showAddAgentDialog();
	}

	@FXML
	private void handleConfiguracion(){
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Configuration.fxml"));
		AnchorPane page;
		try {
			page = (AnchorPane) loader.load();
			ConfigurationController controller = loader.getController();
			controller.setMainapp(mainapp);
			Stage dialog = new Stage();
			dialog.setTitle("Configuraci�n de Parametros");
			dialog.initModality(Modality.WINDOW_MODAL);
			dialog.initOwner(mainapp.getPrimaryStage());
			Scene scene = new Scene(page);
			dialog.setScene(scene);
			dialog.showAndWait();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public StringProperty getMens() {
		return mens;
	}
	public void setMens(StringProperty mens) {
		this.mens = mens;
	}

	public void arbol(){
		mainapp.FillTree(arbol);
		 mainapp.getAgentData().addListener(new ListChangeListener<agent>(){
				@Override
				public void onChanged(javafx.collections.ListChangeListener.Change<? extends agent> c) {
					mainapp.FillTree(arbol);
				}
	    	});

		for (agent agente : mainapp.getAgentData()) {
			agente.getMarco_ontologico_individual().addListener(new ListChangeListener<MarcoOntologicoIndividual>(){
				@Override
				public void onChanged(javafx.collections.ListChangeListener.Change<? extends MarcoOntologicoIndividual> c) {
					mainapp.FillTree(arbol);
				}
			});
		}

		//this.arbol.getRoot().getChildren().add(this.moc);
	}

	//------M�todos para copiar modelos en el portapapeles
	@FXML
	public void handleCopy(agent a){
		AgentModel am=a.getModelo_agentes();

		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Agentes",""));
		filas.add(new Fila("Agente",""));
		filas.add(new Fila("Nombre", a.getNameString()));
		filas.add(new Fila("Posici�n",am.getAgenteposicion()));
		filas.add(new Fila("Componentes",am.getComponentesString()));
		filas.add(new Fila("Marco de Referencia",am.getAgenteMarco_referencia()));
		filas.add(new Fila("Descripcion del Agente",am.getAgenteDescripcion()));
		filas.add(new Fila("Objetivos del Agente",""));
	/*	filas.add(new Fila("Nombre",am.getObjetivoNombre()));
		filas.add(new Fila("Descripcion",am.getObjetivoDescripcion()));
		filas.add(new Fila("Parametros de entrada",am.getObjetivoEntrada().toString()));
		filas.add(new Fila("Parametros de Salida",am.getObjetivoSalida().toString()));
		filas.add(new Fila("Condici�n de activaci�n",am.getObjetivoActivacion()));
		filas.add(new Fila("Condici�n de finalizaci�n",am.getObjetivoFinalizacion()));
		filas.add(new Fila("Condici�n de �xito",am.getObjetivoExito()));
		filas.add(new Fila("Condici�n de fracaso",am.getObjetivoFracaso()));
		filas.add(new Fila("Ontolog�a",am.getObjetivoOntologia()));*/

		filas.add(new Fila("Servicios del Agente",""));

		for (Servicio s : a.getModelo_agentes().getServicios()) {
			filas.add(new Fila("Nombre",s.getNombre().get()));
			filas.add(new Fila("Descripci�n",s.getDescripcion().get()));
			filas.add(new Fila("Tipo",s.getTipo().get()));

			String x="";
			/*for (StringProperty e : s.getServiciosEntrada()) {
				x+=","+e.get();
			}*/

			String y="";
			/*for (StringProperty e : s.getServiciosSalida()) {
				y+=","+e.get();
			}*/
			filas.add(new Fila("Par�metros de Entrada",x));
			filas.add(new Fila("Par�metros de Salida",y));
		}


		filas.add(new Fila("Propiedades del Servicio",""));
		filas.add(new Fila("Nombre",am.getPropiedadesNombre()));
		filas.add(new Fila("Calidad",am.getPropiedadesCalidad()));
		filas.add(new Fila("Auditable",am.getPropiedadesAuditable()));
		filas.add(new Fila("Garant�a",am.getPropiedadesGarantia()));
		filas.add(new Fila("Capacidad",am.getPropiedadesCapacidad()));
		filas.add(new Fila("Confiabilidad",am.getPropiedadesConfiabilidad()));
		filas.add(new Fila("Capacidad del Agente",""));
		filas.add(new Fila("Habilidades",am.getCapacidadHabilidades()));
		filas.add(new Fila("Representaci�n del Conocimiento",am.getCapacidadRepresentacion()));
		filas.add(new Fila("Lenguaje de Comunicaci�n",am.getCapacidadLenguaje()));
		filas.add(new Fila("Restricci�n del Agente",""));
		filas.add(new Fila("Normas",am.getRestriccionNormas()));
		filas.add(new Fila("Preferencias",am.getRestriccionPreferencias()));
		filas.add(new Fila("Permisos",am.getRestriccionPermisos()));
		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Modelo de Agentes copiado en el portapapeles");
	}


	public void handleCopyTareaModel(Tarea tm){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Tarea",""));
		filas.add(new Fila("Nombre",tm.getNombre().get()));
		filas.add(new Fila("Objetivo",tm.getObjetivo()));
		filas.add(new Fila("Descripci�n",tm.getDescripcion()));
		filas.add(new Fila("Servicio asociado",tm.getAsociado().getNombre().get()));

		String x="";
		int i=0;
		for (String s : tm.getSubtareas()) {

			if(i>0)
				x+=",";
			x+=s;
			i++;
		}
		filas.add(new Fila("Subtareas",x));
		filas.add(new Fila("Ingredientes",""));
		for (Parametros p : tm.getParametros()) {
			filas.add(new Fila(p.getNombre().get(),p.getDescripcion().get()));
		}

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Tarea copiada en el portapapeles");
	}

	public void handlecopy_ActoHabla(ActoHabla ah){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Comunicaciones",""));
		filas.add(new Fila("Acto del Habla", ah.getNombre().get()));
		filas.add(new Fila("Tipo",ah.getTipo().get()));
		filas.add(new Fila("Objetivo",ah.getObjetivo().get()));

		String x="";
		int i=0;
		for (agent a : ah.getAgentesAH()) {
			x+=a.getNameString();
			if(i<ah.getAgentesAH().size())
				x+=",";
		}
		filas.add(new Fila("Agentes Participantes",x));
		filas.add(new Fila("Datos intercambiados",ah.getDatosintercambiados().get()));
		filas.add(new Fila("Precondicion",ah.getPrecondicion().get()));
		filas.add(new Fila("Condici�n de Terminaci�n",ah.getCTerminacion().get()));
		filas.add(new Fila("Descripci�n",ah.getDescripcion().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Modelo de Comunicaciones copiado en el portapapeles");

	}

	public void handleCopyCoordinationModelConversacion(conversacion current){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Conversaci�n",""));
		filas.add(new Fila("Conversacion",current.getNombre().get()));
		filas.add(new Fila("Objetivo",current.getObjetivo().get()));

		int i=0;
		String x="";
		for (agent a : current.getParticipantes()) {
			x+=a.getNameString();
			i++;
			if(i<current.getParticipantes().size())
				x+=",";
		}

		filas.add(new Fila("Agentes Participantes",x));
		x="";
		i=0;
		for (ActoHabla a : current.getHablas()) {
			x+=a.getNombre().get();
			i++;
			if(i<current.getHablas().size())
				x+=",";
		}
		filas.add(new Fila("Actos del Habla",x));
		filas.add(new Fila("Precondicion",current.getPrecondicion().get()));
		filas.add(new Fila("Condici�n de Terminaci�n",current.getTerminacion().get()));
		filas.add(new Fila("Descripci�n",current.getDescripcion().get()));
		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Modelo de Coordinaci�n copiado en el portapapeles");
	}

	public void handleCopyUseCase(Caso_uso uc){
		agent padre=mainapp.getParentOf(uc);

		if(padre==null)
			return;

		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Agente",padre.getName().get()));
		filas.add(new Fila("Caso de uso",""));
		filas.add(new Fila("Nombre",uc.getNombre()));
		filas.add(new Fila("Descripcion",uc.getDescripcion()));
		filas.add(new Fila("Precondicion",uc.getPrecondicion()));
		filas.add(new Fila("Condici�n de fracaso",uc.getCondicionFracaso()));
		filas.add(new Fila("Condicion de �xito",uc.getCondicionExito()));

		String actores="";
		for(Actor a: padre.UC_actors(uc.getCodigo())){
			actores+="-"+a.getName().get()+"\n";
		}
		filas.add(new Fila("Actores",actores));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Caso de uso copiado en el portapapeles");
	}

	public void handleCopyRelacion(Servicio ser,agent agente){

		String tasks="";
		int n=1;
		for(Tarea t:agente.getModelo_tareas().getTareas()){
			if(t.getAsociado().getNombre().get().equals(ser.getNombre().get())){
				tasks+=n+".-"+t.getNombre().get()+"\n";
				n++;
			}
		}

		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Relaci�n Servicio-Tarea",""));
		filas.add(new Fila("Servicio","Tareas"));
		filas.add(new Fila(ser.getNombre().get(),tasks));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Relacion servicio-tarea copiada en el portapapeles");
	}

	public void handleCopyServicio(Servicio ser){

		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Servicio del agente",""));
		filas.add(new Fila("Nombre",ser.getNombre().get()));
		filas.add(new Fila("Descripci�n",ser.getDescripcion().get()));
		filas.add(new Fila("Tipo",ser.getTipo().get()));

		String x="";
		int i=0;
		for (Parametros e : ser.getServicioPentrada()) {
			if(i!=0)
				x+=","+e.getNombre().get();
			else
				x+=e.getNombre().get();
			i++;
		}

		String y="";
		i=0;
		for (Parametros e : ser.getServicioPsalida()) {
			if(i!=0)
				y+=","+e.getNombre().get();
			else
				y+=e.getNombre().get();
			i++;
		}
		filas.add(new Fila("Par�metros de Entrada",x));
		filas.add(new Fila("Par�metros de Salida",y));
		filas.add(new Fila("Propiedades del servicio",""));
		filas.add(new Fila("Nombre","Descripci�nP"));
		for (Caracteristica p : ser.getPropiedades()) {
			filas.add(new Fila(p.getNombre().get(),p.getValor_omision().get()+"pSeparator"+p.getDescripcion().get()));
		}

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Servicio copiado en el portapapeles");
	}

	public void handleCopyMecanismoAprendizaje(MecanismoAprendizaje item) {
		if(item==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a v�lida");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Mecanismo de Aprendizaje",""));

		filas.add(new Fila("Nombre",item.getNombre().get()));
		filas.add(new Fila("Tipo",item.getTipo().get()));
		filas.add(new Fila("T�cnica de Representaci�n",item.getTecnica_representacion().get()));
		filas.add(new Fila("Fuente de Aprendizaje",item.getFuente_aprendizaje().get()));
		filas.add(new Fila("Nombre",item.getMecanismo().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Mecanismo de Aprendizaje copiado en el portapapeles");
	}

	public void handleCopyMecanismoRazonamiento(MecanismoRazonamiento item) {
		if(item==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a v�lida");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Mecanismo de Razonamiento",""));

		filas.add(new Fila("Fuente de informaci�n",item.getFuente_informacion().get()));
		filas.add(new Fila("Fuente de alimentaci�n",item.getFuente_alimentacion().get()));
		filas.add(new Fila("T�cnica de inferencia",item.getTecnica_inferencia().get()));
		filas.add(new Fila("Lenguaje de representaci�n del conocimiento",item.getLenguaje_representacion().get()));
		filas.add(new Fila("Relaci�n tarea-inferencia",item.getRelacion().get()));
		filas.add(new Fila("Estrategias de razonamiento",item.getEstrategia_razonamiento().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Mecanismo de Razonamiento copiado en el portapapeles");
	}

	public void handleCopyConocimientoEstrategico(OntologiaSituacionalI item) {
		if(item==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a v�lida");
			return;
		}
		if(item.getEstrategico().getValor_conocimiento()==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a situacional v�lida");
			return;
		}
		filas=FXCollections.observableArrayList();
		String caracterizacion="";
		String omision="";
		for (Caracteristica c : item.getEstrategico().getCaracterizacion()) {
			omision+=c.getNombre().get()+" valor:"+c.getValor_omision().get()+"\n";
			caracterizacion+=c.getNombre().get()+" Tipo:"+c.getTipo().get()+"\n";
		}


		filas.add(new Fila("Conocimiento Estr�tegico",""));

		filas.add(new Fila("Valor del conocimiento",item.getEstrategico().getValor_conocimiento().get()));
		filas.add(new Fila("Agente o ambiente que lo produce",item.getEstrategico().getAgente_generador().getNameString()));
		filas.add(new Fila("Proceso que lo gener�",item.getEstrategico().getProceso_generador().get()));
		filas.add(new Fila("Grado de confiabilidad",item.getEstrategico().getConfiabilidad().get()));
		filas.add(new Fila("Caracterizaci�n",caracterizacion));
		filas.add(new Fila("Valores por omisi�n",omision));
		filas.add(new Fila("Valores max y min","Valor m�ximo:"+item.getEstrategico().getVmaximo().get()+"\n Valor m�nimo:"+item.getEstrategico().getVminimo().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Conocimiento estrat�gico copiado en el portapapeles");
	}

	public void handleCopyConocimientoTareas(OntologiaSituacionalI item) {
		if(item==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a v�lida");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Conocimiento de Tareas",""));

		filas.add(new Fila("Nombre de la tarea",item.getTareas().getNombre().get()));
		filas.add(new Fila("Agente que la realiza",item.getTareas().getAgente().getName().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Conocimiento de Tareas copiado en el portapapeles");

	}

	public void handleCopyOntologiaHistorica(OntologiaHistorica item) {
		if(item==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a v�lida");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Ontolog�a Hist�rica",""));

		String caracterizacion="";
		String omision="";
		for (Caracteristica c : item.getCaracterizacion()) {
			omision+=c.getNombre().get()+" valor:"+c.getValor_omision().get()+"\n";
			caracterizacion+=c.getNombre().get()+"\n";
		}

		filas.add(new Fila("Descripci�n",item.getDescripcion().get()));
		filas.add(new Fila("Caracterizaci�n",caracterizacion));
		filas.add(new Fila("Fuente",item.getFuente().get()));
		filas.add(new Fila("Valores por omision",omision));
		filas.add(new Fila("Valor maximo",item.getVmaximo().get()));
		filas.add(new Fila("Valor minimo",item.getVminimo().get()));

		filas.add(new Fila("Momento (tiempo)",item.getMomento().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Ontolog�a hist�rica copiada en el portapapeles");

	}

	public void handleCopyOntologiaDominio(OntologiaDominio item) {
		if(item==null){
			mainapp.getMensaje().set("Seleccione una ontolog�a v�lida");
			return;
		}
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Ontolog�a de Dominio",""));

		String caracterizacion="";
		String omision="";
		for (Caracteristica c : item.getCaracterizacion()) {
			omision+=c.getNombre().get()+" valor:"+c.getValor_omision().get()+"\n";
			caracterizacion+=c.getNombre().get()+"\n";
		}

		filas.add(new Fila("Descripci�n",item.getDescripcion().get()));
		filas.add(new Fila("Caracterizaci�n",caracterizacion));
		filas.add(new Fila("Fuente",item.getFuente().get()));
		filas.add(new Fila("Valores por omision",omision));
		filas.add(new Fila("Valor maximo",item.getVmaximo().get()));
		filas.add(new Fila("Valor minimo",item.getVminimo().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Ontolog�a de Dominio copiada en el portapapeles");
	}

	public void handleCopyMarcoOntologicoI(MarcoOntologicoIndividual item) {
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Marco Ontol�gico del individuo",""));

		String ontologias="";

		for (OntologiaDominio c : item.getDominio()) {
			ontologias+="Ontolog�a de Dominio\n";
		}

		for (OntologiaHistorica c : item.getHistorica()) {
			ontologias+="Ontolog�a Hist�rica: "+item.getNombre().get()+" \n";
		}

		for (OntologiaSituacionalI c : item.getSituacional()) {
			ontologias+="Ontolog�a Situacional Individual\n";
		}

		filas.add(new Fila("Nombre",item.getNombre().get()));
		filas.add(new Fila("Descripci�n",item.getDescripcion().get()));
		filas.add(new Fila("Ontolog�as que la integran",ontologias));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Marco Ontol�gico Individual copiado en el portapapeles");

	}

	public void handleCopyOntologiaSituacionalC(OntologiaSituacionalC item) {
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Ontolog�a Situacional Colectiva",""));

		String ontologias="";

		String caracterizacion="";
		String omision="";
		for (Caracteristica c : item.getCaracterizacion()) {
			omision+=c.getNombre().get()+" valor:"+c.getValor_omision().get()+"\n";
			caracterizacion+=c.getNombre().get()+"\n";
		}


		filas.add(new Fila("Descripci�n",item.getDescripcion().get()));
		filas.add(new Fila("Caracterizaci�n",caracterizacion));
		filas.add(new Fila("Fuente",item.getFuente().get()));
		filas.add(new Fila("Grado de confiabilidad",item.getConfiabilidad().get()));
		filas.add(new Fila("Valores por omisi�n",omision));
		filas.add(new Fila("Valor max",item.getValor_maximo().get()));
		filas.add(new Fila("Valor min",item.getValor_minimo().get()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Ontolog�a Situacional copiada en el portapapeles");
	}

	public void handleCopyMarcoOntologicoC(MarcoOntologicoColectivo item) {
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Marco Ontol�gico Colectivo",""));


		String agentes="";
		for (agent c : item.getAgentes()) {
			agentes+=c.getNameString()+"\n";
		}

		String objetivos="";
		for (String c : item.getObjetivos()) {
			objetivos+=c+"\n";
		}

		filas.add(new Fila("Nombre",item.getNombre().get()));
		filas.add(new Fila("Objetivos colectivos para los que se usa",objetivos));
		filas.add(new Fila("Descripci�n",item.getDescripcion().get()));
		filas.add(new Fila("Agentes que la conocen",agentes));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainapp.getMensaje().set("Marco Ontol�gico Individual copiado en el portapapeles");
	}
}

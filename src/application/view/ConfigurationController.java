package application.view;

import application.Main;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ConfigurationController {

	private Main mainapp;
	@FXML
	private TextField servicio_desc;
	@FXML
	private ListView<String> servicio_tipo;

	@FXML
	private TextField LenguajeConocimiento_desc;
	@FXML
	private ListView<String> LenguajeConocimiento_list;

	@FXML
	private TextField actos_desc;
	@FXML
	private ListView<String> actosHabla_tipos;

	@FXML
	private ListView<String> CaracterizacionTabla;
	@FXML
	private TextField Caracterizacion_desc;

	@FXML
	private ListView<String> aprendizaje_tecnicas_lista;
	@FXML
	private ListView<String> aprendizaje_mecanismos_lista;
	@FXML
	private ListView<String> aprendizaje_Tipos_lista;
	@FXML
	private TextField aprendizaje_tipo;
	@FXML
	private TextField aprendizaje_tecnica;
	@FXML
	private TextField aprendizaje_mecanismo;

	@FXML
	private ListView<String> razonamiento_tecnicas_lista;
	@FXML
	private ListView<String> razonamiento_lenguajes_lista;
	@FXML
	private ListView<String> razonamiento_estrategias_lista;
	@FXML
	private ListView<String> lista_tipoDatos;
	@FXML
	private TextField razonamiento_tecnica;
	@FXML
	private TextField razonamiento_lenguaje;
	@FXML
	private TextField razonamiento_estrategia;
	@FXML
	private TextField tipo_dato;
	@FXML
	private Label mensaje;

	private boolean Lenguaje_conocimiento_b,servicio_b,actos_b,caracterizacion_b,a_meca_b, a_tec_b, a_tipo_b,r_e_b,r_l_b,r_t_b,tipo_datos_b;
	private String lenguajeConocimiento_o,servicio_o,actos_o,caracterizacion_o,a_meca_o,a_tec_o,a_tipo_o,r_e_o,r_l_o,r_t_o,tipo_datos_o;

	@FXML
	public void initialize(){
		servicio_tipo.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> ShowServicioValues(newValue));
		actosHabla_tipos.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue) ->ShowActoValues(newValue));
		CaracterizacionTabla.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowCaracterizacionValues(newValue));
		aprendizaje_mecanismos_lista.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowAprendizajeMecanismoValue(newValue));
		aprendizaje_tecnicas_lista.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowAprendizajeTecnicaValues(newValue));
		aprendizaje_Tipos_lista.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowAprendizajeTipoValues(newValue));
		razonamiento_estrategias_lista.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->showRazonamientoEstrategiaValues(newValue));
		razonamiento_lenguajes_lista.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowRazonamientoLenguajeValues(newValue));
		razonamiento_tecnicas_lista.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowRazonamientoTecnicaValues(newValue));
		LenguajeConocimiento_list.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowLenguajeConocimientoValues(newValue));
		lista_tipoDatos.getSelectionModel().selectedItemProperty().addListener((observable,oldValue,newValue)->ShowTipoDatoValues(newValue));


		servicio_b=false;
		actos_b=false;
		caracterizacion_b=false;
		a_meca_b=false;
		a_tec_b=false;
		a_tipo_b=false;
		r_e_b=false;
		r_l_b=false;
		r_t_b=false;
		tipo_datos_b=false;
		Lenguaje_conocimiento_b=false;
	}

	private void ShowTipoDatoValues(String newValue) {
		tipo_dato.setText(newValue);
		tipo_datos_b=true;
		tipo_datos_o=newValue;
		mensaje.setText("");
	}

	private void ShowLenguajeConocimientoValues(String newValue) {
		LenguajeConocimiento_desc.setText(newValue);
		Lenguaje_conocimiento_b=true;
		lenguajeConocimiento_o=newValue;
		mensaje.setText("");
	}

	private void ShowRazonamientoTecnicaValues(String newValue) {
		razonamiento_tecnica.setText(newValue);
		r_t_b=true;
		r_t_o=newValue;
		mensaje.setText("");
	}

	private void ShowRazonamientoLenguajeValues(String newValue) {
		razonamiento_lenguaje.setText(newValue);
		r_l_b=true;
		r_l_o=newValue;
		mensaje.setText("");
	}

	private void showRazonamientoEstrategiaValues(String newValue) {
		razonamiento_estrategia.setText(newValue);
		r_e_b=true;
		r_e_o=newValue;
		mensaje.setText("");
	}

	private void ShowAprendizajeTipoValues(String newValue) {
		aprendizaje_tipo.setText(newValue);
		a_tipo_b=true;
		a_tipo_o=newValue;
		mensaje.setText("");
	}

	private void ShowAprendizajeTecnicaValues(String newValue) {
		aprendizaje_tecnica.setText(newValue);
		a_tec_b=true;
		a_tec_o=newValue;
		mensaje.setText("");
	}

	private void ShowAprendizajeMecanismoValue(String newValue) {
		aprendizaje_mecanismo.setText(newValue);
		a_meca_b=true;
		a_meca_o=newValue;
		mensaje.setText("");
	}

	private void ShowCaracterizacionValues(String newValue) {
		Caracterizacion_desc.setText(newValue);
		caracterizacion_b=true;
		caracterizacion_o=newValue;
		mensaje.setText("");
	}

	private void ShowActoValues(String newValue) {
		actos_desc.setText(newValue);
		actos_b=true;
		actos_o=newValue;
		mensaje.setText("");
	}

	private void ShowServicioValues(String newValue) {
		servicio_desc.setText(newValue);
		servicio_b=true;
		servicio_o=newValue;
		mensaje.setText("");
	}


	@FXML
	public void handleAddServicio(){
		if(servicio_desc.getText()!=null && !servicio_desc.getText().equals("")&&mainapp.isValidId(servicio_desc.getText())){
			if(servicio_b==true){
				mainapp.getDatabase().UpdateServicio(servicio_desc.getText(), servicio_o);
				servicio_b=false;
				servicio_desc.setText("");
				servicio_tipo.getSelectionModel().clearSelection();
				mensaje.setText("Servicio actualizado");
				mainapp.MarcarCorrecto(servicio_desc);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveServicio(servicio_desc.getText());
			mensaje.setText("Servicio registrado");
			servicio_desc.setText("");
			mainapp.MarcarCorrecto(servicio_desc);
			return;
		}//fin del if
		mainapp.MarcarError(servicio_desc);
		mensaje.setText("Ingrese una descripci�n v�lida para el servicio");
	}

	@FXML
	public void handleAddTipoDato(){
		if(tipo_dato.getText()!=null && !tipo_dato.equals("")&&mainapp.isValidId(tipo_dato.getText())){
			if(tipo_datos_b==true){
				mainapp.getDatabase().UpdateTipo(tipo_dato.getText(), tipo_datos_o);
				tipo_datos_b=false;
				tipo_dato.setText("");
				mensaje.setText("Tipo de dato actualizado");
				lista_tipoDatos.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(tipo_dato);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveTipos(tipo_dato.getText());
			mensaje.setText("Tipo de dato registrado");
			tipo_dato.setText("");
			mainapp.MarcarCorrecto(tipo_dato);
			return;
		}//fin del if
		mainapp.MarcarError(tipo_dato);
		mensaje.setText("Ingrese un Tipo de dato v�lido");
	}

	@FXML
	public void handleAddActo(){
		if(actos_desc.getText()!=null && !actos_desc.getText().equals("")&&mainapp.isValidId(actos_desc.getText())){
			if(actos_b==true){
				mainapp.getDatabase().UpdateActosHabla(actos_desc.getText(), actos_o);
				actos_b=false;
				actos_desc.setText("");
				actosHabla_tipos.getSelectionModel().clearSelection();
				mensaje.setText("Tipo de acto del habla actualizado");
				mainapp.MarcarCorrecto(actos_desc);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveActoHabla(actos_desc.getText());
			mensaje.setText("Tipo de acto del habla registrado");
			actos_desc.setText("");
			mainapp.MarcarCorrecto(actos_desc);
			return;
		}
		mainapp.MarcarError(actos_desc);
		mensaje.setText("Ingrese un Tipo de acto del habla v�lido");
	}

	@FXML
	public void handleAddCaracterizacion(){
		if(Caracterizacion_desc.getText()!=null && !Caracterizacion_desc.getText().equals("")&&mainapp.isValidId(Caracterizacion_desc.getText())){
			if(caracterizacion_b==true){
				mainapp.getDatabase().UpdateCaracterizacion(Caracterizacion_desc.getText(), caracterizacion_o);
				caracterizacion_b=false;
				Caracterizacion_desc.setText("");
				mensaje.setText("Tipo de caracterizaci�n actualizada");
				CaracterizacionTabla.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(Caracterizacion_desc);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveCaracterizacion(Caracterizacion_desc.getText());
			Caracterizacion_desc.setText("");
			mainapp.MarcarCorrecto(Caracterizacion_desc);
			mensaje.setText("Tipo de caracterizaci�n registrada");
			return;
		}
		mainapp.MarcarError(Caracterizacion_desc);
		mensaje.setText("Ingrese un tipo de caracterizaci�n v�lido");
	}

	@FXML
	public void handleAddLenguajeConocimiento(){
		if(LenguajeConocimiento_desc.getText()!=null && !LenguajeConocimiento_desc.getText().equals("")&&mainapp.isValidId(LenguajeConocimiento_desc.getText())){
			if(Lenguaje_conocimiento_b==true){
				mainapp.getDatabase().UpdateLenguajeComunicacion(LenguajeConocimiento_desc.getText(), lenguajeConocimiento_o);
				Lenguaje_conocimiento_b=false;
				LenguajeConocimiento_desc.setText("");
				//LenguajeConocimiento_list.refresh();
                                LenguajeConocimiento_list.setVisible(false);
                                LenguajeConocimiento_list.setVisible(true);
				mensaje.setText("Tipo de lenguaje de conocimiento actualizado");
				LenguajeConocimiento_list.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(LenguajeConocimiento_desc);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveLenguajeComunicacion(LenguajeConocimiento_desc.getText());
			//LenguajeConocimiento_list.refresh();
                        LenguajeConocimiento_list.setVisible(false);
                        LenguajeConocimiento_list.setVisible(true);
			mensaje.setText("Tipo de lenguaje de conocimiento registrado");
			LenguajeConocimiento_desc.setText("");
			mainapp.MarcarCorrecto(LenguajeConocimiento_desc);
			return;
		}
		mainapp.MarcarError(LenguajeConocimiento_desc);
		mensaje.setText("ingrese un tipo de lenguaje de conocimiento v�lido");
	}

	@FXML
	public void handleAddAprendizaje_tipo(){
		if(aprendizaje_tipo.getText()!=null && !aprendizaje_tipo.getText().equals("")&&mainapp.isValidId(aprendizaje_tipo.getText())){
			if(a_tipo_b==true){
				mainapp.getDatabase().UpdateAprendizajeTipo(aprendizaje_tipo.getText(), a_tipo_o);
				a_tipo_b=false;
				aprendizaje_tipo.setText("");
				mensaje.setText("Tipo de aprendizaje actualizado");
				aprendizaje_Tipos_lista.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(aprendizaje_tipo);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveAprendizaje_tipo(aprendizaje_tipo.getText());
			mainapp.MarcarCorrecto(aprendizaje_tipo);
			mensaje.setText("Tipo de aprendizaje registrado");
			aprendizaje_tipo.setText("");
			mainapp.MarcarCorrecto(aprendizaje_tipo);
			return;
		}
		mensaje.setText("Indique un tipo de aprendizaje valido");
		mainapp.MarcarError(aprendizaje_tipo);
	}

	@FXML
	public void handleAddAprendizaje_tecnica(){
		if(aprendizaje_tecnica.getText()!=null && !aprendizaje_tecnica.equals("")&&mainapp.isValidId(aprendizaje_tecnica.getText())){
			if(a_tec_b==true){
				mainapp.getDatabase().UpdateAprendizajeTec(aprendizaje_tecnica.getText(), a_tec_o);
				a_tec_b=false;
				aprendizaje_tecnica.setText("");
				mensaje.setText("Tecnica de aprendizaje actualizada");
				aprendizaje_tecnicas_lista.getSelectionModel().clearSelection();
				return;
			}//if reemplazo
			mainapp.getDatabase().saveAprendizaje_tec(aprendizaje_tecnica.getText());
			aprendizaje_tecnica.setText("");
			mensaje.setText("Tecnica de aprendizaje registrada");
			return;
		}
		mainapp.MarcarError(aprendizaje_tecnica);
		mensaje.setText("indique una tecnica de aprendizaje valida");
	}

	@FXML
	public void handleAddAprendizaje_mecanismo(){
		if(aprendizaje_mecanismo.getText()!=null && !aprendizaje_mecanismo.getText().equals("")&&mainapp.isValidId(aprendizaje_mecanismo.getText())){
			if(a_meca_b==true){
				mainapp.getDatabase().UpdateAprendizajeMec(aprendizaje_mecanismo.getText(), a_meca_o);
				a_meca_b=false;
				aprendizaje_mecanismo.setText("");
				mensaje.setText("Tipo de mecanismo de aprendizaje actualizado");
				aprendizaje_mecanismos_lista.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(aprendizaje_mecanismo);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveAprendizaje_mec(aprendizaje_mecanismo.getText());
			aprendizaje_mecanismo.setText("");
			mensaje.setText("Tipo de mecanismo de aprendizaje registrado");
			mainapp.MarcarCorrecto(aprendizaje_mecanismo);
			return;
		}
		mainapp.MarcarError(aprendizaje_mecanismo);
		mensaje.setText("Ingrese un tipo de mecanismo de aprendizaje v�lido");
	}

	@FXML
	public void handleAddRazonamiento_lenguaje(){
		if(razonamiento_lenguaje.getText()!=null && !razonamiento_lenguaje.getText().equals("")&&mainapp.isValidId(razonamiento_lenguaje.getText())){
			if(r_l_b==true){
				mainapp.getDatabase().UpdateRazonamiento_lenguaje(razonamiento_lenguaje.getText(), r_l_o);
				r_l_b=false;
				razonamiento_lenguaje.setText("");
				razonamiento_lenguajes_lista.getSelectionModel().clearSelection();
				mensaje.setText("Tipo de lenguaje de razonamiento actualizado");
				mainapp.MarcarCorrecto(razonamiento_lenguaje);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveRazonamiento_lenguaje(razonamiento_lenguaje.getText());
			mensaje.setText("Tipo de lenguaje de razonamiento registrado");
			razonamiento_lenguaje.setText("");
			mainapp.MarcarCorrecto(razonamiento_lenguaje);
			return;
		}
		mensaje.setText("Ingrese un tipo de lenguaje de razonamiento v�lido");
		mainapp.MarcarError(razonamiento_lenguaje);
	}

	@FXML
	public void handleAddRazonamiento_tecnica(){
		if(razonamiento_tecnica.getText()!=null && !razonamiento_tecnica.getText().equals("")&&mainapp.isValidId(razonamiento_tecnica.getText())){
			if(r_t_b==true){
				mainapp.getDatabase().UpdateRazonamiento_lenguaje(razonamiento_lenguaje.getText(), r_t_o);
				r_t_b=false;
				razonamiento_lenguaje.setText("");
				mensaje.setText("T�cnica de razonamiento actualizada");
				razonamiento_tecnicas_lista.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(razonamiento_tecnica);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveRazonamiento_tecnica(razonamiento_tecnica.getText());
			mensaje.setText("T�cnica de razonamiento registrada");
			razonamiento_lenguaje.setText("");
			mainapp.MarcarCorrecto(razonamiento_tecnica);
			return;
		}
		mainapp.MarcarError(razonamiento_tecnica);
		mensaje.setText("Ingrese una t�cnica de razonamiento v�lida");
	}

	@FXML
	public void handleAddRazonamiento_estrategia(){
		if(razonamiento_estrategia.getText()!=null && !razonamiento_estrategia.equals("")&&mainapp.isValidId(razonamiento_estrategia.getText())){
			if(r_e_b==true){
				mainapp.getDatabase().UpdateRazonamiento_estrategia(razonamiento_estrategia.getText(), r_e_o);
				r_e_b=false;
				razonamiento_estrategia.setText("");
				mensaje.setText("Estrategia de razonamiento actualizada");
				razonamiento_estrategias_lista.getSelectionModel().clearSelection();
				mainapp.MarcarCorrecto(razonamiento_estrategia);
				return;
			}//if reemplazo
			mainapp.getDatabase().saveRazonamiento_estrategia(razonamiento_estrategia.getText());
			razonamiento_estrategia.setText("");
			mensaje.setText("Estrategia de razonamiento registrada");
			mainapp.MarcarCorrecto(razonamiento_estrategia);
			return;
		}
		mainapp.MarcarError(razonamiento_estrategia);
		mensaje.setText("Ingrese una estrategia de razonamiento v�lida");
	}

	@FXML
	public void handleNewServicio(){
		servicio_desc.setText(null);
		servicio_b=false;
	}

	@FXML
	public void HandleNewActo(){
		actos_b=false;
		actos_desc.setText(null);
	}

	@FXML
	public void handleNewCaracterizacion(){
		caracterizacion_b=false;
		Caracterizacion_desc.setText(null);
	}

	@FXML
	public void handleNewLenguajeComunicacion(){
		Lenguaje_conocimiento_b=false;
		LenguajeConocimiento_desc.setText(null);
	}

	@FXML
	public void HandleNewTipoAprendizaje(){
		a_tipo_b=false;
		aprendizaje_tipo.setText(null);
	}

	@FXML
	public void HandleNewTecnicaAprendizaje(){
		a_tec_b=false;
		aprendizaje_tipo.setText(null);
	}

	@FXML
	public void HandleNewMecanismoAprendizaje(){
		a_meca_b=false;
		aprendizaje_tipo.setText(null);
	}

	@FXML
	public void handleNewTecnicaRazonamiento(){
		r_t_b=false;
		razonamiento_tecnica.setText(null);
	}

	@FXML
	public void handleNewTipoDato(){
		tipo_datos_b=false;
		tipo_dato.setText(null);
	}

	@FXML
	public void handleNewLenguajeRazonamiento(){
		r_l_b=false;
		razonamiento_lenguaje.setText(null);
	}

	@FXML
	public void handleNewEstrategiaRazonamiento(){
		r_e_b=false;
		razonamiento_estrategia.setText(null);
	}

	@FXML
	public void handleClose(){
		Stage s=(Stage) servicio_desc.getScene().getWindow();
		s.close();
	}

	public Main getMainapp() {
		return mainapp;
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;

		servicio_tipo.setItems(mainapp.getDatabase().getServicio());
		actosHabla_tipos.setItems(mainapp.getDatabase().getActos_habla());
		aprendizaje_tecnicas_lista.setItems(mainapp.getDatabase().getA_tecnicas());
		aprendizaje_mecanismos_lista.setItems(mainapp.getDatabase().getA_mecanismo());
		aprendizaje_Tipos_lista.setItems(mainapp.getDatabase().getA_tipo());
		razonamiento_tecnicas_lista.setItems(mainapp.getDatabase().getR_tenica());
		razonamiento_lenguajes_lista.setItems(mainapp.getDatabase().getR_lenguaje());
		razonamiento_estrategias_lista.setItems(mainapp.getDatabase().getR_estrategia());
		CaracterizacionTabla.setItems(mainapp.getDatabase().getCaracterizacion());
		LenguajeConocimiento_list.setItems(mainapp.getDatabase().getLenguajeComunicacion());
		lista_tipoDatos.setItems(mainapp.getDatabase().getTipo_datos());

		context_servicio();
		context_Acto_habla();
		context_Caracterizacion();
		context_ATecnica();
		context_AMecanismo();
		context_ATipos();
		context_RTecnicas();
		context_RLenguajes();
		context_REstrategias();
		context_lenguajeConocimiento();
		context_Tipo_datos();
	}

	private void context_lenguajeConocimiento() {
		LenguajeConocimiento_list.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowLenguajeConocimientoValues(item);
            });

            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_LenguajeComunicacion(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });

	}

	public void context_ATipos(){
		aprendizaje_Tipos_lista.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowAprendizajeTipoValues(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_AprendizajeTipos(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}
	public void context_RTecnicas(){
		razonamiento_tecnicas_lista.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowRazonamientoTecnicaValues(item);
            });

            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_RazonamientoTecnicas(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_RLenguajes(){
		razonamiento_lenguajes_lista.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowRazonamientoLenguajeValues(item);
            });

            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_RazonamientoLenguajes(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_REstrategias(){
		razonamiento_estrategias_lista.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                showRazonamientoEstrategiaValues(item);
            });

            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_RazonamientoEstrategias(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_AMecanismo(){
		aprendizaje_mecanismos_lista.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowAprendizajeMecanismoValue(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_AprendizajeMecanismo(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_ATecnica(){
		aprendizaje_tecnicas_lista.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowAprendizajeTecnicaValues(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_AprendizajeTecnica(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_Caracterizacion(){
		CaracterizacionTabla.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowCaracterizacionValues(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_Caracterizacion(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_Tipo_datos(){
		lista_tipoDatos.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowTipoDatoValues(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_tipo(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_Acto_habla(){
		actosHabla_tipos.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowActoValues(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_Acto(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}

	public void context_servicio(){
		servicio_tipo.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();

            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Editar"));
            editItem.setOnAction(event -> {
                String item = cell.getItem();
                ShowServicioValues(item);
            });
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar"));
            deleteItem.setOnAction(event ->  mainapp.getDatabase().eliminar_servicio(cell.getItem()));
            contextMenu.getItems().addAll(editItem, deleteItem);

            cell.textProperty().bind(cell.itemProperty());

            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });
	}
}

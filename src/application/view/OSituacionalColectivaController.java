package application.view;

import application.Main;
import application.model.Caracteristica;
import application.model.MarcoOntologicoColectivo;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaSituacionalC;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class OSituacionalColectivaController {

	private MarcoOntologicoColectivo moc;
	private Main mainapp;
	private OntologiaSituacionalC current=new OntologiaSituacionalC();
	@FXML
	private TextArea descripcion;
	@FXML
	private TextField Caracteristica_nombre;
	@FXML
	private TextField valor_inicial;
	@FXML
	private TextField fuente;
	@FXML
	private TextField confiabilidad;
	@FXML
	private TextField maximo;
	@FXML
	private TextField minimo;
	@FXML
	private TextField nombre_ma;
	@FXML
	private ComboBox<String> tipo_ma;
	@FXML
	private ComboBox<String> tecnica_ma;
	@FXML
	private ComboBox<String> tipo_caract;
	@FXML
	private TextField fuente_ma;
	@FXML
	private ComboBox<String> mecanismo_actualizacion;
	@FXML
	private TextArea fuente_inf_mr;
	@FXML
	private TextArea fuente_raz_mr;
	@FXML
	private ComboBox<String> tecnica_mr;
	@FXML
	private ComboBox<String> lenguaje_mr;
	@FXML
	private TextArea relacion_mr;
	@FXML
	private ComboBox<String> estrategia_mr;

	private boolean is_selected=false;
	private OntologiaColectivaController controlador_marco;

	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	@FXML
	private ScrollPane general;
	@FXML
	private ScrollPane Aprendizaje;
	@FXML
	private ScrollPane razonamiento;
	@FXML
	private TabPane tabpane;
	@FXML
	private Tab general_tab;
	@FXML
	private Tab mRazonamiento_tab;
	@FXML
	private Tab mAprendizaje_tab;

	@FXML
	private void initialize() {
		current=new OntologiaSituacionalC();
		ContextMenu contextMenu = new ContextMenu();
        MenuItem CopyItem = new MenuItem();
        CopyItem.textProperty().bind(Bindings.format("Copiar"));
        CopyItem.setOnAction(event -> controlador_marco.Copy(current));

        MenuItem CopyMRItem = new MenuItem();
        CopyMRItem.textProperty().bind(Bindings.format("Copiar mecanismo de Razonamiento"));
        CopyMRItem.setOnAction(event -> controlador_marco.Copy_MR(current));

        MenuItem CopyMAItem = new MenuItem();
        CopyMAItem.textProperty().bind(Bindings.format("Copiar mecanismo de Aprendizaje"));
        CopyMAItem.setOnAction(event -> controlador_marco.Copy_MA(current));
        contextMenu.getItems().addAll(CopyItem,CopyMAItem,CopyMRItem);
        general.setContextMenu(contextMenu);


        ContextMenu contextMenuMR = new ContextMenu();
        MenuItem CopyItemMR = new MenuItem();
        CopyItemMR.textProperty().bind(Bindings.format("Copiar"));
        CopyItemMR.setOnAction(event -> controlador_marco.Copy_MR(current));
        contextMenuMR.getItems().addAll(CopyItemMR);
        razonamiento.setContextMenu(contextMenuMR);

        ContextMenu contextMenuMA = new ContextMenu();
        MenuItem CopyItemMA = new MenuItem();
        CopyItemMA.textProperty().bind(Bindings.format("Copiar"));
        CopyItemMA.setOnAction(event -> controlador_marco.Copy_MA(current));
        contextMenuMA.getItems().addAll(CopyItemMA);
        Aprendizaje.setContextMenu(contextMenuMA);
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	public void ShowCaracteristicaValues(Caracteristica newValue) {

	}

	public void ShowSituacionValues(OntologiaSituacionalC onto){

		if(onto==null)
			return;

		is_selected=true;
		current=onto;

		descripcion.setText(onto.getDescripcion().get());
		current.setCaracterizacion(onto.getCaracterizacion());
		fuente.setText(onto.getFuente().get());
		confiabilidad.setText(onto.getConfiabilidad().get());
		maximo.setText(onto.getValor_maximo().get());
		minimo.setText(onto.getValor_minimo().get());

		current.setAprendizaje(onto.getAprendizaje());
		current.setCaracterizacion(onto.getCaracterizacion());
		controlador_marco.getCaracteristicas().setItems(FXCollections.observableArrayList(current.getCaracterizacion()));
		nombre_ma.setText(onto.getAprendizaje().getNombre().get());
		tipo_ma.getSelectionModel().select(onto.getAprendizaje().getTipo().get());
		tecnica_ma.getSelectionModel().select(onto.getAprendizaje().getTecnica_representacion().get());
		fuente_ma.setText(onto.getAprendizaje().getFuente_aprendizaje().get());
		mecanismo_actualizacion.getSelectionModel().select(onto.getAprendizaje().getMecanismo().get());

		fuente_raz_mr.setText(onto.getRazonamiento().getFuente_informacion().get());
		fuente_inf_mr.setText(onto.getRazonamiento().getFuente_alimentacion().get());
		tecnica_mr.getSelectionModel().select(onto.getRazonamiento().getTecnica_inferencia().get());
		lenguaje_mr.getSelectionModel().select(onto.getRazonamiento().getLenguaje_representacion().get());
		relacion_mr.setText(onto.getRazonamiento().getRelacion().get());
		estrategia_mr.getSelectionModel().select(onto.getRazonamiento().getEstrategia_razonamiento().get());

		//current=new OntologiaSituacionalC();
	}

	public MarcoOntologicoColectivo getMoc() {
		return moc;
	}

	public void setMoc(MarcoOntologicoColectivo moc) {
		this.moc = moc;
		//controlador_marco.getSituaciones().setItems(moc.getOntologias());
		controlador_marco.getCaracteristicas().setItems(FXCollections.observableArrayList());
	}

	@FXML
	public void handleGuardarSituacion(){
		boolean error=false;
		if(descripcion.getText().isEmpty()){
			mainapp.MarcarError(descripcion);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(descripcion);

		if(fuente.getText().isEmpty()){
			mainapp.MarcarError(fuente);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(fuente);

		if(confiabilidad.getText().isEmpty()){
			mainapp.MarcarError(confiabilidad);
			error=true;
		}else
			mainapp.MarcarCorrecto(confiabilidad);

		if(maximo.getText().isEmpty()){
			mainapp.MarcarError(maximo);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(maximo);

		if(minimo.getText().isEmpty()){
			mainapp.MarcarError(minimo);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(minimo);

		if(error){
			mainapp.getMensaje().set("Hay errores en la información general de la ontología");
			tabpane.getSelectionModel().select(general_tab);
			return;
		}

		//Validando Mecanismo de aprendizaje
		if(nombre_ma.getText().isEmpty()){
			error=true;
			mainapp.MarcarError(nombre_ma);
		}
		else
			mainapp.MarcarCorrecto(nombre_ma);

		if(tipo_ma.getSelectionModel().getSelectedItem()==null){
			error=true;
			mainapp.MarcarError(tipo_ma);
		}
		else
			mainapp.MarcarCorrecto(tipo_ma);

		if(tecnica_ma.getSelectionModel().getSelectedItem()==null){
			error=true;
			mainapp.MarcarError(tecnica_ma);
		}
		else
			mainapp.MarcarCorrecto(tecnica_ma);

		if(fuente.getText().isEmpty()){
			mainapp.MarcarError(fuente);
			error=true;
		}
		else
			mainapp.MarcarCorrecto(fuente);

		if(mecanismo_actualizacion.getSelectionModel().getSelectedItem()==null){
			mainapp.MarcarError(mecanismo_actualizacion);
			error=true;
		}else
			mainapp.MarcarCorrecto(mecanismo_actualizacion);

		if(error){
			mainapp.getMensaje().set("Hay errores en la información del mecanismo de aprendizaje");
			tabpane.getSelectionModel().select(mAprendizaje_tab);
			return;
		}
		MecanismoAprendizaje ma=new MecanismoAprendizaje(nombre_ma.getText(), tipo_ma.getSelectionModel().getSelectedItem(), tecnica_ma.getSelectionModel().getSelectedItem(), fuente_ma.getText(), mecanismo_actualizacion.getSelectionModel().getSelectedItem());

		//Validar mecanismo de razonamiento
				if(fuente_inf_mr.getText().isEmpty()){
					mainapp.MarcarError(fuente_inf_mr);
					error=true;
				}
				else
					mainapp.MarcarCorrecto(fuente_inf_mr);

				if(fuente_raz_mr.getText().isEmpty()){
					mainapp.MarcarError(fuente_raz_mr);
					error=true;
				}
				else
					mainapp.MarcarCorrecto(fuente_raz_mr);

				if(tecnica_mr.getSelectionModel().getSelectedItem()==null){
					error=true;
					mainapp.MarcarError(tecnica_mr);
				}
				else
					mainapp.MarcarCorrecto(tecnica_mr);

				if(lenguaje_mr.getSelectionModel().getSelectedItem()==null){
					error=true;
					mainapp.MarcarError(lenguaje_mr);
				}
				else
					mainapp.MarcarCorrecto(lenguaje_mr);

				if(relacion_mr.getText().isEmpty()){
					mainapp.MarcarError(relacion_mr);
					error=true;
				}else
					mainapp.MarcarCorrecto(relacion_mr);

				if(estrategia_mr.getSelectionModel().getSelectedItem()==null){
					mainapp.MarcarError(estrategia_mr);
					error=true;
				}else
					mainapp.MarcarCorrecto(estrategia_mr);

				if(error){
					mainapp.getMensaje().set("Hay errores en la información del mecanismo de Razonamiento");
					tabpane.getSelectionModel().select(mRazonamiento_tab);
					return;
				}
			MecanismoRazonamiento mr=new MecanismoRazonamiento(fuente_raz_mr.getText(), fuente_inf_mr.getText(), tecnica_mr.getSelectionModel().getSelectedItem(), lenguaje_mr.getSelectionModel().getSelectedItem(), relacion_mr.getText(), estrategia_mr.getSelectionModel().getSelectedItem());

		current.setConfiabilidad(new SimpleStringProperty(confiabilidad.getText()));
		current.setFuente(new SimpleStringProperty(fuente.getText()));
		current.setDescripcion(new SimpleStringProperty(descripcion.getText()));
		current.setValor_minimo(new SimpleStringProperty(minimo.getText()));
		current.setValor_maximo(new SimpleStringProperty(maximo.getText()));
		current.setAprendizaje(ma);
		current.setRazonamiento(mr);
		current.setCaracterizacion(controlador_marco.getCaracteristicas().getItems());

		current.setNodo_aprendizaje(ma.nodo_arbol);
		current.setNodo_razonamiento(mr.nodo_arbol);

		if(!is_selected){
			controlador_marco.getSituaciones().getItems().add(current);
			moc.getNodo_arbol().getChildren().add(current.getNodo_arbol());
		}
		is_selected=false;
		current=new OntologiaSituacionalC();
		controlador_marco.getCaracteristicas().setItems(current.getCaracterizacion());
		controlador_marco.getSituaciones().getSelectionModel().clearSelection();
		clean();
	}

	@FXML
	public void handleAddCaracterizacion(){
		if(Caracteristica_nombre.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un nombre para la característica");
			mainapp.MarcarError(Caracteristica_nombre);
			return;
		}
		mainapp.MarcarCorrecto(Caracteristica_nombre);

		if(valor_inicial.getText().isEmpty()){
			mainapp.getMensaje().set("Por favor ingrese un valor inicial para la característica");
			mainapp.MarcarError(valor_inicial);
			return;
		}
		mainapp.MarcarCorrecto(valor_inicial);

		if(tipo_caract.getSelectionModel().getSelectedIndex()==-1){
			mainapp.getMensaje().set("Por favor seleccione un tipo de característica");
			mainapp.MarcarError(tipo_caract);
			return;
		}
		mainapp.MarcarCorrecto(tipo_caract);

		controlador_marco.getCaracteristicas().getItems().add(new Caracteristica(tipo_caract.getSelectionModel().getSelectedItem(), Caracteristica_nombre.getText(), valor_inicial.getText()));
		tipo_caract.getSelectionModel().clearSelection();
		Caracteristica_nombre.setText("");
		valor_inicial.setText("");
	}

	@FXML
	public void handleTerminar(){
		/*Stage stage = (Stage) controlador_marco.getSituaciones().getScene().getWindow();
		stage.close();*/
		handleVolver();
	}

	public void clean(){
		descripcion.setText("");
		Caracteristica_nombre.setText("");
		valor_inicial.setText("");
		fuente.setText("");
		confiabilidad.setText("");
		maximo.setText("");
		minimo.setText("");
		nombre_ma.setText("");
		tipo_ma.getSelectionModel().clearSelection();
		tecnica_ma.getSelectionModel().clearSelection();
		fuente_ma.setText("");
		mecanismo_actualizacion.getSelectionModel().clearSelection();
		fuente_raz_mr.setText("");
		fuente_inf_mr.setText("");
		tecnica_mr.getSelectionModel().clearSelection();
		lenguaje_mr.getSelectionModel().clearSelection();
		relacion_mr.setText("");
		estrategia_mr.getSelectionModel().clearSelection();

	}

	public Main getMainapp() {
		return mainapp;
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;

		tipo_ma.setItems(mainapp.getDatabase().getA_tipo());
		tecnica_ma.setItems(mainapp.getDatabase().getA_tecnicas());
		tipo_caract.setItems(mainapp.getDatabase().getCaracterizacion());
		mecanismo_actualizacion.setItems(mainapp.getDatabase().getA_mecanismo());
		tecnica_mr.setItems(mainapp.getDatabase().getR_tenica());
		lenguaje_mr.setItems(mainapp.getDatabase().getR_lenguaje());
		estrategia_mr.setItems(mainapp.getDatabase().getR_estrategia());
	}

	public OntologiaColectivaController getControlador_marco() {
		return controlador_marco;
	}

	public void setControlador_marco(OntologiaColectivaController controlador_marco) {
		this.controlador_marco = controlador_marco;
	}

	@FXML
	private void handleVolver(){
		controlador_marco.getPrincipal().setVisible(true);
		controlador_marco.getOntologias().setVisible(false);
	}


}

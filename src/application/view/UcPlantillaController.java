package application.view;

import application.Main;
import application.model.Caso_uso;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UcPlantillaController {
	private Main mainapp;

	@FXML
	private TextArea Descripcion;
	@FXML
	private TextField precondicion;
	@FXML
	private TextArea fracaso;
	@FXML
	private TextArea exito;
	@FXML
	private TextField nombre;
	private Caso_uso uc;

	public Main getMainapp() {
		return mainapp;
	}

	public void setMainapp(Main mainapp) {
		this.mainapp = mainapp;
	}

	public Caso_uso getUc() {
		return uc;
	}

	public void setUc(Caso_uso uc) {
		this.uc = uc;
		nombre.setText(uc.getNombre());
		Descripcion.setText(uc.getDescripcion());
		precondicion.setText(uc.getPrecondicion());
		fracaso.setText(uc.CondicionFracaso().get());
		exito.setText(uc.CondicionExito().get());
	}


	@FXML
    private void handleMouseAceptar() {
		//validar entrada
		uc.setNombre(nombre.getText());
		uc.setDescripcion(Descripcion.getText());
		uc.setPrecondicion(precondicion.getText());
		uc.setCondicionFracaso(fracaso.getText());
		uc.setCondicionExito(exito.getText());

		Stage stage = (Stage) nombre.getScene().getWindow();
		stage.close();
    }

	@FXML
	private void handleCancelar(){
		Stage stage = (Stage) nombre.getScene().getWindow();
		stage.close();
	}
}

package application.view;

import java.io.File;
import java.io.IOException;

import org.xml.sax.SAXException;
import application.Main;
import application.XMIParser;
import application.model.Actor;
import application.model.agent;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class addAgentController {

	private Stage dialogStage;
    private boolean okClicked = false;
    private Main mainApp;
    @FXML
    private Label NameFile;
    @FXML
    private TextField NameAgent;
    @FXML
    private ObservableList<Actor> nuevos;
    @FXML
    private Button examinar;

    private agent agentemporal;
    private boolean DatosCorrectos;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	ImageView x=new ImageView(new Image(getClass().getResourceAsStream("/images/icons/view_file-32.png")));
    	examinar.setGraphic(x);
    	setDatosCorrectos(false);

    	Platform.runLater(new Runnable() {
            @Override
            public void run() {
                NameAgent.requestFocus();
            }
        });

    	NameAgent.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(!newValue.equals("")||newValue==null){
					examinar.setDisable(false);
				}//habilito
				if(newValue.equals("")){
					examinar.setDisable(true);
				}//deshabilito
			}
		});
    }

    public TextField getNameAgent() {
		return NameAgent;
	}

	public void setNameAgent(TextField nameAgent) {
		NameAgent = nameAgent;
	}

	/**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleActividades(){
    	FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/actividades.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			ActividadesController controller = loader.getController();
            controller.setMainApp(mainApp);
            mainApp.getRootLayout().setCenter(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    /**
     * Called when the user clicks siguiente.
     */
    @FXML
	private void handleNext() {

    	FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
            controller.setAgente(agentemporal,true);
            controller.setMainApp(mainApp);
			mainApp.getRootLayout().setCenter(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Called when the user clicks Back.
     */
    @FXML
    private void handleBack() {
        dialogStage.close();
    }

   /**
     * Opens a FileChooser to let the user select an address book to load.
     * @throws IOException
     * @throws SAXException
     */
    @FXML
    private void handleOpen() throws IOException, SAXException {
    	//Actores.setItems(nuevos);

    	 FileChooser fileChooser = new FileChooser();
       // System.out.println("handle open");
        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml,*.xmi)", "*.xml","*.xmi");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog

        File file = fileChooser.showOpenDialog(dialogStage);

        if (file != null) {
        	String name=NameAgent.getText();
        	boolean flag=false;

        	if(findByName(name)==null){
        		agentemporal=new agent(NameAgent.getText());
        	}
        	else{
        		agentemporal=findByName(name);
        		flag=true;
        	}//fin else

        	XMIParser parser=new XMIParser(file);
        	if(!flag){
        		mainApp.addAgent(agentemporal);
        		TreeItem<Object> nodo=new TreeItem<Object>(agentemporal);
        		agentemporal.setNodo_arbol(nodo);
        		String msg=parser.UCParsing(agentemporal);
        		mainApp.getRaiz().getChildren().add(nodo);
        		mainApp.getMensaje().set("Agente registrado exitosamente!");
        	}
        	else{
        		mainApp.getMensaje().set("Información importada exitosamente!");
        	}
        	//mainApp.FillTree(mainApp.getRootcontroller().getArbol());
        	mainApp.showAgentModel(agentemporal);

        }
    }

    @FXML
    public void handleRegister(){
    	String name=NameAgent.getText();
    	agentemporal=new agent(name);

    	if(NameAgent.getText()!=null && NameAgent.getText()!="" && findByName(name)==null&&mainApp.isValidId(NameAgent.getText())){
    		TreeItem<Object> nodo=new TreeItem<Object>(agentemporal);
    		agentemporal.setNodo_arbol(nodo);
    		nodo.setExpanded(true);
    		mainApp.addAgent(agentemporal);
    		mainApp.getRaiz().getChildren().add(nodo);
        	NameFile.setText(NameFile.getText()+" Agente registrado exitosamente!");
        	mainApp.getMensaje().set("Agente registrado exitosamente!");
        	mainApp.showAgentModel(agentemporal);
        	NameAgent.setText("");
        	mainApp.MarcarCorrecto(NameAgent);
        	mainApp.setSomething_has_changed(false);
    	}
    	else{
    		mainApp.getMensaje().set("Por favor ingrese un nombre de agente válido!");
    		mainApp.MarcarError(NameAgent);
    	}

 	}

    public boolean isUnique(String name){
    	for (agent agente : mainApp.getAgentData()) {
			if(agente.getName().get().equals(name))
				return false;
		}
    	return true;
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

    public agent findByName(String name){
    	for (agent agente : mainApp.getAgentData()) {
    		if(agente.getName().get().equals(name))
    			return agente;
		}
    	return null;
    }
	public boolean isDatosCorrectos() {
		return DatosCorrectos;
	}

	public void setDatosCorrectos(boolean datosCorrectos) {
		DatosCorrectos = datosCorrectos;
	}
}

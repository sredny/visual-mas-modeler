package application.view;

import java.io.IOException;

import application.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class ActividadesController {
	 private Main mainApp;

	public Main getMainApp() {
		return mainApp;
	}

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}

	@FXML
    private void initialize() {

    }

	/**
     * Called when the user clicks siguiente.
     */
    @FXML
	private void handleNext() {

    	FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/actividades.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			Scene scene = new Scene(page);
			mainApp.changeEscene(scene);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Called when the user clicks Back.
     */
    @FXML
    private void handleBack() {
    	FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/addForm.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			addAgentController controller = loader.getController();
            controller.setMainApp(mainApp);
			Scene scene = new Scene(page);
			mainApp.changeEscene(scene);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

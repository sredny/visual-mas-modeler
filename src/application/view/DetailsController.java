package application.view;

import java.io.IOException;

import application.Main;
import application.model.Actor;
import application.model.Caso_uso;
import application.model.agent;
import application.model.asociacion;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class DetailsController {
	private Main mainApp;
	@FXML
    private ProgressBar progreso;
	@FXML
	private TextArea descripcion;
	@FXML
	private TextArea precondicion;
	@FXML
	private ScrollPane panel;
	@FXML
	private TextArea fracaso;
	@FXML
	private TextArea exito;
	@FXML
	private Button Agregar;
	private Caso_uso caso;
	@FXML
	private Pane central;
	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	@FXML
	private Label titulo;
	@FXML
	private ListView<Actor> lista_actores;
	@FXML
	private ComboBox<agent> comboActores;
	@FXML
	private TextField nombre_cu;
	@FXML
	private Button editar;
	private agent agente;
	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	private String nameTab;

	@FXML
	private void initialize(){

		editar.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/edit.png"))));

		lista_actores.setCellFactory(new Callback<ListView<Actor>, ListCell<Actor>>() {
			@Override
			public ListCell<Actor> call(ListView<Actor> arg0) {
				ListCell<Actor> cell=new ListCell<Actor>(){
				protected void updateItem(Actor i,boolean bln){
					super.updateItem(i, bln);
					if (i != null) {
                        setText(i.getName().get());
                    }
					if(bln || i == null) {
                        setGraphic(null);
                        setText(null);
                      }

				}
			};
			ContextMenu contextMenu = new ContextMenu();
			MenuItem deleteItem=new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar actor"));
            deleteItem.setOnAction(event-> {eliminarActor(cell.getItem());
            								mainApp.setSomething_has_changed(false);
            								});
            contextMenu.getItems().addAll(deleteItem);

            cell.setContextMenu(contextMenu);
				return cell;
			}
	});

		comboActores.setConverter(new StringConverter<agent>() {
			@Override
			public String toString(agent obj) {
				if(obj!=null)
					return obj.getName().get();
				return "";
			}
			@Override
			public agent fromString(String name) {
				return new agent(name);
			}
		});

	}

	private void eliminarActor(Actor a){
		
		agente.deleteAsociation(this.caso.getCodigo(), a.getCodigo().get());
		lista_actores.getItems().remove(a);
		mainApp.setSomething_has_changed(false);
		//lista_actores.refresh();
                lista_actores.setVisible(false);
                lista_actores.setVisible(true);
	}

	public Main getMainApp() {
		return mainApp;
	}

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
		comboActores.setItems(mainApp.getAgentData());

		ContextMenu cm=new ContextMenu();
		    MenuItem copiar=new MenuItem("Copiar descripci�n");
		    copiar.setOnAction(copy_agente());
		    cm.getItems().add(copiar);
		    panel.setContextMenu(cm);

        iniciar_tabla();
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());

        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);

        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	public Caso_uso getCaso() {
		return caso;
	}
	private EventHandler<ActionEvent> copy_agente(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopy();
		}};
	}

	@FXML
	private void handleCopy(){
		filas=FXCollections.observableArrayList();
		tabla.setItems(filas);
		filas.add(new Fila("Agente",agente.getName().get()));
		filas.add(new Fila("Caso de Uso",caso.nombre().get()));
		filas.add(new Fila("Descripci�n",caso.Descripcion().get()));
		filas.add(new Fila("Precondici�n",caso.Precondicion().get()));
		filas.add(new Fila("Condici�n de fracaso",caso.getCondicionFracaso()));
		filas.add(new Fila("Condici�n de �xito",caso.getCondicionExito()));
		String actores="";
		for(Actor a: this.agente.UC_actors(caso.getCodigo())){
			actores+="-"+a.getName().get()+"\n";
		}
		filas.add(new Fila("Actores",actores));

		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Caso de uso copiado en el portapapeles");
	}

	@FXML
	private void handleEdit() throws IOException{
		//mainApp.showUCplantilla(caso);
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/UseCasePlantilla.fxml"));
		AnchorPane page= (AnchorPane) loader.load();
		UcPlantillaController controller = loader.getController();
        controller.setMainapp(mainApp);
        controller.setUc(caso);
		Stage dialog = new Stage();

        dialog.setTitle("Informacion del Caso de Uso");
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(mainApp.getPrimaryStage());
        Scene scene = new Scene(page);
        dialog.setScene(scene);


     // Show the dialog and wait until the user closes it
        dialog.showAndWait();
        setCaso(caso);
	}

	public void setCaso(Caso_uso caso) {
		this.caso = caso;
		nombre_cu.setText(caso.nombre().get());
		titulo.setText(caso.getNombre()+" \nDescripci�n de Caso de Uso");

		if(!caso.Descripcion().get().equals("Sin informaci�n"))
			descripcion.textProperty().set(caso.Descripcion().get());
		else
			descripcion.setPromptText("Sin informaci�n");

		if(!caso.Precondicion().get().equals("Sin informaci�n"))
			precondicion.textProperty().set(caso.Precondicion().get());
		else
			precondicion.setPromptText("Sin informaci�n");

		if(!caso.CondicionFracaso().get().equals("Sin informaci�n"))
			fracaso.textProperty().set(caso.CondicionFracaso().get());
		else
			fracaso.setPromptText("Sin informaci�n");

		if(!caso.CondicionExito().get().equals("Sin informaci�n"))
			exito.textProperty().set(caso.CondicionExito().get());
		else
			exito.setPromptText("Sin informaci�n");
	}

	public agent getAgente() {
		return agente;
	}

	public void setAgente(agent agente) {
		this.agente = agente;
		titulo.setText(agente.getNameString()+" - "+caso.getNombre()+" Descripci�n de Caso de Uso");
		lista_actores.setItems(agente.UC_actors(this.caso.getCodigo()));
	}

	@FXML
	public void handleAgregarActor(){
		agent actor_seleccionado;

		Actor nuevo=new Actor();
		String nombre=comboActores.getEditor().getText();
		agent ac=null;
		if(nombre.isEmpty()||nombre==null){
			actor_seleccionado=comboActores.getSelectionModel().getSelectedItem();
		}
		else{
			actor_seleccionado=new agent(nombre);
			comboActores.getEditor().setText("");
		}
		//comboBox.getEditor().getText();
		mainApp.setSomething_has_changed(false);
		nuevo.setName(actor_seleccionado.getNameString());
		agente.getLista_actores().add(nuevo);
		asociacion nueva_asociacion=new asociacion(nuevo.getCodigo().get(), caso.getCodigo());
		agente.getAsociaciones().add(nueva_asociacion);
		comboActores.getSelectionModel().clearSelection();
		lista_actores.setItems(agente.UC_actors(caso.getCodigo()));
	}

	@FXML
	private void EditName(){
		Guardar();
		nombre_cu.setDisable(false);
		nombre_cu.setEditable(true);

		descripcion.setDisable(true);
		precondicion.setDisable(true);
		fracaso.setDisable(true);
		exito.setDisable(true);
		Guardar();
	}

	@FXML
	private void EditDescripcion(){
		Guardar();
		descripcion.setDisable(false);
		descripcion.setEditable(true);
		nombre_cu.setDisable(true);
		precondicion.setDisable(true);
		fracaso.setDisable(true);
		exito.setDisable(true);

		Guardar();
	}

	@FXML
	private void EditPrecondicion(){
		Guardar();
		precondicion.setDisable(false);
		precondicion.setEditable(true);

		nombre_cu.setDisable(true);
		descripcion.setDisable(true);
		fracaso.setDisable(true);
		exito.setDisable(true);
		Guardar();
	}

	@FXML
	private void EditFracaso(){
		Guardar();
		fracaso.setDisable(false);
		fracaso.setEditable(true);

		nombre_cu.setDisable(true);
		descripcion.setDisable(true);
		precondicion.setDisable(true);
		exito.setDisable(true);
		Guardar();
	}

	@FXML
	private void EditExito(){
		Guardar();
		exito.setDisable(false);
		exito.setEditable(true);

		nombre_cu.setDisable(true);
		descripcion.setDisable(true);
		precondicion.setDisable(true);
		fracaso.setDisable(true);

	}

	public boolean Guardar(){

		if(nombre_cu.getText()!=null && !nombre_cu.getText().isEmpty()){
			caso.setNombre(nombre_cu.getText());

		}
		else{
			mainApp.getMensaje().set("Ingrese un nombre v�lido para el caso de uso");
			nombre_cu.setStyle("-fx-border-color: red ;-fx-stroke-width: 10;");
			return false;
		}

		if(descripcion.getText().isEmpty()||descripcion.getText()==null){
			mainApp.getMensaje().set("Por favor ingrese la descripci�n del caso de uso");
			descripcion.setStyle("-fx-border-color: red ;-fx-stroke-width: 10;");
			return false;
		}

		if(exito.getText().isEmpty()||exito.getText()==null){
			mainApp.getMensaje().set("Por favor ingrese la condici�n de �xito del caso de uso");
			exito.setStyle("-fx-border-color: red ;-fx-stroke-width: 10;");
			return false;
		}
		if(fracaso.getText().isEmpty()||fracaso.getText()==null){
			mainApp.getMensaje().set("Por favor ingrese la condici�n de fracaso del caso de uso");
			fracaso.setStyle("-fx-border-color: red ;-fx-stroke-width: 10;");
			return false;
		}
		if(precondicion.getText().isEmpty()||precondicion.getText()==null){
			mainApp.getMensaje().set("Por favor ingrese la precondici�n del caso de uso");
			precondicion.setStyle("-fx-border-color: red ;-fx-stroke-width: 10;");
			return false;
		}

		String nuevoNombre=agente.getName().get()+". Caso de Uso-"+caso.getNombre();
		mainApp.setSomething_has_changed(false);
		mainApp.rename_tab(getNameTab(), nuevoNombre);
		setNameTab(nuevoNombre);
		mainApp.getMensaje().set("Acto de habla actualizado");
		caso.setDescripcion(descripcion.getText());
		caso.setCondicionExito(exito.getText());
		caso.setCondicionFracaso(fracaso.getText());
		caso.setPrecondicion(precondicion.getText());
		return true;
	}

	public String getNameTab() {
		return nameTab;
	}

	public void setNameTab(String nameTab) {
		this.nameTab = nameTab;
	}

	public Label getTitulo() {
		return titulo;
	}

	public void setTitulo(Label titulo) {
		this.titulo = titulo;
	}

}

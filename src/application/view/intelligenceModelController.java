package application.view;

import java.io.IOException;

import application.Main;
import application.model.MarcoOntologicoColectivo;
import application.model.MarcoOntologicoIndividual;
import application.model.agent;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

public class intelligenceModelController {
	private Main mainApp;
	private agent agente;
	@FXML
	private ListView<MarcoOntologicoColectivo> Colectivos;
	private ObservableList<MarcoOntologicoColectivo> belong_to=FXCollections.observableArrayList();
	@FXML
	private ListView<MarcoOntologicoIndividual> individual;

	@FXML
    private void initialize() {
		individual.setCellFactory(new Callback<ListView<MarcoOntologicoIndividual>, ListCell<MarcoOntologicoIndividual>>() {

				@Override
				public ListCell<MarcoOntologicoIndividual> call(ListView<MarcoOntologicoIndividual> arg0) {
					ListCell<MarcoOntologicoIndividual> cell=new ListCell<MarcoOntologicoIndividual>(){
					protected void updateItem(MarcoOntologicoIndividual i,boolean bln){
						super.updateItem(i, bln);
						if (i != null) {
                            setText(i.getNombre().get());
                        }
					}
				};
					return cell;
				}
		});

		Colectivos.setCellFactory(new Callback<ListView<MarcoOntologicoColectivo>, ListCell<MarcoOntologicoColectivo>>() {

			@Override
			public ListCell<MarcoOntologicoColectivo> call(ListView<MarcoOntologicoColectivo> arg0) {
				ListCell<MarcoOntologicoColectivo> cell=new ListCell<MarcoOntologicoColectivo>(){

                    @Override
                    protected void updateItem(MarcoOntologicoColectivo c, boolean bln) {
                        super.updateItem(c, bln);
                        if (c != null) {
                            setText(c.getNombre().get());
                        }
                    }
                };
				return cell;
			}
		});
		Colectivos.setItems(belong_to);
    }

	public Main getMainApp() {
		return mainApp;
	}


	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;

		if(agente!=null){
			mainApp.filterMarcosC_by(agente);
			Colectivos.setItems(mainApp.getBelongs());
		}
	}

	@FXML
	public void HandleNext(){}


	@FXML
	public void HandleBack(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
            controller.setMainApp(mainApp);
			Scene scene = new Scene(page);
			//mainApp.changeEscene(scene);
			Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText(this.agente.getNameString()+"-M. de Coordinacion");
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void handleIndividual(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/OntologiaIndividual.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			OntologiaIndividualController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainapp(mainApp);
            //mainApp.getRootLayout().setCenter(page);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText(this.agente.getNameString()+"-M. de Inteligencia Individual");
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void handleColectivo(){
		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/OntologiaColectiva.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			OntologiaColectivaController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainapp(mainApp);
           // mainApp.getRootLayout().setCenter(page);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText("M. de Inteligencia Colectivo");
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public agent getAgente() {
		return agente;
	}

	public void setAgente(agent agente) {
		this.agente = agente;
		individual.setItems(agente.getMarco_ontologico_individual());
	}
}

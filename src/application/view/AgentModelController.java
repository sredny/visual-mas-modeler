package application.view;

import java.io.IOException;
import application.Main;
import application.model.Caracteristica;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaDominio;
import application.model.Parametros;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.objetivo;
import application.model.modelos.AgentModel;
import copy.Fila;
import copy.TableUtils;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class AgentModelController {
	private Main mainApp;
	@FXML
   private agent agente;

	 @FXML
	 private TableView<Servicio> ServiceTable;
	 @FXML
	 private TableColumn<Servicio, String> NameColumn;
	 @FXML
	 private TableColumn<Servicio, String> TypeColumn;
	 @FXML
	 private TableView<Parametros> ParametroEntradaTable;
	 @FXML
	 private TableColumn<Parametros, String> PentradaNameColumn;
	 @FXML
	 private TableColumn<Parametros, String> PentradaTypeColumn;
	 @FXML
	 private TableView<Parametros> ParametroSalidaTable;
	 @FXML
	 private TableColumn<Parametros, String> PSalidaNameColumn;
	 @FXML
	 private TableColumn<Parametros, String> PSalidaTypeColumn;
	 @FXML
	 private TableView<Caracteristica> PropiedadesTable;
	 @FXML
	 private TableColumn<Caracteristica, String> PropiedadNameColumn;
	 @FXML
	 private TableColumn<Caracteristica, String> PropiedadValueColumn;
	 @FXML
	 private TableColumn<Caracteristica, String> PropiedadDescripcionColumn;
	@FXML
	private Label nombre;
	@FXML
	private TextField componentes;
	@FXML
	private TextArea marcoReferencia;
	@FXML
	private TextArea DescripcionAgente;
	@FXML
	private TextArea ObjetivosDescripcion;
	@FXML
	private TextField ObjetivosEntrada;
	@FXML
	private TextField ObjetivosSalida;
	@FXML
	private TextArea ObjetivosActivacion;
	@FXML
	private TextArea ObjetivosFinalizacion;
	@FXML
	private TextArea ObjetivosExito;
	@FXML
	private TextArea ObjetivosFracaso;
	@FXML
	private TextArea ObjetivosOntologia;
	@FXML
	private TextArea ServiciosDescripcion;
	@FXML
	private TextArea CapacidadHabilidades;
	@FXML
	private ComboBox<String> CapacidadRepresentacion;
	@FXML
	private ComboBox<String> CapacidadLenguajeC;
	@FXML
	private ComboBox<objetivo> ObjetivoC;

	@FXML
	private TextArea RestriccionNormas;
	@FXML
	private TextArea RestriccionPreferencias;
	@FXML
	private TextArea RestriccionPermisos;
	@FXML
	private TextField AgentePosicion;
	@FXML
	private TextField ObjetivoNombre;
	@FXML
	private TextField ServicioNombre;
	@FXML
	private ComboBox<String> ServicioTipo;
	@FXML
	private TextField servicioPentrada;
	@FXML
	private ComboBox<String> ServicioPentradaTipo;
	@FXML
	private TextField servicioPsalida;
	@FXML
	private ComboBox<String> ServicioPsalidaTipo;
	@FXML
	private ComboBox<String> ServicioPropiedadNombre;
	@FXML
	private TextField servicioPropiedadValor;

	@FXML
	private ScrollPane scrollObjetivo;
	@FXML
	private Button guardar;
	@FXML
	private Button guardar_objetivo;
	@FXML
	private ComboBox<String> comportamiento;
	@FXML
	private TabPane tabpane;
	@FXML
	private Tab servicioTab;

	@FXML
	TableColumn<Fila, String> clave;
	@FXML
	TableColumn<Fila, String> valor;
	@FXML
	private TableView<Fila> tabla= new TableView<>();
	@FXML
	TableColumn<objetivo, String> valor_objetivos;
	@FXML
	private TableView<objetivo> tabla_objetivos= new TableView<>();
	@FXML
	private ScrollPane descripcion;
	@FXML
	private ScrollPane objetivo;
	@FXML
	private ScrollPane servicios;
	@FXML
	private ScrollPane  capacidad;
	@FXML
	private ScrollPane  restriccion;
	@FXML
	private ListView<String> TableComponentes;
	@FXML
	private ListView<String> TableOentrada;
	@FXML
	private ListView<String> TableOsalida;
	@FXML
	private Label titulo;
	@FXML
	private Label edicion_mode;
	@FXML
	private Label edicion_mode_objetivos;
	@FXML
	private TextField descripcion_Propiedad;

	@FXML
	private GridPane gridServicio;
	@FXML
	private GridPane gridDescripcion;
	@FXML
	private GridPane gridObjetivo;
	@FXML
	private GridPane gridCapacidad;
	@FXML
	private GridPane gridRestriccion;
	@FXML
	private Button add_property_button;
	@FXML
	private Button add_Pentrada_button;
	@FXML
	private Button add_Psalida_button;


	private ObservableList<Caracteristica> current_propiedades=FXCollections.observableArrayList();
	private ObservableList<Fila> filas=FXCollections.observableArrayList();
	private ObservableList<String> compo=FXCollections.observableArrayList();
	private ObservableList<String> Pentradas=FXCollections.observableArrayList();
	private ObservableList<String> Psalida=FXCollections.observableArrayList();
	private ObservableList<Parametros> PSentradas=FXCollections.observableArrayList();
	private ObservableList<Parametros> PSsalida=FXCollections.observableArrayList();
	private ObservableList<String> OPentradas=FXCollections.observableArrayList();
	private ObservableList<String> OPsalida=FXCollections.observableArrayList();
	private objetivo current_objetivo;
	private final ObjectProperty<ListCell<Parametros>> dragSource = new SimpleObjectProperty<>();
	private Button Contexto_CopiarServicio,Contexto_CopiarDescripcion,Contexto_CopiarObjetivo, Contexto_CopiarCapacidad,Contexto_CopiarRestriccion;
	private Popup contexto_descripcion,contexto_objetivo,contextoServicio,contexto_capacidad,contexto_restriccion;

	@FXML
    private void initialize() {
		TableComponentes.setItems(compo);
		current_objetivo=new objetivo();
		TableOentrada.setItems(FXCollections.observableArrayList());
		TableOsalida.setItems(FXCollections.observableArrayList());
		//guardar.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/save-16.png"))));
		add_property_button.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/addP.png"))));
		add_Pentrada_button.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/addP.png"))));
		add_Psalida_button.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/addP.png"))));

		NameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		valor_objetivos.setCellValueFactory(cellData -> cellData.getValue().getObjetivoNombre_sp());
	    TypeColumn.setCellValueFactory(cellData -> cellData.getValue().getTipo());

	    PentradaNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		PentradaTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getTipo());
		ParametroEntradaTable.setItems(PSentradas);
		PentradaNameColumn.prefWidthProperty().bind(ParametroEntradaTable.widthProperty().divide(2));
		PentradaTypeColumn.prefWidthProperty().bind(ParametroEntradaTable.widthProperty().divide(2));

		PSalidaNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		PSalidaTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getTipo());
		ParametroSalidaTable.setItems(PSsalida);
		PSalidaNameColumn.prefWidthProperty().bind(ParametroSalidaTable.widthProperty().divide(2));
		PSalidaTypeColumn.prefWidthProperty().bind(ParametroSalidaTable.widthProperty().divide(2));

		PropiedadNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNombre());
		PropiedadValueColumn.setCellValueFactory(cellData -> cellData.getValue().getValor_omision());
		PropiedadDescripcionColumn.setCellValueFactory(cellData -> cellData.getValue().getDescripcion());
		PropiedadesTable.setItems(current_propiedades);
		PropiedadNameColumn.prefWidthProperty().bind(PropiedadesTable.widthProperty().divide(3));
		PropiedadValueColumn.prefWidthProperty().bind(PropiedadesTable.widthProperty().divide(3));
		PropiedadDescripcionColumn.prefWidthProperty().bind(PropiedadesTable.widthProperty().divide(3));

		NameColumn.prefWidthProperty().bind(ServiceTable.widthProperty().divide(2));
		TypeColumn.prefWidthProperty().bind(ServiceTable.widthProperty().divide(2));

		iniciar_popups();

	    ObjetivoC.setConverter(new StringConverter<objetivo>() {
			@Override
			public String toString(objetivo object) {
				return object.getObjetivoNombre_sp().get();
			}

			@Override
			public objetivo fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});

	    iniciar_tabla();

	    contextoListas();
	    contextoTabla();

	    ServiceTable.getSelectionModel().selectedItemProperty().addListener(
	    		(observable, oldValue, newValue) -> ShowServiceValues(newValue));

	    tabla_objetivos.getSelectionModel().selectedItemProperty().addListener(
	    		(observable, oldValue, newValue) -> ShowobjetivoValues(newValue));

	    tabpane.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> {

	        this.salir_edicion();
	        contexto_capacidad.hide();
	    	contexto_descripcion.hide();
	    	contexto_objetivo.hide();
	    	contexto_restriccion.hide();
	    	contextoServicio.hide();
	    });

	    ObjetivoNombre.requestFocus();
	}

	public void iniciar_popups(){
		Contexto_CopiarServicio = new Button("Copiar servicio");

		Contexto_CopiarServicio.getStylesheets().add(this.getClass().getClassLoader().getResource("application/view/Theme.css").toString());
		Contexto_CopiarServicio.getStyleClass().add("contextButton");
		contextoServicio= new Popup();
	    VBox content = new VBox();
	    Contexto_CopiarServicio.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				contextoServicio.hide();
				handleCopyServicio();
			} });

	    content.getChildren().addAll(Contexto_CopiarServicio);
	    contextoServicio.getContent().add(content);
	    contextoServicio.setAutoHide(true);

	    contexto_descripcion = new Popup();
	    content = new VBox();
	    Button b = new Button("Copiar descripci�n");
	    b.getStylesheets().add(this.getClass().getClassLoader().getResource("application/view/Theme.css").toString());
	    b.getStyleClass().add("contextButton");
	    b.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				contexto_descripcion.hide();
				handleCopyDescripcion();
			} });
	    content.getChildren().addAll(b);
	    contexto_descripcion.getContent().add(content);
	    contexto_descripcion.setAutoHide(true);

		contexto_objetivo = new Popup();
	    content = new VBox();
	    b = new Button("Copiar objetivo");

	    b.getStylesheets().add(this.getClass().getClassLoader().getResource("application/view/Theme.css").toString());
	    b.getStyleClass().add("contextButton");
	    b.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				contexto_objetivo.hide();
				handleCopyObjetivo();
			} });
	    content.getChildren().addAll(b);

	    contexto_objetivo.getContent().add(content);
	    contexto_objetivo.setAutoHide(true);

	    contexto_capacidad = new Popup();
	    content = new VBox();
	    b = new Button("Copiar capacidad");

	    b.getStylesheets().add(this.getClass().getClassLoader().getResource("application/view/Theme.css").toString());
	    b.getStyleClass().add("contextButton");
	    b.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				contexto_capacidad.hide();
				handleCopyCapacidad();
			} });
	    content.getChildren().addAll(b);
	    contexto_capacidad.getContent().add(content);
	    contexto_capacidad.setAutoHide(true);

	    contexto_restriccion = new Popup();
	    content = new VBox();
	     b = new Button("Copiar restriccion");

	    b.getStylesheets().add(this.getClass().getClassLoader().getResource("application/view/Theme.css").toString());
	    b.getStyleClass().add("contextButton");
	    b.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				contexto_restriccion.hide();
				handleCopyRestriccion();
			} });
	    content.getChildren().addAll(b);
	    contexto_restriccion.getContent().add(content);
	    contexto_capacidad.setAutoHide(true);


	    //M�todos para aocultar los popups al hacer click right
	    gridDescripcion.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {

            	if(e.getButton().toString().equals("PRIMARY")){
            		contexto_descripcion.hide();
            	}
            };
        });
	    gridObjetivo.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {
            	if(e.getButton().toString().equals("PRIMARY")){
            		contexto_objetivo.hide();
            	}
            };
        });
	    gridServicio.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {
            	if(e.getButton().toString().equals("PRIMARY")){
            		contextoServicio.hide();
            	}
            };
        });
	    gridRestriccion.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {
            	if(e.getButton().toString().equals("PRIMARY")){
            		contexto_restriccion.hide();
            	}
            };
        });
	    gridCapacidad.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {
            	if(e.getButton().toString().equals("PRIMARY")){
            		contexto_capacidad.hide();
            	}
            };
        });
	}

	@FXML
	private void showContextMenuCopyService(ContextMenuEvent event) // showContextMenu(MouseEvent event)
	{
	    contextoServicio.show(gridServicio, event.getScreenX(), event.getScreenY());
	    event.consume();
	}

	@FXML
	private void showContextMenuCopyDescripcion(ContextMenuEvent event) // showContextMenu(MouseEvent event)
	{
	    contexto_descripcion.show(gridDescripcion, event.getScreenX(), event.getScreenY());
	    event.consume();
	}

	@FXML
	private void showContextMenuCopyObjetivo(ContextMenuEvent event) // showContextMenu(MouseEvent event)
	{
	    contexto_objetivo.show(gridObjetivo, event.getScreenX(), event.getScreenY());
	    event.consume();
	}

	@FXML
	private void showContextMenuCopyCapacidad(ContextMenuEvent event) // showContextMenu(MouseEvent event)
	{
	    contexto_capacidad.show(gridCapacidad, event.getScreenX(), event.getScreenY());
	    event.consume();
	}

	@FXML
	private void showContextMenuCopyRestriccion(ContextMenuEvent event) // showContextMenu(MouseEvent event)
	{
	    contexto_restriccion.show(gridRestriccion, event.getScreenX(), event.getScreenY());
	    event.consume();
	}

	public void contextoTabla(){
		ServiceTable.setRowFactory(new Callback<TableView<Servicio>, TableRow<Servicio>>() {
	            public TableRow<Servicio> call(TableView<Servicio> tableView) {
	                final TableRow<Servicio> row = new TableRow<>();
	                final ContextMenu contextMenu = new ContextMenu();
	                final MenuItem removeMenuItem = new MenuItem("Eliminar");
	                final MenuItem copiarSMenuItem = new MenuItem("Copiar");

	                copiarSMenuItem.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent arg0) {
							handleCopyServicio();
						}
					});
	                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
	                    @Override
	                    public void handle(ActionEvent event) {
	                    	agente.getModelo_agentes().getNodo().getChildren().remove(row.getItem().getNodo());
	                    	agente.getModelo_tareas().getNodo_arbol().getChildren().remove(row.getItem().getNodo());

	                    	ServiceTable.getItems().remove(row.getItem());

	                    	vaciarForm();
	                    }
	                });
	                contextMenu.getItems().addAll(removeMenuItem, copiarSMenuItem);
	               // Set context menu on row, but use a binding to make it only show for non-empty rows:
	                row.contextMenuProperty().bind(
	                        Bindings.when(row.emptyProperty())
	                        .then((ContextMenu)null)
	                        .otherwise(contextMenu)
	                );
	                return row ;
	            }
	        });

		Tooltip toolP=new Tooltip("Propiedades del servicio");
		PropiedadesTable.setTooltip(toolP);
		PropiedadesTable.setRowFactory(new Callback<TableView<Caracteristica>, TableRow<Caracteristica>>() {
            public TableRow<Caracteristica> call(TableView<Caracteristica> tableView) {
                final TableRow<Caracteristica> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	PropiedadesTable.getItems().remove(row.getItem());
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );

                return row ;
            }
        });

		Tooltip tool=new Tooltip("Puedes ordenar los parametros haciendo drag&drop");
		ParametroEntradaTable.setTooltip(tool);
		ParametroEntradaTable.setRowFactory(new Callback<TableView<Parametros>, TableRow<Parametros>>() {
            public TableRow<Parametros> call(TableView<Parametros> tableView) {
                final TableRow<Parametros> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	ParametroEntradaTable.getItems().remove(row.getItem());
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                row.setOnDragDetected(event -> {
                    if (! row.isEmpty()) {
                        Integer index = row.getIndex();
                        Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                        db.setDragView(row.snapshot(null, null));
                        ClipboardContent cc = new ClipboardContent();
                        cc.put(Main.SERIALIZED_MIME_TYPE, index);
                        db.setContent(cc);
                        event.consume();
                    }
                });

                row.setOnDragOver(event -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasContent(Main.SERIALIZED_MIME_TYPE)) {
                        if (row.getIndex() != ((Integer)db.getContent(Main.SERIALIZED_MIME_TYPE)).intValue()) {
                            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                            event.consume();
                        }
                    }
                });

                row.setOnDragDropped(event -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasContent(Main.SERIALIZED_MIME_TYPE)) {
                        int draggedIndex = (Integer) db.getContent(Main.SERIALIZED_MIME_TYPE);
                        Parametros draggedPerson = ParametroEntradaTable.getItems().remove(draggedIndex);
                        int dropIndex ;
                        if (row.isEmpty()) {
                            dropIndex = ParametroEntradaTable.getItems().size() ;
                        } else {
                            dropIndex = row.getIndex();
                        }
                        ParametroEntradaTable.getItems().add(dropIndex, draggedPerson);
                        event.setDropCompleted(true);
                        ParametroEntradaTable.getSelectionModel().select(dropIndex);
                        event.consume();
                    }
                });
                return row ;
            }
        });

		Tooltip tool1=new Tooltip("Puedes ordenar los parametros haciendo drag&drop");
		ParametroSalidaTable.setTooltip(tool1);
		ParametroSalidaTable.setRowFactory(new Callback<TableView<Parametros>, TableRow<Parametros>>() {
            public TableRow<Parametros> call(TableView<Parametros> tableView) {
                final TableRow<Parametros> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	ParametroSalidaTable.getItems().remove(row.getItem());
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                row.setOnDragDetected(event -> {
                    if (! row.isEmpty()) {
                        Integer index = row.getIndex();
                        Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                        db.setDragView(row.snapshot(null, null));
                        ClipboardContent cc = new ClipboardContent();
                        cc.put(Main.SERIALIZED_MIME_TYPE, index);
                        db.setContent(cc);
                        event.consume();
                    }
                });

                row.setOnDragOver(event -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasContent(Main.SERIALIZED_MIME_TYPE)) {
                        if (row.getIndex() != ((Integer)db.getContent(Main.SERIALIZED_MIME_TYPE)).intValue()) {
                            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                            event.consume();
                        }
                    }
                });

                row.setOnDragDropped(event -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasContent(Main.SERIALIZED_MIME_TYPE)) {
                        int draggedIndex = (Integer) db.getContent(Main.SERIALIZED_MIME_TYPE);
                        Parametros draggedPerson = ParametroSalidaTable.getItems().remove(draggedIndex);
                        int dropIndex ;
                        if (row.isEmpty()) {
                            dropIndex = ParametroSalidaTable.getItems().size() ;
                        } else {
                            dropIndex = row.getIndex();
                        }
                        ParametroSalidaTable.getItems().add(dropIndex, draggedPerson);
                        event.setDropCompleted(true);
                        ParametroSalidaTable.getSelectionModel().select(dropIndex);
                        event.consume();
                    }
                });
                return row ;
            }
        });

		tabla_objetivos.setRowFactory(new Callback<TableView<objetivo>, TableRow<objetivo>>() {
            public TableRow<objetivo> call(TableView<objetivo> tableView) {
                final TableRow<objetivo> row = new TableRow<>();
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem removeMenuItem = new MenuItem("Eliminar");
                removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	tabla_objetivos.getItems().remove(row.getItem());
                    	vaciarForm();
                    }
                });
                contextMenu.getItems().add(removeMenuItem);
               // Set context menu on row, but use a binding to make it only show for non-empty rows:
                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                        .then((ContextMenu)null)
                        .otherwise(contextMenu)
                );
                return row ;
            }
        });
	}

	public void contextoListas(){
		TableComponentes.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();

            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar \"%s\"", cell.itemProperty()));
            deleteItem.setOnAction(event ->{ TableComponentes.getItems().remove(cell.getItem());
            								 mainApp.setSomething_has_changed(false);
            								});
            contextMenu.getItems().addAll(deleteItem);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });


		TableOentrada.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();

            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar \"%s\"", cell.itemProperty()));
            deleteItem.setOnAction(event ->{ TableOentrada.getItems().remove(cell.getItem());
            								 mainApp.setSomething_has_changed(false);
            								});
            contextMenu.getItems().addAll(deleteItem);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });

		TableOsalida.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Eliminar \"%s\"", cell.itemProperty()));
            deleteItem.setOnAction(event -> {TableOsalida.getItems().remove(cell.getItem());
            								 mainApp.setSomething_has_changed(false);
            								});
            contextMenu.getItems().addAll(deleteItem);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });

	}

	@FXML
	public void HandleAddPentrada(){
		if((!servicioPentrada.getText().equals(null)||!servicioPentrada.getText().isEmpty())&&mainApp.isValidId(servicioPentrada.getText())){

			Parametros p=new Parametros();
			p.setNombre(new SimpleStringProperty(servicioPentrada.getText()));
				if(ServicioPentradaTipo.getSelectionModel().getSelectedItem()!=null){
					p.setTipo(new SimpleStringProperty(ServicioPentradaTipo.getSelectionModel().getSelectedItem()));
					//PSentradas.add(p);
					ParametroEntradaTable.getItems().add(p);
					servicioPentrada.setText("");
					ServicioPentradaTipo.getSelectionModel().clearSelection();
					mainApp.setSomething_has_changed(false);
					mainApp.MarcarCorrecto(ServicioPentradaTipo);
					mainApp.getMensaje().set("Par�metro registrado");
				}else{
					mainApp.getMensaje().set("Por favor indique un tipo de dato");
					mainApp.MarcarError(ServicioPentradaTipo);
				}
		}else{
			mainApp.getMensaje().set("Por favor indique un nombre v�lido");
			mainApp.MarcarError(servicioPentrada);
			return;
		}
		mainApp.MarcarCorrecto(servicioPentrada);
	}

	public void seleccionar_servicio(Servicio s){
		tabpane.getSelectionModel().select(servicioTab);
		ShowServiceValues(s);
		ServiceTable.getSelectionModel().select(s);
	}

	@FXML
	public void handleAddPsalida(){
		if(!servicioPsalida.getText().equals("")||!servicioPsalida.getText().isEmpty()){
			if(mainApp.isValidId(servicioPsalida.getText())){
				Parametros p=new Parametros();
				p.setNombre(new SimpleStringProperty(servicioPsalida.getText()));
				if(ServicioPsalidaTipo.getSelectionModel().getSelectedItem()!=null){
					p.setTipo(new SimpleStringProperty(ServicioPsalidaTipo.getSelectionModel().getSelectedItem()));
					//PSsalida.add(p);
					ParametroSalidaTable.getItems().add(p);
					servicioPsalida.setText("");
					mainApp.setSomething_has_changed(false);
					ServicioPsalidaTipo.getSelectionModel().clearSelection();
					mainApp.MarcarCorrecto(ServicioPsalidaTipo);
					mainApp.getMensaje().set("Par�metro agregado");

				}else{
					mainApp.MarcarError(ServicioPsalidaTipo);
					mainApp.getMensaje().set("Por favor indique un tipo de dato");
				}
			}//fin del if valid id
		}else{
			mainApp.MarcarError(servicioPsalida);
			mainApp.getMensaje().set("Por favor indique un nombre v�lido");
			return;
		}
		mainApp.MarcarCorrecto(servicioPsalida);
	}

	public void iniciar_tabla(){
        clave.setCellValueFactory(cellData -> cellData.getValue().getClave());
        valor.setCellValueFactory(cellData -> cellData.getValue().getValor());
        valor.setVisible(true);
        clave.setVisible(true);
        tabla.setPlaceholder(new Text("No content in table"));
        tabla.setItems(filas);
        tabla.getSelectionModel().setCellSelectionEnabled(true);
        tabla.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabla.setVisible(false);
        TableUtils.installCopyPasteHandler(tabla);
	}

	private EventHandler<ActionEvent> copy_agente(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopyDescripcion();
		}};
	}

	private EventHandler<ActionEvent> copy_objetivo(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopyObjetivo();
		}};
	}

	private EventHandler<ActionEvent> copy_servicio(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopyServicio();
		}};
	}


	private EventHandler<ActionEvent> copy_capacidad(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopyCapacidad();
		}};
	}
	private EventHandler<ActionEvent> copy_restriccion(){ return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handleCopyRestriccion();
		}};
	}

	@FXML
	private void handleCopyRestriccion(){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Restricci�n del Agente",""));

		filas.add(new Fila("Normas",RestriccionNormas.getText()));
		filas.add(new Fila("Permisos",RestriccionPermisos.getText()));
		filas.add(new Fila("Preferencias",RestriccionPreferencias.getText()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Restricci�n del Agente copiada en el portapapeles");
	}
	@FXML
	private void handleCopyCapacidad(){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Capacidad del Agente",""));
		filas.add(new Fila("Habilidades",CapacidadHabilidades.getText()));
		filas.add(new Fila("Representaci�n del Conocimiento",CapacidadRepresentacion.getSelectionModel().getSelectedItem()));
		filas.add(new Fila("Lenguaje de Comunicaci�n",CapacidadLenguajeC.getSelectionModel().getSelectedItem()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Capacidad del Agente copiada en el portapapeles");
	}

	@FXML
	private void handleKeyPressed(KeyEvent ke){
		if(ke.getCode()==KeyCode.ESCAPE){
			salir_edicion();
		}
	}

	@FXML
	private void salir_edicion(){
		if(tabpane.getSelectionModel().getSelectedItem().getText().equals("Objetivos")){
			limpiar_objetivos();
			return;
		}

		vaciarForm();
		limpiar_propiedades();
		PSentradas=FXCollections.observableArrayList();
		current_propiedades=FXCollections.observableArrayList();
		PSsalida=FXCollections.observableArrayList();
		ParametroEntradaTable.setItems(PSentradas);
		ParametroSalidaTable.setItems(PSsalida);
		PropiedadesTable.setItems(FXCollections.observableArrayList());
		//PropiedadesTable.refresh();
                PropiedadesTable.setVisible(false);
                PropiedadesTable.setVisible(true);
		//ParametroEntradaTable.refresh();
                ParametroEntradaTable.setVisible(false);
                ParametroEntradaTable.setVisible(true);
		//ParametroSalidaTable.refresh();
                ParametroSalidaTable.setVisible(false);
                ParametroSalidaTable.setVisible(true);
		edicion_mode.setVisible(false);
	}

	@FXML
	private void handleCopyDescripcion(){
		filas=FXCollections.observableArrayList();

		String componentes="";
		int i=0;
		for (String string : TableComponentes.getItems()) {
			if(i!=0)
				componentes+=","+string;
			else
				componentes+=string;
			i++;
		}

		filas.add(new Fila("Modelo de Agentes",""));
		filas.add(new Fila("Agente",""));
		filas.add(new Fila("Nombre",nombre.getText()));
		filas.add(new Fila("Posici�n",AgentePosicion.getText()));
		filas.add(new Fila("Componentes",componentes));
		filas.add(new Fila("Marco de Referencia",marcoReferencia.getText()));
		filas.add(new Fila("Descripci�n del Agente",DescripcionAgente.getText()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Descripci�n de Agente copiada en el portapapeles");
	}

	@FXML
	private void handleCopyObjetivo(){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Objetivos del Agente",""));
		filas.add(new Fila("Nombre",ObjetivoNombre.getText()));
		filas.add(new Fila("Descripci�n",ObjetivosDescripcion.getText()));

		String entrada="";
		int i=0;
		for (String string : OPentradas) {
			if(i>0)
				entrada+=",";
			entrada+=string;
			i++;
		}

		String salida="";
		i=0;
		for (String string : OPsalida) {
			if(i>0)
				salida+=",";
			salida+=string;
			i++;
		}

		filas.add(new Fila("Par�metros de entrada",entrada));
		filas.add(new Fila("Par�metros de Salida",salida));
		filas.add(new Fila("Condici�n de activaci�n",ObjetivosActivacion.getText()));
		filas.add(new Fila("Condici�n de finalizaci�n",ObjetivosFinalizacion.getText()));
		filas.add(new Fila("Condici�n de �xito",ObjetivosExito.getText()));
		filas.add(new Fila("Condici�n de fracaso",ObjetivosFracaso.getText()));
		filas.add(new Fila("Ontolog�a",ObjetivosOntologia.getText()));

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Objetivos de Agente copiados en el portapapeles");
	}

	@FXML
	private void handleCopyServicio(){

		if(ServiceTable.getSelectionModel().getSelectedItem()==null){
			mainApp.getMensaje().set("Por favor debe seleccionar un servicio registrado");
			return;
		}

		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Servicio del Agente",""));

			filas.add(new Fila("Nombre",ServiceTable.getSelectionModel().getSelectedItem().getNombre().get()));
			filas.add(new Fila("Descripci�n",ServiceTable.getSelectionModel().getSelectedItem().getDescripcion().get()));
			filas.add(new Fila("Tipo",ServiceTable.getSelectionModel().getSelectedItem().getTipo().get()));
			filas.add(new Fila("Objetivo",ServiceTable.getSelectionModel().getSelectedItem().getMiObjetivo().getObjetivoNombre()));

			String x="";
			int i=0;
			for (Parametros e : ParametroEntradaTable.getItems()) {
				if(i!=0)
					x+=","+e.getNombre().get();
				else
					x+=e.getNombre().get();
				i++;
			}

			String y="";
			i=0;
			for (Parametros e : ParametroSalidaTable.getItems()) {
				if(i!=0)
					y+=","+e.getNombre().get();
				else
					y+=e.getNombre().get();
				i++;
			}

			filas.add(new Fila("Par�metros de Entrada",x));
			filas.add(new Fila("Par�metros de Salida",y));

			filas.add(new Fila("Propiedades del servicio",""));
			filas.add(new Fila("Nombre","Descripci�nP"));
			for (Caracteristica p : PropiedadesTable.getItems()) {
				filas.add(new Fila(p.getNombre().get(),p.getValor_omision().get()+"pSeparator"+p.getDescripcion().get()));
			}

		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Servicio copiado en el portapapeles");
	}

	@FXML
	private void handleCopy(){
		filas=FXCollections.observableArrayList();
		filas.add(new Fila("Modelo de Agentes",""));
		filas.add(new Fila("Agente",""));
		filas.add(new Fila("Nombre",nombre.getText()));
		filas.add(new Fila("Posici�n",AgentePosicion.getText()));
		filas.add(new Fila("Componentes",componentes.getText()));
		filas.add(new Fila("Marco de Referencia",marcoReferencia.getText()));
		filas.add(new Fila("Descripci�n del Agente",DescripcionAgente.getText()));
		filas.add(new Fila("Objetivos del Agente",""));
		filas.add(new Fila("Nombre",ObjetivoNombre.getText()));
		filas.add(new Fila("Descripci�n",ObjetivosDescripcion.getText()));
		filas.add(new Fila("Par�metros de entrada",ObjetivosEntrada.getText()));
		filas.add(new Fila("Par�metros de Salida",ObjetivosSalida.getText()));
		filas.add(new Fila("Condici�n de activaci�n",ObjetivosActivacion.getText()));
		filas.add(new Fila("Condici�n de finalizaci�n",ObjetivosFinalizacion.getText()));
		filas.add(new Fila("Condici�n de �xito",ObjetivosExito.getText()));
		filas.add(new Fila("Condici�n de fracaso",ObjetivosFracaso.getText()));
		filas.add(new Fila("Ontolog�a",ObjetivosOntologia.getText()));

		filas.add(new Fila("Servicios del Agente",""));

		for (Servicio s : agente.getModelo_agentes().getServicios()) {
			filas.add(new Fila("Nombre",s.getNombre().get()));
			filas.add(new Fila("Descripci�n",s.getDescripcion().get()));
			filas.add(new Fila("Tipo",s.getTipo().get()));

			String x="";
			for (Parametros e : s.getServicioPentrada()) {
				x+=","+e.toString();
			}

			String y="";
			for (Parametros e : s.getServicioPsalida()) {
				y+=","+e.toString();
			}
			filas.add(new Fila("Par�metros de Entrada",x));
			filas.add(new Fila("Par�metros de Salida",y));
		}

/*
		filas.add(new Fila("Propiedades del Servicio",""));
		filas.add(new Fila("Nombre",PropiedadesNombre.getText()));
		filas.add(new Fila("Calidad",PropiedadesCalidad.getText()));
		filas.add(new Fila("Auditable",PropiedadesAuditable.getText()));
		filas.add(new Fila("Garant�a",PropiedadesGarantia.getText()));
		filas.add(new Fila("Capacidad",PropiedadesCapacidad.getText()));
		filas.add(new Fila("Confiabilidad",PropiedadesConfiabilidad.getText()));
		filas.add(new Fila("Capacidad del Agente",""));
		filas.add(new Fila("Habilidades",CapacidadHabilidades.getText()));
		filas.add(new Fila("Representaci�n del Conocimiento",CapacidadRepresentacion.getSelectionModel().getSelectedItem()));
		filas.add(new Fila("Lenguaje de Comunicaci�n",CapacidadLenguaje.getText()));
		filas.add(new Fila("Restricci�n del Agente",""));
		filas.add(new Fila("Normas",RestriccionNormas.getText()));
		filas.add(new Fila("Preferencias",RestriccionPreferencias.getText()));
		filas.add(new Fila("Permisos",RestriccionPermisos.getText()));
*/
		tabla.setItems(filas);
		TableUtils.copySelectionToClipboard(tabla);
		mainApp.getMensaje().set("Modelo de Agentes copiado en el portapapeles");
	}


	private void ShowobjetivoValues(application.model.objetivo newValue) {

		if(newValue==null)
			return;

		ObjetivoNombre.setText(newValue.getObjetivoNombre());
		ObjetivosActivacion.setText(newValue.getObjetivoActivacion());
		ObjetivosDescripcion.setText(newValue.getObjetivoDescripcion());
		String entry="";
		OPentradas.setAll(newValue.getObjetivoEntrada());

		OPsalida.setAll(newValue.getObjetivoSalida());
		ObjetivosEntrada.setText("");

		ObjetivosExito.setText(newValue.getObjetivoExito());
		ObjetivosFinalizacion.setText(newValue.getObjetivoFinalizacion());
		ObjetivosFracaso.setText(newValue.getObjetivoFracaso());
		ObjetivosOntologia.setText(newValue.getObjetivoOntologia());
		current_objetivo=newValue;

		TableOsalida.setItems(FXCollections.observableArrayList(newValue.getObjetivoSalida()));
		TableOentrada.setItems(FXCollections.observableArrayList(newValue.getObjetivoEntrada()));
		ObjetivosSalida.setText("");
		guardar_objetivo.setText("Guardar Cambios");
		edicion_mode_objetivos.setVisible(true);
		ObjetivoNombre.requestFocus();
		ObjetivoNombre.setFocusTraversable(true);

		ObjetivosDescripcion.setFocusTraversable(true);

		//objetivo.requestFocus();
	}
	private void ShowServiceValues(Servicio s){
		if(s == null)
			return;
		PropiedadesTable.setItems(FXCollections.observableArrayList(s.getPropiedades()));
		edicion_mode.setVisible(true);
		ServicioNombre.setText(s.getNombre().get());
		ServiciosDescripcion.setText(s.getDescripcion().get());
		ServicioTipo.setValue(s.getTipo().get());
		comportamiento.setValue(s.getComportamiento());
		ObjetivoC.getSelectionModel().select(s.getMiObjetivo());

		PSentradas.clear();
		s.getServicioPentrada();

		for (Parametros string : s.getServicioPentrada()) {
			PSentradas.add(string);
		}

		PSsalida.clear();
		for (Parametros string : s.getServicioPsalida()) {
			PSsalida.add(string);
		}
		ParametroEntradaTable.setItems(PSentradas);
		ParametroSalidaTable.setItems(PSsalida);

		guardar.setText("Guardar Cambios");
	}//

	private void fill(){
		nombre.setText(agente.getNameString());
		marcoReferencia.setText(agente.getModelo_agentes().getAgenteMarco_referencia());
		AgentePosicion.setText(agente.getModelo_agentes().getAgenteposicion());
		compo.setAll(agente.getModelo_agentes().getAgenteComponentes());

		DescripcionAgente.setText(agente.getModelo_agentes().getAgenteDescripcion());

		CapacidadHabilidades.setText(agente.getModelo_agentes().getCapacidadHabilidades());
		CapacidadLenguajeC.getSelectionModel().select(agente.getModelo_agentes().getCapacidadLenguaje());
		CapacidadRepresentacion.getSelectionModel().select(agente.getModelo_agentes().getCapacidadRepresentacion());

		RestriccionNormas.setText(agente.getModelo_agentes().getRestriccionNormas());
		RestriccionPermisos.setText(agente.getModelo_agentes().getRestriccionPermisos());
		RestriccionPreferencias.setText(agente.getModelo_agentes().getRestriccionPreferencias());
	}

	public Main getMainApp() {
		return mainApp;
	}
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
		 CapacidadRepresentacion.setItems(mainApp.getDatabase().getR_lenguaje());
		 CapacidadLenguajeC.setItems(mainApp.getDatabase().getLenguajeComunicacion());

		 ServiceTable.setItems(agente.getModelo_agentes().getServicios());
		 ServicioPsalidaTipo.setItems(mainApp.getDatabase().getTipo_datos());
		 ServicioPentradaTipo.setItems(mainApp.getDatabase().getTipo_datos());
		 tabla_objetivos.setItems(agente.getModelo_agentes().getObjetivo());
	     ServicioTipo.setItems(mainApp.getDatabase().getServicio());
	     ObjetivoC.setItems(agente.getModelo_agentes().getObjetivo());
	     comportamiento.setItems(mainApp.getBehaviours());
	}


	private boolean ValidarModelo(AgentModel am){
		if(DescripcionAgente.getText().trim().isEmpty()||DescripcionAgente.getText() == null)
			return false;

		am.setAgenteposicion(AgentePosicion.getText());
		am.setAgenteComponentes(compo);
		//compo=FXCollections.observableArrayList();
		am.setAgenteDescripcion(DescripcionAgente.getText());
		am.setAgenteMarco_referencia(marcoReferencia.getText());
		return true;
	}

	public boolean save_restriccion(AgentModel am){
		String normas=RestriccionNormas.getText();
		String permisos=RestriccionPermisos.getText();
		String preferencias=RestriccionPreferencias.getText();
		boolean error=false;

		if(normas.isEmpty()||normas==null)
			if(permisos.isEmpty()||permisos==null)
				if(preferencias.isEmpty()||preferencias==null)
					return true;

		if(normas.isEmpty()||normas==null){
			error=true;
			mainApp.MarcarError(RestriccionNormas);
		}
		else
			mainApp.MarcarCorrecto(RestriccionNormas);

		if(permisos.isEmpty()||permisos==null){
			error=true;
			mainApp.MarcarError(RestriccionPermisos);
		}
		else
			mainApp.MarcarCorrecto(RestriccionPermisos);

		if(preferencias.isEmpty()||preferencias==null){
			error=true;
			mainApp.MarcarError(RestriccionPreferencias);
		}
		else
			mainApp.MarcarCorrecto(RestriccionPreferencias);

		if(error){
			mainApp.getMensaje().setValue("Por favor ingrese la informaci�n correctamente.");
			return false;
		}

		mainApp.setSomething_has_changed(false);
		am.setRestriccionNormas(RestriccionNormas.getText());
		am.setRestriccionPermisos(RestriccionPermisos.getText());
		am.setRestriccionPreferencias(RestriccionPreferencias.getText());

		return true;
	}

	public boolean save_capacidad(AgentModel am){
		String habilidad=CapacidadHabilidades.getText();
		String lenguaje=CapacidadLenguajeC.getSelectionModel().getSelectedItem();
		String representacion=CapacidadRepresentacion.getSelectionModel().getSelectedItem();

		boolean error=false;

		if(habilidad.isEmpty()||habilidad==null)
			if(lenguaje.isEmpty()||lenguaje==null)
				if(representacion.isEmpty()||representacion==null)
					return true;

		if(habilidad.isEmpty()||habilidad==null){
			error=true;
			mainApp.MarcarError(CapacidadHabilidades);
		}
		else
			mainApp.MarcarCorrecto(CapacidadHabilidades);

		if(lenguaje.isEmpty()||lenguaje==null){
			error=true;
			mainApp.MarcarError(CapacidadLenguajeC);
		}
		else
			mainApp.MarcarCorrecto(CapacidadLenguajeC);

		if(representacion.isEmpty()||representacion==null){
			error=true;
			mainApp.MarcarError(CapacidadRepresentacion);
		}
		else
			mainApp.MarcarCorrecto(CapacidadRepresentacion);

		if(error){
			mainApp.getMensaje().setValue("Por favor ingrese la informaci�n correctamente.");
			return false;
		}
		mainApp.setSomething_has_changed(false);
		am.setCapacidadHabilidades(habilidad);
		am.setCapacidadLenguaje(lenguaje);
		am.setCapacidadRepresentacion(representacion);
		return true;
	}

	public void SaveData(){
		AgentModel am=agente.getModelo_agentes();

		if(!ValidarModelo(am)){

		}

		am.setAgenteposicion(AgentePosicion.getText());
		am.setAgenteComponentes(compo);
		am.setAgenteDescripcion(DescripcionAgente.getText());
		am.setAgenteMarco_referencia(marcoReferencia.getText());

		save_capacidad(am);
		save_restriccion(am);
		mainApp.setSomething_has_changed(false);
		//Registrar_Servicio();
		mainApp.getMensaje().set("Modelo de Agentes Guardado");
		//mainApp.FillTree(mainApp.getRootcontroller().getArbol());

	}

	public void vaciarForm(){
		ServicioNombre.setText("");
		ServicioTipo.getSelectionModel().clearSelection();
		ServiciosDescripcion.setText("");
		PSsalida=FXCollections.observableArrayList();
		Pentradas=FXCollections.observableArrayList();
		current_propiedades=FXCollections.observableArrayList();
		PropiedadesTable.setItems(FXCollections.observableArrayList());
		PSentradas=FXCollections.observableArrayList();
		comportamiento.getSelectionModel().clearSelection();
		ServiceTable.getSelectionModel().clearSelection();
		ObjetivoC.getSelectionModel().clearSelection();
		limpiar_propiedades();
		guardar.setText("Agregar Servicio");
	}

	@FXML
	public void handleRegistrarPropiedad(){
		Caracteristica c=new Caracteristica();
		String nombre=ServicioPropiedadNombre.getEditor().getText();

		if(nombre.isEmpty()||nombre==null){
			nombre=ServicioPropiedadNombre.getSelectionModel().getSelectedItem();
		}
		else{
			ServicioPropiedadNombre.getEditor().setText("");
		}

		if(nombre==null||nombre.isEmpty()||!nombre.matches("[A-Za-z�_][A-Za-z0-9�_ ]*?")){
			mainApp.getMensaje().set("Por favor ingrese un nombre v�lido para la propiedad");
			mainApp.MarcarError(ServicioPropiedadNombre);
			return;
		}
		mainApp.MarcarCorrecto(ServicioPropiedadNombre);
		if(servicioPropiedadValor.getText()==null){
			mainApp.getMensaje().set("Por favor ingrese un valor para la propiedad");
			mainApp.MarcarError(servicioPropiedadValor);
			return;
		}
		mainApp.MarcarCorrecto(servicioPropiedadValor);

		c.setNombre(new SimpleStringProperty(nombre));
		c.setValor_omision(new SimpleStringProperty(servicioPropiedadValor.getText()));
		c.setDescripcion(new SimpleStringProperty(descripcion_Propiedad.getText()));
		PropiedadesTable.getItems().add(c);
		limpiar_propiedades();
		mainApp.setSomething_has_changed(false);
		mainApp.getMensaje().set("Propiedad registrada exitosamente");
	}

	public void limpiar_propiedades(){
		ServicioPropiedadNombre.getSelectionModel().clearSelection();
		servicioPropiedadValor.setText("");
		descripcion_Propiedad.setText("");
	}

	@FXML
	public void registrar_objetivo(){

		boolean error=false;
		if(ObjetivoNombre.getText().isEmpty()||ObjetivoNombre.getText()==null){
			error=true;
			mainApp.MarcarError(ObjetivoNombre);
		}

		if(ObjetivosDescripcion.getText().isEmpty()||ObjetivosDescripcion.getText()==null){
			error=true;
			mainApp.MarcarError(ObjetivosDescripcion);
		}

		if(ObjetivosActivacion.getText().isEmpty()||ObjetivosActivacion.getText()==null){
			error=true;
			mainApp.MarcarError(ObjetivosActivacion);
		}

		if(ObjetivosExito.getText().isEmpty()||ObjetivosExito.getText()==null){
			error=true;
			mainApp.MarcarError(ObjetivosExito);
		}

		if(ObjetivosFinalizacion.getText().isEmpty()||ObjetivosFinalizacion.getText()==null){
			error=true;
			mainApp.MarcarError(ObjetivosFinalizacion);
		}

		if(ObjetivosFracaso.getText().isEmpty()||ObjetivosFracaso.getText()==null){
			error=true;
			mainApp.MarcarError(ObjetivosFracaso);
		}

		if(error){
			mainApp.getMensaje().set("Porfavor corrija los campos marcados en rojo");
			return;
		}

		mainApp.MarcarCorrecto(ObjetivoNombre);
		mainApp.MarcarCorrecto(ObjetivosDescripcion);
		mainApp.MarcarCorrecto(ObjetivosActivacion);
		mainApp.MarcarCorrecto(ObjetivosExito);
		mainApp.MarcarCorrecto(ObjetivosFinalizacion);
		mainApp.MarcarCorrecto(ObjetivosFracaso);
		current_objetivo.setObjetivoNombre(ObjetivoNombre.getText());
		current_objetivo.setObjetivoActivacion(ObjetivosActivacion.getText());
		current_objetivo.setObjetivoDescripcion(ObjetivosDescripcion.getText());

		current_objetivo.setObjetivoEntrada(TableOentrada.getItems());
		current_objetivo.setObjetivoSalida(TableOsalida.getItems());

		current_objetivo.setObjetivoExito(ObjetivosExito.getText());
		current_objetivo.setObjetivoFinalizacion(ObjetivosFinalizacion.getText());
		current_objetivo.setObjetivoFracaso(ObjetivosFracaso.getText());
		current_objetivo.setObjetivoOntologia(ObjetivosOntologia.getText());
		current_objetivo.setObjetivoSalida(TableOsalida.getItems());
		current_objetivo.setObjetivoEntrada(TableOentrada.getItems());
		mainApp.setSomething_has_changed(true);

		if(guardar_objetivo.getText().equals("Registrar objetivo")){
			agente.getModelo_agentes().getObjetivo().add(current_objetivo);
			current_objetivo=new objetivo();
			mainApp.getMensaje().set("Objetivo registrado existosamente");
		}
		else{
			mainApp.getMensaje().set("Objetivo actualizado existosamente");
			guardar_objetivo.setText("Registrar objetivo");
		}
		scrollObjetivo.setVvalue(scrollObjetivo.getVmin());
		limpiar_objetivos();
		//tabla_objetivos.refresh();
                tabla_objetivos.setVisible(false);
                tabla_objetivos.setVisible(true);
	}

	public void limpiar_objetivos(){
		ObjetivoNombre.setText("");
		ObjetivosActivacion.setText("");
		ObjetivosDescripcion.setText("");
		current_objetivo=new objetivo();
		String entry="";
		OPentradas.setAll(FXCollections.observableArrayList());
		OPsalida.setAll(FXCollections.observableArrayList());
		ObjetivosEntrada.setText("");
		ObjetivosExito.setText(current_objetivo.getObjetivoExito());
		ObjetivosFinalizacion.setText(current_objetivo.getObjetivoFinalizacion());
		ObjetivosFracaso.setText(current_objetivo.getObjetivoFracaso());
		ObjetivosOntologia.setText(current_objetivo.getObjetivoOntologia());
		TableOentrada.setItems(current_objetivo.getObjetivoEntrada());
		TableOsalida.setItems(current_objetivo.getObjetivoSalida());
		tabla_objetivos.getSelectionModel().clearSelection();
		ObjetivosSalida.setText("");
		guardar_objetivo.setText("Registrar objetivo");
		edicion_mode_objetivos.setVisible(false);
	}

	@FXML
	public void Registrar_Servicio(){
		Servicio temporal=new Servicio();
		boolean error=false;

		if(edicion_mode.isVisible()){
			temporal=ServiceTable.getSelectionModel().getSelectedItem();
		}

		//comprobar que el nombre del servicio est� correcto
		if(!mainApp.isValidId(ServicioNombre.getText())){
					mainApp.MarcarError(ServicioNombre);
					mainApp.getMensaje().set("El nombre del servicio s�lo puede contener letras y n�meros. Debe comenzar con una letras");
					return;
		}//fin

		if(ServicioNombre.getText().equals("")||ServicioNombre.getText().equals(null)){
			error=true;
			mainApp.MarcarError(ServicioNombre);
		}
		else
			mainApp.MarcarCorrecto(ServicioNombre);


		if(ServiciosDescripcion.getText().equals("")||ServiciosDescripcion.getText().equals(null)){
			mainApp.MarcarError(ServiciosDescripcion);
			error=true;
		}
		else
			mainApp.MarcarCorrecto(ServiciosDescripcion);

		if(ServicioTipo.getSelectionModel().getSelectedItem()==null){
			error=true;
			mainApp.MarcarError(ServicioTipo);
		}
		else if(ServicioTipo.getSelectionModel().getSelectedItem().equals("")){
			error=true;
			mainApp.MarcarError(ServicioTipo);
		}
		else
			mainApp.MarcarCorrecto(ServicioTipo);

		if(ObjetivoC.getSelectionModel().getSelectedItem()==null){
			error=true;
			mainApp.MarcarError(ObjetivoC);
		}
		else
			mainApp.MarcarCorrecto(ObjetivoC);

		if(error){
			mainApp.getMensaje().set("Por favor corrija los campos marcados en rojo");
			return;
		}

		temporal.setNombre(ServicioNombre.getText());
		temporal.setDescripcion(ServiciosDescripcion.getText());
		temporal.setMiObjetivo(ObjetivoC.getSelectionModel().getSelectedItem());

		temporal.setTipo(ServicioTipo.getSelectionModel().getSelectedItem());
		temporal.setMiObjetivo(ObjetivoC.getSelectionModel().getSelectedItem());
		temporal.setServicioPentrada(ParametroEntradaTable.getItems());
		temporal.setPropiedades(PropiedadesTable.getItems());
		PSentradas=FXCollections.observableArrayList();
		ParametroEntradaTable.setItems(PSentradas);
		//ParametroEntradaTable.refresh();
                ParametroEntradaTable.setVisible(false);
                ParametroEntradaTable.setVisible(true);
		temporal.setPropiedades(PropiedadesTable.getItems());

		temporal.setServicioPsalida(ParametroSalidaTable.getItems());
		PSsalida=FXCollections.observableArrayList();
		ParametroSalidaTable.setItems(PSsalida);
		//ParametroSalidaTable.refresh();
                ParametroSalidaTable.setVisible(false);
                ParametroSalidaTable.setVisible(true);

		temporal.setComportamiento(comportamiento.getSelectionModel().getSelectedItem());
		mainApp.setSomething_has_changed(false);
			if(edicion_mode.isVisible()){
				mainApp.getMensaje().set("Servicio Actualizado");
				vaciarForm();
				edicion_mode.setVisible(false);
				guardar.setText("Agregar Servicio");
				ServiceTable.getSelectionModel().clearSelection();
				//ServiceTable.refresh();
                                ServiceTable.setVisible(false);
                                ServiceTable.setVisible(true);
				return;
			}//update


			agente.getModelo_tareas().getNodo_servicios().getChildren().add(temporal.getNodo());
			agente.getModelo_agentes().getServicios().add(temporal);
			vaciarForm();
			mainApp.getMensaje().set("Servicio Registrado");
			mainApp.FillTree(mainApp.getRootcontroller().getArbol());
	}

	@FXML
	public void HandleNext(){
		SaveData();

		FXMLLoader loader = new FXMLLoader();

        loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
			controller.setMainApp(mainApp);
			Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
			String Tabname=agente.getNameString()+"-M. de Tareas";
			current.setText(Tabname);
	        current.setContent(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void HandleComunicaciones(){
		SaveData();

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CommunicationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CommunicationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainApp);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Comunicaciones";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void HandleCoordinacion(){
		SaveData();

		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(mainApp);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
            String Tabname=agente.getNameString()+"-M. de Coordinaci�n";
			current.setText(Tabname);
	        current.setContent(page);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void HandleInteligencia(){
		SaveData();
		mainApp.showIntelligenceModel(getAgente(), new MarcoOntologicoIndividual());
	}


	@FXML
	public void HandleBack(){
		FXMLLoader loader = new FXMLLoader();
		SaveData();
        loader.setLocation(Main.class.getResource("view/AddForm.fxml"));
		try {

			AnchorPane page= (AnchorPane) loader.load();
			addAgentController controller = loader.getController();
            controller.setMainApp(mainApp);
            Tab current= mainApp.getRootcontroller().getTabs().getSelectionModel().getSelectedItem();
	        current.setText("Nuevo Agente");
	        current.setContent(page);
            //mainApp.getRootcontroller().getDerecho().setCenter(page);
            //mainApp.getRootLayout().setCenter(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public agent getAgente() {
		return agente;
	}

	public void setAgente(agent agente,Boolean isNew) {
		this.agente = agente;
		titulo.setText(agente.getNameString()+" - Modelo de Agentes");
		nombre.setText(agente.getNameString());
		if(!isNew)
			fill();
		//ServiceTable=new TableView<Servicio>(this.agente.getModelo_agentes().getServicios());
	}

	@FXML
	private void handleAddComponente(){
		compo.add(componentes.getText());
		componentes.setText("");
	}

	@FXML
	private void handleAddOPentrada(){
		if(!ObjetivosEntrada.getText().isEmpty()&&mainApp.isValidId(ObjetivosEntrada.getText())){
			TableOentrada.getItems().add(ObjetivosEntrada.getText());
			mainApp.MarcarCorrecto(ObjetivosEntrada);
		}else{
			mainApp.MarcarError(ObjetivosEntrada);
			mainApp.getMensaje().set("Por favor indique un nombre v�lido");
			}
		ObjetivosEntrada.setText("");
	}

	@FXML
	private void handleAddOPSalida(){
		if(!ObjetivosSalida.getText().isEmpty()&&mainApp.isValidId(ObjetivosSalida.getText())){

			TableOsalida.getItems().add(ObjetivosSalida.getText());
			mainApp.MarcarCorrecto(ObjetivosSalida);
		}else{
			mainApp.MarcarError(ObjetivosSalida);
			mainApp.getMensaje().set("Por favor indique un nombre v�lido");
			}
		ObjetivosSalida.setText("");
	}


	public Label getNombre() {
		return nombre;
	}

	public void setNombre(Label nombre) {
		this.nombre = nombre;
	}

	public Label getTitulo() {
		return titulo;
	}

	public void setTitulo(Label titulo) {
		this.titulo = titulo;
	}
}

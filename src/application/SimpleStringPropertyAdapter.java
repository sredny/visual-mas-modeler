package application;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import javafx.beans.property.SimpleStringProperty;

public class SimpleStringPropertyAdapter extends XmlAdapter<String, SimpleStringProperty> {
		@Override
		public SimpleStringProperty unmarshal(String v) throws Exception {
		    return new SimpleStringProperty(v);
		}

		@Override
		public String marshal(SimpleStringProperty v) throws Exception {
		    if(null == v) {
		        return null;
		    }
		    return v.get(); // Or whatever the correct method is
		}
}
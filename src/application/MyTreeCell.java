package application;
import java.io.IOException;
import java.util.Optional;

import org.xml.sax.SAXException;

import application.UML.estado;
import application.model.ActoHabla;
import application.model.Caso_uso;
import application.model.MarcoOntologicoColectivo;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.MecanismoRazonamiento;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalC;
import application.model.OntologiaSituacionalI;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;
import application.view.AgentModelController;
import application.view.DetailsController;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class MyTreeCell extends TreeCell<Object>{

    private ContextMenu addMenu = new ContextMenu();
    private Main mainapp;


    public MyTreeCell(){

    }

    public MyTreeCell(Main main) {
    	mainapp=main;
	}

	@Override
    public void updateItem( Object item, boolean empty ){
		addMenu=new ContextMenu();
		super.updateItem( item, empty );
        if ( empty ) {
        	getStyleClass().add("treeview-empty");
            setText( null );
            setGraphic(null);
        }
        else{
            setText( getDisplayText( item ) );
            //addMenuItem.setText( getContextMenuText( item ) );

            if(item instanceof String){

            	if(item.equals("Modelo de inteligencia Colectivo"))
            		getStyleClass().add("treeview-nodoRaiz");

            	if(item.equals("Agentes")){
            		MenuItem nuevo= new MenuItem("Nuevo agente");
            		addMenu.getItems().add(nuevo);
            		nuevo.setOnAction(nuevo_agente());
            		getStyleClass().add("treeview-nodoRaiz");
            	}//es la raiz

            	if(item.equals("Diagrama de Actividades")){
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/ActivityIcon.png"))));
            	}//es modelo de actividades
            	if(item.equals("Modelos")){
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/parallel_tasks-16.png"))));
            	}
            	if(item.equals("Diagrama de Casos de Uso")){

            		agent a=mainapp.getParentOfNewDiagram((String)item);
            		MenuItem UML= new MenuItem("Importar Casos de Uso");
            		MenuItem nuevo= new MenuItem("Nuevo Caso de Uso");
            		nuevo.setOnAction(nuevo_Caso(1));
            		UML.setOnAction(UML(1));
            		addMenu.getItems().addAll(nuevo,UML);
            	}

            	if(item.equals("Modelo de inteligencia Colectivo")){
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/Collective.png"))));
            	}
            }//es string

            if ( item instanceof AgentModel){
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/agentIcon.png"))));
            }

            if ( item instanceof estado){
            	estado e=((estado)item);
            	if(e.getTipo().equals("initial"))
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/InitialState.png"))));
            	if(e.getTipo().equals("UML:ActionState"))
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/activity.png"))));
            	if(e.getTipo().equals("branch"))
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/branch.png"))));
            	if(e.getTipo().equals("UML:SubactivityState"))
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/subtask.png"))));
            	if(e.getTipo().equals("UML:FinalState"))
            		setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/finalState.png"))));
            }

            if(item instanceof TareaModel){
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/JobModel.png"))));
            }

            if(item instanceof Tarea){
            	MenuItem t= new MenuItem("Copiar tarea");
        		t.setOnAction(copiar_Tarea((Tarea)item));

        		MenuItem delete= new MenuItem("Eliminar tarea");
        		delete.setOnAction(eliminar_Tarea((Tarea)item));

        		addMenu.getItems().addAll(t,delete);
            }

            if(item instanceof CommunicationModel){
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/CommunicationIcon.png"))));
            }

            if(item instanceof ActoHabla){
            	MenuItem t= new MenuItem("Copiar Acto de Habla");
        		t.setOnAction(copiar_AH((ActoHabla)item));
        		addMenu.getItems().addAll(t);
            }

            if(item instanceof CoordinationModel){
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/Link.png"))));
            }

            if(item instanceof conversacion){
            	MenuItem t= new MenuItem("Copiar Conversacion");
        		t.setOnAction(copiar_conversacion((conversacion)item));
        		addMenu.getItems().addAll(t);
            }

            if ( item instanceof ObservableList<?>){
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/inteligencia_16.png"))));
            }

            if (item instanceof Caso_uso){
            	MenuItem t= new MenuItem("Copiar Caso de uso");
        		t.setOnAction(copiar_CasoUso((Caso_uso)item));
        		MenuItem t2= new MenuItem("Eliminar Caso de uso");
        		t2.setOnAction(eliminar_CasoUso((Caso_uso)item));

        		addMenu.getItems().addAll(t,t2);
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/Use-case.png"))));
            }

            if(item instanceof Servicio){
            	MenuItem eliminar=new MenuItem("Eliminar");
            	MenuItem Agregar_Tarea=new MenuItem("Agregar Tarea");
            	MenuItem copiar=new MenuItem("Copiar servicio");
            	MenuItem relacion=new MenuItem("Copiar relacion Servicio-Tareas");

            	relacion.setOnAction(copiar_relacion((Servicio)item));
            	Agregar_Tarea.setOnAction(ServicioAgregarTarea((Servicio)item));
            	eliminar.setOnAction(ServicioEliminar((Servicio)item));
            	copiar.setOnAction(ServicioCopiar((Servicio)item));
            	addMenu.getItems().addAll(eliminar,Agregar_Tarea,relacion,copiar);
            }

            if(item instanceof agent){
            	MenuItem eliminar=new MenuItem("Eliminar");
            	MenuItem editar= new MenuItem("Editar");
        		editar.setOnAction(editar((agent)item));

            	MenuItem MA= new MenuItem("Agregar M. de Agentes");
        		MA.setOnAction(nuevo_MA((agent)item));
        		MenuItem CMA= new MenuItem("Copiar M. de Agentes");
        		CMA.setOnAction(copiar_MA((agent)item));

        		MenuItem MT= new MenuItem("Agregar M. de Tareas");
        		MT.setOnAction(nuevo_MT((agent)item));
        		MenuItem MC= new MenuItem("Agregar M. de Comunicaciones");
        		MC.setOnAction(nuevo_MC((agent)item));
        		MenuItem MCo= new MenuItem("Agregar M. de Coordinaci�n");
        		MCo.setOnAction(nuevo_MCo((agent)item));
        		MenuItem MI= new MenuItem("Agregar M. de Inteligencia");
        		MI.setOnAction(nuevo_MI((agent)item));
        		MenuItem UML= new MenuItem("Importar Casos de Uso");
        		UML.setOnAction(UML(0));
        		MenuItem nuevo= new MenuItem("Nuevo Caso de Uso");
        		nuevo.setOnAction(nuevo_Caso(0));
            	eliminar.setOnAction(eliminar_agente(item));

            	addMenu.getItems().addAll(eliminar,editar,new SeparatorMenuItem(),MA,MT,MC,MCo,MI,new SeparatorMenuItem(),nuevo,UML,CMA);
            	setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/icons/spy-16.png"))));
            }//es agente


            if(item instanceof MarcoOntologicoIndividual){
            	MenuItem copiar=new MenuItem("Copiar Marco ontol�gico en el portapapeles");
            	MenuItem eliminar=new MenuItem("Eliminar Marco Ontol�gico");

            	copiar.setOnAction(copiar_MarcoOntologicoIndividual((MarcoOntologicoIndividual)item));
            	eliminar.setOnAction(eliminar_MarcoOntologicoI((MarcoOntologicoIndividual)item));
            	addMenu.getItems().addAll(copiar,eliminar);
            }


            if(item instanceof OntologiaSituacionalI){
            	MenuItem copiar=new MenuItem("Copiar Conocimiento estrat�gico en el portapapeles");
            	MenuItem copiarT=new MenuItem("Copiar Conocimiento de tareas en el portapapeles");
            	MenuItem eliminar=new MenuItem("Eliminar ontolog�a");

            	copiar.setOnAction(copiar_ConocimientoEstrategico((OntologiaSituacionalI)item));
            	copiarT.setOnAction(copiar_ConocimientoTareas((OntologiaSituacionalI)item));
            	eliminar.setOnAction(eliminar_ontologiaSituacionalI((OntologiaSituacionalI)item));
            	addMenu.getItems().addAll(copiar,copiarT,eliminar);
            }

            if(item instanceof OntologiaHistorica){
            	MenuItem copiar=new MenuItem("Copiar en el portapapeles");
            	MenuItem eliminar=new MenuItem("Eliminar ontolog�a");

            	copiar.setOnAction(copiar_OntologiaHistorica((OntologiaHistorica)item));
            	eliminar.setOnAction(eliminar_ontologiaHistorica((OntologiaHistorica)item));
            	addMenu.getItems().addAll(copiar,eliminar);
            }

            if(item instanceof OntologiaDominio){
            	MenuItem copiar=new MenuItem("Copiar en el portapapeles");
            	MenuItem eliminar=new MenuItem("Eliminar ontolog�a");

            	copiar.setOnAction(copiar_OntologiaDominio((OntologiaDominio)item));
            	eliminar.setOnAction(eliminar_ontologiaDominio((OntologiaDominio)item));
            	addMenu.getItems().addAll(copiar,eliminar);
            }

            if(item instanceof MecanismoAprendizaje){
            	MenuItem copiar=new MenuItem("Copiar en el portapapeles");

            	copiar.setOnAction(copiar_MecanismoAprendizaje((MecanismoAprendizaje)item));
            	addMenu.getItems().addAll(copiar);
            }

            if(item instanceof MecanismoRazonamiento){
            	MenuItem copiar=new MenuItem("Copiar en el portapapeles");

            	copiar.setOnAction(copiar_MecanismoRazonamiento((MecanismoRazonamiento)item));
            	addMenu.getItems().addAll(copiar);
            }

            if(item instanceof MarcoOntologicoColectivo){
            	MenuItem copiar=new MenuItem("Copiar en el portapapeles");
            	MenuItem eliminar=new MenuItem("Eliminar Marco ontol�gico");

            	copiar.setOnAction(copiar_moc((MarcoOntologicoColectivo)item));
            	eliminar.setOnAction(eliminar_moc((MarcoOntologicoColectivo)item));
            	addMenu.getItems().addAll(copiar,eliminar);
        	}

        	if(item instanceof OntologiaSituacionalC){
        		MenuItem copiar=new MenuItem("Copiar en el portapapeles");
            	MenuItem eliminar=new MenuItem("Eliminar ontolog�a");

            	copiar.setOnAction(copiar_OntologiaSituacionalC((OntologiaSituacionalC)item));
            	eliminar.setOnAction(eliminar_ontologiaSituacionalC((OntologiaSituacionalC)item));
            	addMenu.getItems().addAll(copiar,eliminar);
        	}

            setContextMenu(addMenu);
            //setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/images/A.png"))));
        }

    }


private EventHandler<ActionEvent> eliminar_Tarea(Tarea item) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				agent a=(agent) mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem().getParent().getParent().getParent().getParent().getValue();
				a.getModelo_tareas().getTareas().remove(item);
				Servicio s= (Servicio) mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem().getParent().getValue();
				s.getNodo().getChildren().remove(item.getNodo_arbol());

			}
		};
	}

private EventHandler<ActionEvent> eliminar_CasoUso(Caso_uso item) {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				agent a= (agent) mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem().getParent().getParent().getValue();
				a.getCasos_uso().remove(item);
				a.getCasos_Uso_nodo().getChildren().remove(item.getNodo_arbol());

				for (Tab mytab : mainapp.getRootcontroller().getTabs().getTabs()) {
					if(mytab.getText().contains(item.getNombre())){
						mainapp.getRootcontroller().getTabs().getTabs().remove(mytab);
					}

				}
				mainapp.getMensaje().set("Caso de uso eliminado satisfactoriamente");
			}
		};
	}

private String getDisplayText( Object item ){

	if(item instanceof MarcoOntologicoIndividual){
		return ((MarcoOntologicoIndividual)item).getNombre().get();
	}

	if(item instanceof OntologiaSituacionalI){
		return ("Ontolog�a Situacional Individual");
	}

	if(item instanceof OntologiaHistorica){
		return ("Ontolog�a Hist�rica");
	}

	if(item instanceof OntologiaDominio){
		return ("Ontolog�a de Dominio");
	}

	if(item instanceof MecanismoAprendizaje){
		return ("Mecanismo de Aprendizaje");
	}

	if(item instanceof MecanismoRazonamiento){
		return ("Mecanismo de Razonamiento");
	}

	if(item instanceof MarcoOntologicoColectivo){
		return (((MarcoOntologicoColectivo) item).getNombre().get());
	}

	if(item instanceof OntologiaSituacionalC){
		return ("O. situacional");
	}

	if ( item instanceof String ){
        return (( String ) item);
    }
	if ( item instanceof AgentModel ){
        return ("Modelo de Agente");
    }
	if ( item instanceof Servicio ){
        return ((Servicio)item).getNombre().get();
    }
	if ( item instanceof TareaModel ){
        return ("Modelo de Tareas");
    }
	if(item instanceof Tarea){
		return ((Tarea)item).getNombre().get();
	}
	if ( item instanceof CommunicationModel ){
        return ("Modelo de Comunicaciones");
    }
	if ( item instanceof ActoHabla ){
        return ((ActoHabla)item).getNombre().get();
    }
	if ( item instanceof CoordinationModel ){
        return ("Modelo de Coordinacion");
    }
	if ( item instanceof conversacion ){
        return ((conversacion)item).getNombre().get();
    }
    if ( item instanceof agent ){
        return (( agent ) item).getNameString();
    }
    if ( item instanceof estado ){
        return (( estado ) item).getName();
    }
    else if ( item instanceof Caso_uso ){
        return (( Caso_uso ) item).getNombre();
    }
    if ( item instanceof MarcoOntologicoIndividual){
        return ((( MarcoOntologicoIndividual ) item).getNombre().get());
    }
    if(item instanceof ObservableList<?>){
    	return("Modelo de Inteligencia");
    }
    else{
        return null;
    }
}

public void setMainapp(Main mainapp) {
	this.mainapp = mainapp;
}



private EventHandler<ActionEvent> editar(agent item) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				TreeItem<Object> nodo=mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
				String oldname=item.getNameString();
				TextInputDialog dialog = new TextInputDialog("");
				dialog.setTitle("Edici�n");
				dialog.setHeaderText("Nuevo nombre para el agente");
				dialog.setContentText("Por favor ingrese un nuevo nombre para el agente (*):");

				dialog.getEditor().setPromptText(item.getNameString());
				final Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);

				// Traditional way to get the response value.
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()){

					String nombre=result.get();
					if(nombre.length()==0){
						mainapp.getMensaje().set("Por favor indique un nombre v�lido. El nombre especificado no puede ser vac�o");
						dialog.setHeaderText("Por favor indique un nombre v�lido. El nombre especificado no puede ser vac�o");
						dialog.getEditor().setPromptText("");
						mainapp.MarcarError(dialog.getEditor());
						dialog.showAndWait();
					}

					else if(mainapp.isUnique(nombre)||nombre.equals(item.getNameString())){
						item.getName().set(nombre);
						mainapp.setSomething_has_changed(true);

						//mainapp.getRootcontroller().getArbol().refresh();
                                                mainapp.getRootcontroller().getArbol().setVisible(false);
                                                mainapp.getRootcontroller().getArbol().setVisible(true);
						for (Tab tab : mainapp.getRootcontroller().getTabs().getTabs()) {
							String tabName=tab.getText();
							tab.setText(tabName.replace(oldname,nombre));

							//actualizar etiquetas
							if(mainapp.getAgents_controller().containsKey(tab.getText())){
								AgentModelController c=mainapp.getAgents_controller().get(tab.getText());
								c.getTitulo().setText(c.getTitulo().getText().replace(c.getNombre().getText(), nombre));
								c.getNombre().setText(nombre);
							}
							if(mainapp.getUCS_controller().containsKey(tab.getText())){
								DetailsController c=mainapp.getUCS_controller().get(tab.getText());
								c.getTitulo().setText(nombre+" - "+c.getCaso().getNombre()+" Descripci�n de Caso de Uso");
							}
						}//fin del for
					}
					else{
						dialog.setHeaderText("Por favor indique un nombre v�lido. El nombre especificado ya se encuenta registrado");
						dialog.getEditor().setPromptText("");
						mainapp.getMensaje().set("Por favor indique un nombre v�lido. El nombre especificado ya se encuenta registrado");
						mainapp.MarcarError(dialog.getEditor());
						dialog.showAndWait();
						}

					//mainapp.FillTree(mainapp.getRootcontroller().getArbol());
				}
			}
		};
	}

private EventHandler<ActionEvent> eliminar_ontologiaSituacionalC(OntologiaSituacionalC item) {
	return new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent e) {
        	mainapp.setSomething_has_changed(false);
            TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
            boolean remove = c.getParent().getChildren().remove(c);
     //falta eliminarlo del objeto
        }
	};
}

private EventHandler<ActionEvent> copiar_OntologiaSituacionalC(OntologiaSituacionalC item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyOntologiaSituacionalC(item);
		}
	};
}

private EventHandler<ActionEvent> eliminar_moc(MarcoOntologicoColectivo item) {
	return new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent e) {
        	mainapp.setSomething_has_changed(false);
            TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
            boolean remove = c.getParent().getChildren().remove(c);
     //falta eliminarlo del objeto
        }
	};
}

private EventHandler<ActionEvent> copiar_moc(MarcoOntologicoColectivo item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyMarcoOntologicoC(item);
		}
	};
}

private EventHandler<ActionEvent> eliminar_ontologiaDominio(OntologiaDominio item) {
	// TODO Auto-generated method stub
	return new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent e) {
        	mainapp.setSomething_has_changed(false);
        	TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
            boolean remove = c.getParent().getChildren().remove(c);
     //falta eliminarlo del objeto
        }
	};
}

private EventHandler<ActionEvent> eliminar_ontologiaHistorica(OntologiaHistorica item) {
	// TODO Auto-generated method stub
		return new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent e) {
	        	mainapp.setSomething_has_changed(false);
	        	TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
	            boolean remove = c.getParent().getChildren().remove(c);
	     //falta eliminarlo del objeto
	        }
		};
}

private EventHandler<ActionEvent> eliminar_ontologiaSituacionalI(OntologiaSituacionalI item) {
	// TODO Auto-generated method stub
		return new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent e) {
	        	mainapp.setSomething_has_changed(false);
	        	TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
	            boolean remove = c.getParent().getChildren().remove(c);
	     //falta eliminarlo del objeto
	        }
		};
}

private EventHandler<ActionEvent> eliminar_MarcoOntologicoI(MarcoOntologicoIndividual item) {
	// TODO Auto-generated method stub
		return new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent e) {
	        	mainapp.setSomething_has_changed(false);
	        	TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
	            boolean remove = c.getParent().getChildren().remove(c);
	     //falta eliminarlo del objeto
	        }
		};
	}

private EventHandler<ActionEvent> copiar_MarcoOntologicoIndividual(MarcoOntologicoIndividual item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyMarcoOntologicoI(item);
		}
	};
}


private EventHandler<ActionEvent> copiar_OntologiaDominio(OntologiaDominio item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyOntologiaDominio(item);
		}
	};
}


private EventHandler<ActionEvent> copiar_OntologiaHistorica(OntologiaHistorica item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyOntologiaHistorica(item);
		}
	};
}


private EventHandler<ActionEvent> copiar_ConocimientoEstrategico(OntologiaSituacionalI item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyConocimientoEstrategico(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_ConocimientoTareas(OntologiaSituacionalI item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyConocimientoTareas(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_MA(agent item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopy(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_MecanismoAprendizaje(MecanismoAprendizaje item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyMecanismoAprendizaje(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_MecanismoRazonamiento(MecanismoRazonamiento item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyMecanismoRazonamiento(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_Tarea(Tarea item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyTareaModel(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_CasoUso(Caso_uso item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyUseCase(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_conversacion(conversacion item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handleCopyCoordinationModelConversacion(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_AH(ActoHabla item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			mainapp.getRootcontroller().handlecopy_ActoHabla(item);
		}
	};
}

private EventHandler<ActionEvent> copiar_relacion(Servicio item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			agent agente=mainapp.getParentOf(item.getId());
			if(agente!=null)
				mainapp.getRootcontroller().handleCopyRelacion(item, agente);
		}
	};
}



private EventHandler<ActionEvent> ServicioCopiar(Servicio item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
				mainapp.getRootcontroller().handleCopyServicio(item);
		}
	};
}


private EventHandler<ActionEvent> nuevo_Caso(Integer levels) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
    		agent agente=null;

    		if(levels==0)
    			agente=(agent) c.getValue();
    		if(levels==1)
    			agente=(agent) c.getParent().getValue();

			TextInputDialog dialog = new TextInputDialog("");
			dialog.setTitle("Caso de Uso");
			dialog.setHeaderText("Nuevo Caso de uso");
			dialog.setContentText("Por favor ingrese un nombre de caso de uso (*):");

			// Traditional way to get the response value.
			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()){

				String nombre=result.get();
				if(nombre.length()==0)
					return;
				Caso_uso new_uc=new Caso_uso(nombre);
				agente.getCasos_uso().add(new_uc);
				agente.getCasos_Uso_nodo().getChildren().add(new_uc.getNodo_arbol());
				//mainapp.FillTree(mainapp.getRootcontroller().getArbol());
				mainapp.setSomething_has_changed(false);
				mainapp.showUCDetails(new_uc,agente);
			}

		}
	};
}



private EventHandler<ActionEvent> ServicioAgregarTarea(Servicio item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			agent agente=mainapp.getParentOf(item.getId());
			if(agente!=null){
				mainapp.showTareasModel(agente,item);
			}
}};}

private EventHandler<ActionEvent> ServicioEliminar(Servicio item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			agent agente=mainapp.getParentOf(item.getId());
			if(agente!=null){
				mainapp.setSomething_has_changed(false);
				agente.getModelo_agentes().getServicios().remove(item);
				mainapp.FillTree(mainapp.getRootcontroller().getArbol());
			}
}};}

private EventHandler<ActionEvent> RelacionServicioTareas(Servicio item) {
	return new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			agent agente=mainapp.getParentOf(item.getId());
			if(agente!=null){
				agente.getModelo_agentes().getServicios().remove(item);
				mainapp.FillTree(mainapp.getRootcontroller().getArbol());
			}
}};}


private EventHandler<ActionEvent> eliminar_agente(Object item){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		mainapp.getAgentData().remove(item);
		mainapp.getRaiz().getChildren().remove(((agent)item).getNodo_arbol());
		mainapp.setSomething_has_changed(true);
		//envio el item para q lo borren de la lista
	}};}

private EventHandler<ActionEvent> nuevo_agente(){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		try {
			mainapp.showAddAgentDialog();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}};}


private EventHandler<ActionEvent> nuevo_MA(agent agente){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		/*Tab nueva=new Tab(agente.getNameString()+"-M. de Agentes");
		nueva.setClosable(true);
		mainapp.getRootcontroller().getTabs().getTabs().add(nueva);
		mainapp.getRootcontroller().getTabs().getSelectionModel().select(nueva);*/
		mainapp.showAgentModel(agente);
	}};}

private EventHandler<ActionEvent> nuevo_MT(agent agente){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		/*Tab nueva=new Tab(agente.getNameString()+"-M. de Tareas");
		nueva.setClosable(true);
		mainapp.getRootcontroller().getTabs().getTabs().add(nueva);
		mainapp.getRootcontroller().getTabs().getSelectionModel().select(nueva);*/
		mainapp.showTareasModel(agente);
	}};}
private EventHandler<ActionEvent> nuevo_MC(agent agente){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		/*Tab nueva=new Tab(agente.getNameString()+"-M. de Comunicaciones");
		nueva.setClosable(true);
		mainapp.getRootcontroller().getTabs().getTabs().add(nueva);
		mainapp.getRootcontroller().getTabs().getSelectionModel().select(nueva);*/
		mainapp.showCommunicationModel(agente);
	}};}

private EventHandler<ActionEvent> nuevo_MCo(agent agente){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		/*Tab nueva=new Tab(agente.getNameString()+"-M. de Coordinacion");
		nueva.setClosable(true);
		mainapp.getRootcontroller().getTabs().getTabs().add(nueva);
		mainapp.getRootcontroller().getTabs().getSelectionModel().select(nueva);*/
		mainapp.showCoordinationModel(agente);
	}};}

private EventHandler<ActionEvent> nuevo_MI(agent agente){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		/*Tab nueva=new Tab(agente.getNameString()+"-M. de Inteligencia");
		nueva.setClosable(true);
		mainapp.getRootcontroller().getTabs().getTabs().add(nueva);
		mainapp.getRootcontroller().getTabs().getSelectionModel().select(nueva);*/
		mainapp.showIntelligenceModel(agente, new MarcoOntologicoIndividual());
	}};}


private EventHandler<ActionEvent> UML(Integer levels){ return new EventHandler<ActionEvent>() {
	@Override
	public void handle(ActionEvent event) {
		TreeItem<Object> c = (TreeItem<Object>)mainapp.getRootcontroller().getArbol().getSelectionModel().getSelectedItem();
		agent agente=null;

		if(levels==0)
			agente=(agent) c.getValue();
		if(levels==1)
			agente=(agent) c.getParent().getValue();
		try {
			mainapp.AttachUML(agente);
		} catch (SAXException | IOException e) {
			mainapp.getRootcontroller().setMens(new SimpleStringProperty("Error al ejecutar la accion"));
		}
	}};}
}
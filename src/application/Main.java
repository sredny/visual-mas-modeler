package application;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Timer;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.ws.Action;

import org.xml.sax.SAXException;

import Visitador.nodoDisplayVisitor;
import application.CodeGenerator.clase;
import application.UML.estado;
import application.model.ActoHabla;
import application.model.Caso_uso;
import application.model.MarcoOntologicoColectivo;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalI;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;
import application.view.AgentModelController;
import application.view.CommunicationModelController;
import application.view.CoordinationModelController;
import application.view.DetailsController;
import application.view.OntologiaColectivaController;
import application.view.OntologiaIndividualController;
import application.view.RootController;
import application.view.TareaModelController;
import application.view.UcPlantillaController;
import application.view.addAgentController;
import application.view.intelligenceModelController;
import application.view.principalController;
import bd.bd;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

@XmlRootElement
public class Main extends Application{


	private Stage primaryStage;
    private BorderPane rootLayout;
    private StringProperty mensaje;
  	//  private drawer draw;
    private bd database;
     private MarcoOntologicoColectivo moc;
    private ObservableList<MarcoOntologicoColectivo> Inteligencia_colectiva=FXCollections.observableArrayList();
    private ObservableList<agent> agentData = FXCollections.observableArrayList();
    private ObservableList<MarcoOntologicoColectivo> Colectivo=FXCollections.observableArrayList();
    private ObservableList<MarcoOntologicoColectivo> Belongs=FXCollections.observableArrayList();
	private principalController controllerPrincipal;
	public static String caracter_sustitucion="";
	public static Boolean agregar_comentarios=true;
	private RootController rootcontroller;
	private File directorio=null;
	private TreeItem<Object> raiz;
	private TreeItem<Object> nodo_moc;
	private String name_application;
	private Map<String, AgentModelController> Agents_controller=new HashMap<>();
	private Map<String, DetailsController> UCS_controller=new HashMap<>();
	private Timeline timeline;
	private ObservableList<String> behaviours;
	public static ObservableList<String> special_behaviours;
	public static ObservableList<String> basicConstruct_behaviours;
	public static ObservableList<String> behaviour_containstSerialVersionUID;
	public static ObservableList<String> behaviour_withSpecialConstruct;
	public static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat("application/x-java-serialized-object");
	public boolean something_has_changed;

	public Main(){
    //	setDraw(new drawer());
    //	draw.setMainapp(this);
    	setDatabase(new bd());
    	//database.eliminar_url();

    	if(this.database.getUrl()!=null){
    		File f=new File(this.database.getUrl().get());
    		if(f.isDirectory()){
    			setUrl_fromDB(true);
    			this.directorio=f;
    		}
    	}

    	something_has_changed=true;
    	behaviours=FXCollections.observableArrayList();
    	special_behaviours=FXCollections.observableArrayList();
    	behaviour_containstSerialVersionUID=FXCollections.observableArrayList();
    	behaviour_withSpecialConstruct=FXCollections.observableArrayList();
    	/*"ReceiverBehaviour",*/
    	behaviours.addAll( "AchieveREInitiator",
						   "AchieveREResponder",
						   "Behaviour",
    					   "SimpleBehaviour",
    					   "CyclicBehaviour",
    					   "ContractNetInitiator",
						   "ContractNetResponder",
						   "CompositeBehaviour",
						   "DataStore",
						   "FSMBehaviour",
						   "IteratedAchieveREInitiator",
						   "LoaderBehaviour",
						   "OneShotBehaviour",
						   "OntologyServer",
						   "ParallelBehaviour",
						   "ProposeResponder",
    					   "ProposeInitiator",
    					   "SerialBehaviour",
    					   "SequentialBehaviour",
    					   "SSContractNetResponder",
						   "SSIteratedAchieveREResponder",
						   "SSIteratedContractNetResponder",
						   "SSResponderDispatcher",
						   "SimpleAchieveREInitiator",
					       "SimpleAchieveREResponder",
						   "SubscriptionInitiator",
						   "SubscriptionResponder",
    					   "TickerBehaviour",
    					   "ThreadedBehaviourFactory",
    					   "TwoPh0Initiator",
						   "TwoPh1Initiator",
						   "TwoPh2Initiator",
						   "TwoPhInitiator",
						   "TwoPhResponder",
    					   "WakerBehaviour",
    					   "WrapperBehaviour");


    	/*comportamientos que no tienen metodo action
    	 * que tienen un constructor especial
    	 * o que tienen m�todo abstractos
    	 * */
    	special_behaviours.addAll("FSMBehaviour",
    							"LoaderBehaviour",
    							"OntologyServer",
    							"ParallelBehaviour",
    							"SerialBehaviour",
    							"TickerBehaviour",
    							"WakerBehaviour",
    							"WrapperBehaviour"
    							);
    	/*
    	 * Comportamientos que contienen serialVersionUID
    	 * como atributo
    	 * */
    	behaviour_containstSerialVersionUID.addAll("FSMBehaviour",
    											"LoaderBehaviour",
    											"ParallelBehaviour",
    											"TickerBehaviour",
    											"WakerBehaviour");
    	behaviour_withSpecialConstruct.addAll("WrapperBehaviour","WakerBehaviour","TickerBehaviour","OntologyServer");
    	//database.vaciar_tipo();
    }

	public ObservableList<String> getSpecial_behaviours() {
		return special_behaviours;
	}

	public void setSpecial_behaviours(ObservableList<String> special_behaviours) {
		this.special_behaviours = special_behaviours;
	}

	public boolean isSomething_has_changed() {
		return something_has_changed;
	}

	public void setSomething_has_changed(boolean something_has_changed) {
		this.something_has_changed = something_has_changed;
	}

	public ObservableList<String> getBehaviours() {
		return behaviours;
	}

	//son todos los comportamientos mas la opcion "No aplica"
	public ObservableList<String> getOptionalBehaviours() {
		ObservableList<String> b=FXCollections.observableArrayList(behaviours);
		b.add(0, "No aplica");
		return b;
	}

	public void setBehaviours(ObservableList<String> behaviours) {
		this.behaviours = behaviours;
	}

	public void setAgentData(ObservableList<agent> agentData) {
		this.agentData = agentData;
	}

	@Override
	public void start(Stage primaryStage) {
		this.name_application="Visual MAS Modeler";
		 this.primaryStage = primaryStage;
	     this.primaryStage.setTitle(this.name_application);
	     this.primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/icons/logo/Snowflake-16.png")));
	     initRootLayout();
	     raiz = new TreeItem<Object>("Agentes");
	     nodo_moc=new TreeItem<Object>("Modelo de inteligencia Colectivo");

	     TreeItem<Object> root= new TreeItem<Object>("root");
	     root.getChildren().add(raiz);
	     root.getChildren().add(nodo_moc);
	     this.getRootcontroller().getArbol().setRoot(root);
	     this.getRootcontroller().getArbol().setShowRoot(false);

	     showPrincipalView();

	}

	 @Override
	  public void stop() throws Exception {

	  }

	public TreeItem<Object> getRaiz() {
		return raiz;
	}

	public void setRaiz(TreeItem<Object> raiz) {
		this.raiz = raiz;
	}

	public RootController getRootcontroller() {
		return rootcontroller;
	}

	public void setRootcontroller(RootController rootcontroller) {
		this.rootcontroller = rootcontroller;
	}
	public void aceptar(){
		for (agent a : agentData) {
			a.accept(new nodoDisplayVisitor(),new clase());
		}
	}

	/**
     * Initializes the root layout.
	 *
     */
    @SuppressWarnings("unchecked")
	public void initRootLayout() {
    		mensaje=new SimpleStringProperty("");

    		// add listener for stringProperty
            // to be called when stringProperty changed
            mensaje.addListener(new ChangeListener(){
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                	if(!((String)newValue).equals("")){
                		timeline = new Timeline(new KeyFrame(
                	        Duration.millis(5000),
                	        ae -> getMensaje().set("")));
                		timeline.setCycleCount(1);
                		timeline.play();
                	}
                }
            });


            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
            try {
				rootLayout = (BorderPane) loader.load();
				rootcontroller= loader.getController();
				rootcontroller.setMainapp(this);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);

            primaryStage.setScene(scene);
            scene.getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent ev) {
                	try {
						Boolean close=rootcontroller.DesplegarAlertaFinal();
						if(!close)
							ev.consume();
					} catch (JAXBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            });
            primaryStage.show();

    }

    public principalController GetPrincipalC(){
    	return controllerPrincipal;
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showPrincipalView() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/principal.fxml"));
            AnchorPane principalOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            //rootLayout.setCenter(principalOverview);
            rootcontroller.getDerecho().setCenter(principalOverview);

         // Give the controller access to the main app.
            controllerPrincipal = loader.getController();
            controllerPrincipal.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public static void main(String[] args) {
		launch(args);
	}

	@XmlTransient
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}


	@XmlTransient
	public BorderPane getRootLayout() {
		return rootLayout;
	}

	public void setRootLayout(BorderPane rootLayout) {
		this.rootLayout = rootLayout;
	}

	public ObservableList<agent> getAgentData() {
    	return agentData;
    }

	public void addAgent(agent a){
		agentData.add(a);
	}

	public void changeEscene(Scene scene){
		primaryStage.setScene(scene);
	}


	@FXML
	public boolean showUCplantilla(Caso_uso uc){

		try {
			FXMLLoader loader=new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/UseCasePlantilla.fxml"));
			AnchorPane page= (AnchorPane) loader.load();
			UcPlantillaController controller = loader.getController();
	        controller.setMainapp(this);
	        controller.setUc(uc);

			Stage dialog = new Stage();
	        dialog.setTitle("Informacion del Caso de Uso");
	        dialog.initModality(Modality.WINDOW_MODAL);
	        dialog.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialog.setScene(scene);

	     // Show the dialog and wait until the user closes it
	        dialog.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	@FXML
	public void AttachUML(agent agente) throws SAXException, IOException{
		 FileChooser fileChooser = new FileChooser();

	        // Set extension filter
	        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
	                "XML files (*.xml,*.xmi)", "*.xml","*.xmi");
	        fileChooser.getExtensionFilters().add(extFilter);
	        // Show save file dialog
	        File file = fileChooser.showOpenDialog(primaryStage);
	        if (file != null) {
	        	XMIParser parser=new XMIParser(file);
	        	String msg=parser.UCParsing(agente);
	        	this.FillTree(this.getRootcontroller().getArbol());
	        	rootcontroller.setMens(new SimpleStringProperty(msg));
	        	setSomething_has_changed(false);
	        	//this.getRootcontroller().getArbol().refresh();
                        this.getRootcontroller().getArbol().setVisible(false);
                        this.getRootcontroller().getArbol().setVisible(true);
	        }
	}

	/**
     * Called when the user clicks on the add button.
     */
    @FXML
    public boolean showAddAgentDialog() throws IOException{

        try {

        	// Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(Main.class.getResource("view/addForm.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        addAgentController controllerAD = loader.getController();
	        controllerAD.setMainApp(this);
	        if(FindTabByName("Nuevo Agente")!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName("Nuevo Agente"));
            	return true;
            }
	        setSomething_has_changed(false);
	        Tab current= new Tab();
				current.setClosable(true);
				rootcontroller.getTabs().getTabs().add(current);
				rootcontroller.getTabs().getSelectionModel().select(current);
				Hyperlink hlink = new Hyperlink();
				Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
				hlink.setGraphic(new ImageView(image));
				hlink.setFocusTraversable(false);
				current.setGraphic(hlink);
					hlink.setOnAction(new EventHandler<ActionEvent>() {
				        @Override
				        public void handle(ActionEvent e) {
				        	 rootcontroller.getTabs().getTabs().remove(current);
				         }
					});
            current.setText("Nuevo Agente");
            current.setContent(page);
            controllerAD.getNameAgent().requestFocus();
            cerrar_pestanaInicio();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return true;

    }

    public principalController getControllerPrincipal() {
		return controllerPrincipal;
	}

	public void setControllerPrincipal(principalController controllerPrincipal) {
		this.controllerPrincipal = controllerPrincipal;
	}

	/*
     * Called when need to show M.Agentes
     * */
    public boolean showAgentModel(agent agente){

    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
			controller.setAgente(agente,false);
            controller.setMainApp(this);

            Tab current;
            String Tabname=agente.getNameString()+"-M. de Agente";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
            	Agents_controller.put(Tabname, controller);
            	current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            }
    		current.setContent(page);
    		Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	             controller.SaveData();
    	             Agents_controller.remove(current.getText());
    	             rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

    }

    /*
     * Called when need to show M.Agentes
     * */
    public boolean showService(agent agente,Servicio servicio){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/AgentsModel.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			AgentModelController controller = loader.getController();
			controller.setAgente(agente,false);
			controller.seleccionar_servicio(servicio);
            controller.setMainApp(this);
            Tab current;
            String Tabname=agente.getNameString()+"-M. de Agente";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
            	current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            }
    		current.setContent(page);
    		Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	            rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

    }


    public Tab FindTabByName(String name){
    	rootcontroller.getTabs().getSelectionModel();
    	for (Tab tab : rootcontroller.getTabs().getTabs()) {
    		if(tab.getText().equals(name))
    			return tab;
		}
    	return null;
    }

    public void cerrar_pestanaInicio(){
    	rootcontroller.getTabs().getSelectionModel();
    	for (Tab tab : rootcontroller.getTabs().getTabs()) {
    		if(tab.getText().equals("Inicio")){
    			rootcontroller.getTabs().getTabs().remove(tab);
    			return;
    		}
		}
    }

    /*
     * Called when need to show communication model
     * */
    public boolean showCommunicationModel(agent agente){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CommunicationModel.fxml"));
        try {
			AnchorPane page= (AnchorPane) loader.load();
			CommunicationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(this);
            Tab current;
            String Tabname=agente.getNameString()+"-M. de Comunicacion";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }

            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
    }

	private void showActoHabla(agent agente, ActoHabla acto) {
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CommunicationModel.fxml"));
        try {
			AnchorPane page= (AnchorPane) loader.load();
			CommunicationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(this);
            controller.setCurrent(acto);
            controller.getTabla_actos().getSelectionModel().select(acto);

            Tab current;
            String Tabname=agente.getNameString()+"-M. de Comunicacion";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }

            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


    /*
     * called when need to show coordination model
     * */
    public boolean showConversation(agent agente,conversacion current_conversacion){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
        try {
			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(this);
            controller.setCurrent(current_conversacion);
            Tab current;
            String Tabname=agente.getNameString()+"-M. de Coordinaci�n";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(Tabname);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;
    }

    /*
     * called when need to show coordination model
     * */
    public boolean showCoordinationModel(agent agente){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/CoordinationModel.fxml"));
        try {
			AnchorPane page= (AnchorPane) loader.load();
			CoordinationModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(this);
            //rootLayout.setCenter(page);
            //rootcontroller.getDerecho().setCenter(page);
            Tab current;
            String Tabname=agente.getNameString()+"-M. de Coordinaci�n";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(Tabname);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;
    }

    /*
     * Called when need to show tareas model
     * */
    public boolean showTareasModel(agent agente){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
        try {
        	AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(this);

            Tab current;
            String Tabname=agente.getNameString()+"-M. de Tareas";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(Tabname);
	            current.setContent(page);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	// controller.handle_AgregarTarea();
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
        }catch(IOException e){return false;}
        return true;
    }

    /*
     * Called when need to show tareas model
     * */
    public boolean showTarea(agent agente,Tarea t){
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
        try {
        	AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
			controller.seleccionarTarea(t);
            controller.setMainApp(this);

            Tab current;
            String Tabname=agente.getNameString()+"."+t.getNombre().get()+"-M. de Tareas";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(Tabname);
	            current.setContent(page);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
        }catch(IOException e){return false;}
        return true;
    }

    /*
     * Called when need to show tareas model
     * */
    public void showTareasModel(agent agente, Servicio s){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/TareaModel.fxml"));
        try {
        	AnchorPane page= (AnchorPane) loader.load();
			TareaModelController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainApp(this);
            controller.getServicio().getSelectionModel().select(s);

            Tab current;
            String Tabname=agente.getNameString()+"-M. de Tareas";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(agente.getNameString()+"-M. de Tareas");
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(agente.getNameString()+"-M. de Tareas");
	            current.setContent(page);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
        }catch(IOException e){e.printStackTrace();}
    }

    /*
     * Called when need to show intelligence model
     * */
    public void showIntelligenceModel(agent agente,MarcoOntologicoIndividual moi){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/OntologiaIndividual.fxml"));
        try {
        	AnchorPane page= (AnchorPane) loader.load();
			OntologiaIndividualController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainapp(this);
            controller.setCurrent(moi);
            Tab current;
            String Tabname=agente.getNameString()+"-M. de Inteligencia Individual";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
        		current.setContent(page);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(Tabname);
	            current.setContent(page);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
           // rootLayout.setCenter(page);
            //  Node tabContent = current.getContent();

           // rootcontroller.getDerecho().setCenter(page);
        }catch(IOException e){e.printStackTrace();}
    }

    /*
     * Called when need to show intelligence model
     * */
    public void showIntelligenceModelHistorica(agent agente,MarcoOntologicoIndividual moi){
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/OntologiaIndividual.fxml"));
        try {
        	AnchorPane page= (AnchorPane) loader.load();
			OntologiaIndividualController controller = loader.getController();
			controller.setAgente(agente);
            controller.setMainapp(this);
            controller.setCurrent(moi);
            Tab current;
            String Tabname=agente.getNameString()+"-M. de Inteligencia Individual";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
        		current.setContent(page);
            }
            else{
	            current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
	            current.setText(Tabname);
	            current.setContent(page);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
           // rootLayout.setCenter(page);
            //  Node tabContent = current.getContent();

           // rootcontroller.getDerecho().setCenter(page);
        }catch(IOException e){e.printStackTrace();}
    }

	public void NuevoColectivo(){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/OntologiaColectiva.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			OntologiaColectivaController controller = loader.getController();
            controller.setMainapp(this);
            Tab current;
            String Tabname="M. de Inteligencia Colectivo";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {

    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void NuevoColectivo(MarcoOntologicoColectivo moc){
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/OntologiaColectiva.fxml"));
		try {
			AnchorPane page= (AnchorPane) loader.load();
			OntologiaColectivaController controller = loader.getController();
            controller.setMainapp(this);
            controller.setMoc(moc);
            Tab current;
            String Tabname="M. de Inteligencia Colectivo";
            if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            }
            current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controller.fieldsNonEmpty()){
    	        		if(confirmacion_alerta()){
    	        			rootcontroller.getTabs().getTabs().remove(current);}
    	        	}
    	        	else
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    public boolean showUCDetails(TreeItem<Object> nodo){
    	try {
    		nodo.setExpanded(true);
        	// Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(Main.class.getResource("view/Descripcion.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        DetailsController controllerAD = loader.getController();

	        controllerAD.setMainApp(this);
	        Caso_uso uc=(Caso_uso)nodo.getValue();
	        controllerAD.setCaso(uc);
	        controllerAD.setAgente((agent)nodo.getParent().getParent().getValue());
	        Tab current;
	        String Tabname=((agent)nodo.getParent().getParent().getValue()).getName().get()+". Caso de Uso-"+uc.getNombre();
	        UCS_controller.put(Tabname, controllerAD);
	        controllerAD.setNameTab(Tabname);
        	if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            	current.setContent(page);
            }
	        current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);
    		cerrar_pestanaInicio();

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controllerAD.Guardar()){
    	        		controllerAD.getMainApp().getMensaje().set("Se guard� informaci�n del caso de uso "+controllerAD.getCaso().getNombre()+" exitosamente");
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	        	}
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return true;
    }

    public boolean showUCDetails(Caso_uso nodo,agent agente){

    	try {
    		// Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(Main.class.getResource("view/Descripcion.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        DetailsController controllerAD = loader.getController();
	        controllerAD.setMainApp(this);
	        Caso_uso uc=nodo;
	        controllerAD.setCaso(uc);
	        controllerAD.setAgente(agente);
	        Tab current;
	        cerrar_pestanaInicio();
	        String Tabname=agente.getName().get()+". Caso de Uso-"+uc.getNombre();
	        UCS_controller.put(Tabname, controllerAD);
	        controllerAD.setNameTab(Tabname);
	        if(FindTabByName(Tabname)!=null){
            	rootcontroller.getTabs().getSelectionModel().select(FindTabByName(Tabname));
            	return true;
            }
            else if(rootcontroller.getTabs().getTabs().size()==0||FindTabByName(Tabname)==null){
            	current=new Tab(Tabname);
        		current.setClosable(true);
        		rootcontroller.getTabs().getTabs().add(current);
        		rootcontroller.getTabs().getSelectionModel().select(current);
            }
            else{
            	current= rootcontroller.getTabs().getSelectionModel().getSelectedItem();
            	current.setText(Tabname);
            	current.setContent(page);
            }
	        current.setContent(page);
            Hyperlink hlink = new Hyperlink();
     		Image image = new Image(getClass().getResourceAsStream("/imagenes/close.png"));
     		hlink.setGraphic(new ImageView(image));
     		hlink.setFocusTraversable(false);
    		current.setGraphic(hlink);

    		hlink.setOnAction(new EventHandler<ActionEvent>() {
    	        @Override
    	        public void handle(ActionEvent e) {
    	        	if(controllerAD.Guardar()){
    	        		controllerAD.getMainApp().getMensaje().set("Se guard� informaci�n del caso de uso "+controllerAD.getCaso().getNombre()+" exitosamente");
    	        		rootcontroller.getTabs().getTabs().remove(current);
    	        	}
    	         }
    		});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return true;
    }

    public void rename_tab(String oldName, String NewName){
    	for (Tab tp : this.rootcontroller.getTabs().getTabs()) {
    			if(oldName.equals(tp.getText()))
    				tp.setText(NewName);
		}

    }

    private void showMIC() {
    	this.NuevoColectivo();
	}

    private void ShowMOC(MarcoOntologicoColectivo moc){
    	this.NuevoColectivo(moc);
    }

    public boolean confirmacion_alerta(){
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Di�logo de confirmaci�n");
    	alert.setHeaderText("Si cierras la pesta�a no se guardaran los cambios");
    	alert.setContentText("Desea continuar?");

    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK){
    	    return true;
    	} else {
    	    return false;
    	}
    }


	@SuppressWarnings("unchecked")
	public void FillTree(TreeView<Object> arbol){
		Main main=this;

        	arbol.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {

				if(event.getClickCount() == 2){
					TreeItem<Object> item = arbol.getSelectionModel().getSelectedItem();
					if(item==null)
						return;
					if(item.getValue() instanceof String){
						if(((String)item.getValue()).equals("Descripcion")){
							showUCDetails(item.getParent());
						}

						if(((String)item.getValue()).equals("Modelo de inteligencia Colectivo")){
							showMIC();
						}

					}//es String

					if(item.getValue() instanceof CommunicationModel){
						showCommunicationModel((application.model.agent) item.getParent().getParent().getValue());
					}

					if(item.getValue() instanceof ActoHabla){
						showActoHabla((agent)item.getParent().getParent().getParent().getValue(),(ActoHabla)item.getValue());
					}

					if(item.getValue() instanceof Servicio){
						agent ag=(agent)item.getParent().getParent().getParent().getParent().getValue();
						showService(ag, (Servicio)item.getValue());
					}

					if(item.getValue() instanceof CoordinationModel){
						showCoordinationModel((application.model.agent) item.getParent().getParent().getValue());
					}

					if(item.getValue() instanceof AgentModel){
						showAgentModel((application.model.agent) item.getParent().getParent().getValue());
					}//modelo de agentes

					if(item.getValue() instanceof TareaModel){
						showTareasModel((application.model.agent) item.getParent().getParent().getValue());
					}//modelo de tareas

					if(item.getValue() instanceof Tarea){
						showTarea((application.model.agent) item.getParent().getParent().getParent().getParent().getParent().getValue(),(Tarea)item.getValue());
					}

					if(item.getValue() instanceof estado){

					}

					if(item.getValue() instanceof conversacion){
						showConversation((application.model.agent) item.getParent().getParent().getParent().getValue(),(conversacion) item.getValue());
					}

					if(item.getValue() instanceof ObservableList<?>){
						FXMLLoader loader = new FXMLLoader();
						 showIntelligenceModel((application.model.agent) item.getParent().getParent().getValue(), new MarcoOntologicoIndividual());
					}

					if(item.getValue() instanceof MarcoOntologicoIndividual){
						showIntelligenceModel((application.model.agent) item.getParent().getParent().getParent().getValue(),(MarcoOntologicoIndividual)item.getValue());
					}//if marco ontologico individual

					if(item.getValue() instanceof MarcoOntologicoColectivo){
						ShowMOC((MarcoOntologicoColectivo)item.getValue());
					}

					if(item.getValue() instanceof OntologiaSituacionalI){
					}

					if(item.getValue() instanceof agent){
						showAgentModel((application.model.agent) item.getValue());

					}
					if(item.getValue() instanceof OntologiaHistorica){
						showIntelligenceModelHistorica((application.model.agent) item.getParent().getParent().getParent().getValue(),(MarcoOntologicoIndividual)item.getValue());
					}

					if(item.getValue() instanceof OntologiaDominio){
					}


				}//doble click
			  }
        	});
//		}

        arbol.setCellFactory( new Callback<TreeView<Object>, TreeCell<Object>>()
        {
            @Override
            public TreeCell<Object> call( TreeView<Object> p )
            {
                return new MyTreeCell(main);
            }
        } );
	}//fin de llenar el arbol

	public ObservableList<MarcoOntologicoColectivo> getColectivo() {
		return Colectivo;
	}
	public void setColectivo(ObservableList<MarcoOntologicoColectivo> colectivo) {
		Colectivo = colectivo;
	}

	public bd getDatabase() {
		return database;
	}
	public void setDatabase(bd database) {
		this.database = database;
	}

	public void filterMarcosC_by(agent a){
		Belongs=FXCollections.observableArrayList();
		for (MarcoOntologicoColectivo m : Colectivo) {
			if(m.getAgentes().contains(a)){
				Belongs.add(m);
			}
		}
	}

	public void refactor(ObservableList<agent> lista,List<String> tofind){
		for (String string : tofind) {
			for(agent agente : agentData){
				if(string.equals(agente.id)){
					lista.add(agente);
				}
			}//fin for agente
		}
	}

	public void refactor(agent lista,String tofind){

			for(agent agente : agentData){
				if(tofind.equals(agente.id)){
					lista=agente;
				}
			}//fin for agente

	}

	public agent refactor(String tofind){

		for(agent agente : agentData){
			if(tofind.equals(agente.id)){
				return agente;
			}
		}//fin for agente
		return null;
}

	public String project_toXML(){
		//Generar XML de: each agente,Colectivo
		String inicial="<?xml version = '1.0' encoding = 'UTF-8'?>\n";

		inicial+="<XML xmi.version ='1.0'>\n<XML.header>\n"
		  +"\t<XML.documentation>\n"
		  +"\t\t<XML.exporter>Visual MAS Modeler</XML.exporter>\n"
		  +"\t\t<XML.exporterVersion>1.0</XML.exporterVersion>\n"
		  +"\t\t<XML.notice></XML.notice>\n"
		  +"\t</XML.documentation>\n"
		  +"</XML.header>\n"
		  +"<XML.Content>\n";

		for (agent agente : agentData) {
			inicial+=agente.to_XML(1);
		}

		inicial+="<InteligenciaColectiva>\n";
		for (MarcoOntologicoColectivo colectivo : Colectivo) {
			inicial+=colectivo.to_XML(1);
		}
		inicial+="</InteligenciaColectiva>\n";
		inicial+="</XML.Content>\n";
		inicial+="</XML>";


		return inicial;
	}

	//recibe un servicio y devuelve el agente padre
	public agent getParentOf(String servicio_id){
		agent a=null;

		for (agent agente : this.agentData) {
			for (Servicio  servicio: agente.getModelo_agentes().getServicios()) {
				if(servicio.getId()==servicio_id)
					return agente;
			}
		}
		return a;
	}

	//recibe un caso de uso y devuelve el agente padre
		public agent getParentOf(Caso_uso uc){

			for (agent agente : this.agentData) {
				for (Caso_uso  caso: agente.getCasos_uso()) {
					if(caso==uc)
						return agente;
				}
			}
			return null;
		}
	/*
	 * M�todo que retorna el agente al que pertenece un
	 * item de tipo String en el segundo nivel
	 * del arbol
	 * */
	public agent getParentOfNewDiagram(String object){
		for (Object o : rootcontroller.getArbol().getRoot().getChildren()) {
			TreeItem<Object> nodo=(TreeItem<Object>)o;
			for(TreeItem<Object> n : nodo.getChildren()){
				if(n.getValue() == object){
					return (agent)n.getParent().getValue();
				}
			}//fin del for

		}
		return null;
	}

	public Map<String, AgentModelController> getAgents_controller() {
		return Agents_controller;
	}

	public void setAgents_controller(Map<String, AgentModelController> agents_controller) {
		Agents_controller = agents_controller;
	}

	public boolean isUrl_fromDB() {
		return url_fromDB;
	}

	public void setUrl_fromDB(boolean url_fromDB) {
		this.url_fromDB = url_fromDB;
	}

	public static DataFormat getSerializedMimeType() {
		return SERIALIZED_MIME_TYPE;
	}

	 public boolean isUnique(String name){
	    	for (agent agente : agentData) {
				if(agente.getName().get().equals(name))
					return false;
			}
	    	return true;
	    }

	public String getCaracter_sustitucion() {
		return caracter_sustitucion;
	}

	public void setCaracter_sustitucion(String caracter_sustitucion) {
		this.caracter_sustitucion = caracter_sustitucion;
	}

	public Boolean getAgregar_comentarios() {
		return agregar_comentarios;
	}

	public void setAgregar_comentarios(Boolean agregar_comentarios) {
		this.agregar_comentarios = agregar_comentarios;
	}

	public Map<String, DetailsController> getUCS_controller() {
		return UCS_controller;
	}

	public void setUCS_controller(Map<String, DetailsController> uCS_controller) {
		UCS_controller = uCS_controller;
	}

	private boolean url_fromDB;

	public String getName_application() {
		return name_application;
	}

	public void setName_application(String name_application) {
		this.name_application = name_application;
	}

	public TreeItem<Object> getNodo_moc() {
		return nodo_moc;
	}

	public void setNodo_moc(TreeItem<Object> nodo_moc) {
		this.nodo_moc = nodo_moc;
	}

	public File getDirectorio() {
		return directorio;
	}

	public void setDirectorio(File directorio) {
		setUrl_fromDB(false);
		this.directorio = directorio;
	}

	public void reiniciar_arbol(){
		setSomething_has_changed(false);
		raiz.getChildren().clear();
		nodo_moc.getChildren().clear();
	}


    public ObservableList<MarcoOntologicoColectivo> getBelongs() {
		return Belongs;
	}

	public void setBelongs(ObservableList<MarcoOntologicoColectivo> belongs) {
		Belongs = belongs;
	}

	public StringProperty getMensaje() {
		return mensaje;
	}
	public void setMensaje(StringProperty mensaje) {
		this.mensaje = mensaje;
	}

	public ObservableList<MarcoOntologicoColectivo> getInteligencia_colectiva() {
		return Inteligencia_colectiva;
	}

	public void setInteligencia_colectiva(ObservableList<MarcoOntologicoColectivo> inteligencia_colectiva) {
		Inteligencia_colectiva = inteligencia_colectiva;
	}

	public void MarcarError(Node nodo){
		nodo.setStyle("-fx-border-color: #d9534f ;-fx-stroke-width: 10;-fx-border-width: 2;-fx-background-radius: 3, 2, 4, 0;");
	}

	public void MarcarCorrecto(Node nodo){
		nodo.setStyle("-fx-border-color: #5bc0de ;-fx-border-width: 1;-fx-stroke-width: 10;");
	}


	public static String FormatearClase(String nombre){
		nombre=nombre.toLowerCase();
		String pedazo[]=nombre.split(" ");
		String resultado="";

		for (int i=0;i<pedazo.length;i++) {
			pedazo[i] = pedazo[i].substring(0, 1).toUpperCase() + pedazo[i].substring(1);
		}

		for(int i=0;i<pedazo.length-1;i++){
			resultado+=pedazo[i]+Main.caracter_sustitucion;
		}
		resultado+=pedazo[pedazo.length-1];

		return resultado;
	}

	public static String ProcesarClaseGetContructor(String comportamiento, String MiClase){

		ObservableList<String> constructorBasico=FXCollections.observableArrayList();
		constructorBasico.addAll("Behaviour","SimpleBehaviour","CyclicBehaviour","OneShotBehaviour","FSMBehaviour","LoaderBehaviour","SequentialBehaviour","CompositeBehaviour","SerialBehaviour");

		if(constructorBasico.contains(comportamiento)){
			return "\n\tpublic "+MiClase+"(Agent a) {"
					+"\n\t\tsuper(a);"
				+"\n\t}";
		}

		if(comportamiento.equals("DataStore")||comportamiento.equals("ThreadedBehaviourFactory")){
			return "\n\tpublic "+MiClase+"(Agent owner) {"
						+"\n\t\tthis.owner = owner;"
					+"\n\t}";
		}

		if(comportamiento.equals("OntologyServer")){
			return "\n\tpublic "+MiClase+"(Agent a, Ontology onto, int performative) {"
						+"\n\t\tsuper(a, onto, performative);"
					+"\n\t}";
		}

		if(comportamiento.equals("ParallelBehaviour")){
			return "public "+MiClase+"(Agent a, int endCondition) {"
						+"\n\t\tsuper(a, endCondition);"
					+"\n\t}";
		}

		if(comportamiento.equals("TickerBehaviour")){
			return "public "+MiClase+"(Agent a, long period)  {"
						+"\n\t\tsuper(a, period);"
					+"\n\t}";
		}
		if(comportamiento.equals("WakerBehaviour")){
			return "public "+MiClase+"(Agent a, Date wakeupDate)  {"
						+"\n\t\tsuper(a, wakeupDate);"
					+"\n\t}";
		}

		if(comportamiento.equals("WrapperBehaviour")){
			return "public "+MiClase+"(Agent a, Behaviour wrapped) {"
						+"\n\t\tsuper(wrapped);"
						+"\n\t\tsetAgent(a);"
					+"\n\t}";
		}

		if(comportamiento.equals("AchieveREInitiator")||comportamiento.equals("IteratedAchieveREInitiator")||comportamiento.equals("ProposeInitiator")||comportamiento.equals("SimpleAchieveREInitiator")||comportamiento.equals("SubscriptionInitiator")){
			return "public "+MiClase+"(Agent a, ACLMessage msg) {"
						+"\n\t\tsuper(a, msg);"
					+"\n\t}";
		}

		if(comportamiento.equals("AchieveREResponder")||comportamiento.equals("ContractNetResponder")||comportamiento.equals("ProposeResponder")||comportamiento.equals("SSResponderDispatcher")||comportamiento.equals("SimpleAchieveREResponder")||comportamiento.equals("SubscriptionResponder")||comportamiento.equals("TwoPhResponder")){
			return "public "+MiClase+"(Agent a, MessageTemplate mt) {"
						+"\n\t\tsuper(a, mt);"
					+"\n\t}";
		}

		if(comportamiento.equals("ContractNetInitiator")||comportamiento.equals("SSContractNetResponder")||comportamiento.equals("SSIteratedContractNetResponder")||comportamiento.equals("TwoPhInitiator")){
			return "public "+MiClase+"(Agent a, ACLMessage cfp){"
						+"\n\t\tsuper(a, cfp);"
					+"\n\t}";
		}

		if(comportamiento.equals("SSIteratedAchieveREResponder")){
			return "public "+MiClase+"(Agent a, ACLMessage request) {"
						+"\n\t\tsuper(a, request);"
					+"\n\t}";
		}

		if(comportamiento.equals("TwoPh0Initiator")){
			return "public "+MiClase+"(Agent a, ACLMessage cfp, String outputKey){"
						+"\n\t\tsuper(a, cfp, outputKey);"
					+"\n\t}";
		}
		if(comportamiento.equals("TwoPh1Initiator")){
			return "public "+MiClase+"(Agent a, ACLMessage queryIf, String outputKey){"
						+"\n\t\tsuper(a, queryIf, outputKey);"
					+"\n\t}";
		}
		if(comportamiento.equals("TwoPh2Initiator")){
			return "public "+MiClase+"(Agent a, ACLMessage acceptance){"
						+"\n\t\tsuper(a, acceptance);"
					+"\n\t}";
		}


		return null;

	}

	/*
	 * Devuelve la lineas de codigo para crear una nueva
	 * instancia de ese comportamiento
	 * */
	public static String ProcesarClaseGetDeclaracionInstancia(String comportamiento, String MiClase){

		ObservableList<String> constructorBasico=FXCollections.observableArrayList();
		constructorBasico.addAll("Behaviour","SimpleBehaviour","CyclicBehaviour","OneShotBehaviour","FSMBehaviour","LoaderBehaviour","SequentialBehaviour","CompositeBehaviour","SerialBehaviour");

		if(constructorBasico.contains(comportamiento)){
			return "new "+MiClase+"(this)";
		}

		if(comportamiento.equals("DataStore")||comportamiento.equals("ThreadedBehaviourFactory")){
			return "new "+MiClase+"(this)";
		}

		if(comportamiento.equals("OntologyServer")){
			return "new "+MiClase+"(this,onto,performative)";
		}

		if(comportamiento.equals("ParallelBehaviour")){
			return "new "+MiClase+"(this, sendCondition)";
		}

		if(comportamiento.equals("TickerBehaviour")){
			return "new "+MiClase+"(this,period)";
		}
		if(comportamiento.equals("WakerBehaviour")){
			return "new "+MiClase+"(this, wakeupDate)";
		}

		if(comportamiento.equals("WrapperBehaviour")){
			return "new "+MiClase+"(this, wrapped)";
		}

		if(comportamiento.equals("AchieveREInitiator")||comportamiento.equals("IteratedAchieveREInitiator")||comportamiento.equals("ProposeInitiator")||comportamiento.equals("SimpleAchieveREInitiator")||comportamiento.equals("SubscriptionInitiator")){
			return "new "+MiClase+"(this,msg)";
		}

		if(comportamiento.equals("AchieveREResponder")||comportamiento.equals("ContractNetResponder")||comportamiento.equals("ProposeResponder")||comportamiento.equals("SSResponderDispatcher")||comportamiento.equals("SimpleAchieveREResponder")||comportamiento.equals("SubscriptionResponder")||comportamiento.equals("TwoPhResponder")){
			return "new "+MiClase+"(this,mt)";
		}

		if(comportamiento.equals("ContractNetInitiator")||comportamiento.equals("SSContractNetResponder")||comportamiento.equals("SSIteratedContractNetResponder")||comportamiento.equals("TwoPhInitiator")){
			return "new "+MiClase+"(this,cfp)";
		}

		if(comportamiento.equals("SSIteratedAchieveREResponder")){
			return "new "+MiClase+"(this,request)";
		}

		if(comportamiento.equals("TwoPh0Initiator")){
			return "new "+MiClase+"(this, cfp, outputKey)";
		}
		if(comportamiento.equals("TwoPh1Initiator")){
			return "new "+MiClase+"(this, queryIf, outputKey)";
		}
		if(comportamiento.equals("TwoPh2Initiator")){
			return "new "+MiClase+"(this,acceptance);";
		}

		return comportamiento+"()";

	}

	public static void ProcesarClaseGetAtributos(String comportamiento, clase miclase){
		if(comportamiento.equals("DataStore")||comportamiento.equals("ThreadedBehaviourFactory"))
			miclase.add_attribute("private Agent owner");
	}

	public static void ProcesarClaseGetImports(String comportamiento, clase miclase){
		if(comportamiento.equals("OntologyServer"))
			miclase.add_library("import jade.content.onto.*;");
		if(comportamiento.equals("CompositeBehaviour")||comportamiento.equals("SerialBehaviour"))
			miclase.add_library("import jade.util.leap.*;");
		if(comportamiento.equals("WakerBehaviour"))
			miclase.add_library("import java.util.Date;");
	}


	public static String ProcesarClaseGetMetodos(String comportamiento, String MiClase){

		ObservableList<String> constructorBasico=FXCollections.observableArrayList();
		constructorBasico.addAll("Behaviour","SimpleBehaviour");

		if(constructorBasico.contains(comportamiento)){
			return "\n\t@Override"
						+"\n\tpublic void action() {"
					    +"\n\t		throw new UnsupportedOperationException(\"Not supported yet.\");"
						+"\n\t}"

						+"\n\t@Override"
						+"\n\tpublic boolean done() {"
						+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
						+"\n\t}";
		}

		if(comportamiento.equals("CyclicBehaviour")||comportamiento.equals("OneShotBehaviour")){
			return "\n\t@Override"
						+"\n\tpublic void action() {"
						+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
						+"\n\t}";
		}

		if(comportamiento.equals("CompositeBehaviour")||comportamiento.equals("SerialBehaviour")){
			return "\n\n\t@Override"
				+"\n\tprotected void scheduleFirst() {"
				+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
				+"\n\t}"

				+"\n\n\t@Override"
				+"\n\tprotected void scheduleNext(boolean bln, int i) {"
				+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
				+"\n\t}"

				+"\n\n\t@Override"
				+"\n\tprotected boolean checkTermination(boolean bln, int i) {"
				+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
				+"\n\t}"

				+"\n\n\t@Override"
				+"\n\tprotected Behaviour getCurrent() {"
				+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
				+"\n\t}"

				+"\n\n\t@Override"
				+"\n\tpublic Collection getChildren() {"
				+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
				+"\n\t}";
		}

		if(comportamiento.equals("TickerBehaviour")){
			return "\n\t@Override"
					+"\n\tprotected void onTick() {"
					+"\n\t    throw new UnsupportedOperationException(\"Not supported yet.\");"
					+"\n\t}";
		}

		if(comportamiento.equals("SSResponderDispatcher")){
			return "\nprotected Behaviour createResponder(ACLMessage aclm) {"
					+"\n\t	throw new UnsupportedOperationException(\"Not supported yet.\");"
					+"\n\t}";
		}


		return null;

	}

	public static String FormatearAtributoMetodo(String nombre){
		nombre=nombre.toLowerCase();
		String pedazo[]=nombre.split(" ");
		String resultado="";

		for (int i=1;i<pedazo.length;i++) {
			pedazo[i] = pedazo[i].substring(0, 1).toUpperCase() + pedazo[i].substring(1);
		}

		for(int i=0;i<pedazo.length-1;i++){
			resultado+=pedazo[i]+Main.caracter_sustitucion;
		}
		resultado+=pedazo[pedazo.length-1];

		return resultado;
	}

	public boolean isValidId(String id){
	    Pattern p = Pattern.compile("([a-zA-Z$_]|~[\\u0000-\\u007F\\uD800-\\uDBFF]|[\\uD800-\\uDBFF][\\uDC00-\\uDFFF])([a-zA-Z0-9$_]|~[\\u0000-\\u007F\\uD800-\\uDBFF]|[\\uD800-\\uDBFF][\\uDC00-\\uDFFF])*");
	    id = id.replaceAll("\\s+",""); //eliminar todos los espacios en blanco
	    Matcher m = p.matcher(id);

	    return m.matches();   //retorna verdadero si cadena_prueba encaja con la expresi�n regular
	}

}

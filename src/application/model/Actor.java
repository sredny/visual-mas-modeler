package application.model;

import java.math.BigInteger;
import java.security.SecureRandom;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class Actor {
	private  StringProperty name,tipo,codigo;
	private ImageView man;
	private Text name_t;
	private SecureRandom random = new SecureRandom();
	private TreeItem<Object> nodo_arbol=null;

	public Actor(){
		name=new SimpleStringProperty("");
		name_t=new Text();
		tipo=new SimpleStringProperty("Externo");
		codigo=new SimpleStringProperty(new BigInteger(130, random).toString(32));
		setNodo_arbol(new TreeItem<Object>(this));
	}


	public Actor(String name_n,String tipo_n,String codigo_n){
		name=new SimpleStringProperty(name_n);
		tipo=new SimpleStringProperty(tipo_n);
		codigo=new SimpleStringProperty(codigo_n);
		name_t=new Text();
		setNodo_arbol(new TreeItem<Object>(this));
	}

	public StringProperty getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = new SimpleStringProperty(tipo);
	}

	public StringProperty getName() {
		return name;
	}

	public void setName(String name) {
		this.name = new SimpleStringProperty(name);
		name_t.setText(name);
	}

	public StringProperty getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = new SimpleStringProperty(codigo);
	}

	public Text getName_t() {
		return name_t;
	}

	public void setName_t(Text name_t) {
		this.name_t = name_t;
	}

	public ImageView getMan() {
		return man;
	}

	public void setMan(ImageView man) {
		this.man = man;
		this.name_t.setLayoutX(man.getLayoutX());
		this.name_t.setLayoutY(man.getLayoutY()+75);
		this.name_t.setVisible(true);
	}

	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<Actor nombre='"+this.name.get()+"' tipo='"+this.tipo.get()+"' codigo='"+this.codigo.get()+"' />\n";
		return XML;
	}


	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}


	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}

}

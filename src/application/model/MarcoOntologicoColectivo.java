package application.model;

import java.util.ArrayList;
import java.util.List;

import application.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class MarcoOntologicoColectivo {
	private StringProperty nombre;
	private List<String> objetivos;
	private List<String> agentes_s;//lista que contiene los ids de los agentes le�dos del .msa
	private StringProperty descripcion;
	private ObservableList<OntologiaSituacionalC> ontologias=FXCollections.observableArrayList();
	private ObservableList<agent> agentes=FXCollections.observableArrayList();
	private TreeItem<Object> nodo_arbol;

	public List<String> getAgentes_s() {
		return agentes_s;
	}

	 public void refactor(Main main){
		 main.refactor(agentes, agentes_s);
		 for (OntologiaSituacionalC o : this.getOntologias()) {
			nodo_arbol.getChildren().add(o.getNodo_arbol());
		}
	 }

	public void setAgentes_s(List<String> agentes_s) {
		this.agentes_s = agentes_s;
	}


	public MarcoOntologicoColectivo(){
		nombre=new SimpleStringProperty("");
		objetivos=new ArrayList<String>();
		agentes_s=new ArrayList<String>();
		descripcion=new SimpleStringProperty("");
		ontologias=FXCollections.observableArrayList();
		agentes=FXCollections.observableArrayList();
		nodo_arbol=new TreeItem<Object>(this);
	}

	public MarcoOntologicoColectivo(String nombre,String descripcion,List<String> obje){
		this.nombre=new SimpleStringProperty(nombre);
		this.descripcion=new SimpleStringProperty(descripcion);
		objetivos=obje;
		ontologias=FXCollections.observableArrayList();
		agentes=FXCollections.observableArrayList();
	}

	public MarcoOntologicoColectivo(String nombre,String descripcion){
		this.nombre=new SimpleStringProperty(nombre);
		this.descripcion=new SimpleStringProperty(descripcion);
		objetivos=new ArrayList<String>();
		ontologias=FXCollections.observableArrayList();
		agentes=FXCollections.observableArrayList();
	}

	public StringProperty getNombre() {
		return nombre;
	}
	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}
	public List<String> getObjetivos() {
		return objetivos;
	}
	public void setObjetivos(List<String> objetivos) {
		this.objetivos = objetivos;
	}
	public StringProperty getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}
	public ObservableList<OntologiaSituacionalC> getOntologias() {
		return ontologias;
	}
	public void setOntologias(ObservableList<OntologiaSituacionalC> ontologias) {
		this.ontologias = ontologias;
	}
	public ObservableList<agent> getAgentes() {
		return agentes;
	}
	public void setAgentes(ObservableList<agent> agentes) {
		this.agentes = agentes;
	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}

	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<MarcoOntologicoColectivo name='"+this.nombre.get()+"' descripcion='"+this.descripcion.get()+"'>\n";
		XML+=inde+"<Objetivos>\n";
		for (String string : objetivos) {
			XML+=inde+"\t<Objetivo>"+string+"</Objetivo>\n";
		}
		XML+=inde+"</Objetivos>\n";

		XML+=inde+"<Agentes>\n";
		for (agent agente : agentes) {
			XML+=inde+"\t<Agente>"+agente.getId()+"</Agente>\n";
		}
		XML+=inde+"</Agentes>\n";

		XML+=inde+"<OntologiasSituacionalesColectivas>\n";
		for (OntologiaSituacionalC o : ontologias) {
			XML+=o.to_XML(indentacion+1);
		}
		XML+=inde+"</OntologiasSituacionalesColectivas>\n";

		XML+=inde+"</MarcoOntologicoColectivo>\n";
		return XML;
	}
}

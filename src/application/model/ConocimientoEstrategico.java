package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class ConocimientoEstrategico implements nodo{
	private StringProperty Valor_conocimiento,proceso_generador,confiabilidad,Vmaximo,Vminimo;
	private agent agente_generador;
	private String agenteID;
	public TreeItem<Object> nodo_arbol;

	public String getAgenteID() {
		return agenteID;
	}

	public void setAgenteID(String agenteID) {
		this.agenteID = agenteID;
	}

	public void refactor(Main main){
		agente_generador=main.refactor(agenteID);
	}

	private ObservableList<Caracteristica> caracterizacion;


	public ConocimientoEstrategico(){
		nodo_arbol=new TreeItem<>("Conocimiento Estratégico");
		Valor_conocimiento=new SimpleStringProperty();
		proceso_generador=new SimpleStringProperty();
		confiabilidad=new SimpleStringProperty();
		Vmaximo=new SimpleStringProperty();
		Vminimo=new SimpleStringProperty();
		agente_generador=new agent();
		caracterizacion=FXCollections.observableArrayList();
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<ConocimientoEstrategico>\n";
			XML+=inde+"\t<ValorConocimiento>"+this.Valor_conocimiento.get()+"</ValorConocimiento>\n";
			XML+=inde+"\t<ProcesoGenerador>"+this.proceso_generador.get()+"</ProcesoGenerador>\n";
			XML+=inde+"\t<Confiabilidad>"+this.confiabilidad.get()+"</Confiabilidad>\n";
			XML+=inde+"\t<VMaximo>"+this.Vmaximo.get()+"</VMaximo>\n";
			XML+=inde+"\t<VMinimo>"+this.Vminimo.get()+"</VMinimo>\n";
			if(this.agente_generador!=null)
				XML+=inde+"\t<AgenteGenerador>"+this.agente_generador.id+"</AgenteGenerador>\n";
			XML+=inde+"\t<Caracterizacion>\n";
			for (Caracteristica caracteristica : caracterizacion) {
				XML+=caracteristica.to_XML(indentacion+2);
			}
			XML+=inde+"\t</Caracterizacion>\n";
		XML+=inde+"</ConocimientoEstrategico>\n";

		return XML;
	}

	public ConocimientoEstrategico(String valor,String proceso,String confiabilidad,String maximo,String minimo,agent agente){
		Valor_conocimiento=new SimpleStringProperty(valor);
		proceso_generador=new SimpleStringProperty(proceso);
		this.confiabilidad=new SimpleStringProperty(confiabilidad);
		Vmaximo=new SimpleStringProperty(maximo);
		Vminimo=new SimpleStringProperty(minimo);
		this.agente_generador=agente;
	}

	public StringProperty getValor_conocimiento() {
		return Valor_conocimiento;
	}
	public void setValor_conocimiento(StringProperty valor_conocimiento) {
		Valor_conocimiento = valor_conocimiento;
	}
	public StringProperty getProceso_generador() {
		return proceso_generador;
	}
	public void setProceso_generador(StringProperty proceso_generador) {
		this.proceso_generador = proceso_generador;
	}
	public StringProperty getConfiabilidad() {
		return confiabilidad;
	}
	public void setConfiabilidad(StringProperty confiabilidad) {
		this.confiabilidad = confiabilidad;
	}
	public StringProperty getVmaximo() {
		return Vmaximo;
	}
	public void setVmaximo(StringProperty vmaximo) {
		Vmaximo = vmaximo;
	}
	public StringProperty getVminimo() {
		return Vminimo;
	}
	public void setVminimo(StringProperty vminimo) {
		Vminimo = vminimo;
	}
	public agent getAgente_generador() {
		return agente_generador;
	}
	public void setAgente_generador(agent agente_generador) {
		this.agente_generador = agente_generador;
	}
	public ObservableList<Caracteristica> getCaracterizacion() {
		return caracterizacion;
	}
	public void setCaracterizacion(ObservableList<Caracteristica> caracterizacion) {
		this.caracterizacion = caracterizacion;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}


}

package application.model.modelos;
import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.conversacion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class CoordinationModel implements nodo{
	private ObservableList<conversacion> conversaciones=FXCollections.observableArrayList();
	private TreeItem<Object> nodo_arbol=null;

	public CoordinationModel(){
		conversaciones=FXCollections.observableArrayList();
		nodo_arbol=new TreeItem<Object>(this);
	}

	public void refactor(Main main){
		for (conversacion conversacion : conversaciones) {
			conversacion.refactor(main);
		}
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<CoordinationModel>\n";
			for (conversacion conversacion : conversaciones) {
				XML+=conversacion.to_XML(indentacion+1);
			}
		XML+=inde+"</CoordinationModel>\n";
		return XML;

	}

	public ObservableList<conversacion> getConversaciones() {
		return conversaciones;
	}

	public void setConversaciones(ObservableList<conversacion> conversaciones) {
		this.conversaciones = conversaciones;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		for (conversacion conversacion : conversaciones) {
			conversacion.accept(visitor,newClass);
		}
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}


}

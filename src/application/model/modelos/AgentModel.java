package application.model.modelos;

import java.util.ArrayList;
import java.util.List;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.CodeGenerator.clase;
import application.model.Servicio;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class AgentModel implements nodo{
	private String Agenteposicion,AgenteMarco_referencia,AgenteDescripcion;
	private List<String> AgenteComponentes;
	private ObservableList<application.model.objetivo> objetivo=FXCollections.observableArrayList();
	private ObservableList<Servicio> servicios=FXCollections.observableArrayList();
	private String PropiedadesNombre,PropiedadesCalidad,PropiedadesAuditable,PropiedadesGarantia,PropiedadesCapacidad,PropiedadesConfiabilidad;
	private String CapacidadHabilidades,CapacidadRepresentacion,CapacidadLenguaje;
	private String RestriccionNormas,RestriccionPreferencias,RestriccionPermisos;
	private TreeItem<Object> nodo=null;
	//private TreeItem<Object> nodoServicios=new TreeItem<Object>("Servicios");

		public String getComponentesString(){
			return this.AgenteComponentes.toString();
	}


	public AgentModel(){
		setNodo(new TreeItem<Object>(this));
		//nodo.getChildren().add(nodoServicios);
		Agenteposicion="";
		AgenteComponentes=new ArrayList<String>();

		AgenteMarco_referencia="";
		AgenteDescripcion="";
		PropiedadesNombre="";
		PropiedadesCalidad="";
		PropiedadesAuditable="";
		PropiedadesGarantia="";
		PropiedadesCapacidad="";
		PropiedadesConfiabilidad="";
		CapacidadHabilidades="";
		CapacidadRepresentacion="";
		CapacidadLenguaje="";
		RestriccionNormas="";
		RestriccionPreferencias="";
		RestriccionPermisos="";
	}

/*	public TreeItem<Object> getNodoServicios() {
		return nodoServicios;
	}
	public void setNodoServicios(TreeItem<Object> nodoServicios) {
		this.nodoServicios = nodoServicios;
	}*/
	public ObservableList<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(ObservableList<Servicio> servicios) {
		this.servicios = servicios;
	}
	public String getAgenteDescripcion() {
		return AgenteDescripcion;
	}
	public void setAgenteDescripcion(String agenteDescripcion) {
		AgenteDescripcion = agenteDescripcion;
	}
	public String getAgenteMarco_referencia() {
		return AgenteMarco_referencia;
	}
	public void setAgenteMarco_referencia(String agenteMarco_referencia) {
		AgenteMarco_referencia = agenteMarco_referencia;
	}
	public String getAgenteposicion() {
		return Agenteposicion;
	}
	public void setAgenteposicion(String agenteposicion) {
		Agenteposicion = agenteposicion;
	}
	public List<String> getAgenteComponentes() {
		return AgenteComponentes;
	}
	public void setAgenteComponentes(List<String> agenteComponentes) {
		AgenteComponentes = agenteComponentes;
	}
	public String getPropiedadesNombre() {
		return PropiedadesNombre;
	}
	public void setPropiedadesNombre(String propiedadesNombre) {
		PropiedadesNombre = propiedadesNombre;
	}
	public String getPropiedadesCalidad() {
		return PropiedadesCalidad;
	}
	public void setPropiedadesCalidad(String propiedadesCalidad) {
		PropiedadesCalidad = propiedadesCalidad;
	}
	public String getPropiedadesAuditable() {
		return PropiedadesAuditable;
	}
	public void setPropiedadesAuditable(String propiedadesAuditable) {
		PropiedadesAuditable = propiedadesAuditable;
	}
	public String getPropiedadesGarantia() {
		return PropiedadesGarantia;
	}
	public void setPropiedadesGarantia(String propiedadesGarantia) {
		PropiedadesGarantia = propiedadesGarantia;
	}
	public String getPropiedadesCapacidad() {
		return PropiedadesCapacidad;
	}
	public void setPropiedadesCapacidad(String propiedadesCapacidad) {
		PropiedadesCapacidad = propiedadesCapacidad;
	}
	public String getPropiedadesConfiabilidad() {
		return PropiedadesConfiabilidad;
	}
	public void setPropiedadesConfiabilidad(String propiedadesConfiabilidad) {
		PropiedadesConfiabilidad = propiedadesConfiabilidad;
	}
	public String getCapacidadHabilidades() {
		return CapacidadHabilidades;
	}
	public void setCapacidadHabilidades(String capacidadHabilidades) {
		CapacidadHabilidades = capacidadHabilidades;
	}
	public String getCapacidadRepresentacion() {
		return CapacidadRepresentacion;
	}
	public void setCapacidadRepresentacion(String capacidadRepresentacion) {
		CapacidadRepresentacion = capacidadRepresentacion;
	}
	public String getCapacidadLenguaje() {
		return CapacidadLenguaje;
	}
	public void setCapacidadLenguaje(String capacidadLenguaje) {
		CapacidadLenguaje = capacidadLenguaje;
	}
	public String getRestriccionNormas() {
		return RestriccionNormas;
	}
	public void setRestriccionNormas(String restriccionNormas) {
		RestriccionNormas = restriccionNormas;
	}
	public String getRestriccionPreferencias() {
		return RestriccionPreferencias;
	}
	public void setRestriccionPreferencias(String restriccionPreferencias) {
		RestriccionPreferencias = restriccionPreferencias;
	}
	public String getRestriccionPermisos() {
		return RestriccionPermisos;
	}
	public void setRestriccionPermisos(String restriccionPermisos) {
		RestriccionPermisos = restriccionPermisos;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass,TareaModel tareas) {
		for (Servicio s : servicios) {
			s.accept(visitor,newClass,tareas);
		}
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass) {


	}

	public String Propiedades_Tostring(){
		String cadena="";
		if(!PropiedadesNombre.equals("")){
			cadena+="Propiedades:\n";
			cadena+="\tCalidad:"+PropiedadesCalidad+"\n";
			cadena+="\tAuditable:"+PropiedadesAuditable+"\n";
			cadena+="\tGarantia:"+PropiedadesGarantia+"\n";
			cadena+="\tCapacidad:"+PropiedadesCapacidad+"\n";
			cadena+="\tConfiabilidad:"+PropiedadesConfiabilidad+"\n";
		}
		return cadena;
	}

	public String Capacidad_Tostring(){
		String cadena="";
		if(!CapacidadHabilidades.equals("")){
			cadena+="Capacidad:\n";
			cadena+="\tHabilidades:"+CapacidadHabilidades+"\n";
			cadena+="\tRepresentación del conocimiento:"+CapacidadRepresentacion+"\n";
			cadena+="\tLenguaje de Comunicación:"+CapacidadLenguaje+"\n";
		}
		return cadena;
	}

	public String Restriccion_Tostring(){
		String cadena="";
		cadena+="Restriccion:\n";
		if(!RestriccionNormas.equals(""))
			cadena+="\tNormas:"+RestriccionNormas+"\n";
		if(!RestriccionPreferencias.equals(""))
			cadena+="\tPreferencias:"+RestriccionPreferencias+"\n";
		if(!RestriccionPermisos.equals(""))
			cadena+="\tPermisos:"+RestriccionPermisos+"\n";

		return cadena;
	}

	public TreeItem<Object> getNodo() {
		return nodo;
	}
	public void setNodo(TreeItem<Object> nodo) {
		this.nodo = nodo;
	}
	public ObservableList<application.model.objetivo> getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(ObservableList<application.model.objetivo> objetivo) {
		this.objetivo = objetivo;
	}
	public void refactor() {
		for (Servicio s : this.servicios) {
			for (application.model.objetivo o : this.objetivo) {
				if(o.getId().equals(s.getObjetivoId())){
					s.setMiObjetivo(o);
				}
			}
		}
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<AgentModel>\n";

		//===Agente
		XML+= inde+"\t<Posicion>" +this.Agenteposicion+"</Posicion>\n";
		XML+=inde+"\t<MarcoReferencia>"+this.AgenteMarco_referencia+"</MarcoReferencia>\n";
		XML+=inde+"\t<AgenteDescripcion>"+this.AgenteDescripcion+"</AgenteDescripcion>\n";
		XML+=inde+"\t<Componentes>\n";
		for (String compo : AgenteComponentes) {
			XML+=inde+"\t\t<componente>"+compo+"</componente>\n";
		}
		XML+=inde+"\t</Componentes>\n";
		//===Objetivos
		XML+=inde+"\t<Objetivos>\n";

		for (application.model.objetivo obj : objetivo) {
				XML+=inde+"\t\t<Objetivo>\n";
				XML+=inde+"\t\t\t<Id>"+obj.getId()+"</Id>\n";
				XML+=inde+"\t\t\t<Nombre>"+obj.getObjetivoNombre()+"</Nombre>\n";
				XML+=inde+"\t\t\t<Descripcion>"+obj.getObjetivoDescripcion()+"</Descripcion>\n";
				XML+=inde+"\t\t\t<Activacion>"+obj.getObjetivoActivacion()+"</Activacion>\n";
				XML+=inde+"\t\t\t<Finalizacion>"+obj.getObjetivoFinalizacion()+"</Finalizacion>\n";
				XML+=inde+"\t\t\t<Exito>"+obj.getObjetivoExito()+"</Exito>\n";
				XML+=inde+"\t\t\t<Fracaso>"+obj.getObjetivoFracaso()+"</Fracaso>\n";
				XML+=inde+"\t\t\t<Ontologia>"+obj.getObjetivoOntologia()+"</Ontologia>\n";
				XML+=inde+"\t\t\t<Entradas>\n";
					for (String entrada : obj.getObjetivoEntrada()) {
						XML+=inde+"\t\t\t\t<Entrada>"+entrada+"</Entrada>\n";
					}
				XML+=inde+"\t\t\t</Entradas>\n";
				XML+=inde+"\t\t\t<Salidas>\n";
					for (String salida : obj.getObjetivoSalida()) {
						XML+=inde+"\t\t\t\t<Salida>"+salida+"</Salida>\n";
					}
				XML+=inde+"\t\t\t</Salidas>\n";
				XML+=inde+"\t\t</Objetivo>\n";
		}
		XML+=inde+"\t</Objetivos>\n";

		//===Servicios
		XML+=inde+"\t<Servicios>\n";
		for (Servicio servicio : servicios) {
			XML+=servicio.to_XML(indentacion+2);
		}
		XML+=inde+"\t</Servicios>\n";

		//===Propiedades
		XML+=inde+"\t<Propiedades>\n";
			XML+=inde+"\t\t<Nombre>"+this.PropiedadesNombre+"</Nombre>\n";
			XML+=inde+"\t\t<Calidad>"+this.PropiedadesCalidad+"</Calidad>\n";
			XML+=inde+"\t\t<Auditable>"+this.PropiedadesAuditable+"</Auditable>\n";
			XML+=inde+"\t\t<Garantia>"+this.PropiedadesGarantia+"</Garantia>\n";
			XML+=inde+"\t\t<Capacidad>"+this.PropiedadesCapacidad+"</Capacidad>\n";
			XML+=inde+"\t\t<Confiabilidad>"+this.PropiedadesConfiabilidad+"</Confiabilidad>\n";
		XML+=inde+"\t</Propiedades>\n";

		//===Capacidad
		XML+=inde+"\t<Capacidad>\n";
			XML+=inde+"\t\t<Habilidades>"+this.CapacidadHabilidades+"</Habilidades>\n";
			XML+=inde+"\t\t<Representacion>"+this.CapacidadRepresentacion+"</Representacion>\n";
			XML+=inde+"\t\t<Lenguaje>"+this.CapacidadLenguaje+"</Lenguaje>\n";
		XML+=inde+"\t</Capacidad>\n";

		//===Restriccion
		XML+=inde+"\t<Restriccion>\n";
			XML+=inde+"\t\t<Normas>"+this.RestriccionNormas+"</Normas>\n";
			XML+=inde+"\t\t<Preferencias>"+this.RestriccionPreferencias+"</Preferencias>\n";
			XML+=inde+"\t\t<Permisos>"+this.RestriccionPermisos+"</Permisos>\n";
		XML+=inde+"\t</Restriccion>\n";

		//XML+=modelo_agentes.to_XML(indentacion+1);
		XML+="\n"+inde+"</AgentModel>\n";
		return XML;
	}

}

package application.model.modelos;


import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.Tarea;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class TareaModel implements nodo{
	private ObservableList<Tarea> tareas=FXCollections.observableArrayList();
	private TreeItem<Object> nodo_arbol=null;
	private TreeItem<Object> nodo_servicios=null;

	public TareaModel(){
		nodo_arbol=new TreeItem<Object>(this);
		nodo_servicios=new TreeItem<Object>("Servicios");
		nodo_arbol.getChildren().add(nodo_servicios);
	}

	public void refactor(Main main){
		for (Tarea tarea : tareas) {
			tarea.refactor(main);
			tarea.getAsociado().getNodo().getChildren().add(tarea.getNodo_arbol());
			//nodo_arbol.getChildren().add(tarea.getNodo_arbol());
		}
	}


	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="\n<TaskModel>\n";
			for (Tarea tarea : tareas) {
				XML+=inde+"\t<Task>\n";
				XML+=tarea.to_XML(indentacion+2);
				XML+=inde+"\t</Task>\n";
			}
		XML+="\n"+inde+"</TaskModel>\n";
		return XML;

	}

	public TreeItem<Object> getNodo_servicios() {
		return nodo_servicios;
	}

	public void setNodo_servicios(TreeItem<Object> nodo_servicios) {
		this.nodo_servicios = nodo_servicios;
	}

	public ObservableList<Tarea> getTareas() {
		return tareas;
	}

	public void setTareas(ObservableList<Tarea> tareas) {
		this.tareas = tareas;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		/*for (Tarea tarea : tareas) {
			tarea.accept(visitor,newClass);
		}*/
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {

	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}
}

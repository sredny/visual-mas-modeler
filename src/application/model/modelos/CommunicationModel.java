package application.model.modelos;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.ActoHabla;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class CommunicationModel implements nodo{
	private ObservableList<ActoHabla> acto =FXCollections.observableArrayList();
	private TreeItem<Object> nodo_arbol=null;

	public CommunicationModel(){
		acto=FXCollections.observableArrayList();
		nodo_arbol=new TreeItem<Object>(this);
	}

	public void refactor(Main main){
		for (ActoHabla actoHabla : acto) {
			actoHabla.refactor(main);
			nodo_arbol.getChildren().add(actoHabla.getNodo_arbol());
		}
	}

	public ObservableList<ActoHabla> getActo() {
		return acto;
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<CommunicationModel>\n";
		for(ActoHabla ac: acto){
			XML+=ac.to_XML(indentacion+1);
		}
		XML+=inde+"</CommunicationModel>\n";

		return XML;
	}

	public void setActo(ObservableList<ActoHabla> acto) {
		this.acto = acto;
	}


	public ObservableList<ActoHabla> getActor() {
		return acto;
	}

	public void setActor(ObservableList<ActoHabla> actor) {
		this.acto = actor;
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass) {
		for (ActoHabla actoHabla : acto) {
			actoHabla.accept(visitor,newClass);
		}
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}



}

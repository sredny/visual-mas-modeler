package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class OntologiaDominio implements nodo{
	private MecanismoAprendizaje aprendizaje;
	private MecanismoRazonamiento razonamiento;
	private StringProperty descripcion,fuente,Vmaximo,Vminimo;
	private ObservableList<Caracteristica> caracterizacion;
	public TreeItem<Object> nodoarbol;
	public TreeItem<Object> nodoAprendizaje;
	public TreeItem<Object> nodoRazonamiento;

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<OntologiaDominio>\n";
			XML+=inde+"<Descripcion>"+this.descripcion.get()+"</Descripcion>\n";
			XML+=inde+"<Fuente>"+this.fuente.get()+"</Fuente>\n";
			XML+=inde+"<VMaximo>"+this.Vmaximo.get()+"</VMaximo>\n";
			XML+=inde+"<VMinimo>"+this.Vminimo.get()+"</VMinimo>\n";
			XML+=aprendizaje.to_XML(indentacion);
			XML+=razonamiento.to_XML(indentacion);
			XML+=inde+"<Caracterizacion>\n";
			for (Caracteristica caracteristica : caracterizacion) {
				XML+=caracteristica.to_XML(indentacion+1);
			}
			XML+=inde+"</Caracterizacion>\n";
		XML+="</OntologiaDominio>\n";
		return XML;
}

	public OntologiaDominio(){
		aprendizaje=new MecanismoAprendizaje();
		razonamiento=new MecanismoRazonamiento();
		nodoAprendizaje=new TreeItem<Object>(aprendizaje);
		nodoRazonamiento=new TreeItem<Object>(razonamiento);

		descripcion=new SimpleStringProperty("Sin información");
		fuente=new SimpleStringProperty("Sin información");
		Vmaximo=new SimpleStringProperty("Sin información");
		Vminimo=new SimpleStringProperty("Sin información");
		caracterizacion=FXCollections.observableArrayList();
		nodoarbol=new TreeItem<Object>(this);
		nodoarbol.getChildren().addAll(nodoAprendizaje,nodoRazonamiento);
	}

	public OntologiaDominio(MecanismoAprendizaje aprendizaje,MecanismoRazonamiento razonamiento,String descripcion,String fuente,String maximo,String minimo){
		this.aprendizaje=aprendizaje;
		this.razonamiento=razonamiento;
		this.descripcion=new SimpleStringProperty(descripcion);
		this.fuente=new SimpleStringProperty(fuente);
		this.Vmaximo=new SimpleStringProperty(maximo);
		this.Vminimo=new SimpleStringProperty(minimo);
		caracterizacion=FXCollections.observableArrayList();
	}

	public MecanismoAprendizaje getAprendizaje() {
		return aprendizaje;
	}
	public void setAprendizaje(MecanismoAprendizaje aprendizaje) {
		this.aprendizaje = aprendizaje;
		nodoAprendizaje=aprendizaje.nodo_arbol;
	}
	public MecanismoRazonamiento getRazonamiento() {
		return razonamiento;
	}
	public void setRazonamiento(MecanismoRazonamiento razonamiento) {
		this.razonamiento = razonamiento;
		nodoRazonamiento=razonamiento.nodo_arbol;
	}
	public StringProperty getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}
	public StringProperty getFuente() {
		return fuente;
	}
	public void setFuente(StringProperty fuente) {
		this.fuente = fuente;
	}
	public StringProperty getVmaximo() {
		return Vmaximo;
	}
	public void setVmaximo(StringProperty vmaximo) {
		Vmaximo = vmaximo;
	}
	public StringProperty getVminimo() {
		return Vminimo;
	}
	public void setVminimo(StringProperty vminimo) {
		Vminimo = vminimo;
	}
	public ObservableList<Caracteristica> getCaracterizacion() {
		return caracterizacion;
	}
	public void setCaracterizacion(ObservableList<Caracteristica> caracterizacion) {
		this.caracterizacion = caracterizacion;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		aprendizaje.accept(visitor,newClass);
		razonamiento.accept(visitor,newClass);
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}


}

package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;

public class MecanismoRazonamiento implements nodo{
	private StringProperty fuente_informacion,fuente_alimentacion,tecnica_inferencia,lenguaje_representacion,relacion,estrategia_razonamiento;
	public TreeItem<Object> nodo_arbol;

	public MecanismoRazonamiento(){
		fuente_informacion=new SimpleStringProperty();
		fuente_alimentacion=new SimpleStringProperty();
		tecnica_inferencia=new SimpleStringProperty();
		lenguaje_representacion=new SimpleStringProperty();
		relacion=new SimpleStringProperty();
		estrategia_razonamiento=new SimpleStringProperty();
		nodo_arbol=new TreeItem<Object>(this);
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde+"<MecanismoRazonamiento>\n";
		XML+=inde+"\t<FuenteInformacion>"+this.fuente_informacion.get()+"</FuenteInformacion>\n";
		XML+=inde+"\t<FuenteAlimentacion>"+this.fuente_alimentacion.get()+"</FuenteAlimentacion>\n";
		XML+=inde+"\t<TecnicaInferencia>"+this.tecnica_inferencia.get()+"</TecnicaInferencia>\n";
		XML+=inde+"\t<LenguajeRepresentacion>"+this.lenguaje_representacion.get()+"</LenguajeRepresentacion>\n";
		XML+=inde+"\t<Relacion>"+this.relacion.get()+"</Relacion>\n";
		XML+=inde+"\t<EstrategiaRazonamiento>"+this.estrategia_razonamiento.get()+"</EstrategiaRazonamiento>\n";

		XML+=inde+"</MecanismoRazonamiento>\n";

		return XML;
	}

	public MecanismoRazonamiento(String fuente_i,String fuente_a,String tecnica,String lenguaje,String relacion,String estrategia){
		this.fuente_informacion=new SimpleStringProperty(fuente_i);
		this.fuente_alimentacion=new SimpleStringProperty(fuente_a);
		this.tecnica_inferencia=new SimpleStringProperty(tecnica);
		this.lenguaje_representacion=new SimpleStringProperty(lenguaje);
		this.relacion=new SimpleStringProperty(relacion);
		this.estrategia_razonamiento=new SimpleStringProperty(estrategia);
		nodo_arbol=new TreeItem<Object>(this);
	}

	public StringProperty getFuente_informacion() {
		return fuente_informacion;
	}

	public void setFuente_informacion(StringProperty fuente_informacion) {
		this.fuente_informacion = fuente_informacion;
	}

	public StringProperty getFuente_alimentacion() {
		return fuente_alimentacion;
	}

	public void setFuente_alimentacion(StringProperty fuente_alimentacion) {
		this.fuente_alimentacion = fuente_alimentacion;
	}

	public StringProperty getTecnica_inferencia() {
		return tecnica_inferencia;
	}

	public void setTecnica_inferencia(StringProperty tecnica_inferencia) {
		this.tecnica_inferencia = tecnica_inferencia;
	}

	public StringProperty getLenguaje_representacion() {
		return lenguaje_representacion;
	}

	public void setLenguaje_representacion(StringProperty lenguaje_representacion) {
		this.lenguaje_representacion = lenguaje_representacion;
	}

	public StringProperty getRelacion() {
		return relacion;
	}

	public void setRelacion(StringProperty relacion) {
		this.relacion = relacion;
	}

	public StringProperty getEstrategia_razonamiento() {
		return estrategia_razonamiento;
	}

	public void setEstrategia_razonamiento(StringProperty estrategia_razonamiento) {
		this.estrategia_razonamiento = estrategia_razonamiento;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {

	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}



}

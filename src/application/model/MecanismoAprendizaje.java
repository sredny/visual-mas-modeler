package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;

public class MecanismoAprendizaje implements nodo{
	private StringProperty nombre,tipo,tecnica_representacion,fuente_aprendizaje,mecanismo;
	public TreeItem<Object> nodo_arbol;

	public MecanismoAprendizaje(){
		nombre=new SimpleStringProperty("");
		tipo=new SimpleStringProperty("");
		tecnica_representacion=new SimpleStringProperty("");
		fuente_aprendizaje=new SimpleStringProperty("");
		mecanismo=new SimpleStringProperty("");
		nodo_arbol=new TreeItem<Object>(this);
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde+"<MecanismoAprendizaje>\n";
			XML+=inde+"\t<Nombre>"+this.nombre.get()+"</Nombre>\n";
			XML+=inde+"\t<Tipo>"+this.tipo.get()+"</Tipo>\n";
			XML+=inde+"\t<TecnicaRepresentacion>"+this.tecnica_representacion.get()+"</TecnicaRepresentacion>\n";
			XML+=inde+"\t<FuenteAprendizaje>"+this.fuente_aprendizaje.get()+"</FuenteAprendizaje>\n";
			XML+=inde+"\t<MecanismoActualizacion>"+this.mecanismo.get()+"</MecanismoActualizacion>\n";
		XML+=inde+"</MecanismoAprendizaje>\n";

		return XML;
	}

	public MecanismoAprendizaje(String nombre,String tipo,String tecnica,String fuente,String mecanismo){
		this.nombre=new SimpleStringProperty(nombre);
		this.tipo=new SimpleStringProperty(tipo);
		this.tecnica_representacion=new SimpleStringProperty(tecnica);
		this.fuente_aprendizaje=new SimpleStringProperty(fuente);
		this.mecanismo=new SimpleStringProperty(mecanismo);
		nodo_arbol=new TreeItem<Object>(this);
	}

	public StringProperty getNombre() {
		return nombre;
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public StringProperty getTipo() {
		return tipo;
	}

	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}

	public StringProperty getTecnica_representacion() {
		return tecnica_representacion;
	}

	public void setTecnica_representacion(StringProperty tecnica_representacion) {
		this.tecnica_representacion = tecnica_representacion;
	}

	public StringProperty getFuente_aprendizaje() {
		return fuente_aprendizaje;
	}

	public void setFuente_aprendizaje(StringProperty fuente_aprendizaje) {
		this.fuente_aprendizaje = fuente_aprendizaje;
	}

	public StringProperty getMecanismo() {
		return mecanismo;
	}

	public void setMecanismo(StringProperty mecanismo) {
		this.mecanismo = mecanismo;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}



}

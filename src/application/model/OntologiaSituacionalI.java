package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.scene.control.TreeItem;

public class OntologiaSituacionalI implements nodo{
	private MecanismoAprendizaje aprendizaje;
	private MecanismoRazonamiento razonamiento;
	private ConocimientoEstrategico estrategico;
	private ConocimientoTareas tareas;
	public TreeItem<Object> nodoarbol;
	public TreeItem<Object> nodoAprendizaje;
	public TreeItem<Object> nodoRazonamiento;


	public OntologiaSituacionalI(){
		estrategico=new ConocimientoEstrategico();
		tareas=new ConocimientoTareas();
		aprendizaje=new MecanismoAprendizaje();
		razonamiento=new MecanismoRazonamiento();

		nodoAprendizaje=new TreeItem<Object>(aprendizaje);
		nodoRazonamiento=new TreeItem<Object>(razonamiento);
		nodoarbol=new TreeItem<Object>(this);
		nodoarbol.getChildren().addAll(nodoAprendizaje,nodoRazonamiento);
	}

	public void refactor(Main main){
		estrategico.refactor(main);
		tareas.refactor(main);

		nodoarbol.getChildren().add(estrategico.nodo_arbol);
		nodoarbol.getChildren().add(tareas.nodo_arbol);
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<OntologiaSituacionalIndividual>\n";
			XML+=aprendizaje.to_XML(indentacion);
			XML+=razonamiento.to_XML(indentacion);
			XML+=estrategico.to_XML(indentacion+1);
			XML+=tareas.to_XML(indentacion+1);
		XML+=inde+"</OntologiaSituacionalIndividual>\n";

		return XML;
	}

	public ConocimientoEstrategico getEstrategico() {
		return estrategico;
	}
	public void setEstrategico(ConocimientoEstrategico estrategico) {
		this.estrategico = estrategico;
	}
	public ConocimientoTareas getTareas() {
		return tareas;
	}
	public void setTareas(ConocimientoTareas tareas) {
		this.tareas = tareas;
	}

	public MecanismoAprendizaje getAprendizaje() {
		return aprendizaje;
	}

	public void setAprendizaje(MecanismoAprendizaje aprendizaje) {
		nodoarbol.getChildren().remove(nodoAprendizaje);
		this.aprendizaje = aprendizaje;
		this.nodoAprendizaje=aprendizaje.nodo_arbol;
		nodoarbol.getChildren().add(nodoAprendizaje);
	}

	public MecanismoRazonamiento getRazonamiento() {
		return razonamiento;
	}

	public void setRazonamiento(MecanismoRazonamiento razonamiento) {
		nodoarbol.getChildren().remove(nodoRazonamiento);
		this.razonamiento = razonamiento;
		this.nodoRazonamiento=razonamiento.nodo_arbol;
		nodoarbol.getChildren().add(this.nodoRazonamiento);
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		aprendizaje.accept(visitor,newClass);
		razonamiento.accept(visitor,newClass);
		estrategico.accept(visitor,newClass);
		tareas.accept(visitor,newClass);
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub
	}


}

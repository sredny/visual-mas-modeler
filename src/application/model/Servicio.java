package application.model;

import java.util.ArrayList;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class Servicio implements nodo{

		private StringProperty nombre,descripcion,tipo,comportamiento;
		//private List<StringProperty> ServiciosEntrada,ServiciosSalida;
		private List<Parametros> ServicioPentrada,ServicioPsalida;
		private ObservableList<Caracteristica> propiedades;
		private String id;
		private SecureRandom random = new SecureRandom();
		private TreeItem<Object> nodo=null;
		private objetivo miObjetivo;
		private String ObjetivoId;

		public Servicio(){
			setMiObjetivo(null);
			nodo=new TreeItem<Object>(this);
			setPropiedades(FXCollections.observableArrayList());
			nombre=new SimpleStringProperty(null);
			descripcion=new SimpleStringProperty(null);
			tipo=new SimpleStringProperty(null);
			comportamiento=new SimpleStringProperty("SimpleBehaviour");
			//ServiciosEntrada=new ArrayList<StringProperty>();
			//ServiciosSalida=new ArrayList<StringProperty>();
			setServicioPentrada(new ArrayList<Parametros>());
			setServicioPsalida(new ArrayList<Parametros>());
			id=new BigInteger(130, random).toString(32);
		}


		public Servicio(String name,String type,String descrip){
			nodo=new TreeItem<Object>(this);
			nombre=new SimpleStringProperty(name);
			tipo=new SimpleStringProperty(type);
			descripcion=new SimpleStringProperty(descrip);
			id=new BigInteger(130, random).toString(32);
			nodo=new TreeItem<Object>(this);
		}

		@Override
		public String to_XML(int indentacion) {
			String inde="";
			String XML="";
			for(int i=0;i<indentacion;i++)
				inde+="\t";

			XML=inde;
			XML+="<Servicio nombre='"+nombre.get()+"' id='"+this.id+"'>\n";
			XML+=inde+"\t<descripcion>"+this.descripcion.get()+"</descripcion>\n";
			XML+=inde+"\t<tipo>"+this.tipo.get()+"</tipo>\n";
			XML+=inde+"\t<Comportamiento>"+this.comportamiento.get()+"</Comportamiento>\n";
			if(miObjetivo!=null)
				XML+=inde+"\t<Objetivo>"+miObjetivo.getId()+"</Objetivo>\n";
			XML+=inde+"\t<Parametros>\n";
			XML+=inde+"\t\t<Entrada>\n";

			for(Parametros p: ServicioPentrada){
					XML+=inde+p.to_XML(indentacion+1);
				}
			XML+=inde+"\t\t</Entrada>\n";

			XML+=inde+"\t\t<Salida>\n";
			for(Parametros p: ServicioPsalida){
					XML+=inde+"\t\t"+p.to_XML(indentacion+1);
				}
			XML+=inde+"\t\t</Salida>\n";
			XML+=inde+"\t</Parametros>\n";

			XML+=inde+"\t<Propiedades>\n";
				for (Caracteristica c : this.propiedades) {
					XML+=c.to_XML(indentacion+2);
				}
			XML+=inde+"\t</Propiedades>\n";
			XML+="</Servicio>\n";

			return XML;
		}

		public StringProperty getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = new SimpleStringProperty(nombre);
		}
		public StringProperty getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = new SimpleStringProperty(descripcion);
		}
		public StringProperty getTipo() {
			return tipo;
		}
		public void setTipo(String tipo) {
			this.tipo = new SimpleStringProperty(tipo);
		}


		@Override
		public void accept(nodoVisitor visitor,clase newClass, TareaModel tareas) {
			visitor.visit(this,newClass,tareas);
		}

		@Override
		public void accept(nodoVisitor visitor, clase newClass) {
			// TODO Auto-generated method stub
		}

		public String getComportamiento() {
			return comportamiento.get();
		}

		public void setComportamiento(String comportamiento) {
			this.comportamiento = new SimpleStringProperty(comportamiento);
		}

		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}

		public String EntradaTo_String(){
			String cadena="";
			if(ServicioPentrada.size()>0){
				cadena+="Parametros de entrada:\n";
				for (Parametros p : ServicioPentrada) {
					cadena+="\t\t"+p.getNombre().get()+"  Tipo:"+p.getType()+"\n";
				}
			}
			return cadena;
		}

		public String EntradaTo_StringMetodo(){
			String cadena="";
			int i=0;
				for (Parametros p : ServicioPentrada) {
					if(i!=0)
						cadena+=",";
					i++;
					cadena+=p.getType()+" "+Main.FormatearAtributoMetodo(p.getNombre().get());
				}
			return cadena;
		}

		public String SalidaTo_String(){
			String cadena="";
			/*if(ServiciosSalida.size()>0){
				cadena+="Parametros de Salida:\n";
				for (StringProperty stringProperty : ServiciosSalida) {
					cadena+="\t\t"+stringProperty.get()+"\n";
				}
			}*/
			return cadena;
		}


		public TreeItem<Object> getNodo() {
			return nodo;
		}


		public void setNodo(TreeItem<Object> nodo) {
			this.nodo = nodo;
		}


		public List<Parametros> getServicioPentrada() {
			return ServicioPentrada;
		}


		public void setServicioPentrada(List<Parametros> servicioPentrada) {
			ServicioPentrada = servicioPentrada;
		}


		public List<Parametros> getServicioPsalida() {
			return ServicioPsalida;
		}


		public void setServicioPsalida(List<Parametros> servicioPsalida) {
			ServicioPsalida = servicioPsalida;
		}


		public ObservableList<Caracteristica> getPropiedades() {
			return propiedades;
		}


		public void setPropiedades(ObservableList<Caracteristica> propiedades) {
			this.propiedades = propiedades;
		}


		public objetivo getMiObjetivo() {
			return miObjetivo;
		}


		public void setMiObjetivo(objetivo miObjetivo) {
			this.miObjetivo = miObjetivo;
		}


		public String getObjetivoId() {
			return ObjetivoId;
		}


		public void setObjetivoId(String objetivoId) {
			ObjetivoId = objetivoId;
		}

}

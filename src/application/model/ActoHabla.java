package application.model;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.security.SecureRandom;

public class ActoHabla implements nodo{
	 private StringProperty nombre,tipo,objetivo,datosintercambiados,precondicion,CTerminacion,descripcion;
	 private ObservableList<agent> agentesAH=FXCollections.observableArrayList();
	 private List<String> agentesids;
	 private SecureRandom random = new SecureRandom();
	 public String id=new BigInteger(130, random).toString(32);
	 private TreeItem<Object> nodo_arbol=null;

	public ActoHabla(){
		nodo_arbol=new TreeItem<Object>(this);
		nombre=new SimpleStringProperty();
		tipo=new SimpleStringProperty();
		objetivo=new SimpleStringProperty();
		datosintercambiados=new SimpleStringProperty();
		precondicion=new SimpleStringProperty();
		CTerminacion=new SimpleStringProperty();
		descripcion=new SimpleStringProperty();
		agentesAH= FXCollections.observableArrayList();
		agentesids= new ArrayList<>();

	}

	public List<String> getAgentesids() {
		return agentesids;
	}

	public void setAgentesids(List<String> agentesids) {
		this.agentesids = agentesids;
	}

	public ObservableList<agent> getAgentesAH() {
		return agentesAH;
	}

	public void setAgentesAH(ObservableList<agent> agentesAH) {
		this.agentesAH = agentesAH;
	}

	public StringProperty getNombre() {
		return nombre;
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public StringProperty getTipo() {
		return tipo;
	}

	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}

	public StringProperty getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(StringProperty objetivo) {
		this.objetivo = objetivo;
	}

	public StringProperty getDatosintercambiados() {
		return datosintercambiados;
	}

	public void setDatosintercambiados(StringProperty datosintercambiados) {
		this.datosintercambiados = datosintercambiados;
	}

	public StringProperty getPrecondicion() {
		return precondicion;
	}

	public void setPrecondicion(StringProperty precondicion) {
		this.precondicion = precondicion;
	}

	public StringProperty getCTerminacion() {
		return CTerminacion;
	}

	public void setCTerminacion(StringProperty cTerminacion) {
		CTerminacion = cTerminacion;
	}

	public StringProperty getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}


	public void setAgentes(ObservableList<agent> agentes) {
		this.agentesAH = agentes;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}
	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}
	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}
	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}

	 public void refactor(Main main){
		 main.refactor(agentesAH, agentesids);
	 }
		@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<ActoHabla nombre='"+this.nombre.get()+"' tipo='"+this.tipo.get()+"' id='"+this.id+"'>\n";
			XML+=inde+"\t<objetivo>"+this.objetivo.get()+"</objetivo>\n";
			XML+=inde+"\t<DatosIntercambiado>"+this.datosintercambiados.get()+"</DatosIntercambiado>\n";
			XML+=inde+"\t<Precondicion>"+this.precondicion.get()+"</Precondicion>\n";
			XML+=inde+"\t<CondicionTerminacion>"+this.CTerminacion.get()+"</CondicionTerminacion>\n";
			XML+=inde+"\t<Descripcion>"+this.descripcion.get()+"</Descripcion>\n";
			XML+=inde+"\t<AgentesParticipantes>\n";
			for (agent agent : agentesAH) {
				XML+=inde+"\t\t<Agente>"+agent.id+"</Agente>\n";
			}
			XML+=inde+"\t</AgentesParticipantes>\n";
		XML+=inde+"</ActoHabla>\n";
		return XML;
	}
}

package application.model;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class OntologiaSituacionalC {
   private MecanismoAprendizaje aprendizaje;
   private MecanismoRazonamiento razonamiento;
   private StringProperty descripcion,fuente,confiabilidad,Valor_minimo,Valor_maximo;
   private ObservableList<Caracteristica> caracterizacion=FXCollections.observableArrayList();
   private TreeItem<Object> nodo_arbol;
   private TreeItem<Object> nodo_aprendizaje;
   private TreeItem<Object> nodo_razonamiento;

   public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde+"<OntologiaSituacionalColectiva>\n";
		XML+=inde+"\t<Descripcion>"+this.descripcion.get()+"</Descripcion>\n";
		XML+=inde+"\t<Fuente>"+this.fuente.get()+"</Fuente>\n";
		XML+=inde+"\t<Confiabilidad>"+this.confiabilidad.get()+"</Confiabilidad>\n";
		XML+=inde+"\t<ValorMinimo>"+this.Valor_minimo.get()+"</ValorMinimo>\n";
		XML+=inde+"\t<ValorMaximo>"+this.Valor_maximo.get()+"</ValorMaximo>\n";
		XML+=aprendizaje.to_XML(indentacion+1);
		XML+=razonamiento.to_XML(indentacion+1);
		XML+=inde+"\t<Caracterizacion>\n";
		for (Caracteristica caracteristica : caracterizacion) {
			XML+=caracteristica.to_XML(indentacion+2);
		}
		XML+=inde+"\t</Caracterizacion>\n";
		XML+=inde+"</OntologiaSituacionalColectiva>\n";

		return XML;
   }
   public OntologiaSituacionalC(){
	aprendizaje=new MecanismoAprendizaje();
	nodo_aprendizaje=new TreeItem<Object>(aprendizaje);
	razonamiento=new MecanismoRazonamiento();
	nodo_razonamiento=new TreeItem<Object>(razonamiento);
	descripcion=new SimpleStringProperty("");
	fuente=new SimpleStringProperty("");
	confiabilidad=new SimpleStringProperty("");
	Valor_maximo=new SimpleStringProperty("");
	Valor_minimo=new SimpleStringProperty("");
	caracterizacion=FXCollections.observableArrayList();

	nodo_arbol=new TreeItem<Object>(this);
	nodo_arbol.getChildren().addAll(nodo_aprendizaje,nodo_razonamiento);
   }

   public OntologiaSituacionalC(MecanismoAprendizaje ma,MecanismoRazonamiento mr,String descripcion,String fuente,String confiabilidad,String minimo,String maximo){
	   this.aprendizaje=ma;
	   this.razonamiento=mr;
	   this.descripcion=new SimpleStringProperty(descripcion);
	   this.fuente=new SimpleStringProperty(fuente);
	   this.confiabilidad=new SimpleStringProperty(confiabilidad);
	   this.Valor_minimo=new SimpleStringProperty(minimo);
	   this.Valor_maximo=new SimpleStringProperty(maximo);
   }

	public MecanismoRazonamiento getRazonamiento() {
		return razonamiento;
	}
	public void setRazonamiento(MecanismoRazonamiento razonamiento) {
		this.razonamiento = razonamiento;
		nodo_arbol.getChildren().remove(nodo_razonamiento);
		nodo_razonamiento=new TreeItem<Object>(this.razonamiento);
		nodo_arbol.getChildren().add(nodo_razonamiento);
	}
	public MecanismoAprendizaje getAprendizaje() {
		return aprendizaje;
	}
	public void setAprendizaje(MecanismoAprendizaje aprendizaje) {
		this.aprendizaje = aprendizaje;
		nodo_arbol.getChildren().remove(nodo_aprendizaje);
		nodo_aprendizaje=new TreeItem<Object>(this.aprendizaje);
		nodo_arbol.getChildren().add(nodo_aprendizaje);
	}
	public StringProperty getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}
	public StringProperty getFuente() {
		return fuente;
	}
	public void setFuente(StringProperty fuente) {
		this.fuente = fuente;
	}
	public StringProperty getConfiabilidad() {
		return confiabilidad;
	}
	public void setConfiabilidad(StringProperty confiabilidad) {
		this.confiabilidad = confiabilidad;
	}

	public StringProperty getValor_minimo() {
		return Valor_minimo;
	}
	public void setValor_minimo(StringProperty valor_minimo) {
		Valor_minimo = valor_minimo;
	}
	public StringProperty getValor_maximo() {
		return Valor_maximo;
	}
	public void setValor_maximo(StringProperty valor_maximo) {
		Valor_maximo = valor_maximo;
	}

	public ObservableList<Caracteristica> getCaracterizacion() {
		return caracterizacion;
	}

	public void setCaracterizacion(ObservableList<Caracteristica> caracterizacion) {
		this.caracterizacion = caracterizacion;
	}
	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}
	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}
	public TreeItem<Object> getNodo_razonamiento() {
		return nodo_razonamiento;
	}
	public void setNodo_razonamiento(TreeItem<Object> nodo_razonamiento) {
		this.nodo_razonamiento = nodo_razonamiento;
	}
	public TreeItem<Object> getNodo_aprendizaje() {
		return nodo_aprendizaje;
	}
	public void setNodo_aprendizaje(TreeItem<Object> nodo_aprendizaje) {
		this.nodo_aprendizaje = nodo_aprendizaje;
	}
}

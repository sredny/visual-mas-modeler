package application.model;

import java.math.BigInteger;
import java.security.SecureRandom;

import application.UML.Actividades;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

public class Caso_uso {
	private SimpleStringProperty nombre,codigo;
	private SimpleStringProperty descripcion,precondicion,condicionFracaso,condicionExito;
	private Ellipse elllipse;
	private Text name;
	private Actividades diagrama_actividades;
	private agent parent;
	private TreeItem<Object> nodo_arbol=null;
	private TreeItem<Object> nodo_descripcion=null;
	private SecureRandom random = new SecureRandom();


	public Caso_uso(){
		nodo_arbol=new TreeItem<Object>(this);
		nodo_descripcion=new TreeItem<Object>("Descripcion");

		diagrama_actividades=new Actividades();
		nodo_arbol.getChildren().addAll(nodo_descripcion,diagrama_actividades.getNodo_arbol());

		nombre=new SimpleStringProperty("Sin información");
		codigo=new SimpleStringProperty(new BigInteger(130, random).toString(32));
		descripcion=new SimpleStringProperty("Sin información");
		precondicion=new SimpleStringProperty("Sin información");
		condicionFracaso=new SimpleStringProperty("Sin información");
		condicionExito=new SimpleStringProperty("Sin información");
	}

	public Caso_uso(String name){
		nodo_arbol=new TreeItem<Object>(this);
		nodo_descripcion=new TreeItem<Object>("Descripcion");
		diagrama_actividades=new Actividades();
		nodo_arbol.getChildren().addAll(nodo_descripcion,diagrama_actividades.getNodo_arbol());
		nombre=new SimpleStringProperty(name);
		codigo=new SimpleStringProperty(new BigInteger(130, random).toString(32));

		descripcion=new SimpleStringProperty("Sin información");
		precondicion=new SimpleStringProperty("Sin información");
		condicionFracaso=new SimpleStringProperty("Sin información");
		condicionExito=new SimpleStringProperty("Sin información");
	}

	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<CasoUso>\n";
			XML+=inde+"\t<Nombre>"+this.nombre.get()+"</Nombre>\n";
			XML+=inde+"\t<Codigo>"+this.codigo.get()+"</Codigo>\n";
			XML+=inde+"\t<Descripcion>"+this.descripcion.get()+"</Descripcion>\n";
			XML+=inde+"\t<Precondicion>"+this.precondicion.get()+"</Precondicion>\n";
			XML+=inde+"\t<CondicionFracaso>"+this.condicionFracaso.get()+"</CondicionFracaso>\n";
			XML+=inde+"\t<CondicionExito>"+this.condicionExito.get()+"</CondicionExito>\n";
			XML+=diagrama_actividades.to_XML(indentacion+1);

		XML+=inde+"</CasoUso>\n";
		return XML;
	}

	public StringProperty nombre(){
		return nombre;
	}

	public String getNombre() {
		return nombre.get();
	}

	public Text get_name(){
		return name;
	}

	public void refactor(){
		name.setX(elllipse.getLayoutX());
		name.setY(elllipse.getLayoutY());
		name.toFront();
	}

	public void setNombre(String nombre) {
		this.nombre = new SimpleStringProperty(nombre);
		this.name= new Text(nombre);
	}

	public StringProperty Codigo() {
		return codigo;
	}

	public String getCodigo() {
		return codigo.get();
	}

	public void setCodigo(String codigo) {
		this.codigo = new SimpleStringProperty(codigo);
	}

	public StringProperty Descripcion() {
		return descripcion;
	}

	public String getDescripcion() {
		return descripcion.get();
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = new SimpleStringProperty(descripcion);
	}

	public StringProperty Precondicion() {
		return precondicion;
	}

	public String getPrecondicion() {
		return precondicion.get();
	}

	public void setPrecondicion(String precondicion) {
		this.precondicion = new SimpleStringProperty(precondicion);
	}

	public StringProperty CondicionFracaso() {
		return condicionFracaso;
	}

	public String getCondicionFracaso() {
		return condicionFracaso.get();
	}

	public void setCondicionFracaso(String condicionFracaso) {
		this.condicionFracaso = new SimpleStringProperty(condicionFracaso);
	}

	public StringProperty CondicionExito() {
		return condicionExito;
	}

	public String getCondicionExito() {
		return condicionExito.get();
	}

	public void setCondicionExito(String condicionExito) {
		this.condicionExito = new SimpleStringProperty(condicionExito);
	}

	public Ellipse getElllipse() {
		return elllipse;
	}

	public void setName(Text text) {
		this.name = text;
	}

	public Actividades getDiagrama_actividades() {
		return diagrama_actividades;
	}

	public void setDiagrama_actividades(Actividades diagrama_actividades_) {
		this.nodo_arbol.getChildren().remove(this.diagrama_actividades.getNodo_arbol());
		this.diagrama_actividades = diagrama_actividades_;
		this.nodo_arbol.getChildren().add(diagrama_actividades_.getNodo_arbol());
	}

	public agent getParent() {
		return parent;
	}

	public void setParent(agent parent) {
		this.parent = parent;
	}


	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}

	public TreeItem<Object> getNodo_descripcion() {
		return nodo_descripcion;
	}

	public void setNodo_descripcion(TreeItem<Object> nodo_descripcion) {
		this.nodo_descripcion = nodo_descripcion;
	}




}

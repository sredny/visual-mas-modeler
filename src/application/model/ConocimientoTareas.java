package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;

public class ConocimientoTareas implements nodo{
	private StringProperty nombre;
	private agent agente;
	private String agenteID;
	public TreeItem<Object> nodo_arbol;

	public String getAgenteID() {
		return agenteID;
	}

	public void setAgenteID(String agenteID) {
		this.agenteID = agenteID;
	}

	public ConocimientoTareas(){
		nombre=new SimpleStringProperty();
		agente=new agent();
		nodo_arbol=new TreeItem<Object>("Conocimiento de Tareas");
	}
	public void refactor(Main main){
		if(agenteID!=null)
			agente=main.refactor(agenteID);
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<ConocimientoTareas>\n";
			XML+=inde+"\t<Nombre>"+this.nombre.get()+"</Nombre>\n";
			XML+=inde+"\t<Agente>"+this.agente.id+"</Agente>\n";
		XML+=inde+"</ConocimientoTareas>\n";

		return XML;
	}

	public ConocimientoTareas(String nombre,agent agente){
		this.nombre=new SimpleStringProperty(nombre);
		this.agente=agente;
	}

	public StringProperty getNombre() {
		return nombre;
	}
	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}
	public agent getAgente() {
		return agente;
	}
	public void setAgente(agent agente) {
		this.agente = agente;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}


}

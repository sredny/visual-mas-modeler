package application.model;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class MarcoOntologicoIndividual implements nodo{
	private StringProperty nombre,descripcion;
	private ObservableList<OntologiaHistorica> historica=FXCollections.observableArrayList();
	private ObservableList<OntologiaDominio> dominio=FXCollections.observableArrayList();
	private ObservableList<OntologiaSituacionalI> situacional=FXCollections.observableArrayList();
	private TreeItem<Object> nodoarbol=null;

	public MarcoOntologicoIndividual(){
		nombre=new SimpleStringProperty("");
		descripcion=new SimpleStringProperty("");
		historica=FXCollections.observableArrayList();
		dominio=FXCollections.observableArrayList();
		situacional=FXCollections.observableArrayList();
		setNodoarbol(new TreeItem<Object>(this));
	}

	public void refactor(Main main){

		for (OntologiaSituacionalI s : situacional) {
			s.refactor(main);
			nodoarbol.getChildren().add(s.nodoarbol);
		}

		for(OntologiaHistorica h : historica){
			nodoarbol.getChildren().add(h.nodoarbol);
		}

		for(OntologiaDominio d: dominio){
			nodoarbol.getChildren().add(d.nodoarbol);
		}

	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<MarcoOntologicoIndividual nombre='"+this.nombre.get()+"' descripcion='"+this.descripcion.get()+"'>\n";
			XML+=inde+"\t<OntologiasHistoricas>\n";
			for (OntologiaHistorica history : historica) {
				XML+=history.to_XML(indentacion+2);
			}
			XML+=inde+"\t</OntologiasHistoricas>\n";

			XML+=inde+"\t<OntologiasDominio>\n";
			for (OntologiaDominio domini : dominio) {
				XML+=domini.to_XML(indentacion+2);
			}
			XML+=inde+"\t</OntologiasDominio>\n";

			XML+=inde+"\t<OntologiasSituacional>\n";
			for (OntologiaSituacionalI situ : situacional) {
				XML+=situ.to_XML(indentacion+2);
			}
			XML+=inde+"\t</OntologiasSituacional>\n";
		XML+=inde+"</MarcoOntologicoIndividual>\n";

		return XML;
	}

	public MarcoOntologicoIndividual(String nombre,String descripcion){
		this.nombre=new SimpleStringProperty(nombre);
		this.descripcion=new SimpleStringProperty(descripcion);
		historica=FXCollections.observableArrayList();
		dominio=FXCollections.observableArrayList();
		situacional=FXCollections.observableArrayList();
		setNodoarbol(new TreeItem<Object>(this));
	}

	public StringProperty getNombre() {
		return nombre;
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public StringProperty getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}

	public ObservableList<OntologiaHistorica> getHistorica() {
		return historica;
	}

	public void setHistorica(ObservableList<OntologiaHistorica> historica) {
		this.historica = historica;
	}

	public ObservableList<OntologiaDominio> getDominio() {
		return dominio;
	}

	public void setDominio(ObservableList<OntologiaDominio> dominio) {
		this.dominio = dominio;
	}

	public ObservableList<OntologiaSituacionalI> getSituacional() {
		return situacional;
	}

	public void setSituacional(ObservableList<OntologiaSituacionalI> situacional) {
		this.situacional = situacional;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		for (OntologiaDominio ontologiaDominio : dominio) {
			ontologiaDominio.accept(visitor,newClass);
		}

		for (OntologiaHistorica ontologiahistorica : historica) {
			ontologiahistorica.accept(visitor,newClass);
		}

		for (OntologiaSituacionalI ontologiasituacional : situacional) {
			ontologiasituacional.accept(visitor,newClass);
		}

		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {

	}

	public TreeItem<Object> getNodoarbol() {
		return nodoarbol;
	}

	public void setNodoarbol(TreeItem<Object> nodoarbol) {
		this.nodoarbol = nodoarbol;
	}
}

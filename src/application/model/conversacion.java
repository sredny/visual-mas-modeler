package application.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class conversacion implements nodo{
	private StringProperty nombre;
	private ObservableList<agent> participantes;
	private ObservableList<ActoHabla> hablas;
	private StringProperty precondicion,Terminacion,descripcion,objetivo;
	private List<String> agentes,actos_habla;
	private TreeItem<Object> nodo_arbol=null;
	private File diagrama_secuencia;

	public conversacion(){
		nodo_arbol=new TreeItem<Object>(this);
		nombre=new SimpleStringProperty();
		participantes=FXCollections.observableArrayList();
		hablas=FXCollections.observableArrayList();
		precondicion=new SimpleStringProperty();
		Terminacion=new SimpleStringProperty();
		descripcion=new SimpleStringProperty();
		agentes=new ArrayList<>();
		actos_habla=new ArrayList<>();
		objetivo=new SimpleStringProperty();
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<Conversation nombre='"+this.nombre.get()+"' precondicion='"+this.precondicion.get()+"' terminacion='"+this.Terminacion.get()+"' descripcion='"+this.descripcion.get()+"' objetivo='"+this.objetivo.get()+"'>\n";
		XML+=inde+"<agentes>\n";
		for (agent agente : participantes) {
			if(agente!=null)
				XML+=inde+"\t<agente>"+agente.id+"</agente>\n";
		}
		XML+=inde+"</agentes>\n";
		XML+=inde+"<ActosHabla>\n";
			for (ActoHabla actoHabla : hablas) {
				XML+=inde+"\t<ActoHabla>"+actoHabla.id+"</ActoHabla>\n";
			}
		XML+=inde+"</ActosHabla>\n";
		XML+=inde+"</Conversation>\n";
		return XML;
	}

	public StringProperty getNombre() {
		return nombre;
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public ObservableList<agent> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(ObservableList<agent> participantes) {
		this.participantes = participantes;
	}

	public ObservableList<ActoHabla> getHablas() {
		return hablas;
	}

	public void setHablas(ObservableList<ActoHabla> hablas) {
		this.hablas = hablas;
	}

	public StringProperty getPrecondicion() {
		return precondicion;
	}

	public void setPrecondicion(StringProperty precondicion) {
		this.precondicion = precondicion;
	}

	public StringProperty getTerminacion() {
		return Terminacion;
	}

	public void setTerminacion(StringProperty terminacion) {
		Terminacion = terminacion;
	}

	public StringProperty getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}


	public File getDiagrama_secuencia() {
		return diagrama_secuencia;
	}


	public void setDiagrama_secuencia(File diagrama_secuencia) {
		this.diagrama_secuencia = diagrama_secuencia;
	}

	public StringProperty getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(StringProperty objetivo) {
		this.objetivo = objetivo;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub
	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}

	public void refactor(Main main){
		main.refactor(participantes, agentes);

		for (agent agente : main.getAgentData()) {
			for (ActoHabla ah : agente.getModelo_comunicaciones().getActor()) {
				if(actos_habla.contains(ah.id)){
					hablas.add(ah);
				}//fin if
			}

		}
	}

	public List<String> getAgentes() {
		return agentes;
	}

	public void setAgentes(List<String> agentes) {
		this.agentes = agentes;
	}

	public List<String> getActos_habla() {
		return actos_habla;
	}

	public void setActos_habla(List<String> actos_habla) {
		this.actos_habla = actos_habla;
	}


}

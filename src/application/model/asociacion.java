package application.model;

public class asociacion {
	private String actor_id,cu_id,tipo;
	private String base,target;


	public asociacion(){
		actor_id="";
		cu_id="";
		tipo="";
	}

	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<asociacion actor='"+this.actor_id+"' casoUso='"+this.cu_id+"' tipo='"+this.tipo+"' base='"+this.base+"' target='"+this.target+"'/>\n";

		return XML;
	}

	public asociacion(String actor_n,String cu_n){
		actor_id=actor_n;
		cu_id=cu_n;
	}

	public String getActor_id() {
		return actor_id;
	}

	public void setActor_id(String actor_id) {
		this.actor_id = actor_id;
	}

	public String getCu_id() {
		return cu_id;
	}

	public void setCu_id(String cu_id) {
		this.cu_id = cu_id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
}

package application.model;

import java.util.ArrayList;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class agent implements nodo{

	private SimpleStringProperty name;
	private ObservableList<Actor> lista_actores= FXCollections.observableArrayList();
	private ObservableList<Caso_uso> casos_uso;
	private AgentModel modelo_agentes;
	private TareaModel modelo_tareas;
	public String id;
	private CommunicationModel modelo_comunicaciones;
	private CoordinationModel modelo_coordinacion;
	private List<asociacion> asociaciones= new ArrayList<>();
	private ObservableList<MarcoOntologicoIndividual> marco_ontologico_individual=FXCollections.observableArrayList();
	private SecureRandom random = new SecureRandom();
	public TreeItem<Object> nodo_arbol=null;
	public TreeItem<Object> casos_Uso_nodo=null;
	public TreeItem<Object> ModeloInteligencia=null;

	public agent(){
		casos_uso= FXCollections.observableArrayList();
		name=new SimpleStringProperty("");
		modelo_agentes=new AgentModel();
		modelo_tareas=new TareaModel();
		modelo_comunicaciones=new CommunicationModel();
		setModelo_coordinacion(new CoordinationModel());
		setMarco_ontologico_individual(FXCollections.observableArrayList());
		id=new BigInteger(130, random).toString(32);
		nodo_arbol=new TreeItem<Object>(this);
		casos_Uso_nodo=new TreeItem<Object>("Casos de Uso");
		ModeloInteligencia=new TreeItem<Object>(this.getMarco_ontologico_individual());
	}

	public agent(String name_n){
		name=new SimpleStringProperty(name_n);
		modelo_agentes=new AgentModel();
		id=new BigInteger(130, random).toString(32);
		modelo_tareas=new TareaModel();
		modelo_comunicaciones=new CommunicationModel();
		setModelo_coordinacion(new CoordinationModel());
		nodo_arbol=new TreeItem<Object>(this);
		casos_Uso_nodo=new TreeItem<Object>("Casos de Uso");
		casos_uso= FXCollections.observableArrayList();
		ModeloInteligencia=new TreeItem<Object>(this.getMarco_ontologico_individual());
	}

	public void imprimir(){
		System.out.println("Nombre:"+name);
		System.out.println("Actores:"+lista_actores.size());
		System.out.println("Casos de uso:"+casos_uso.size());
		System.out.println("Asociaciones:"+asociaciones.size());
	}
	public StringProperty getName() {
		return name;
	}

	public String getNameString(){
		return name.get();
	}

	public ObservableList<Actor> getLista_actores() {
		return lista_actores;
	}

	public ObservableList<Caso_uso> getCasos_uso() {
		return casos_uso;
	}

	public TreeItem<Object> getCasos_Uso_nodo() {
		return casos_Uso_nodo;
	}

	public void setCasos_Uso_nodo(TreeItem<Object> casos_Uso_nodo) {
		this.casos_Uso_nodo = casos_Uso_nodo;
	}

	public List<asociacion> getAsociaciones() {
		return asociaciones;
	}

	public void setAsociaciones(List<asociacion> asociaciones) {
		this.asociaciones = asociaciones;
	}

	public AgentModel getModelo_agentes() {
		return modelo_agentes;
	}

	public TareaModel getModelo_tareas() {
		return modelo_tareas;
	}

	public void setModelo_tareas(TareaModel modelo_tareas) {
		this.modelo_tareas = modelo_tareas;
	}

	public CommunicationModel getModelo_comunicaciones() {
		return modelo_comunicaciones;
	}

	public void setModelo_comunicaciones(CommunicationModel modelo_comunicaciones) {
		this.modelo_comunicaciones = modelo_comunicaciones;
	}

	public CoordinationModel getModelo_coordinacion() {
		return modelo_coordinacion;
	}

	public void setModelo_coordinacion(CoordinationModel modelo_coordinacion) {
		this.modelo_coordinacion = modelo_coordinacion;
	}

	public ObservableList<MarcoOntologicoIndividual> getMarco_ontologico_individual() {
		return marco_ontologico_individual;
	}

	public void setMarco_ontologico_individual(ObservableList<MarcoOntologicoIndividual> marco_ontologico_individual) {
		this.marco_ontologico_individual = marco_ontologico_individual;
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}

	public void setName(SimpleStringProperty name) {
		this.name = name;
	}

	public void setModelo_agentes(AgentModel modelo_agentes) {
		this.modelo_agentes = modelo_agentes;
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SecureRandom getRandom() {
		return random;
	}

	public void setRandom(SecureRandom random) {
		this.random = random;
	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void refactor(Main main){
		modelo_agentes.refactor();
		modelo_tareas.refactor(main);
		modelo_comunicaciones.refactor(main);
		modelo_coordinacion.refactor(main);

		TreeItem<Object> nodoModelos= new TreeItem<Object>("Modelos");
		casos_Uso_nodo=new TreeItem<Object>("Diagrama de Casos de Uso");
		nodo_arbol.getChildren().addAll(casos_Uso_nodo,nodoModelos);

		for (Caso_uso uc : this.casos_uso) {
			casos_Uso_nodo.getChildren().add(uc.getNodo_arbol());
		}

		ModeloInteligencia=new TreeItem<Object>(this.getMarco_ontologico_individual());
		for (MarcoOntologicoIndividual moi : marco_ontologico_individual) {
			moi.refactor(main);
			ModeloInteligencia.getChildren().add(moi.getNodoarbol());
		}


		nodoModelos.getChildren().add(modelo_agentes.getNodo());
		nodoModelos.getChildren().add(modelo_tareas.getNodo_arbol());
		nodoModelos.getChildren().add(modelo_comunicaciones.getNodo_arbol());
		nodoModelos.getChildren().add(modelo_coordinacion.getNodo_arbol());
		nodoModelos.getChildren().add(ModeloInteligencia);
	}

	public void deleteAsociation(String ucId,String actorId){
		asociacion x=null;
		for(asociacion a:asociaciones){
			if(a.getActor_id().equals(actorId) && a.getCu_id().equals(ucId)){
				x=(a);
			}
		}
		if(x!=null)
			asociaciones.remove(x);
	}

	public ObservableList<Actor> UC_actors(String uc_id){
		ObservableList<Actor> actors=FXCollections.observableArrayList();

		for(asociacion a: asociaciones){
			if(a.getCu_id().equals(uc_id)){
				for(Actor actor: lista_actores){
					if(a.getActor_id().equals(actor.getCodigo().get())){
						actors.add(actor);
					}
				}
			}
		}//fin for asociaciones

		return actors;
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<agent name='"+this.name.get()+"' id='"+this.id+"'>\n";
		XML+=modelo_agentes.to_XML(indentacion+1);
		XML+=modelo_tareas.to_XML(indentacion+1);
		XML+=modelo_comunicaciones.to_XML(indentacion+1);
		XML+=modelo_coordinacion.to_XML(indentacion+1);
		XML+=inde+"<MarcoOntologicoIndividual>\n";
			for (MarcoOntologicoIndividual MOI : marco_ontologico_individual) {
				XML+=MOI.to_XML(indentacion+1);
			}
		XML+=inde+"</MarcoOntologicoIndividual>\n";

		XML+=inde+"<Asociaciones>\n";
		for (asociacion asociacion : asociaciones) {
			XML+=asociacion.to_XML(indentacion+1);
		}
		XML+="</Asociaciones>\n";

		XML+=inde+"<Actores>\n";
		for (Actor actor : lista_actores) {
			XML+=actor.to_XML(indentacion+1);
		}
		XML+="</Actores>\n";

		XML+=inde+"<CasosUso>\n";
		for (Caso_uso caso : casos_uso) {
			XML+=caso.to_XML(indentacion+1);
		}
		XML+="</CasosUso>\n";

		XML+="\n"+inde+"</agent>\n";
		return XML;

	}


	@SuppressWarnings("unchecked")
	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
		nodo_arbol.setExpanded(true);
		TreeItem<Object> nodoModelos= new TreeItem<Object>("Modelos");
		casos_Uso_nodo=new TreeItem<Object>("Diagrama de Casos de Uso");
		ModeloInteligencia=new TreeItem<Object>(this.getMarco_ontologico_individual());
		nodo_arbol.getChildren().addAll(casos_Uso_nodo,nodoModelos);

		nodoModelos.getChildren().add(modelo_agentes.getNodo());
		nodoModelos.getChildren().add(modelo_tareas.getNodo_arbol());
		nodoModelos.getChildren().add(modelo_comunicaciones.getNodo_arbol());
		nodoModelos.getChildren().add(modelo_coordinacion.getNodo_arbol());
		nodoModelos.getChildren().add(ModeloInteligencia);
	}

	public TreeItem<Object> getModeloInteligencia() {
		return ModeloInteligencia;
	}

	public void setModeloInteligencia(TreeItem<Object> modeloInteligencia) {
		ModeloInteligencia = modeloInteligencia;
	}

}

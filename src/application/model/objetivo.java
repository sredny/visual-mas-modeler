package application.model;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class objetivo {
	private String ObjetivoNombre,ObjetivoDescripcion,ObjetivoActivacion,ObjetivoFinalizacion,ObjetivoExito,ObjetivoFracaso,ObjetivoOntologia;
	private ObservableList<String> ObjetivoEntrada,ObjetivoSalida;
	private SecureRandom random = new SecureRandom();
	public String id;

	public objetivo(){
		ObjetivoEntrada=FXCollections.observableArrayList();
		ObjetivoSalida=FXCollections.observableArrayList();
		ObjetivoNombre="";
		ObjetivoDescripcion="";
		ObjetivoActivacion="";
		ObjetivoFinalizacion="";
		ObjetivoExito="";
		ObjetivoFracaso="";
		ObjetivoOntologia="";
		id=new BigInteger(130, random).toString(32);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getObjetivoNombre() {
		return ObjetivoNombre;
	}
	public SimpleStringProperty getObjetivoNombre_sp() {
		return new SimpleStringProperty(this.getObjetivoNombre());
	}
	public void setObjetivoNombre(String objetivoNombre) {
		ObjetivoNombre = objetivoNombre;
	}
	public String getObjetivoDescripcion() {
		return ObjetivoDescripcion;
	}
	public void setObjetivoDescripcion(String objetivoDescripcion) {
		ObjetivoDescripcion = objetivoDescripcion;
	}
	public String getObjetivoActivacion() {
		return ObjetivoActivacion;
	}
	public void setObjetivoActivacion(String objetivoActivacion) {
		ObjetivoActivacion = objetivoActivacion;
	}
	public String getObjetivoFinalizacion() {
		return ObjetivoFinalizacion;
	}
	public void setObjetivoFinalizacion(String objetivoFinalizacion) {
		ObjetivoFinalizacion = objetivoFinalizacion;
	}
	public String getObjetivoExito() {
		return ObjetivoExito;
	}
	public void setObjetivoExito(String objetivoExito) {
		ObjetivoExito = objetivoExito;
	}
	public String getObjetivoFracaso() {
		return ObjetivoFracaso;
	}
	public void setObjetivoFracaso(String objetivoFracaso) {
		ObjetivoFracaso = objetivoFracaso;
	}
	public String getObjetivoOntologia() {
		return ObjetivoOntologia;
	}
	public void setObjetivoOntologia(String objetivoOntologia) {
		ObjetivoOntologia = objetivoOntologia;
	}
	public ObservableList<String> getObjetivoEntrada() {
		return ObjetivoEntrada;
	}
	public void setObjetivoEntrada(ObservableList<String> objetivoEntrada) {
		ObjetivoEntrada = objetivoEntrada;
	}
	public ObservableList<String> getObjetivoSalida() {
		return ObjetivoSalida;
	}
	public void setObjetivoSalida(ObservableList<String> objetivoSalida) {
		ObjetivoSalida = objetivoSalida;
	}

	public String ObjetivoTo_String(){

		String cadena="";
		if(!this.ObjetivoNombre.equals(""))
			cadena="Objetivo:"+this.ObjetivoNombre+"\n";
		else
			return cadena;

		if(!this.ObjetivoDescripcion.equals(""))
			cadena+="Descripcion de objetivo:"+this.ObjetivoDescripcion+"\n";

		if(ObjetivoEntrada.size()>0)
			cadena+="Par�metros de entrada:";
			for (String string : ObjetivoEntrada) {
				cadena+=string+"\n";
			}

		if(ObjetivoSalida.size()>0)
			cadena+="Par�metros de salida:";
			for (String string : ObjetivoSalida) {
				cadena+=string+"\n";
			}

		if(!this.ObjetivoActivacion.equals(""))
			cadena+="Condicion de activaci�n:"+this.ObjetivoActivacion+"\n";

		if(!this.ObjetivoFinalizacion.equals(""))
			cadena+="Condicion de Finalizaci�n:"+this.ObjetivoFinalizacion+"\n";

		if(!this.ObjetivoFinalizacion.equals(""))
			cadena+="Condicion de �xito:"+this.ObjetivoFinalizacion+"\n";

		if(!this.ObjetivoFracaso.equals(""))
			cadena+="Condici�n de facaso:"+this.ObjetivoFracaso+"\n";

		if(!this.ObjetivoOntologia.equals(""))
			cadena+="Ontolog�a:"+this.ObjetivoOntologia+"\n";

		return cadena;
	}


}

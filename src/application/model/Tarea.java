package application.model;

import java.util.ArrayList;
import java.util.List;

import Visitador.nodo;
import Visitador.nodoVisitor;
import application.Main;
import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

public class Tarea implements nodo{
	private String descripcion,objetivo,precondicion;
	private StringProperty servicio_asociado,nombre,comportamiento;
	private Servicio asociado;
	private List<String> subtareas;
	private ObservableList<Parametros> parametros=FXCollections.observableArrayList();
	private TreeItem<Object> nodo_arbol=null;


	public Tarea(){
		nodo_arbol=new TreeItem<Object>(this);
		subtareas=new ArrayList<String>();
		setNombre(new SimpleStringProperty());
		comportamiento=null;
		descripcion="";
		objetivo="";
		precondicion="";
		asociado=new Servicio();
		servicio_asociado=new SimpleStringProperty("");
	}

	@Override
	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";
		XML+=inde+"<Descripcion>"+this.descripcion+"</Descripcion>\n";
		XML+=inde+"<Objetivo>"+this.objetivo+"</Objetivo>\n";
		XML+=inde+"<Precondicion>"+this.precondicion+"</Precondicion>\n";
		XML+=inde+"<ServicioAsociado>"+this.servicio_asociado.get()+"</ServicioAsociado>\n";
		XML+=inde+"<Nombre>"+this.nombre.get()+"</Nombre>\n";
		XML+=inde+"<ServicioID>"+this.servicio_asociado.get()+"</ServicioID>\n";

		if(this.comportamiento!=null)
			XML+=inde+"<Comportamiento>"+this.comportamiento.get()+"</Comportamiento>\n";

		XML+=inde+"<SubTasks>\n";
			for (String sub : subtareas) {
				XML+=inde+"\t<SubTask>"+sub+"</SubTask>\n";
			}
		XML+=inde+"</SubTasks>\n";
		XML+=inde+"<Parametros>\n";

			for (Parametros parametros2 : parametros) {
				XML+=parametros2.to_XML(indentacion+1);
			}

		XML+=inde+"</Parametros>\n";
		return XML;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public List<String> getSubtareas() {
		return subtareas;
	}

	public String getSubtareasS(){
		String x="";
			for(int i=0;i<subtareas.size();i++){
				if(i==subtareas.size()||i==0)
					x+=subtareas.get(i);
				else
					x+=","+subtareas.get(i);
			}

		return x;
	}

	public void setSubtareas(List<String> subtareas) {
		this.subtareas = subtareas;
	}
	public String getPrecondicion() {
		return precondicion;
	}
	public void setPrecondicion(String precondicion) {
		this.precondicion = precondicion;
	}


	public StringProperty getServicio_asociado() {
		return servicio_asociado;
	}

	public void setServicio_asociado(StringProperty servicio_asociado) {
		this.servicio_asociado = servicio_asociado;
	}

	public StringProperty getNombre() {
		return nombre;
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public ObservableList<Parametros> getParametros() {
		return parametros;
	}

	public void setParametros(ObservableList<Parametros> parametros) {
		this.parametros = parametros;
	}


	public Servicio getAsociado() {
		return asociado;
	}


	public void setAsociado(Servicio asociado) {
		this.asociado = asociado;
		this.setServicio_asociado(new SimpleStringProperty(asociado.getId()));
	}

	@Override
	public void accept(nodoVisitor visitor,clase newClass) {
		visitor.visit(this,newClass);
	}

	@Override
	public void accept(nodoVisitor visitor, clase newClass, TareaModel tareas) {
		// TODO Auto-generated method stub

	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}

	public void refactor(Main main){

		for (agent agente : main.getAgentData()) {
			for (Servicio s : agente.getModelo_agentes().getServicios()) {
				if(s.getId().equals(servicio_asociado.get())){
					asociado=s;
					break;
				}
			}
		}
	}

	public String getAllParameters(){
		int i=0;
		String parametrosS="";

		for (Parametros p : parametros) {
			if(i!=0){
				parametrosS+=",";
			}
			i++;
			parametrosS+=p.getType()+" "+Main.FormatearAtributoMetodo(p.getNombre().get().replace(" ", Main.caracter_sustitucion));
		}
		return parametrosS;
	}

	public String getAllParametersACL(){
		int i=0;
		String parametrosS="";

		for (Parametros p : parametros) {
			if(i!=0){
				parametrosS+=",";
			}
			i++;
			parametrosS+=i+".-"+Main.FormatearAtributoMetodo(p.getNombre().get().replace(" ", Main.caracter_sustitucion))+" -Tipo:"+p.getType();
		}
		return parametrosS;
	}

	public boolean exist(String name){
		for (Parametros p : parametros) {
			if(p.getNombre().get().equals(name)){
				return true;
			}
		}
		return false;
	}


	public StringProperty getComportamiento() {
		return comportamiento;
	}


	public void setComportamiento(StringProperty comportamiento) {
		this.comportamiento = comportamiento;
	}



}

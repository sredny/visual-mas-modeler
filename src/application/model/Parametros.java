package application.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Parametros {
	private StringProperty nombre,descripcion,tipo;

	public Parametros(){
		this.nombre=new SimpleStringProperty();
		descripcion=new SimpleStringProperty();
	}

	public Parametros(String nombre,String des,String tipo){
		this.nombre=new SimpleStringProperty(nombre);
		this.descripcion=new SimpleStringProperty(des);
		this.tipo=new SimpleStringProperty(tipo);
	}

	public String to_XML(int iden){
		String inde="";
		String XML="";
		for(int i=0;i<iden;i++)
			inde+="\t";
		XML+=inde+"<Parametro>\n";
		XML+=inde+"\t<Nombre>"+this.nombre.get()+"</Nombre>\n";
		XML+=inde+"\t<Descripcion>"+this.descripcion.get()+"</Descripcion>\n";
		XML+=inde+"\t<Tipo>"+this.tipo.get()+"</Tipo>\n";
		XML+=inde+"</Parametro>\n";
		return XML;
	}

	public StringProperty getNombre() {
		return nombre;
	}
	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}
	public StringProperty getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}

	public StringProperty getTipo() {
		return tipo;
	}

	public String getType() {
		return tipo.get();
	}

	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}


}

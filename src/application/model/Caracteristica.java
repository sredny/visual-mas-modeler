package application.model;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Caracteristica {
	private StringProperty tipo,nombre,valor_omision,descripcion;
	private boolean is_property;

	public boolean isIs_property() {
		return is_property;
	}

	public void setIs_property(boolean is_property) {
		this.is_property = is_property;
	}

	public Caracteristica(){
		is_property=false;
		setTipo(new SimpleStringProperty(""));
		setNombre(new SimpleStringProperty(""));
		setDescripcion(new SimpleStringProperty(""));
		setValor_omision(new SimpleStringProperty(""));
	}

	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde+"<Caracteristica>\n";
		XML+=inde+"\t<tipo>"+this.tipo.get()+"</tipo>\n";
		XML+=inde+"\t<nombre>"+this.nombre.get()+"</nombre>\n";
		XML+=inde+"\t<ValorOmision>"+this.valor_omision.get()+"</ValorOmision>\n";
		XML+=inde+"\t<Descripcion>"+this.descripcion.get()+"</Descripcion>\n";
		XML+=inde+"</Caracteristica>\n";
		return XML;
	}

	public Caracteristica(String tipo,String nombre,String Valor){
		this.setNombre(new SimpleStringProperty(nombre));
		this.setTipo(new SimpleStringProperty(tipo));
		this.setValor_omision(new SimpleStringProperty(Valor));
		setDescripcion(new SimpleStringProperty(""));

	}

	public StringProperty getTipo() {
		return tipo;
	}

	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}

	public StringProperty getNombre() {
		return nombre;
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public StringProperty getValor_omision() {
		return valor_omision;
	}

	public void setValor_omision(StringProperty valor_omision) {
		this.valor_omision = valor_omision;
	}

	public StringProperty getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(StringProperty descripcion) {
		this.descripcion = descripcion;
	}
}

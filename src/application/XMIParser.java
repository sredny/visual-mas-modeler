package application;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import application.UML.Actividades;
import application.UML.estado;
import application.model.Actor;
import application.model.Caso_uso;
import application.model.agent;
import application.model.asociacion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TreeItem;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class XMIParser {
	private File file;

	public XMIParser(){}

	public XMIParser(File file_n){
		file=file_n;
	}//fin constructor

	public String UCParsing(agent agente){
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		//validar de que quiere continuar
		if(agente.getCasos_uso().size()>0){
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmación");
			alert.setHeaderText("El agente ya contiene información de Casos de Usos");
			alert.setContentText("żDesea sustituirla?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() != ButtonType.OK)
				return "Operación cancelada por el usuario";
			else
				agente.getCasos_uso().clear();
		}//fin del if


		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = null;
			try {
				doc = dBuilder.parse(file);
				doc.getDocumentElement().normalize();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				alerta_error("Error de sintaxis: \n\t"+e.getMessage());
				return "";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				alerta_error("Error de I/O \n\t"+e.getMessage());
				return "";
			}

			NodeList nList = doc.getElementsByTagName("UML:Namespace.ownedElement");//obtengo todos los namespaces
			for(int i=0;i<nList.getLength();i++){
				if(hasUC(nList.item(i).getChildNodes())){
					NodeList hijos=nList.item(i).getChildNodes();
					for(int j=0;j<hijos.getLength();j++){
						if (hijos.item(j).getNodeType() == Node.ELEMENT_NODE) {
							if(hijos.item(j).getNodeName().equals("UML:Actor")){
								Actor temp=new Actor();
								Element eElement = (Element) hijos.item(j);
								temp.setName(eElement.getAttribute("name"));
								temp.setCodigo(eElement.getAttribute("xmi.id"));
								temp.setTipo("Externo");
								agente.getLista_actores().add(temp);
							}//fin actors

							if(hijos.item(j).getNodeName().equals("UML:UseCase")){
								Caso_uso temp=new Caso_uso();
								Element eElement = (Element) hijos.item(j);
								temp.setNombre(eElement.getAttribute("name"));
								temp.setCodigo(eElement.getAttribute("xmi.id"));
								agente.getCasos_uso().add(temp);

								//aqui debo detectar el diagrama de actividades
								if(eElement.hasChildNodes()){
									NodeList useCaseChildren=eElement.getChildNodes();
									for(int x=0;x<useCaseChildren.getLength();x++){
										if(useCaseChildren.item(x).getNodeName().equals("UML:Namespace.ownedElement")){
											NodeList NamespaceChildren=useCaseChildren.item(x).getChildNodes();
											for(int y=0;y<NamespaceChildren.getLength();y++){
												if(NamespaceChildren.item(y).getNodeName().equals("UML:ActivityGraph")){
													//aqui leo estados,y asociaciones
													Actividades diagrama=temp.getDiagrama_actividades();
													//temp.setDiagrama_actividades(diagrama);

													diagrama.setId(((Element)NamespaceChildren.item(y)).getAttribute("xmi.id"));
													diagrama.setNombre(((Element)NamespaceChildren.item(y)).getAttribute("name"));

													NodeList parts=NamespaceChildren.item(y).getChildNodes();

													for(int z=0;z<parts.getLength();z++){
														if(parts.item(z).getNodeName().equals("UML:StateMachine.top")){
															NodeList composite=parts.item(z).getChildNodes();
															for(int compo=0;compo<composite.getLength();compo++){
																if(composite.item(compo).getNodeName().equals("UML:CompositeState")){
																	NodeList suvertex=composite.item(compo).getChildNodes();
																	for(int suver=0;suver<suvertex.getLength();suver++){
																		if(suvertex.item(suver).getNodeName().equals("UML:CompositeState.subvertex")){
																			NodeList states=suvertex.item(suver).getChildNodes();
																			for(int state=0;state<states.getLength();state++){
																				//pueden ser:UML:Pseudostate ,UML:ActionState,UML:SubactivityState,UML:FinalState
																				estado Estado=new estado();

																				if(states.item(state).getNodeType() == Node.ELEMENT_NODE){
																					Element elemento= (Element)states.item(state);

																					//---lo comun--
																					Estado.setId(elemento.getAttribute("xmi.id"));
																					if(elemento.hasAttribute("name"))
																						Estado.setName(elemento.getAttribute("name"));

																					String outgoing[]=elemento.getAttribute("outgoing").split(" ");
																							String incoming[]=elemento.getAttribute("incoming").split(" ");
																							for(int k=0;k<outgoing.length;k++){
																								Estado.getOutgoing().add(outgoing[k]);

																							}//for de agregar las salidas
																							for(int k=0;k<incoming.length;k++){

																								Estado.getIncoming().add(incoming[k]);
																							}//for de agregar las entradas

																				//----lo especifico
																					if(elemento.getNodeName().equals("UML:Pseudostate")){
																							String tipo=elemento.getAttribute("kind");
																							Estado.setTipo(tipo);
																					}

																				if(elemento.getNodeName().equals("UML:ActionState")||elemento.getNodeName().equals("UML:SubactivityState")){
																						Estado.setTipo(elemento.getNodeName());
																						NodeList sons = elemento.getChildNodes();

																						//para drle nombre a las actividades de argouml
																						for (int h=0;h<sons.getLength();h++){
																								if(sons.item(h).getNodeName().equals("UML:State.entry")){
																									NodeList grandsons=sons.item(h).getChildNodes();

																								 for(int g=0;g<grandsons.getLength();g++){
																									if(grandsons.item(g).getNodeName().equals("UML:UninterpretedAction")){
																										NodeList grandgrandsons= grandsons.item(g).getChildNodes();

																										for(int gg=0;gg<grandgrandsons.getLength();gg++){
																											if(grandgrandsons.item(gg).getNodeName().equals("UML:Action.script")){
																												NodeList SauroSons=grandgrandsons.item(gg).getChildNodes();
																												for(int ss=0;ss<SauroSons.getLength();ss++){
																													if(SauroSons.item(ss).getNodeName().equals("UML:ActionExpression")){
																														Element sauroElement=(Element)SauroSons.item(ss);
																														if(sauroElement.hasAttribute("body")){
																															Estado.setName(sauroElement.getAttribute("body"));
																														}

																													}
																												}
																											}//fin del if es actionscript
																										}//fin de gg
																									}//fin del if de q es uninterpretedaction
																								 }//fin del for g
																								}
																						}
																					}
																				if(states.item(state).getNodeName().equals("UML:FinalState")){
																						Estado.setTipo(elemento.getNodeName());
																				}

																				diagrama.getEstados().add(Estado);
																				TreeItem<Object> actividad=new TreeItem<>(Estado);
																				//actD.getChildren().add(actividad);
																				Estado.setNodo_arbol(actividad);
																				diagrama.getNodo_arbol().getChildren().add(actividad);

																				}//if Node.ELEMENT_NODE
																			}//fin del for
																		}//if es suvertex
																	}//fin del for suver
																}//fin del if, es compositestate
															}//fin del for compo
														}//encuentra los estados


														if(parts.item(z).getNodeName().equals("UML:StateMachine.transitions")){
															NodeList transiciones=parts.item(z).getChildNodes();
															for(int trans=0;trans<transiciones.getLength();trans++){
																if(transiciones.item(trans).getNodeType() == Node.ELEMENT_NODE){
																	asociacion asocia=new asociacion();
																	Element transition= (Element)transiciones.item(trans);
																	asocia.setTipo(transition.getNodeName());
																	asocia.setBase(transition.getAttribute("source"));
																	asocia.setTarget(transition.getAttribute("target"));
																	diagrama.getAsociaciones().add(asocia);
																}//fin del if Node.Element_node
															}//fin del for
														}//encuentra las transiciones

													}//fin del for
												}//if, es diagrama de actividades
											}//fin del for namespace
										}//entra al namespace
									}//fin del for
								}//fin del if de que tiene hijos
								agente.getCasos_Uso_nodo().getChildren().add(temp.getNodo_arbol());

							}//fin de if uses cases

							if(hijos.item(j).getNodeName().equals("UML:Include")||hijos.item(j).getNodeName().equals("UML:Extend")){
								Element eElement = (Element) hijos.item(j);
								asociacion include=new asociacion();
								include.setTipo(eElement.getNodeName());

								if(eElement.hasChildNodes()){
									NodeList extremos=eElement.getChildNodes();
									for (int c=0;c< extremos.getLength();c++) {
										if((extremos.item(c).getNodeName().equals("UML:Include.addition"))||(extremos.item(c).getNodeName().equals("UML:Extend.extension")) &&extremos.item(c).hasChildNodes()){
											if(extremos.item(c).getChildNodes().item(0).getNodeType() == Node.ELEMENT_NODE){
												Element grandson=(Element) extremos.item(c).getChildNodes().item(0);
												String id=grandson.getAttribute("xmi.idref");
												include.setTarget(id);
											}
										}
										if((extremos.item(c).getNodeName().equals("UML:Include.base")||extremos.item(c).getNodeName().equals("UML:Extend.base")) &&extremos.item(c).hasChildNodes()){
											if(extremos.item(c).getChildNodes().item(0).getNodeType() == Node.ELEMENT_NODE){
												Element grandson=(Element) extremos.item(c).getChildNodes().item(0);
												String id=grandson.getAttribute("xmi.idref");
												include.setTarget(id);
											}
										}
									}
									agente.getAsociaciones().add(include);
								}//if argo

								if(!eElement.hasChildNodes()){
									include.setBase(eElement.getAttribute("base"));
									include.setTarget(eElement.getAttribute("addition"));
									agente.getAsociaciones().add(include);
								}//if es startuml
							}//fin de obtener includes

							if(hijos.item(j).getNodeName().equals("UML:Association")){
										//aqui deberia estar la magia
								NodeList connections= hijos.item(j).getChildNodes();//every item in connection should be and association.connection

								for (int c=0;c<connections.getLength();c++) {
									if(connections.item(c).getNodeName().equals("UML:Association.connection")){

										@SuppressWarnings("unused")
										NodeList associationend= connections.item(c).getChildNodes();
										Node primero=connections.item(c).getFirstChild();
										if(primero.hasChildNodes() && primero.getNodeName().equals("UML:AssociationEnd.participant")){
											//parseArgo(connections.item(c));
											agente.getAsociaciones().add(parseArgo(connections.item(c)));
											//System.out.println("esto se hizo con argo");
										}//fin del if
										if(!primero.hasChildNodes() && connections.item(c).getNodeName().equals("UML:Association.connection")){
											agente.getAsociaciones().add(parseStar(connections.item(c)));
											//hecho con ARGO
										}
									}//es una coneccion
								}//fin del for

							}//fin asociations
						}//fin del if
					}//fin del for

				}

			}//fin del for
			Alert alerta = new Alert(AlertType.INFORMATION);
			alerta.setTitle("Importación exitosa");
			alerta.setHeaderText(null);
			alerta.setContentText("La importación de información UML se realizó de manera exitosa!");
			alerta.showAndWait();
			return "Información UML cargada exitosamente";

		} catch (ParserConfigurationException e) {
			String content="Por favor verifique la validez del archivo UML ingresado \n Debe ser version 1.0 o 1.1 \n Generado con alguna de los modeladores soportados";
			alerta_error(content);
			return "falló al analizar el archivo";
		}

	}//fin de parsear los casos de uso

	public void alerta_error(String Content){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error al cargar información");
		alert.setHeaderText("No se pudo cargar correctamente la información UML");
		alert.setContentText(Content);
		alert.showAndWait();
	}

	public asociacion parseArgo(Node nodo){
		String id_ac="",id_uc="";
			if(nodo.getChildNodes().getLength()==2){
				Node primero=  nodo.getFirstChild();
				Node Segundo= nodo.getLastChild();

				if(primero.getNodeName().equals("UML:AssociationEnd") &&Segundo.getNodeName().equals("UML:AssociationEnd")){
					if(primero.getChildNodes().getLength()==1 && primero.getFirstChild().getNodeName().equals("UML:AssociationEnd.participant") &&primero.getFirstChild().hasChildNodes()){
						if(Segundo.getChildNodes().getLength()==1 && Segundo.getFirstChild().getNodeName().equals("UML:AssociationEnd.participant") &&Segundo.getFirstChild().hasChildNodes()){
							Element eElement1 = (Element)primero.getFirstChild().getFirstChild() ;
							Element eElement2 = (Element)Segundo.getFirstChild().getFirstChild() ;
							if(eElement1.getNodeName().equals("UML:UseCase")){ id_uc=eElement1.getAttribute("xmi.idref");}
							if(eElement1.getNodeName().equals("UML:Actor")){   id_ac=eElement1.getAttribute("xmi.idref");}
							if(eElement2.getNodeName().equals("UML:UseCase")){ id_uc=eElement2.getAttribute("xmi.idref");}
							if(eElement2.getNodeName().equals("UML:Actor")){   id_ac=eElement2.getAttribute("xmi.idref");}
						}//if segundo
					}//entra a leer actor or use case
				}//valido q ambos sean association end
			}//es un associationend
			else{

			}
  			if(!id_ac.equals("") && !id_uc.equals(""))
				return new asociacion(id_ac, id_uc);
			else
				return new asociacion();

	}//fin de parsear para argo

	public asociacion parseStar(Node nodo){

		NodeList hijos= nodo.getChildNodes();
		String id_act="",id_uc="";

		for(int i=0;i<hijos.getLength();i++){

			if(hijos.item(i).getNodeName().equals("UML:AssociationEnd")){
				Element eElement = (Element) hijos.item(i);
				String val=eElement.getAttribute("type");
				if(val.contains("UMLActor"))
					id_act=val;
				if(val.contains("UMLUseCase"))
					id_uc=val;
			}//fin del if

		}//fin del for

		return new asociacion(id_act, id_uc);
	}

	public boolean hasUC(NodeList nodos){
		for(int i=0;i<nodos.getLength();i++){
			if(nodos.item(i).getNodeName().equals("UML:UseCase"))

				return true;
		}//fin del for
		return false;
	}

}

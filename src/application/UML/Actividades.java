package application.UML;

import java.util.ArrayList;
import java.util.List;

import application.model.asociacion;
import javafx.scene.control.TreeItem;

public class Actividades {
	private List<estado> estados;
	private List<asociacion> asociaciones;
	private String id, nombre;
	private TreeItem<Object> nodo_arbol;

	public Actividades(){
		estados=new ArrayList<estado>();
		asociaciones=new ArrayList<>();
		id="";
		nombre="";
		nodo_arbol=new TreeItem<Object>("Diagrama de Actividades");
	}

	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde;
		XML+="<DiagramaActividades id='"+this.id+"' nombre='"+this.nombre+"'>\n";
		XML+=inde+"\t<Asociaciones>\n";
			for (asociacion asociacion : asociaciones) {
				XML+=asociacion.to_XML(indentacion+2);
			}
		XML+=inde+"\t</Asociaciones>\n";

		XML+=inde+"\t<Estados>\n";
		for (estado estado : estados) {
			XML+=estado.to_XML(indentacion+2);
		}
		XML+=inde+"\t</Estados>\n";

		XML+=inde+"</DiagramaActividades>\n";
		return XML;
	}


	public List<estado> getEstados() {
		return estados;
	}

	public void setEstados(List<estado> estados) {
		this.estados = estados;
	}

	public List<asociacion> getAsociaciones() {
		return asociaciones;
	}

	public void setAsociaciones(List<asociacion> asociaciones) {
		this.asociaciones = asociaciones;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}

	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}
}

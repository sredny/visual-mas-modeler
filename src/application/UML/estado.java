package application.UML;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TreeItem;

public class estado {
	private String id,name,tipo;
	private List<String> incoming,outgoing;
	private TreeItem<Object> nodo_arbol=null;

	public estado(){
		id="";
		name="Unespecified";
		tipo="";
		incoming=new ArrayList<String>();
		outgoing=new ArrayList<String>();
		setNodo_arbol(new TreeItem<Object>(this));
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public List<String> getIncoming() {
		return incoming;
	}
	public void setIncoming(List<String> incoming) {
		this.incoming = incoming;
	}
	public List<String> getOutgoing() {
		return outgoing;
	}
	public void setOutgoing(List<String> outgoing) {
		this.outgoing = outgoing;
	}
	public TreeItem<Object> getNodo_arbol() {
		return nodo_arbol;
	}
	public void setNodo_arbol(TreeItem<Object> nodo_arbol) {
		this.nodo_arbol = nodo_arbol;
	}


	public String to_XML(int indentacion) {
		String inde="";
		String XML="";
		for(int i=0;i<indentacion;i++)
			inde+="\t";

		XML=inde+"<estado id='"+this.id+"' nombre='"+this.name+"' tipo='"+this.tipo+"'>\n";
		for (String string : incoming) {
			XML+=inde+"\t<Relation tipo='entrada'>"+string+"</Relation>\n";
		}
		for (String string : outgoing) {
			XML+=inde+"\t<Relation tipo='salida'>"+string+"</Relation>\n";
		}
		XML+=inde+"</estado>\n";
		return XML;
	}
}

@XmlJavaTypeAdapters({
@XmlJavaTypeAdapter(value=SimpleStringPropertyAdapter.class, type=String.class),
@XmlJavaTypeAdapter(value=ObservableListAdapter.class, type=ObservableList.class)})
package application;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import javafx.collections.ObservableList;

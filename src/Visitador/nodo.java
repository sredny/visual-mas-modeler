package Visitador;

import application.CodeGenerator.clase;
import application.model.modelos.TareaModel;
import javafx.scene.control.TreeItem;

public interface nodo {
	public void accept(nodoVisitor visitor, clase newClass);
	public void accept(nodoVisitor visitor, clase newClass,TareaModel tareas);

	public String to_XML(int indentacion);
}

package Visitador;

import application.CodeGenerator.clase;
import application.model.ActoHabla;
import application.model.ConocimientoEstrategico;
import application.model.ConocimientoTareas;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalI;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;

public interface nodoVisitor {

	public void visit(TareaModel tareaModel, clase newClass);

	public void visit(Tarea tarea, clase newClass);

	public void visit(CommunicationModel communicationModel, clase newClass);

	public void visit(CoordinationModel coordinationModel, clase newClass);

	public void visit(conversacion conversacion, clase newClass);

	public void visit(MarcoOntologicoIndividual marcoOntologicoIndividual, clase newClass);

	public void visit(OntologiaHistorica ontologiaHistorica, clase newClass);

	public void visit(MecanismoAprendizaje mecanismoAprendizaje, clase newClass);

	public void visit(OntologiaDominio ontologiaDominio, clase newClass);

	public void visit(OntologiaSituacionalI ontologiaSituacionalI, clase newClass);

	public void visit(ConocimientoTareas conocimientoTareas, clase newClass);

	public void visit(ConocimientoEstrategico conocimientoEstrategico, clase newClass);

	public void visit(AgentModel agentModel, clase newClass);

	public void visit(Servicio servicio, clase newClass,TareaModel tareas);

	public void visit(ActoHabla actoHabla, clase newClass);

	public void visit(agent agent, clase newClass);


}

package Visitador;

import application.Main;
import application.CodeGenerator.Generador;
import application.CodeGenerator.clase;
import application.model.ActoHabla;
import application.model.Caracteristica;
import application.model.ConocimientoEstrategico;
import application.model.ConocimientoTareas;
import application.model.MarcoOntologicoIndividual;
import application.model.MecanismoAprendizaje;
import application.model.OntologiaDominio;
import application.model.OntologiaHistorica;
import application.model.OntologiaSituacionalI;
import application.model.Parametros;
import application.model.Servicio;
import application.model.Tarea;
import application.model.agent;
import application.model.conversacion;
import application.model.modelos.AgentModel;
import application.model.modelos.CommunicationModel;
import application.model.modelos.CoordinationModel;
import application.model.modelos.TareaModel;

public class nodoDisplayVisitor implements nodoVisitor{

	@Override
	public void visit(agent agente,clase newClass) {
		Generador.reinit();

	/*Codigo*/

		newClass.setNombre(Main.FormatearClase(agente.getName().get()));
		newClass.add_library("import jade.core.Agent;");
		newClass.add_library("import jade.core.AID;");
		newClass.add_library("import jade.domain.*;");
		newClass.add_library("import jade.lang.acl.*;");
		newClass.add_library("import jade.proto.*;");
		newClass.add_library("import jade.domain.FIPAAgentManagement.DFAgentDescription;");
		newClass.add_library("import jade.domain.FIPAAgentManagement.ServiceDescription;");
		newClass.add_library("import jade.domain.DFService;");
		newClass.add_library("import jade.domain.DFService;");
		newClass.setExtend(" Agent");

		newClass.setPrincipal_method("protected void setup()");
		newClass.push_line_inside("DFAgentDescription dfd = new DFAgentDescription(); \n"
				+ "\t\tdfd.setName(getAID());");

		newClass.add_method("protected void takeDown() {"
				+ "\n\t\ttry { DFService.deregister(this); }"
				+ "\n\t\tcatch (Exception e) {}"
				+ "\n\t}");

		//comentario de documentacion
		String doc="";
		doc+="Nombre del Agente:"+agente.getNameString();

		if(Main.agregar_comentarios)
			newClass.setPreambulo(doc);

	/*Visit children*/
		agente.getModelo_agentes().accept(this,newClass,agente.getModelo_tareas());//enviar modelo de tareas para capturar relaciones con servicios
		agente.getModelo_tareas().accept(this,newClass);
		agente.getModelo_comunicaciones().accept(this,newClass);
		agente.getModelo_coordinacion().accept(this,newClass);

		for (MarcoOntologicoIndividual moi : agente.getMarco_ontologico_individual()) {
			moi.accept(this,newClass);
		}

		newClass.push_line_inside("try { \n"
							+ "\t\t\tDFService.register(this, dfd);\n"+
							"\t\t}\n\t\t catch (Exception e) {}");

		Generador.setClase_principal(newClass);
		Generador.print_code();
	/*Codigo*/

	}

	@Override
	public void visit(AgentModel agentModel,clase newClass) {
		if(Main.agregar_comentarios){
			if(agentModel.getAgenteposicion().equals(""))
				newClass.addPreambulo("Posicion:Sin informaci�n");
			else
				newClass.addPreambulo("Posicion:"+agentModel.getAgenteposicion());
			if(agentModel.getAgenteMarco_referencia().equals(""))
				newClass.addPreambulo(agentModel.getAgenteMarco_referencia());
			else
				newClass.addPreambulo("Marco de referencia:Sin informaci�n");
			if(agentModel.getComponentesString().equals("[]"))
				newClass.addPreambulo("Componentes:No posee componentes");
			else
				newClass.addPreambulo("Componentes:"+agentModel.getComponentesString());
			//newClass.addPreambulo("Objetivo:"+agentModel.ObjetivoTo_String());
			if(agentModel.Propiedades_Tostring().equals(""))
				newClass.addPreambulo("Propiedades:"+agentModel.Propiedades_Tostring());
			if(agentModel.Capacidad_Tostring().equals(""))
				newClass.addPreambulo("Capacidad:"+agentModel.Capacidad_Tostring());
			if(agentModel.Restriccion_Tostring().equals(""))
				newClass.addPreambulo("Restricci�n:"+agentModel.Restriccion_Tostring());
		}
	}


	@Override
	public void visit(Servicio servicio, clase newClass, TareaModel tareas) {
		String serviceName=Main.FormatearAtributoMetodo(servicio.getNombre().get());
		String addBehaviour="";
		clase servicioClase=new clase(Main.FormatearClase(servicio.getNombre().get()));

		for (Caracteristica p : servicio.getPropiedades()) {
			String equal="";
			try{
				int i = Integer.parseInt(p.getValor_omision().get());
			  	equal="="+p.getValor_omision().get();
			}catch(NumberFormatException er){
				 equal="="+p.getValor_omision().get()+";//Es posible que exista una discordancia con el tipo de dato";
			  }

			String CaracteristicaFormateada=Main.FormatearAtributoMetodo(p.getNombre().get());
			servicioClase.add_attribute("private Int "+CaracteristicaFormateada+equal);

			String getter="public int "+Main.FormatearAtributoMetodo("get "+p.getNombre().get())+"(){\n";
			getter+="\t\t return this."+CaracteristicaFormateada+"\n";
			getter+="\t }";

			String setter="public void "+Main.FormatearAtributoMetodo("set "+p.getNombre().get())+"(int "+p.getNombre().get()+"){\n";
			setter+="\t\t this."+CaracteristicaFormateada+"="+p.getNombre().get()+"\n";
			setter+="\t }";

			servicioClase.getMethods().add(getter);
			servicioClase.getMethods().add(setter);
		}//fin del for de caracteristicas

		servicioClase.setConstructor(Main.ProcesarClaseGetContructor(servicio.getComportamiento(), servicioClase.getNombre()));
		servicioClase.getMethods().add(Main.ProcesarClaseGetMetodos(servicio.getComportamiento(), servicioClase.getNombre()));
		Main.ProcesarClaseGetImports(servicio.getComportamiento(), newClass);
		Main.ProcesarClaseGetAtributos(servicio.getComportamiento(), servicioClase);
		//Constructor sobrecargado

		servicioClase.setClasePadre(newClass);
		servicioClase.setExtend(servicio.getComportamiento());
		servicioClase.setComments(servicio.getDescripcion().get());

		/*1-recorrer tareas
		 * 2-si es el servicio asociado:
		 * 		2.1-generar m�todo y comentarios
		 */
		for (Tarea tarea : tareas.getTareas()) {
			if(tarea.getAsociado().getNombre().get().equals(servicio.getNombre().get())){
				tarea.accept(this, servicioClase);
			}//fin if
		}//fin del 1er for


		//Comentarios
		if(Main.agregar_comentarios){
			servicioClase.addPreambulo("Servicio: "+servicio.getNombre().get());
			servicioClase.addPreambulo("\tDescripcion:"+servicio.getDescripcion().get());
			servicioClase.addPreambulo("\tTipo de servicio:"+servicio.getTipo().get());
			servicioClase.addPreambulo("\tTipo de comportamiento:"+servicio.getComportamiento());
		}

		newClass.add_class(servicioClase);
		newClass.add_library("import jade.core.behaviours.*;");

		//registro el servicio en el DF

		newClass.push_line_inside("\n\tServiceDescription "+serviceName+" = new ServiceDescription();\n"
				          +"\t\t "+serviceName+".setType(\""+serviceName+"\");"
				          +"\n\t\t"+" "+serviceName+".setName(getLocalName());");
		newClass.push_line_inside("addBehaviour("+Main.ProcesarClaseGetDeclaracionInstancia(servicio.getComportamiento(), servicioClase.getNombre())+");");

		//comentarios->servicio.EntradaTo_StringMetodo();
				if(!servicio.EntradaTo_String().equals(""))
					servicioClase.addPreambulo("Se espera que el servicio reciba como Par�metros de entrada a trav�s de un mensaje ACL los par�metros:\n\t "+servicio.EntradaTo_StringMetodo());

		//codigo para generar la clase con los parametros de salida
				//si solo es un parametro, lo retorno inmediatamente
				String salidaP="Se espera que el servicio retorne, a trav�s de un mensaje ACl el par�metro: ";
				if(servicio.getServicioPsalida().size()==1){
					Parametros miParametro=servicio.getServicioPsalida().get(0);
					salidaP+="\n"+miParametro.getNombre().get()+ "- Tipo de dato:"+miParametro.getType();
					servicioClase.addPreambulo(salidaP);
					String parametro_formateado=Main.FormatearAtributoMetodo(miParametro.getNombre().get());
					servicioClase.add_attribute("private "+miParametro.getType()+" "+parametro_formateado);

				}//fin if

				//Si es mas de un parametro, creo la clase artificio
				else if(servicio.getServicioPsalida().size()>1){
					String classnameS=Main.FormatearClase(servicio.getNombre().get()+" ReturnValue");
					salidaP+="\nvalorRetorno- Tipo de dato:"+classnameS;
					servicioClase.addPreambulo(salidaP);

					//declaro un objeto de la clase y lo retorno
					servicioClase.add_attribute("private "+classnameS+" valorRetorno");
					//servicioClase.getLines_inside_start_method().add("return valorRetorno;");

					clase salida=new clase(classnameS);
					for (Parametros p : servicio.getServicioPsalida()) {
						String param=p.getType()+" "+p.getNombre().get();
						salida.add_attribute(param);

						salida.setPrincipal_method("public void "+classnameS+"()");

						String getter="public "+p.getType()+" "+Main.FormatearAtributoMetodo("get "+p.getNombre().get())+"(){\n";
						getter+="\t\t return this."+Main.FormatearAtributoMetodo(p.getNombre().get()+";");
						getter+="\n\t }";

						String setter="public void "+ Main.FormatearAtributoMetodo("set "+p.getNombre().get())+"("+p.getType()+" "+Main.FormatearAtributoMetodo(p.getNombre().get())+"){\n";
						setter+="\t\t this."+Main.FormatearAtributoMetodo(p.getNombre().get())+"="+Main.FormatearAtributoMetodo(p.getNombre().get()+";");
						setter+="\n\t }";
						salida.getMethods().add(getter);
						salida.getMethods().add(setter);
					}
					newClass.add_class(salida);
				}//salida > 0

	}

	@Override
	public void visit(TareaModel tareaModel,clase newClass) {

	}

	@Override
	public void visit(Tarea tarea,clase newClass) {
		String comentarios="";

	if(Main.agregar_comentarios){
		comentarios="\n/*\tNombre de la tarea:"+tarea.getNombre().get()+
							"\n\tObjetivo:"+tarea.getObjetivo()+
							"\n\tDescripcion:"+tarea.getDescripcion()+
							"\n\tSubtareas:"+tarea.getSubtareasS()+
							"\n\tPrecondicion:"+tarea.getPrecondicion()+"*/";
		if(tarea.getParametros().size()>0){
			comentarios+="\n/*\tIngredientes:";
			for (Parametros p : tarea.getParametros()) {
				String desc=p.getDescripcion().get();
				if(desc.equals("")||desc==null)
					desc="Sin informaci�n";
				comentarios+="\n\t\tNombre:"+Main.FormatearAtributoMetodo(p.getNombre().get())+
							"\n\t\t\tTipo:"+p.getTipo().get()+
							"\n\t\t\tDescripcion:"+desc+"\n";
			}
			comentarios+="*/";
		}//fin if
	}

		if(tarea.getComportamiento()==null)
			newClass.add_method(comentarios+"\n\tpublic void "+Main.FormatearAtributoMetodo(tarea.getNombre().get())+"("+tarea.getAllParameters()+") {\n\t}");
		else if(tarea.getComportamiento().get().equals("No aplica")||tarea.getComportamiento().get().equals("null")){
			newClass.add_method(comentarios+"\n\tpublic void "+Main.FormatearAtributoMetodo(tarea.getNombre().get())+"("+tarea.getAllParameters()+") {\n\t}");
		}
		else{
			//genero la clase
			clase tareaClase=new clase(Main.FormatearClase(tarea.getNombre().get()));
			if(tarea.getParametros().size()>0){
				String param="Se espera que la tarea reciba los siguentes par�metros a trav�s de una mensaje ACL:\n\t"+tarea.getAllParametersACL();
				tareaClase.addPreambulo(param);
			}
			tareaClase.setConstructor(Main.ProcesarClaseGetContructor(tarea.getComportamiento().get(), tareaClase.getNombre()));
			tareaClase.getMethods().add(Main.ProcesarClaseGetMetodos(tarea.getComportamiento().get(), tareaClase.getNombre()));
			tareaClase.setExtend(tarea.getComportamiento().get());
			System.out.println("Los imports se agregan a:"+newClass.getNombre());
			//newClass.getClasePadre() para enviar la clase que se refiere al agente y no la clase del servicio
			Main.ProcesarClaseGetImports(tarea.getComportamiento().get(), newClass.getClasePadre());
			Main.ProcesarClaseGetAtributos(tarea.getComportamiento().get(), tareaClase);

			if(Main.behaviour_containstSerialVersionUID.contains(tarea.getComportamiento().get())){
				tareaClase.add_attribute("private static final long serialVersionUID = 1L");
			}

			newClass.add_class(tareaClase);
		}
	}

	@Override
	public void visit(CommunicationModel communicationModel,clase newClass) {

	}

	@Override
	public void visit(ActoHabla actoHabla,clase newClass) {

	}

	@Override
	public void visit(CoordinationModel coordinationModel,clase newClass) {

	}

	@Override
	public void visit(conversacion conversacion,clase newClass) {

	}

	@Override
	public void visit(MarcoOntologicoIndividual marcoOntologicoIndividual,clase newClass) {

	}

	@Override
	public void visit(OntologiaHistorica ontologiaHistorica,clase newClass) {

	}

	@Override
	public void visit(MecanismoAprendizaje mecanismoAprendizaje,clase newClass) {

	}

	@Override
	public void visit(OntologiaDominio ontologiaDominio,clase newClass) {

	}

	@Override
	public void visit(OntologiaSituacionalI ontologiaSituacionalI,clase newClass) {

	}

	@Override
	public void visit(ConocimientoTareas conocimientoTareas,clase newClass) {

	}

	@Override
	public void visit(ConocimientoEstrategico conocimientoEstrategico,clase newClass) {

	}


}

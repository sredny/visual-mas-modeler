package copy;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Fila {
	private StringProperty clave,valor;

	public Fila(){
		setClave(new SimpleStringProperty());
		setValor(new SimpleStringProperty());
	}

	public Fila(String clave_n,String valor_n){
		setClave(new SimpleStringProperty(clave_n));
		setValor(new SimpleStringProperty(valor_n));
	}

	public StringProperty getClave() {
		return clave;
	}

	public void setClave(StringProperty clave) {
		this.clave = clave;
	}

	public StringProperty getValor() {
		return valor;
	}

	public void setValor(StringProperty valor) {
		this.valor = valor;
	}


}

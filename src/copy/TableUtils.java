package copy;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.StringTokenizer;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

public class TableUtils {

    private static NumberFormat numberFormatter = NumberFormat.getNumberInstance();


    /**
     * Install the keyboard handler:
     *   + CTRL + C = copy to clipboard
     *   + CTRL + V = paste to clipboard
     * @param table
     */
    public static void installCopyPasteHandler(TableView<?> table) {

        // install copy/paste keyboard handler
        table.setOnKeyPressed(new TableKeyEventHandler());
    }

    /**
     * Copy/Paste keyboard event handler.
     * The handler uses the keyEvent's source for the clipboard data. The source must be of type TableView.
     */
    public static class TableKeyEventHandler implements EventHandler<KeyEvent> {

        KeyCodeCombination copyKeyCodeCompination = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
        KeyCodeCombination pasteKeyCodeCompination = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_ANY);

        public void handle(final KeyEvent keyEvent) {

            if (copyKeyCodeCompination.match(keyEvent)) {

                if( keyEvent.getSource() instanceof TableView) {

                    // copy to clipboard
                    copySelectionToClipboard( (TableView<?>) keyEvent.getSource());

                    // event is handled, consume it
                    keyEvent.consume();

                }

            }
            else if (pasteKeyCodeCompination.match(keyEvent)) {

                if( keyEvent.getSource() instanceof TableView) {

                    // copy to clipboard
                    pasteFromClipboard( (TableView<?>) keyEvent.getSource());

                    // event is handled, consume it
                    keyEvent.consume();

                }

            }

        }

    }

    /**
     * Get table selection and copy it to the clipboard.
     * @param table
     */
    public static void copySelectionToClipboard(TableView<?> table) {

        int i=0;
        for (@SuppressWarnings("unused") Object p : table.getItems()) {
        	table.getSelectionModel().select(i);
        	i++;
		}

        StringBuilder plainBuffer = new StringBuilder();
        StringBuilder htmlBuffer = new StringBuilder();

        @SuppressWarnings("rawtypes")
		ObservableList<TablePosition> positionList = table.getSelectionModel().getSelectedCells();


        int prevRow = -1;

        htmlBuffer.append( "<html>\n<body>\n<table style='border: 1px solid black;border-collapse: collapse;'>\n");

        htmlBuffer.append( " <tr style='border: 1px solid black;'>\n");

        int i1=0;
        for (@SuppressWarnings("rawtypes") TablePosition position : positionList) {

            int viewRow = position.getRow();
            int viewCol = position.getColumn();

            // determine whether we advance in a row (tab) or a column
            // (newline).
            if(viewRow==0){

            }//primera fila entonces colspan
            if (prevRow == viewRow) {

                plainBuffer.append('\t');

            } else if (prevRow != -1) {

                plainBuffer.append('\n');
                htmlBuffer.append( " </tr style='border: 1px solid black;'>\n <tr>\n");
            }

            // create string from cell
            String text = "";

            Object observableValue = (Object) table.getVisibleLeafColumn(viewCol).getCellObservableValue( viewRow); // table position gives the view index => we need to operate on the view columns

            // null-check: provide empty string for nulls
            if (observableValue == null) {
                text = "";
            }
            else if( observableValue instanceof DoubleProperty) { // TODO: handle boolean etc

                text = numberFormatter.format( ((DoubleProperty) observableValue).get());

            }
            else if( observableValue instanceof IntegerProperty) {

                text = numberFormatter.format( ((IntegerProperty) observableValue).get());

            }
            else if( observableValue instanceof StringProperty) {

                text = ((StringProperty) observableValue).get();

            }
            else {
                System.out.println("Unsupported observable value: " + observableValue);
            }

            // add new item to clipboard
            plainBuffer.append(text);

            if(i1%2==0){
            	if(viewRow==0&&text!=null){
            		htmlBuffer.append( "<td colspan='2' style='border: 1px solid black;font-weight: bold;width:100px;color:white;background:#4BACC6'>" + text + "</td>\n");//este es el titulo de la tabla
            		htmlBuffer.append( "<td style='border: 1px solid black;font-weight: bold;width:100px;color:white;background:#4BACC6'></td>\n");
            	}
            	else{
            		if(text.equals("Descripci�nP")){
            			htmlBuffer.append( "<td style='border: 1px solid black;font-weight: bold;width:100px'>Valor</td>\n");
            			htmlBuffer.append( "<td style='border: 1px solid black;font-weight: bold;width:100px'>Descripci�n</td>\n");
                	}
            		else if(text.contains("pSeparator")){
            			String res[]=text.split("pSeparator");
            			htmlBuffer.append( "<td style='border: 1px solid black;width:100px'>"+res[0]+"</td>\n");
            			htmlBuffer.append( "<td style='border: 1px solid black;width:100px'>"+res[1]+"</td>\n");
            		}
            	else{
            		htmlBuffer.append( "<td style='border: 1px solid black;font-weight: bold;width:100px'>" + text + "</td>\n");

            		}
            	}
            }else if(viewRow!=0){
            	if(text.equals("Descripci�nP")){
        			htmlBuffer.append( "<td style='border: 1px solid black;font-weight: bold;width:100px'>Valor</td>\n");
        			htmlBuffer.append( "<td style='border: 1px solid black;font-weight: bold;width:100px'>Descripci�n</td>\n");
            	}
        		else if(text.contains("pSeparator")){
        			String res[]=text.split("pSeparator");
        			htmlBuffer.append( "<td style='border: 1px solid black;width:100px'>"+res[0]+"</td>\n");
        			htmlBuffer.append( "<td style='border: 1px solid black;width:100px'>"+res[1]+"</td>\n");
        		 }else{

                 	htmlBuffer.append( "<td colspan='2' style='border: 1px solid black;width:300px'>" + text + "</td>\n");
             	}
        		}


            	i1++;
            // remember previous
            prevRow = viewRow;
        }

        htmlBuffer.append( " </tr style='border: 1px solid black;'>\n");
        htmlBuffer.append( "</table>\n</body>\n</html>");

        // create clipboard content
        final ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(plainBuffer.toString());
        clipboardContent.putHtml(htmlBuffer.toString());

        // set clipboard content
        Clipboard.getSystemClipboard().setContent(clipboardContent);


    }

    public static void pasteFromClipboard( TableView<?> table) {

        // abort if there's not cell selected to start with
        if( table.getSelectionModel().getSelectedCells().size() == 0) {
            return;
        }

        // get the cell position to start with
        @SuppressWarnings("rawtypes")
		TablePosition pasteCellPosition = table.getSelectionModel().getSelectedCells().get(0);



        String pasteString = Clipboard.getSystemClipboard().getString();

        System.out.println(pasteString);

        int rowClipboard = -1;

        StringTokenizer rowTokenizer = new StringTokenizer( pasteString, "\n");
        while( rowTokenizer.hasMoreTokens()) {

            rowClipboard++;

            String rowString = rowTokenizer.nextToken();

            StringTokenizer columnTokenizer = new StringTokenizer( rowString, "\t");

            int colClipboard = -1;

            while( columnTokenizer.hasMoreTokens()) {

                colClipboard++;

                // get next cell data from clipboard
                String clipboardCellContent = columnTokenizer.nextToken();

                // calculate the position in the table cell
                int rowTable = pasteCellPosition.getRow() + rowClipboard;
                int colTable = pasteCellPosition.getColumn() + colClipboard;

                // skip if we reached the end of the table
                if( rowTable >= table.getItems().size()) {
                    continue;
                }
                if( colTable >= table.getColumns().size()) {
                    continue;
                }

                // System.out.println( rowClipboard + "/" + colClipboard + ": " + cell);

                // get cell
                TableColumn<?, ?> tableColumn = table.getVisibleLeafColumn(colTable);  // table position gives the view index => we need to operate on the view columns
                ObservableValue<?> observableValue = tableColumn.getCellObservableValue(rowTable);


                // TODO: handle boolean, etc
                if( observableValue instanceof DoubleProperty) {

                    try {

                        double value = numberFormatter.parse(clipboardCellContent).doubleValue();
                        ((DoubleProperty) observableValue).set(value);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                else if( observableValue instanceof IntegerProperty) {

                    try {

                        int value = NumberFormat.getInstance().parse(clipboardCellContent).intValue();
                        ((IntegerProperty) observableValue).set(value);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                else if( observableValue instanceof StringProperty) {

                    ((StringProperty) observableValue).set(clipboardCellContent);

                } else {

                    System.out.println("Unsupported observable value: " + observableValue);

                }
            }

        }

    }

}
## Copyright and license

Visual MAS Modeler 1.0
Copyright (1) 2016  Sredny M. Buitrago C.

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License,